DROP INDEX IF EXISTS CmdrSystemCoordIndex;
DROP INDEX IF EXISTS CmdrFromDistIndex;
DROP INDEX IF EXISTS CmdrToDistIndex;
DROP INDEX IF EXISTS CmdrDistanceIndex;
DROP INDEX IF EXISTS CmdrFromRefDistIndex;
DROP INDEX IF EXISTS CmdrToRefDistIndex;
DROP INDEX IF EXISTS CmdrRefDistanceIndex;

CREATE INDEX CmdrSystemCoordIndex ON CmdrSystems(xCoord);
CREATE INDEX CmdrFromDistIndex ON CmdrDistances(FromSystem);
CREATE INDEX CmdrToDistIndex ON CmdrDistances(ToSystem);
CREATE INDEX CmdrDistanceIndex ON CmdrDistances(Distance);
CREATE INDEX CmdrFromRefDistIndex ON CmdrRefDistances(FromSystem);
CREATE INDEX CmdrToRefDistIndex ON CmdrRefDistances(ToSystem);
CREATE INDEX CmdrRefDistanceIndex ON CmdrRefDistances(Distance);
