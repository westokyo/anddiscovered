ALTER TABLE Planets RENAME TO Planets_v49;
ALTER TABLE Stars RENAME TO Stars_v49;
ALTER TABLE CmdrSystems RENAME TO CmdrSystems_v49;

DROP INDEX IF EXISTS CmdrSystemCoordIndex;
DROP INDEX IF EXISTS CmdrFromDistIndex;
DROP INDEX IF EXISTS CmdrToDistIndex;
DROP INDEX IF EXISTS CmdrDistanceIndex;
DROP INDEX IF EXISTS CmdrFromRefDistIndex;
DROP INDEX IF EXISTS CmdrToRefDistIndex;
DROP INDEX IF EXISTS CmdrRefDistanceIndex;
DROP INDEX IF EXISTS StarIndex;
DROP INDEX IF EXISTS PlanetIndex;
DROP INDEX IF EXISTS MainStarIndex;
DROP INDEX IF EXISTS StellarBodiesIndex;
DROP INDEX IF EXISTS PlanetaryBodiesIndex;
DROP INDEX IF EXISTS CmdrSystemIndex;

CREATE TABLE CmdrSystems (_id INTEGER PRIMARY KEY, System TEXT COLLATE NOCASE, xCoord DOUBLE, yCoord DOUBLE, zCoord DOUBLE, TrilatSystems INTEGER DEFAULT 4, Note TEXT, MainStar INTEGER);

CREATE TABLE Satellites (_id INTEGER PRIMARY KEY, SystemId INTEGER NOT NULL, BodyId INTEGER NOT NULL, CategoryId INTEGER NOT NULL, ParentId INTEGER, ParentCategoryId INTEGER, DisplayOrder INTEGER NOT NULL DEFAULT 0);
CREATE TABLE Stars (_id INTEGER PRIMARY KEY, Type INTEGER NOT NULL, ScanLevel INTEGER NOT NULL, FirstDiscovered INTEGER NOT NULL, Name TEXT COLLATE NOCASE, Distance DOUBLE, Age DOUBLE, Mass DOUBLE, Radius DOUBLE, SurfaceTemp DOUBLE, OrbitalPeriod DOUBLE, SemiMajorAxis DOUBLE, OrbitalEccentricity DOUBLE, OrbitalInclination DOUBLE, ArgPeriapsis DOUBLE);
CREATE TABLE Planets (_id INTEGER PRIMARY KEY, Type INTEGER NOT NULL, ScanLevel INTEGER NOT NULL, FirstDiscovered INTEGER NOT NULL, Name TEXT COLLATE NOCASE, Distance DOUBLE, Terraformable INTEGER NOT NULL DEFAULT 0, Mass DOUBLE, Radius DOUBLE, SurfaceTemp DOUBLE, SurfacePressure DOUBLE, Volcanism INTEGER, AtmosphereType INTEGER, OrbitalPeriod DOUBLE, SemiMajorAxis DOUBLE, OrbitalEccentricity DOUBLE, OrbitalInclination DOUBLE, ArgPeriapsis DOUBLE, RotationPeriod DOUBLE, TidalLocked BOOLEAN, AxisTilt DOUBLE);
CREATE TABLE Asteroids (_id INTEGER PRIMARY KEY, Type INTEGER NOT NULL, ScanLevel INTEGER NOT NULL, FirstDiscovered INTEGER NOT NULL, Name TEXT COLLATE NOCASE, Distance DOUBLE, MoonMasses DOUBLE, OrbitalPeriod DOUBLE, SemiMajorAxis DOUBLE, OrbitalEccentricity DOUBLE, OrbitalInclination DOUBLE, ArgPeriapsis DOUBLE);
CREATE TABLE Rings (_id INTEGER PRIMARY KEY, BodyId INTEGER NOT NULL, CategoryId INTEGER NOT NULL, RingLevel INTEGER NOT NULL, Type INTEGER NOT NULL DEFAULT -1, Quality INTEGER NOT NULL DEFAULT -1, Mass DOUBLE, SemiMajorAxis DOUBLE, InnerRadius DOUBLE, OuterRadius DOUBLE);

CREATE TABLE Composition (_id INTEGER PRIMARY KEY, BodyId INTEGER NOT NULL, CategoryId INTEGER NOT NULL, Type INTEGER NOT NULL DEFAULT -1, Percentage INTEGER NOT NULL DEFAULT 0);
CREATE TABLE Atmosphere (_id INTEGER PRIMARY KEY, BodyId INTEGER NOT NULL, CategoryId INTEGER NOT NULL, Type INTEGER NOT NULL DEFAULT -1, Percentage INTEGER NOT NULL DEFAULT 0);

INSERT INTO CmdrSystems (_id, System, xCoord, yCoord, zCoord, TrilatSystems, Note, MainStar) SELECT _id, System, xCoord, yCoord, zCoord, TrilatSystems, Note, MainStar FROM CmdrSystems_v49;

-- We cannot accept Stars/Planets that are of type Unknown.
DELETE FROM Stars_v49 WHERE Star=19;
DELETE FROM Planets_v49 WHERE Planet=13;

INSERT INTO Stars (_id, Type, ScanLevel, FirstDiscovered) SELECT _id, Star, 0, 0 FROM Stars_v49;
INSERT INTO Satellites (SystemId, BodyId, CategoryId) SELECT SystemId, _id, 0 FROM Stars_v49;

INSERT INTO Planets (_id, Type, ScanLevel, FirstDiscovered) SELECT _id, Planet, 0, 0 FROM Planets_v49;
INSERT INTO Satellites (SystemId, BodyId, CategoryId) SELECT SystemId, _id, 1 FROM Planets_v49;

CREATE INDEX CmdrSystemIndex ON CmdrSystems(System);
CREATE INDEX CmdrSystemCoordIndex ON CmdrSystems(xCoord);
CREATE INDEX CmdrFromDistIndex ON CmdrDistances(FromSystem);
CREATE INDEX CmdrToDistIndex ON CmdrDistances(ToSystem);
CREATE INDEX CmdrDistanceIndex ON CmdrDistances(Distance);
CREATE INDEX CmdrFromRefDistIndex ON CmdrRefDistances(FromSystem);
CREATE INDEX CmdrToRefDistIndex ON CmdrRefDistances(ToSystem);
CREATE INDEX CmdrRefDistanceIndex ON CmdrRefDistances(Distance);

CREATE INDEX SatellitesIndex ON Satellites(SystemId, ParentId, ParentCategoryId);
CREATE INDEX CompositionIndex ON Composition(BodyId, CategoryId);
CREATE INDEX AtmosphereIndex ON Atmosphere(BodyId, CategoryId);

-- Now we are going to convert the main star entries
CREATE TABLE TempSats (SystemId INTEGER, StarId INTEGER, Type INTEGER);
INSERT INTO TempSats (SystemId, StarId) SELECT SystemId, BodyId FROM Satellites WHERE CategoryId=0;
UPDATE TempSats SET Type=(SELECT Type FROM Stars WHERE _id=StarId);
UPDATE CmdrSystems SET MainStar=(SELECT StarId FROM TempSats WHERE MainStar=Type AND _id=SystemId);
DROP TABLE TempSats;

-- Convert the Planet and Star types to zero base
UPDATE Planets SET Type= CASE Type
                            WHEN 1 THEN 0
                            WHEN 2 THEN 1
                            WHEN 3 THEN 2
                            WHEN 4 THEN 3
                            WHEN 5 THEN 4
                            WHEN 6 THEN 5
                            WHEN 7 THEN 6
                            WHEN 8 THEN 7
                            WHEN 9 THEN 8
                            WHEN 10 THEN 9
                            WHEN 11 THEN 10
                            WHEN 12 THEN 11
                            WHEN 13 THEN -1
                            WHEN 14 THEN 12
                            WHEN 15 THEN 13
                            WHEN 16 THEN 14
                            WHEN 17 THEN 15
                          END;

UPDATE Stars SET Type= CASE Type
                           WHEN 1 THEN 0
                           WHEN 2 THEN 1
                           WHEN 3 THEN 2
                           WHEN 4 THEN 3
                           WHEN 5 THEN 4
                           WHEN 6 THEN 5
                           WHEN 7 THEN 6
                           WHEN 8 THEN 7
                           WHEN 9 THEN 8
                           WHEN 10 THEN 9
                           WHEN 11 THEN 10
                           WHEN 12 THEN 11
                           WHEN 13 THEN 12
                           WHEN 14 THEN 13
                           WHEN 15 THEN 14
                           WHEN 16 THEN 15
                           WHEN 17 THEN 16
                           WHEN 18 THEN 17
                           WHEN 19 THEN -1
                      END;

UPDATE Planets SET Type=11 WHERE Type=-1;
