package com.fussyware.AndDiscovered.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.ThumbnailAdapter;
import com.fussyware.AndDiscovered.dialog.CancelableAlertDialogFragment;
import com.fussyware.AndDiscovered.dialog.OkAlertDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.eddatabase.SystemImageInfo;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.service.ProxyBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wes on 9/30/15.
 */
public class PhotoGalleryActivity
        extends BaseActivity
        implements CancelableAlertDialogFragment.OnAlertListener
{
    private ThumbnailAdapter imageAdapter;

    private ActionMode mainActionMode;
    private final ActionMode.Callback mainActionCallback = new LongClickActionBar();

    private LocalBroadcastManager broadcastManager;
    private final BroadcastReceiver broadcastReceiver = new BroadcastIntentReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.photo_gallery_activity_title);
        setContentView(R.layout.photo_gallery_activity_layout);

        ThumbnailOnItemClickListener clickListener = new ThumbnailOnItemClickListener();

        imageAdapter = new ThumbnailAdapter(this, ThumbnailAdapter.USE_SYSTEM_NAME);
        imageAdapter.setOnItemClickListener(clickListener);
        imageAdapter.setOnItemLongClickListener(clickListener);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.photo_gallery_grid);
        recyclerView.setAdapter(imageAdapter);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ProxyBase.BROADCAST_IMAGE_EVENT);

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastManager.registerReceiver(broadcastReceiver, filter);

        ArrayList<SystemImageInfo> list = new ArrayList<>();
        Collections.addAll(list, CmdrDbHelper.getInstance().getSystemImages());
        imageAdapter.setImageList(list);
    }

    @Override
    protected void onDestroy()
    {
        if (mainActionMode != null) {
            mainActionMode.finish();
        }

        super.onDestroy();
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if (imageAdapter.getItemCount() == 0) {
            DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.photo_gallery_no_items_title,
                                                                        R.string.photo_gallery_no_items_message);
            fragment.show(getSupportFragmentManager(), FragmentTag.system_photo_gallery_fragment.no_items_dialog);
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        super.onBackPressed();
        return true;
    }

    @Override
    public void onPositive(String tag)
    {
        List<SystemImageInfo> list = imageAdapter.getSelected();

        for (SystemImageInfo info : list) {
            imageAdapter.remove(info);

            CmdrSystemInfo systemInfo = CmdrDbHelper.getInstance().getSystem(info.getSystem());

            for (SystemImageInfo imageInfo : systemInfo.getSystemImages()) {
                imageInfo.delete();
            }
        }

        mainActionMode.finish();
    }

    @Override
    public void onNegative(String tag)
    {
    }

    private class BroadcastIntentReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            switch (action) {
                case ProxyBase.BROADCAST_IMAGE_EVENT: {
                    SystemImageInfo info = intent.getParcelableExtra(ProxyBase.EXTRA_IMAGE_INFO);
                    List<SystemImageInfo> list = imageAdapter.getItems();
                    boolean found = false;

                    for (SystemImageInfo imageInfo : list) {
                        if (imageInfo.getSystem().equals(info.getSystem())) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        imageAdapter.add(info);
                    }

                    break;
                }
                default:
                    break;
            }
        }
    }

    private class ThumbnailOnItemClickListener implements ThumbnailAdapter.OnItemClickListener,
                                                          ThumbnailAdapter.OnItemLongClickListener
    {
        @Override
        public void onItemClick(View parent, View view, int position)
        {
            if (mainActionMode == null) {
                SystemImageInfo info = imageAdapter.getItem(position);
                Intent intent = new Intent(getApplicationContext(),
                                           SystemPhotoGalleryActivity.class);

                intent.putExtra("system", info.getSystem());
                startActivity(intent);
            } else {
                imageAdapter.setSelected(!imageAdapter.isSelected(position), position);
            }
        }

        @Override
        public boolean onItemLongClick(View parent, View view, int position)
        {
            imageAdapter.setSelected(!imageAdapter.isSelected(position), position);

            if (mainActionMode == null) {
                mainActionMode = startActionMode(mainActionCallback);
            }

            return true;
        }
    }

    private class LongClickActionBar implements ActionMode.Callback
    {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu)
        {
            mode.getMenuInflater().inflate(R.menu.photo_gallery_context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu)
        {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item)
        {
            switch (item.getItemId()) {
                case R.id.photo_delete_menu: {
                    if (imageAdapter.getSelectedCount() > 0) {
                        DialogFragment fragment = CancelableAlertDialogFragment.newInstance(R.string.photo_delete_title);
                        fragment.show(getSupportFragmentManager(),
                                      FragmentTag.system_photo_gallery_fragment.alert_delete_dialog);
                    }
                    return true;
                }
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode)
        {
            mainActionMode = null;
            imageAdapter.clearSelected();
        }
    }
}
