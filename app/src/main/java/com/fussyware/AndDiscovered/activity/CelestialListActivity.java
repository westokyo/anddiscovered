package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.ChooseMainStarDialog;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrStarBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.fragment.CelestialBodyListFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;

/**
 * Created by wes on 11/10/15.
 */
public
class CelestialListActivity
        extends BaseActivity
        implements ChooseMainStarDialog.OnDismissListener,
                   CelestialBodyListFragment.OnCelestialBodyClickListener

{
    private static final String LOG_NAME = CelestialListActivity.class.getSimpleName();
    private static final int RESULT_FOR_SYSTEM_INFO = 201;

    private CmdrSystemInfo systemInfo;

    private TitleInfoFragment titleFragment;
    private CelestialBodyListFragment celestialFragment;

    boolean modified;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(getString(R.string.celestial_list_activity_title));
        setContentView(R.layout.celestial_list_activity_layout);
        setDrawerIndicatorEnabled(false);

        titleFragment = (TitleInfoFragment) getSupportFragmentManager().findFragmentById(R.id.title_info_fragment);
        celestialFragment = (CelestialBodyListFragment) getSupportFragmentManager().findFragmentById(R.id.celestial_list_fragment);

        if (savedInstanceState == null) {
            systemInfo = getIntent().getParcelableExtra("system");

            celestialFragment.setSystem(systemInfo);
            titleFragment.setSystem(systemInfo);
        } else {
            systemInfo = savedInstanceState.getParcelable("system");
            modified = savedInstanceState.getBoolean("modified");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("system", systemInfo);
        outState.putBoolean("modified", modified);
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            if (!celestialFragment.onBackPressed()) {
                onActivityDone();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if ((resultCode == RESULT_OK) && (requestCode == RESULT_FOR_SYSTEM_INFO)) {
            modified = true;

            CelestialBody body = data.getParcelableExtra("celestial_body");
            celestialFragment.onBodyUpdated(body);
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        onActivityDone();
        return true;
    }

    @Override
    protected void onNewSystemEntered(SystemInfo system, DistanceInfo distance)
    {
        if (system instanceof CmdrSystemInfo) {
            systemInfo = (CmdrSystemInfo) system;
            titleFragment.setSystem(systemInfo);
            celestialFragment.setSystem(systemInfo);
        }
    }

    @Override
    protected void onUpdateSystems()
    {
        /** Data is stale so we have to get it from the DB again. */
        systemInfo = CmdrDbHelper.getInstance()
                                 .getSystem(systemInfo.getSystem());

        titleFragment.setSystem(systemInfo);
    }

    @Override
    public void onMainStarCancel()
    {
        super.onBackPressed();
    }

    /** The select a new Main Star dialog is finished. We are guaranteed to have
     * a new main star.
     *
     * @param tag
     * @param type
     */
    @Override
    public void onDone(String tag, StarType type)
    {
        switch (tag) {
            case FragmentTag.celestial_body_list_fragment.choose_main_star_dialog: {
                celestialFragment.addMainStar(CmdrStarBody.create(systemInfo, type, null));
                modified = true;
                break;
            }
            default:
                break;
        }
    }

    private void onActivityDone()
    {
        Intent intent = new Intent();
        intent.putExtra("modified", modified);

        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onCelestialBodyClick(CelestialBody body)
    {
        Intent intent = new Intent(getApplicationContext(),
                                   CelestialInfoActivity.class);
        intent.putExtra("celestial", body);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivityForResult(intent, RESULT_FOR_SYSTEM_INFO);
    }
}
