package com.fussyware.AndDiscovered.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.fussyware.AndDiscovered.edproxy.ProxyConnectionState;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.CancelableAlertDialogFragment;
import com.fussyware.AndDiscovered.dialog.EditCelestialTypeDialogFragment;
import com.fussyware.AndDiscovered.dialog.TextDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.SystemImageInfo;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.squareup.picasso.Picasso;

import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.text.DecimalFormat;

/**
 * Created by wes on 9/30/15.
 */
public class ImageFullscreenActivity
        extends BaseActivity
        implements EditCelestialTypeDialogFragment.onDoneListener,
                   CancelableAlertDialogFragment.OnAlertListener
{
    private static final String LOG_NAME = ImageFullscreenActivity.class.getSimpleName();
    private static final DecimalFormat distanceFormatter = new DecimalFormat("0.00");

    static {
        distanceFormatter.setRoundingMode(RoundingMode.HALF_UP);
    }

    private TextView celestialNameView;
    private TextView distanceView;
    private TextView typeView;

    private View titleBarLayout;
    private final TitlebarHideRunner titlebarHider = new TitlebarHideRunner();

    private ActionMode mainActionMode;
    private ActionMode editActionMode;

    private final ActionMode.Callback mainActionCallback = new LongClickActionBar();
    private final ActionMode.Callback editActionCallback = new EditInfoActionBar();

    private SystemImageInfo imageInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // TODO: Put title here
        setContentView(R.layout.image_fullscreen_layout);
        setDrawerIndicatorEnabled(false);

        if (savedInstanceState == null) {
            imageInfo = getIntent().getParcelableExtra("image_info");
        } else {
            imageInfo = savedInstanceState.getParcelable("image_info");
        }

        createOverlay(savedInstanceState);
        createImage(savedInstanceState);

        titleBarLayout.postDelayed(titlebarHider, 8000);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if (editActionMode != null) {
            editActionMode.finish();
        }

        if (mainActionMode != null) {
            mainActionMode.finish();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putParcelable("image_info", imageInfo);
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        super.onBackPressed();
        return true;
    }

    @Override
    protected void onNewSystemEntered(SystemInfo system, DistanceInfo distance)
    {
        onBackPressed();
    }

    private void createOverlay(Bundle savedInstanceState)
    {
        titleBarLayout = findViewById(R.id.title_bar_layout);
        titleBarLayout.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                if (mainActionMode == null) {
                    mainActionMode = startActionMode(mainActionCallback);
                }

                return true;
            }
        });

        celestialNameView = (TextView) findViewById(R.id.fullscreen_celestial_name);
        distanceView = (TextView) findViewById(R.id.fullscreen_distance);
        typeView = (TextView) findViewById(R.id.fullscreen_type_name);

        TextView systemNameView = (TextView) findViewById(R.id.fullscreen_system);
        systemNameView.setText(imageInfo.getSystem());

        if (!imageInfo.getCelestialName().isEmpty()) {
            celestialNameView.setText("-" + imageInfo.getCelestialName());
        }

        if (imageInfo.isStar()) {
            typeView.setText(imageInfo.getStarType().toString());
        } else if (imageInfo.isPlanet()) {
            typeView.setText(imageInfo.getPlanetType().toString());
        } else {
            typeView.setText("Unknown");
        }

        distanceView.setText(distanceFormatter.format(imageInfo.getDistance()) + "Ls");
    }

    private void createImage(Bundle savedInstanceState)
    {
        final ImageView imageView = (ImageView) findViewById(R.id.fullscreen_image);

        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showTitlebar();
            }
        });

        imageView.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                if (mainActionMode == null) {
                    mainActionMode = startActionMode(mainActionCallback);
                }

                return true;
            }
        });

        imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                try {
                    Picasso.with(ImageFullscreenActivity.this)
                           .load(ProxyConnectionState
                                         .getInstance()
                                         .generateImageURL(imageInfo.getImagePath()).toString())
                           .resize(imageView.getWidth(), 0)
                           .placeholder(R.drawable.ic_placeholder)
                           .error(R.drawable.ic_placeholder_error)
                           .into(imageView);
                } catch (IllegalStateException e) {
                    AndLog.d(LOG_NAME, "No connection was established with the Proxy", e);
                    Picasso.with(ImageFullscreenActivity.this)
                           .load(R.drawable.ic_placeholder_error)
                           .resize(imageView.getWidth(), 0)
                           .into(imageView);
                } catch (MalformedURLException e) {
                    AndLog.d(LOG_NAME, "Failed to generate the image URL.", e);
                    Picasso.with(ImageFullscreenActivity.this)
                           .load(R.drawable.ic_placeholder_error)
                           .resize(imageView.getWidth(), 0)
                           .into(imageView);
                }
            }
        });
    }

    private void showTitlebar()
    {
        titleBarLayout.removeCallbacks(titlebarHider);
        titleBarLayout.setVisibility(View.VISIBLE);
        titleBarLayout.postDelayed(titlebarHider, 5000);
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.photo_fullscreen_fragment.edit_celestial_type_dialog: {
                typeView.setText(text);

                PlanetType planetType = PlanetType.getPlanetType(text);
                if (planetType == PlanetType.Unknown) {
                    StarType starType = StarType.getStarType(text);
                    if (starType != StarType.Unknown) {
                        imageInfo.setStarType(starType);
                    }
                } else {
                    imageInfo.setPlanetType(planetType);
                }

                showTitlebar();

                break;
            }
            case FragmentTag.photo_fullscreen_fragment.edit_celestial_name_dialog:
                celestialNameView.setText("-" + text);
                imageInfo.setCelestialName(text);

                showTitlebar();
                break;
            case FragmentTag.photo_fullscreen_fragment.edit_celestial_distance_dialog: {
                Double dist = Double.valueOf(text);

                distanceView.setText(distanceFormatter.format(dist) + "Ls");
                imageInfo.setDistance(dist);

                showTitlebar();
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onPositive(String tag)
    {
        String system = imageInfo.getSystem();

        imageInfo.delete();
        mainActionMode.finish();

        Intent intent = new Intent(getApplicationContext(), SystemPhotoGalleryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("system", system);
        startActivity(intent);
    }

    @Override
    public void onNegative(String tag)
    {
        // ignored
    }

    private class LongClickActionBar implements ActionMode.Callback
    {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu)
        {
            mode.getMenuInflater().inflate(R.menu.photo_context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu)
        {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item)
        {
            switch (item.getItemId()) {
                case R.id.photo_edit_menu:
                    if (editActionMode == null) {
                        editActionMode = startActionMode(editActionCallback);
                    }
                    return true;
                case R.id.photo_delete_menu: {
                    DialogFragment fragment = CancelableAlertDialogFragment.newInstance(R.string.photo_delete_title);
                    fragment.show(getSupportFragmentManager(), FragmentTag.photo_fullscreen_fragment.alert_delete_dialog);
                    return true;
                }
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode)
        {
            mainActionMode = null;
        }
    }
    private class EditInfoActionBar implements ActionMode.Callback
    {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu)
        {
            mode.getMenuInflater().inflate(R.menu.photo_edit_context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu)
        {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item)
        {
            switch (item.getItemId()) {
                case R.id.photo_edit_name: {
                    DialogFragment fragment = TextDialogFragment.newInstance(getResources().getString(R.string.photo_edit_celestial_name),
                                                                             "",
                                                                             (imageInfo.getCelestialName().isEmpty()) ? "" : imageInfo.getCelestialName(),
                                                                             InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                    fragment.show(getSupportFragmentManager(), FragmentTag.photo_fullscreen_fragment.edit_celestial_name_dialog);
                    return true;
                }
                case R.id.photo_edit_type: {
                    DialogFragment fragment = EditCelestialTypeDialogFragment
                            .newInstance(typeView.getText()
                                                 .toString());
                    fragment.show(getSupportFragmentManager(),
                                  FragmentTag.photo_fullscreen_fragment.edit_celestial_type_dialog);
                    return true;
                }
                case R.id.photo_edit_distance:
                    DialogFragment fragment = TextDialogFragment.newInstance(getResources().getString(R.string.photo_edit_distance),
                                                                             getResources().getString(R.string.photo_distance_dialog_summary),
                                                                             distanceFormatter.format(imageInfo.getDistance()),
                                                                             InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    fragment.show(getSupportFragmentManager(), FragmentTag.photo_fullscreen_fragment.edit_celestial_distance_dialog);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode)
        {
            editActionMode = null;
        }
    }

    private class TitlebarHideRunner implements Runnable
    {
        @Override
        public void run()
        {
            titleBarLayout.setVisibility(View.GONE);
        }
    }
}
