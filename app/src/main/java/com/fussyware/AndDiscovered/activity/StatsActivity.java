package com.fussyware.AndDiscovered.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.DateSystemPickerDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrDistanceInfo;
import com.fussyware.AndDiscovered.fragment.BaseStatsFragment;
import com.fussyware.AndDiscovered.fragment.StatsMainFragment;
import com.fussyware.AndDiscovered.fragment.StatsPlanetFragment;
import com.fussyware.AndDiscovered.fragment.StatsStarFragment;
import com.fussyware.AndDiscovered.view.ActiveViewPager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by wes on 1/2/16.
 */
public class StatsActivity
        extends BaseActivity
        implements DateSystemPickerDialogFragment.OnDoneListener
{
    private static final String TAG = StatsActivity.class.getSimpleName();
    private static final String START_DIALOG_TAG = "stats_activity.date_start_system_picker";
    private static final String END_DIALOG_TAG = "stats_activity.date_end_system_picker";

    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    private final CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();
    private final Drawable[] navIcons = new Drawable[3];

    private ViewPagerAdapter adapter;

    private Date minDate;
    private Date maxDate;

    private Date fromDate;
    private Date toDate;

    private ImageView imageView;

    private TextView startSystemView;
    private TextView startDateView;
    private TextView endSystemView;
    private TextView endDateView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        String fromSystemName;
        String toSystemName;

        CmdrDistanceInfo fromDistance = dbHelper.getFirstDistance();
        CmdrDistanceInfo toDistance = dbHelper.getLastDistance();

        int pagerPosition;

        if (fromDistance == null) {
            fromSystemName = getString(R.string.unknown);
            minDate = new Date(0);
        } else {
            fromSystemName = fromDistance.getFirstSystem();
            minDate = fromDistance.getCreatedDate();
        }

        if (toDistance == null) {
            toSystemName = getString(R.string.unknown);
            maxDate = new Date();
        } else {
            toSystemName = toDistance.getFirstSystem();
            maxDate = toDistance.getCreatedDate();
        }

        if (savedInstanceState == null) {
            fromDate = minDate;
            toDate = maxDate;
            pagerPosition = 0;
        } else {
            fromSystemName = savedInstanceState.getString("from_system");
            toSystemName = savedInstanceState.getString("to_system");

            fromDate = new Date(savedInstanceState.getLong("from_date"));
            toDate = new Date(savedInstanceState.getLong("to_date"));

            pagerPosition = savedInstanceState.getInt("page_selected");
        }

        setContentView(R.layout.stats_activity_layout);
        setTitle(R.string.stats_title);

        dbHelper.createStatViews(fromDate, toDate);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.setPageSelected(pagerPosition);

        ActiveViewPager viewPager = (ActiveViewPager) findViewById(R.id.view_pager);
        viewPager.addOnPageChangeListener(adapter);
        viewPager.setAdapter(adapter);

        imageView = (ImageView) findViewById(R.id.pager_navigation_icons);

        navIcons[0] = getResources().getDrawable(R.drawable.pager_three_left_on);
        navIcons[1] = getResources().getDrawable(R.drawable.pager_three_center_on);
        navIcons[2] = getResources().getDrawable(R.drawable.pager_three_right_on);

        startSystemView = (TextView) findViewById(R.id.start_from_system);
        startDateView = (TextView) findViewById(R.id.start_from_date);

        endSystemView = (TextView) findViewById(R.id.end_on_system);
        endDateView = (TextView) findViewById(R.id.end_on_date);

        startSystemView.setText(fromSystemName);
        startDateView.setText(DATE_FORMATTER.format(fromDate));

        endSystemView.setText(toSystemName);
        endDateView.setText(DATE_FORMATTER.format(toDate));

        View startSystem = findViewById(R.id.start_from_system_layout);
        startSystem.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogFragment df = DateSystemPickerDialogFragment.newInstance(minDate,
                                                                               maxDate,
                                                                               fromDate);
                df.show(getSupportFragmentManager(), START_DIALOG_TAG);
            }
        });

        View endSystem = findViewById(R.id.end_on_system_layout);
        endSystem.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogFragment df = DateSystemPickerDialogFragment.newInstance(fromDate,
                                                                               maxDate,
                                                                               toDate);
                df.show(getSupportFragmentManager(), END_DIALOG_TAG);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString("from_system", startSystemView.getText().toString());
        outState.putString("to_system", endSystemView.getText().toString());

        outState.putLong("from_date", fromDate.getTime());
        outState.putLong("to_date", toDate.getTime());

        outState.putInt("page_selected", adapter.getPageSelected());
    }

    @Override
    protected void onNewSystemEntered(SystemInfo system, DistanceInfo distance)
    {
        super.onNewSystemEntered(system, distance);

        Date newDate;

        if (distance instanceof CmdrDistanceInfo) {
            newDate = ((CmdrDistanceInfo) distance).getCreatedDate();
        } else {
            newDate = new Date();
        }

        /* If we are already the latest date then make sure we stay that way */
        if (maxDate.equals(toDate)) {
            toDate = newDate;

            endSystemView.setText(distance.getFirstSystem());
            endDateView.setText(DATE_FORMATTER.format(toDate));
        }

        maxDate = newDate;

        adapter.onRefresh();
    }

    @Override
    public void onDone(String tag, String system, Date date)
    {
        switch (tag) {
            case START_DIALOG_TAG:
                fromDate = date;

                startSystemView.setText(system);
                startDateView.setText(DATE_FORMATTER.format(fromDate));

                if (fromDate.compareTo(toDate) > 0) {
                    toDate = fromDate;

                    endSystemView.setText(system);
                    endDateView.setText(DATE_FORMATTER.format(toDate));
                }

                break;
            case END_DIALOG_TAG:
                toDate = date;

                endSystemView.setText(system);
                endDateView.setText(DATE_FORMATTER.format(toDate));

                break;
            default:
                break;
        }

        dbHelper.createStatViews(fromDate, toDate);
        adapter.onRefresh();
    }

    @Override
    public void onCancel(String tag)
    {

    }

    private class ViewPagerAdapter
            extends FragmentPagerAdapter
            implements ViewPager.OnPageChangeListener
    {
        private BaseStatsFragment[] fragments = new BaseStatsFragment[3];

        private int currentPosition;

        ViewPagerAdapter(FragmentManager fm)
        {
            super(fm);

            currentPosition = 0;
        }

        int getPageSelected()
        {
            return currentPosition;
        }

        void setPageSelected(int position)
        {
            currentPosition = position;
        }

        void onRefresh()
        {
            for (BaseStatsFragment fragment : fragments) {
                if (fragment != null) {
                    fragment.invalidate();
                }
            }

            if (fragments[currentPosition] != null) {
                fragments[currentPosition].onRefresh();
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            fragments[position] = (BaseStatsFragment) super.instantiateItem(container, position);
            return fragments[position];
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position) {
                case 0:
                    return new StatsMainFragment();
                case 1:
                    return new StatsStarFragment();
                case 2:
                    return new StatsPlanetFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount()
        {
            return fragments.length;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {

        }

        @Override
        public void onPageSelected(int position)
        {
            currentPosition = position;
            imageView.setImageDrawable(navIcons[position]);
            fragments[position].onRefresh();
        }

        @Override
        public void onPageScrollStateChanged(int state)
        {

        }
    }
}
