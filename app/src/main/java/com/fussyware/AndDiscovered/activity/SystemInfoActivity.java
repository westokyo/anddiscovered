package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;

import com.fussyware.AndDiscovered.AndDiscoveredActivity;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.ChooseMainStarDialog;
import com.fussyware.AndDiscovered.dialog.TextPickerDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrStarBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.fragment.CelestialBodyListFragment;
import com.fussyware.AndDiscovered.fragment.CelestialInfoParentFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.NotesFragment;
import com.fussyware.AndDiscovered.fragment.SystemInfoSubMenuFragment;
import com.fussyware.AndDiscovered.fragment.SystemPhotoGalleryFragment;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by wes on 9/29/15.
 */
public class SystemInfoActivity
        extends BaseActivity
        implements TextPickerDialogFragment.OnTextChangedListener,
                   NotesFragment.OnDoneListener,
                   SystemInfoSubMenuFragment.OnSubMenuListener,
                   ChooseMainStarDialog.OnDismissListener,
                   CelestialBodyListFragment.OnCelestialBodyClickListener

{
    private static final String LOG_NAME = SystemInfoActivity.class.getSimpleName();

    private static final int RESULT_CELESTIAL = 1;
    private static final int RESULT_DISTANCE = 2;
    private static final int RESULT_NOTES = 3;
    private static final int RESULT_PHOTO = 4;

    private CmdrDbHelper dbHelper;

    private TitleInfoFragment titleFragment;
    private NotesFragment notesFragment;
    private CelestialBodyListFragment cbListFragment;
    private CelestialInfoParentFragment cbParentFragment;
    private SystemPhotoGalleryFragment photoFragment;
    private HolderFragment holderFragment;

    boolean allInOneView;


    private CmdrSystemInfo systemInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FragmentManager manager = getSupportFragmentManager();

        setTitle(R.string.system_info_activity_title);
        setContentView(R.layout.system_info_layout);
        setDrawerIndicatorEnabled(false);

        holderFragment = (HolderFragment) manager.findFragmentByTag(HolderFragment.HOLDER_FRAGMENT);
        if (holderFragment == null) {
            holderFragment = new HolderFragment();
            holderFragment.setRetainInstance(true);
            manager.beginTransaction()
                   .add(holderFragment, HolderFragment.HOLDER_FRAGMENT)
                   .commit();
        }

        dbHelper = CmdrDbHelper.getInstance();

        titleFragment = (TitleInfoFragment) getSupportFragmentManager().findFragmentById(R.id.title_info_fragment);

        allInOneView = (findViewById(R.id.fragment_holder) != null);

        if (savedInstanceState == null) {
            CmdrSystemInfo system = getIntent().getParcelableExtra("system");

            holderFragment.backStack.clear();
            holderFragment.systemsModified.clear();

            AndLog.d(LOG_NAME, "New create for info activity.");
            setSystem(system);
        } else {
            AndLog.d(LOG_NAME, "Restore session for info activity.");
            systemInfo = savedInstanceState.getParcelable("system");

            if (savedInstanceState.getBoolean("all_in_one") != allInOneView) {
                setSystem(systemInfo);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("system", systemInfo);
        outState.putBoolean("all_in_one", allInOneView);
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        CmdrSystemInfo system = intent.getParcelableExtra("system");
        AndLog.d(LOG_NAME, "New intent with system: " + ((system == null) ? "null" : system.toString()));
        setSystem(system);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RESULT_CELESTIAL:
                case RESULT_DISTANCE:
                case RESULT_NOTES:
                    if (data.getBooleanExtra("modified", false)) {
                        synchronized (holderFragment.systemsModified) {
                            if (!holderFragment.systemsModified.contains(systemInfo.getSystem())) {
                                holderFragment.systemsModified.add(systemInfo.getSystem());
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        Intent intent = new Intent(getApplicationContext(), AndDiscoveredActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        synchronized (holderFragment.systemsModified) {
            if (!holderFragment.systemsModified.isEmpty()) {
                intent.putExtra("modified_list",
                                holderFragment.systemsModified.toArray(new String[holderFragment.systemsModified.size()]));

                setResult(Activity.RESULT_OK, intent);
            }
        }

        startActivity(intent);

        return true;
    }

    private void internalBackPressed()
    {
        synchronized (holderFragment.backStack) {
            if (holderFragment.backStack.empty()) {
                synchronized (holderFragment.systemsModified) {
                    if (holderFragment.systemsModified.isEmpty()) {
                        super.onBackPressed();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("modified_list",
                                        holderFragment.systemsModified.toArray(new String[holderFragment.systemsModified.size()]));

                        setResult(Activity.RESULT_OK, intent);

                        finish();
                    }
                }
            } else {
                setSystem(holderFragment.backStack.pop());
            }
        }
    }

    private boolean isTopFragmentStack(String fragmentName)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();

        if (count > 0) {
            String name = fragmentManager.getBackStackEntryAt(count - 1).getName();

            return fragmentName.equals(name);
        } else {
            return CelestialBodyListFragment
                    .class
                    .getCanonicalName()
                    .equals(fragmentName);
        }
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            if (allInOneView) {
                if (isTopFragmentStack(CelestialBodyListFragment
                                               .class
                                               .getCanonicalName())) {
                    if (!cbListFragment.onBackPressed()) {
                        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            getSupportFragmentManager().popBackStack();
                        } else {
                            internalBackPressed();
                        }
                    }
                } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    internalBackPressed();
                }
            } else {
                internalBackPressed();
            }
        }
    }

    @Override
    protected void onNewSystemEntered(SystemInfo system, DistanceInfo distance)
    {
        AndLog.d(LOG_NAME, "New system entered: " + ((system == null) ? "null" : system.toString()));
        setSystem((CmdrSystemInfo) system);
    }

    @Override
    protected void onUpdateSystems()
    {
        CmdrSystemInfo info = dbHelper.getSystem(systemInfo.getSystem());

        AndLog.d(LOG_NAME, "Update system: " + ((info == null) ? "null" : info.toString()));
        systemInfo = info;
        titleFragment.setSystem(info);
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.notes_fragment.tag:
                synchronized (holderFragment.systemsModified) {
                    if (!holderFragment.systemsModified.contains(systemInfo.getSystem())) {
                        holderFragment.systemsModified.add(systemInfo.getSystem());
                    }
                }

                break;
            case FragmentTag.celestial_body_list_fragment.duplicate_dialog:
                cbListFragment.onDuplicate(Integer.valueOf(text));
                break;
            default:
                super.onDone(tag, text);
                break;
        }
    }

    private void setSystem(@NonNull CmdrSystemInfo info)
    {
        systemInfo = info;
        titleFragment.setSystem(info);

        if (allInOneView) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            while (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStackImmediate();
            }

            if (cbListFragment == null) {
                cbListFragment = CelestialBodyListFragment.newInstance(info);

                fragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_holder,
                                 cbListFragment,
                                 FragmentTag.celestial_body_list_fragment.tag)
                        .commit();
            } else {
                cbListFragment.setSystem(info);
            }
        }
    }

    @Override
    public void onSubMenuCelestialClick()
    {
        if (allInOneView) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            int count = fragmentManager.getBackStackEntryCount();

            if (count > 0) {
                String name = fragmentManager.getBackStackEntryAt(count - 1).getName();

                if (!CelestialBodyListFragment.class.getCanonicalName().equals(name)) {
                    if (cbListFragment == null) {
                        cbListFragment = CelestialBodyListFragment.newInstance(systemInfo);
                    }

                    fragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_holder,
                                 cbListFragment,
                                 FragmentTag.celestial_body_list_fragment.tag)
                        .addToBackStack(CelestialBodyListFragment.class.getCanonicalName())
                        .commit();
                }
            }
        } else {
            AndLog.d(LOG_NAME, "sub menu celestial click: " + ((systemInfo == null) ? "null" : systemInfo.toString()));
            Intent intent = new Intent(getApplicationContext(),
                                       CelestialListActivity.class);
            intent.putExtra("system", systemInfo);

            startActivityForResult(intent, RESULT_CELESTIAL);
        }
    }

    @Override
    public void onSubMenuJournalClick()
    {
        if (allInOneView) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            int count = fragmentManager.getBackStackEntryCount();
            String name = null;

            if (count > 0) {
                name = fragmentManager.getBackStackEntryAt(count - 1).getName();
            }

            if (!NotesFragment.class.getCanonicalName().equals(name)) {
                if ((notesFragment == null) || notesFragment.isDetached()){
                    notesFragment = NotesFragment.newInstance(systemInfo);
                } else {
                    notesFragment.setSystem(systemInfo);
                }

                fragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_holder,
                                 notesFragment,
                                 FragmentTag.notes_fragment.tag)
                        .addToBackStack(NotesFragment.class.getCanonicalName())
                        .commit();
            }
        } else {
            Intent intent = new Intent(getApplicationContext(), JournalActivity.class);
            intent.putExtra("system", systemInfo.getSystem());

            startActivityForResult(intent, RESULT_NOTES);
        }
    }

    @Override
    public void onSubMenuPhotoGalleryClick()
    {
        if (allInOneView) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            int count = fragmentManager.getBackStackEntryCount();
            String name = null;

            if (count > 0) {
                name = fragmentManager.getBackStackEntryAt(count - 1).getName();
            }

            if (!SystemPhotoGalleryFragment.class.getCanonicalName().equals(name)) {
                if ((photoFragment == null) || photoFragment.isDetached()) {
                    photoFragment = SystemPhotoGalleryFragment.newInstance(systemInfo.getSystem());
                } else {
                    photoFragment.setSystem(systemInfo.getSystem());
                }

                fragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_holder,
                                 photoFragment,
                                 FragmentTag.system_photo_gallery_fragment.tag)
                        .addToBackStack(SystemPhotoGalleryFragment.class.getCanonicalName())
                        .commit();
            }
        } else {
            Intent intent = new Intent(getApplicationContext(), SystemPhotoGalleryActivity.class);
            intent.putExtra("system", systemInfo.getSystem());

            startActivityForResult(intent, RESULT_PHOTO);
        }
    }

    @Override
    public void onDone(String tag, StarType type)
    {
        CmdrSystemInfo info = systemInfo;

        switch (tag) {
            case FragmentTag.celestial_body_list_fragment.choose_main_star_dialog: {
                cbListFragment.addMainStar(CmdrStarBody.create(info, type, null));

                synchronized (holderFragment.systemsModified) {
                    if (!holderFragment.systemsModified.contains(info.getSystem())) {
                        holderFragment.systemsModified.add(info.getSystem());
                    }
                }

                break;
            }
        }
    }

    @Override
    public void onMainStarCancel()
    {
        internalBackPressed();
    }

    @Override
    public void onCelestialBodyClick(CelestialBody body)
    {
        if (allInOneView) {
            cbParentFragment = CelestialInfoParentFragment.newInstance(body);

            Bundle bundle = new Bundle();
            bundle.putParcelable("celestial_body", body);

            cbParentFragment.setArguments(bundle);

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_holder,
                             cbParentFragment,
                             FragmentTag.celestial_info_fragment.tag)
                    .addToBackStack(CelestialInfoParentFragment.class.getCanonicalName())
                    .commit();
        }
    }

    public static class HolderFragment extends Fragment
    {
        public static final String HOLDER_FRAGMENT = "celestial_info_activity.holder_fragment";

        final Stack<CmdrSystemInfo> backStack = new Stack<>();
        final ArrayList<String> systemsModified = new ArrayList<>();
    }
}
