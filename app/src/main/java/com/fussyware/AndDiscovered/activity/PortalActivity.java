package com.fussyware.AndDiscovered.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.fragment.CoriolisPortalFragment;
import com.fussyware.AndDiscovered.fragment.EDDBPortalFragment;
import com.fussyware.AndDiscovered.fragment.EDSMPortalFragment;
import com.fussyware.AndDiscovered.fragment.GalnetNewsFragment;
import com.fussyware.AndDiscovered.fragment.InterstellarNewsFragment;
import com.fussyware.AndDiscovered.fragment.PowerPlayHeraldFragment;
import com.fussyware.AndDiscovered.fragment.ThruddsPortalFragment;

/**
 * Created by wes on 12/19/15.
 */
public class PortalActivity extends BaseActivity
{
    private static final String LOG_NAME = PortalActivity.class.getSimpleName();
    private static final int NEWS_START_POSITION = 0;
    private static final int TOOLS_START_POSITION = 3;

    private FragmentManager fragmentManager;

    private int visibleFragment;
    private Fragment[] fragments = {
            new GalnetNewsFragment(),
            new InterstellarNewsFragment(),
            new PowerPlayHeraldFragment(),
            new CoriolisPortalFragment(),
            new EDSMPortalFragment(),
            new EDDBPortalFragment(),
            new ThruddsPortalFragment(),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.portals_activity_title);
        setContentView(R.layout.portal_activity_layout);

        fragmentManager = getSupportFragmentManager();

        final Spinner newsSpinner = (Spinner) findViewById(R.id.news_spinner);
        final Spinner toolsSpinner = (Spinner) findViewById(R.id.tools_spinner);

        ArrayAdapter<CharSequence> newsAdapter = ArrayAdapter.createFromResource(this,
                                                                             R.array.news_portal_array,
                                                                             android.R.layout.simple_spinner_item);
        newsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> toolsAdapter = ArrayAdapter.createFromResource(this,
                                                                                 R.array.tools_portal_array,
                                                                                 android.R.layout.simple_spinner_item);
        toolsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        newsSpinner.setAdapter(newsAdapter);
        toolsSpinner.setAdapter(toolsAdapter);

        newsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position != 0) {
                    position = position + NEWS_START_POSITION - 1;

                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.fragment_holder,
                               fragments[position],
                               fragments[position].getClass().getCanonicalName());
                    ft.addToBackStack(null);
                    ft.commit();

                    visibleFragment = position;

                    newsSpinner.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        toolsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position != 0) {
                    position = position + TOOLS_START_POSITION - 1;

                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.fragment_holder,
                               fragments[position],
                               fragments[position].getClass().getCanonicalName());
                    ft.addToBackStack(null);
                    ft.commit();

                    visibleFragment = position;

                    toolsSpinner.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        for (int i = 0; i < fragments.length; i++) {
            Fragment tmp = fragmentManager.findFragmentByTag(fragments[i].getClass().getCanonicalName());
            if (tmp != null) {
                fragments[i] = tmp;
            }
        }

        if (savedInstanceState == null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_holder,
                             fragments[0],
                             fragments[0].getClass().getCanonicalName())
                    .commit();
        } else {
            visibleFragment = savedInstanceState.getInt("news_fragment_position");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("news_fragment_position", visibleFragment);
    }

    @Override
    public void onBackPressed()
    {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        super.onBackPressed();
        return true;
    }
}
