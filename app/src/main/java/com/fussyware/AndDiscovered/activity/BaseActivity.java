package com.fussyware.AndDiscovered.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.fussyware.AndDiscovered.AndDiscoveredActivity;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.dialog.TextDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.preference.PreferenceTag;
import com.fussyware.AndDiscovered.service.EDProxyWebsocketService;
import com.fussyware.AndDiscovered.service.ProxyBase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by wes on 9/30/15.
 */
public abstract class BaseActivity
        extends FragmentActivity
        implements TextDialogFragment.OnDoneListener
{
    private static final String LOG_NAME = BaseActivity.class.getSimpleName();
    private static final int DISCONNECTED = 1;
    private static final int CONNECTING = 2;
    private static final int CONNECTED = 3;

    protected SharedPreferences preferences;
    protected ActionBarDrawerToggle drawerToggle;
    protected DrawerLayout drawerLayout;

    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;

    private int connectedState = DISCONNECTED;
    private MenuItem connectionIcon;

    private LocalBroadcastManager broadcastManager;
    private final BroadcastReceiver broadcastReceiver = new ConnectionBroadcastReceiver();
    private PowerStateReceiver powerStateReceiver;

    private ListView drawerList;
    private TextView cmdrText;
    private TextView routeText;

    private boolean isPaused;
    private SystemInfo newSystem;
    private SystemInfo systemEntered;
    private DistanceInfo distanceEntered;
    private boolean updateSystems;

    protected ProxyBase proxyService;
    private final ServiceConnection proxyConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            proxyService = ((ProxyBase.EDProxyBinder) service).getService();
            proxyService.resendState();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            proxyService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            connectedState = savedInstanceState.getInt("connectedState");
        }

        prefListener = new SharedPreferences.OnSharedPreferenceChangeListener()
        {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
            {
                switch (key) {
                    case PreferenceTag.CMDR_NAME: {
                        String cmdr = preferences.getString(PreferenceTag.CMDR_NAME,
                                                            getResources().getString(R.string.unknown));
                        cmdrText.setText(cmdr);
                        break;
                    }
                    case PreferenceTag.ROUTE: {
                        String route = preferences.getString(PreferenceTag.ROUTE,
                                                             getResources().getString(R.string.unknown));
                        routeText.setText(route);
                        break;
                    }
                    case PreferenceTag.STAY_AWAKE:
                        powerStateReceiver.setStayAwake(preferences.getBoolean(key, false));
                        break;
                    default:
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter();

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        filter.addAction(Intent.ACTION_POWER_CONNECTED);
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);

        /** Register the power state before we listen for shared pref
         * changes. This way we do not run into race conditions.
         */
        powerStateReceiver = new PowerStateReceiver();
        powerStateReceiver.setStayAwake(preferences.getBoolean(PreferenceTag.STAY_AWAKE, false));

        registerReceiver(powerStateReceiver, filter);

        preferences.registerOnSharedPreferenceChangeListener(prefListener);

        filter = new IntentFilter();
        filter.addAction(ProxyBase.BROADCAST_DISCOVERY_STARTED);
        filter.addAction(ProxyBase.BROADCAST_DISCOVERY_FAILED);
        filter.addAction(ProxyBase.BROADCAST_DISCOVERY_SUCCESS);
        filter.addAction(ProxyBase.BROADCAST_CONNECTED);
        filter.addAction(ProxyBase.BROADCAST_DISCONNECTED);

        filter.addAction(ProxyBase.BROADCAST_NEW_DISTANCE);
        filter.addAction(ProxyBase.BROADCAST_NEW_SYSTEM);
        filter.addAction(ProxyBase.BROADCAST_SYSTEM_UPDATED);

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastManager.registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        bindService(new Intent(getApplicationContext(),
                               EDProxyWebsocketService.class),
                    proxyConnection,
                    BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        if (proxyService != null) {
            unbindService(proxyConnection);
        }
    }

    @Override
    protected void onPause()
    {
        isPaused = true;
        newSystem = null;
        systemEntered = null;
        distanceEntered = null;
        updateSystems = false;

        super.onPause();
    }

    @Override
    protected void onResumeFragments()
    {
        super.onResumeFragments();

        if (newSystem != null) {
            onNewSystemCreated(newSystem);
            newSystem = null;
        }

        if (systemEntered != null) {
            onNewSystemEntered(systemEntered, distanceEntered);
            systemEntered = null;
            distanceEntered = null;
        }

        if (updateSystems) {
            onUpdateSystems();
            updateSystems = false;
        }

        isPaused = false;
    }

    @Override
    public void setContentView(int layoutResID)
    {
        super.setContentView(layoutResID);
        onCreateDrawer();
    }

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);
        onCreateDrawer();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params)
    {
        super.setContentView(view, params);
        onCreateDrawer();
    }

    @Override
    protected void onDestroy()
    {
        broadcastManager.unregisterReceiver(broadcastReceiver);
        unregisterReceiver(powerStateReceiver);

        preferences.unregisterOnSharedPreferenceChangeListener(prefListener);

        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("connectedState", connectedState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        connectionIcon = menu.findItem(R.id.connection_state);

        switch (connectedState) {
            case CONNECTED:
                connectionIcon.setIcon(R.drawable.edproxy_connected_icon);
                break;
            case DISCONNECTED:
                connectionIcon.setIcon(R.drawable.edproxy_disconnected_icon);
                break;
            default:
                break;
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else {
                    return onHomeItemSelected(item);
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            super.onBackPressed();
        }
    }

    protected boolean onDrawerBackPressed()
    {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }

        return false;
    }

    protected boolean onHomeItemSelected(MenuItem item)
    {
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    protected void setDrawerIndicatorEnabled(boolean enabled)
    {
        drawerToggle.setDrawerIndicatorEnabled(enabled);
    }

    protected void onCreateDrawerList()
    {
        drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerList.setAdapter(new ArrayAdapter<>(this,
                                                 R.layout.drawer_item_layout,
                                                 getResources().getStringArray(R.array.drawerlist_string_array)));
        drawerList.setOnItemClickListener(new DrawerOnClickListener());
    }

    private void onCreateDrawer()
    {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        cmdrText = (TextView) findViewById(R.id.drawer_cmdr_name);
        routeText = (TextView) findViewById(R.id.drawer_route);

        String cmdr = preferences.getString(PreferenceTag.CMDR_NAME,
                                            getResources().getString(R.string.unknown));
        String route = preferences.getString(PreferenceTag.ROUTE,
                                             getResources().getString(R.string.unknown));

        cmdrText.setText(cmdr);
        routeText.setText(route);

        View routeLayout = findViewById(R.id.drawer_route_layout);
        routeLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                TextDialogFragment tdf = TextDialogFragment.newInstance(getResources().getString(R.string.route_destination_dialog_title),
                                                                        getResources().getString(R.string.route_destination_dialog_summary),
                                                                        preferences.getString(PreferenceTag.ROUTE, ""),
                                                                        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS,
                                                                        true);
                tdf.show(getSupportFragmentManager(), FragmentTag.main_activity.route_dialog);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this,
                                                 drawerLayout,
                                                 R.string.drawer_open,
                                                 R.string.drawer_close) {
            private CharSequence previous_title;

            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);

                if (getActionBar() != null) {
                    previous_title = getActionBar().getTitle();
                    getActionBar().setTitle(R.string.drawer_navigation_title);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView)
            {
                super.onDrawerClosed(drawerView);

                if (getActionBar() != null) {
                    getActionBar().setTitle(previous_title);
                }
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }

        onCreateDrawerList();
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.main_activity.route_dialog: {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(PreferenceTag.ROUTE, text);
                editor.apply();

                break;
            }
            default:
                break;
        }
    }

    protected void onNewSystemCreated(SystemInfo system)
    {

    }

    protected void onNewSystemEntered(SystemInfo system, DistanceInfo distance)
    {

    }

    protected void onUpdateSystems()
    {

    }

    private class DrawerOnClickListener implements AdapterView.OnItemClickListener
    {
        private void exportDatabase()
        {
            File outfile = new File(getFilesDir(), "databases/Commander.db");

            AndLog.d(LOG_NAME, "Exporting the commander database.");
            try {
                new File(getFilesDir(), "databases").mkdirs();

                FileOutputStream fos = new FileOutputStream(outfile);
                FileInputStream fis = new FileInputStream(new File(getApplicationInfo().dataDir + "/databases/Commander.db"));
                byte[] buffer = new byte[1024];
                int length;

                while ((length = fis.read(buffer)) > 0) {
                    fos.write(buffer, 0, length);
                }

                fis.close();
                fos.close();
            } catch (FileNotFoundException e) {
                AndLog.e(LOG_NAME, "Failed to open/create export commander database.", e);
            } catch (IOException e) {
                AndLog.e(LOG_NAME, "Failed to write out exported commander database.", e);
            }

            Uri uri = FileProvider.getUriForFile(BaseActivity.this,
                                                 "com.fussyware.AndDiscovered.fileprovider",
                                                 outfile);

            Intent intent = ShareCompat
                    .IntentBuilder
                    .from(BaseActivity.this)
                    .setType("*/*")
                    .setStream(uri)
                    .setChooserTitle("Export Commander Database")
                    .createChooserIntent()
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            startActivity(intent);
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            Intent intent;

            // TODO: This sucks giant &^%$#@! Replace with something more abstract please!
            switch (position) {
                case 0:
                    intent = new Intent(getApplicationContext(), AndDiscoveredActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 1:
                    intent = new Intent(getApplicationContext(), PhotoGalleryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(getApplicationContext(), RouteOptimizerActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 3:
                    intent = new Intent(getApplicationContext(), StatsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 4:
                    intent = new Intent(getApplicationContext(), PortalActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case 5:
                    exportDatabase();
                    break;
                case 6:
                    startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                    break;
                default:
                    break;
            }

            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private class ConnectionBroadcastReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            switch (action) {
                case ProxyBase.BROADCAST_DISCOVERY_STARTED:
                    AndLog.d(LOG_NAME, "EDProxy discovery started.");
                    connectedState = CONNECTING;
                    break;
                case ProxyBase.BROADCAST_DISCOVERY_SUCCESS:
                    String hostname = intent.getStringExtra(ProxyBase.EXTRA_HOSTNAME);
                    int port = intent.getIntExtra(ProxyBase.EXTRA_PORT, -1);
                    AndLog.d(LOG_NAME,
                             "EDProxy discovery success: hostname [" +
                             hostname +
                             "], port [" +
                             Integer.toString(port) +
                             "]");
                    break;
                case ProxyBase.BROADCAST_DISCOVERY_FAILED:
                    AndLog.d(LOG_NAME, "EDProxy discovery failed!");
                    if (connectionIcon != null) {
                        connectionIcon.setIcon(R.drawable.edproxy_disconnected_icon);
                    }

                    connectedState = DISCONNECTED;
                    break;
                case ProxyBase.BROADCAST_CONNECTED:
                    if (connectionIcon != null) {
                        connectionIcon.setIcon(R.drawable.edproxy_connected_icon);
                    }

                    connectedState = CONNECTED;
                    break;
                case ProxyBase.BROADCAST_DISCONNECTED:
                    AndLog.d(LOG_NAME, "EDProxy disconnected!");
                    if (connectionIcon != null) {
                        connectionIcon.setIcon(R.drawable.edproxy_disconnected_icon);
                    }

                    connectedState = DISCONNECTED;
                    break;
                case ProxyBase.BROADCAST_NEW_DISTANCE: {
                    DistanceInfo distanceInfo = intent.getParcelableExtra("distance_info");
                    SystemInfo systemInfo = CmdrDbHelper.getInstance()
                                                        .getSystem(distanceInfo.getFirstSystem());

                    if (isPaused) {
                        AndLog.d(LOG_NAME, "Save off system entered for when fragment is attached.");
                        systemEntered = systemInfo;
                        distanceEntered = distanceInfo;
                    } else {
                        onNewSystemEntered(systemInfo, distanceInfo);
                    }
                }
                case ProxyBase.BROADCAST_NEW_SYSTEM: {
                    SystemInfo info = intent.getParcelableExtra("system");

                    if (isPaused) {
                        AndLog.d(LOG_NAME, "Save off new system for when fragment is attached.");
                        newSystem = info;
                    } else {
                        onNewSystemCreated(info);
                    }

                    break;
                }
                case ProxyBase.BROADCAST_SYSTEM_UPDATED:
                    if (isPaused) {
                        AndLog.d(LOG_NAME, "Save off update system for when fragment is attached.");
                        updateSystems = true;
                    } else {
                        onUpdateSystems();
                    }

                    break;
                default:
                    break;
            }
        }
    }

    private class PowerStateReceiver extends BroadcastReceiver
    {
        private boolean stayAwake;
        private boolean powerConnected;

        PowerStateReceiver()
        {
            Intent intent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            if ((intent != null) && (intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0) != 0)) {
                powerConnected = true;
                AndLog.d(LOG_NAME, "Power is ON.");
            }
        }

        synchronized void setStayAwake(boolean enabled)
        {
            if (stayAwake != enabled) {
                stayAwake = enabled;

                if (powerConnected) {
                    if (stayAwake) {
                        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    } else {
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    }
                }
            }
        }

        @Override
        public synchronized void onReceive(Context context, Intent intent)
        {
            switch (intent.getAction()) {
                case Intent.ACTION_POWER_CONNECTED:
                    AndLog.d(LOG_NAME, "Power is connected!");

                    if (stayAwake) {
                        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    }

                    powerConnected = true;

                    break;
                case Intent.ACTION_POWER_DISCONNECTED:
                    AndLog.d(LOG_NAME, "Power is disconnected!");
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                    powerConnected = false;

                    break;
            }
        }
    }
}
