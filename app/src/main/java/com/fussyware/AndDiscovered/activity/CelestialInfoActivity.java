package com.fussyware.AndDiscovered.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.AsteroidBody;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.fragment.CelestialAsteroidDetailsFragment;
import com.fussyware.AndDiscovered.fragment.CelestialInfoCommonDetailsFragment;
import com.fussyware.AndDiscovered.fragment.CelestialInfoHeaderFragment;
import com.fussyware.AndDiscovered.fragment.CelestialPlanetDetailsFragment;
import com.fussyware.AndDiscovered.fragment.CelestialStarDetailsFragment;
import com.fussyware.AndDiscovered.fragment.TitleInfoFragment;

import java.security.InvalidParameterException;

/**
 * Created by wes on 9/30/15.
 */
public class CelestialInfoActivity extends BaseActivity
{
    private static final String TAG = CelestialInfoActivity.class.getSimpleName();

    private CelestialBody celestialBody;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.celestial_info_activity_title);
        setContentView(R.layout.celestial_info_activity_layout);
        setDrawerIndicatorEnabled(false);

        TitleInfoFragment titleFragment = (TitleInfoFragment) getSupportFragmentManager().findFragmentById(
                R.id.title_info_fragment);
        CelestialInfoHeaderFragment headerFragment = (CelestialInfoHeaderFragment) getSupportFragmentManager()
                .findFragmentById(R.id.celestial_info_header_fragment);
        CelestialInfoCommonDetailsFragment commonFragment = (CelestialInfoCommonDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.cb_common_details_fragment);
        CelestialPlanetDetailsFragment planetFragment = (CelestialPlanetDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.cb_planet_details_fragment);
        CelestialStarDetailsFragment starFragment = (CelestialStarDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.cb_star_details_fragment);
        CelestialAsteroidDetailsFragment asteroidFragment = (CelestialAsteroidDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.cb_asteroid_details_fragment);

        if (savedInstanceState == null) {
            celestialBody = getIntent().getParcelableExtra("celestial");

            titleFragment.setSystem(celestialBody.getSystem());
            headerFragment.setCelestialBody(celestialBody);
            commonFragment.setCelestialBody(celestialBody);

            switch (celestialBody.getSatelliteCategory()) {
                case Star:
                    starFragment.setCelestialBody((StarBody) celestialBody);
                    break;
                case Planet:
                    planetFragment.setCelestialBody((PlanetBody) celestialBody);
                    break;
                case Asteroid:
                    asteroidFragment.setCelestialBody((AsteroidBody) celestialBody);
                    break;
                case Unknown:
                    throw new InvalidParameterException("A valid Satellite category must be provided.");
            }
        } else {
            celestialBody = savedInstanceState.getParcelable("celestial");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial", celestialBody);
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            onActivityResult();
        }
    }

    @Override
    protected boolean onHomeItemSelected(MenuItem item)
    {
        onActivityResult();
        return true;
    }

    @Override
    protected void onNewSystemEntered(SystemInfo system, DistanceInfo distance)
    {
        super.onBackPressed();
    }

    private void onActivityResult()
    {
        Intent intent = new Intent();
        intent.putExtra("celestial_body", celestialBody);

        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
