package com.fussyware.AndDiscovered.celestial;

import android.os.Parcel;
import android.support.annotation.NonNull;

import java.security.InvalidParameterException;

/**
 * Created by wes on 11/7/15.
 */
public abstract class StarBody extends CelestialBody
{
    private final StarType type;

    private Double age;
    private Double mass;
    private Double radius;
    private Double surfaceTemp;

    protected StarBody(@NonNull SystemInfo system,
                       @NonNull StarType type,
                       CelestialBody parent)
    {
        super(system, ScanLevel.Level_2, false, parent);

        this.type = type;
    }

    protected StarBody(@NonNull SystemInfo system,
                       @NonNull StarType type,
                       @NonNull ScanLevel scanLevel,
                       boolean firstDiscovered,
                       CelestialBody parent,
                       String name,
                       Double distance,
                       Double age,
                       Double mass,
                       Double radius,
                       Double surfaceTemp,
                       Double orbitalPeriod,
                       Double semiMajorAxis,
                       Double orbitalEccentricity,
                       Double orbitalInclination,
                       Double argPeriapsis)
    {
        super(system, scanLevel, firstDiscovered, parent);

        if (type == StarType.Unknown) {
            throw new InvalidParameterException("A valid star type must be specified during creation.");
        }

        this.type = type;
        this.name =  (name == null) ? "" : name;
        this.distance = distance;
        this.age = age;
        this.mass = mass;
        this.radius = radius;
        this.surfaceTemp = surfaceTemp;
        this.orbitalPeriod = orbitalPeriod;
        this.semiMajorAxis = semiMajorAxis;
        this.orbitalEccentricity = orbitalEccentricity;
        this.orbitalInclination = orbitalInclination;
        this.argPeriapsis = argPeriapsis;
    }

    protected StarBody(Parcel parcel)
    {
        super(parcel);

        type = StarType.getStarType(parcel.readInt());
        age = (Double) parcel.readValue(null);
        mass = (Double) parcel.readValue(null);
        radius = (Double) parcel.readValue(null);
        surfaceTemp = (Double) parcel.readValue(null);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeInt(type.value());
        dest.writeValue(age);
        dest.writeValue(mass);
        dest.writeValue(radius);
        dest.writeValue(surfaceTemp);
    }

    public StarType getType()
    {
        return type;
    }

    public Double getAge()
    {
        return age;
    }

    public void setAge(double age)
    {
        if ((this.age == null) || (this.age != age)) {
            this.age = age;
            updateAge(age);
        }
    }

    public Double getMass()
    {
        return mass;
    }

    public void setMass(double mass)
    {
        if ((this.mass == null) || (this.mass != mass)) {
            this.mass = mass;
            updateMass(mass);
        }
    }

    public Double getRadius()
    {
        return radius;
    }

    public void setRadius(double radius)
    {
        if ((this.radius == null) || (this.radius != radius)) {
            this.radius = radius;
            updateRadius(radius);
        }
    }

    public Double getSurfaceTemp()
    {
        return surfaceTemp;
    }

    public void setSurfaceTemp(double surfaceTemp)
    {
        if ((this.mass == null) || (this.surfaceTemp != surfaceTemp)) {
            this.surfaceTemp = surfaceTemp;
            updateSurfaceTemp(surfaceTemp);
        }
    }

    @Override
    public SatelliteCategory getSatelliteCategory()
    {
        return SatelliteCategory.Star;
    }

    /** Star specific routines */
    protected abstract void updateAge(double age);
    protected abstract void updateMass(double mass);
    protected abstract void updateRadius(double radius);
    protected abstract void updateSurfaceTemp(double surfaceTemp);
}
