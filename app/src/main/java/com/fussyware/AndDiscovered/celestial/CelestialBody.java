package com.fussyware.AndDiscovered.celestial;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by wes on 11/7/15.
 */
public abstract class CelestialBody
        implements Iterable<CelestialBody>,
                   Parcelable
{
    private final SystemInfo system;

    protected CelestialBody parent;
    protected boolean firstDiscovered;
    protected String name;
    protected Double distance;
    protected ScanLevel scanLevel;
    protected Double semiMajorAxis;
    protected Double orbitalPeriod;
    protected Double orbitalEccentricity;
    protected Double orbitalInclination;
    protected Double argPeriapsis;

    CelestialBody(@NonNull SystemInfo system,
                  @NonNull ScanLevel scanLevel,
                  boolean firstDiscovered,
                  CelestialBody parent)
    {
        this.system = system;
        this.parent = parent;
        this.scanLevel = scanLevel;
        this.firstDiscovered = firstDiscovered;
    }

    protected CelestialBody(Parcel parcel)
    {
        system = parcel.readParcelable(SystemInfo.class.getClassLoader());
        parent = parcel.readParcelable(CelestialBody.class.getClassLoader());
        name = parcel.readString();
        distance = (Double) parcel.readValue(null);
        scanLevel = ScanLevel.getScanLevel(parcel.readInt());
        firstDiscovered = (parcel.readInt() == 1);
        semiMajorAxis = (Double) parcel.readValue(null);
        orbitalPeriod = (Double) parcel.readValue(null);
        orbitalEccentricity = (Double) parcel.readValue(null);
        orbitalInclination = (Double) parcel.readValue(null);
        argPeriapsis = (Double) parcel.readValue(null);
    }

    public SystemInfo getSystem()
    {
        return system;
    }

    public CelestialBody getParent()
    {
        return parent;
    }

    public Double getDistance()
    {
        return distance;
    }

    public ScanLevel getScanLevel()
    {
        return scanLevel;
    }

    public Double getSemiMajorAxis()
    {
        return semiMajorAxis;
    }

    public Double getOrbitalPeriod()
    {
        return orbitalPeriod;
    }

    public Double getOrbitalEccentricity()
    {
        return orbitalEccentricity;
    }

    public Double getOrbitalInclination()
    {
        return orbitalInclination;
    }

    public Double getArgPeriapsis()
    {
        return argPeriapsis;
    }

    public String getName()
    {
        return (name == null) ? "" : name;
    }

    public boolean isFirstDiscovered()
    {
        return firstDiscovered;
    }

    public void setFirstDiscovered(boolean firstDiscovered)
    {
        this.firstDiscovered = firstDiscovered;
        updateFirstDisocovered(firstDiscovered);
    }

    public void setParent(CelestialBody parent)
    {
        if (this.parent != parent) {
            this.parent = parent;
            updateParent(parent);
        }
    }

    public void setName(String name)
    {
        if (name == null) {
            name = "";
        }

        if (!name.equals(this.name)) {
            this.name = name;

            updateName(name);
        }
    }

    public void setDistance(double distance)
    {
        if ((this.distance == null) || (this.distance != distance)) {
            this.distance = distance;
            updateDistance(distance);
        }
    }

    public void setScanLevel(ScanLevel scanLevel)
    {
        if (this.scanLevel != scanLevel) {
            this.scanLevel = scanLevel;
            updateScanLevel(scanLevel);
        }
    }

    public void setSemiMajorAxis(double semiMajorAxis)
    {
        if ((this.semiMajorAxis == null) || (this.semiMajorAxis != semiMajorAxis)) {
            this.semiMajorAxis = semiMajorAxis;
            updateSemiMajorAxis(semiMajorAxis);
        }
    }

    public void setOrbitalPeriod(double orbitalPeriod)
    {
        if ((this.orbitalPeriod == null) || (this.orbitalPeriod != orbitalPeriod)) {
            this.orbitalPeriod = orbitalPeriod;
            updateOrbitalPeriod(orbitalPeriod);
        }
    }

    public void setOrbitalEccentricity(double orbitalEccentricity)
    {
        if ((this.orbitalEccentricity == null) || (this.orbitalEccentricity != orbitalEccentricity)) {
            this.orbitalEccentricity = orbitalEccentricity;
            updateOrbitalEccentricity(orbitalEccentricity);
        }
    }

    public void setOrbitalInclination(double orbitalInclination)
    {
        if ((this.orbitalInclination == null) || (this.orbitalInclination != orbitalInclination)) {
            this.orbitalInclination = orbitalInclination;
            updateOrbitalInclination(orbitalInclination);
        }
    }

    public void setArgPeriapsis(double argPeriapsis)
    {
        if ((this.argPeriapsis == null) || (this.argPeriapsis != argPeriapsis)) {
            this.argPeriapsis = argPeriapsis;
            updateArgPeriapsis(argPeriapsis);
        }
    }

    public boolean hasSatellites()
    {
        return !getSatellites().isEmpty();
    }

    @Override
    public boolean equals(Object o)
    {
        return ((o != null) &&
                (o.getClass() == getClass()) &&
                (getId().equals(((CelestialBody) o).getId())));
    }

    @Override
    public int hashCode()
    {
        return getId().hashCode();
    }

    /** Iterate over all <b>satellites (i.e. children)</b> of this
     * {@link CelestialBody}.
     *
     * @return An {@link Iterator} that will iterate over all child
     * satellites.
     */
    @Override
    public Iterator<CelestialBody> iterator()
    {
        return new CelestialIterator(this);
    }

    @NonNull
    public abstract Object getId();

    public abstract SatelliteCategory getSatelliteCategory();
    public abstract List<CelestialBody> getSatellites();

    public boolean hasRings()
    {
        return (getRings().size() != 0);
    }

    public abstract List<Ring> getRings();
    public abstract void add(int level);

    public abstract void delete();

    /** Common routines for a Celestial Body. */
    protected abstract void updateFirstDisocovered(boolean firstDiscovered);
    protected abstract void updateParent(CelestialBody body);
    protected abstract void updateName(String name);
    protected abstract void updateDistance(double distance);
    protected abstract void updateScanLevel(ScanLevel scanLevel);
    protected abstract void updateSemiMajorAxis(double semiMajorAxis);
    protected abstract void updateOrbitalPeriod(double orbitalPeriod);
    protected abstract void updateOrbitalEccentricity(double orbitalEccentricity);
    protected abstract void updateOrbitalInclination(double orbitalInclination);
    protected abstract void updateArgPeriapsis(double argPeriapsis);

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeParcelable(system, 0);
        dest.writeParcelable(getParent(), 0);
        dest.writeString(name);
        dest.writeValue(distance);
        dest.writeInt(scanLevel.value);
        dest.writeInt(firstDiscovered ? 1 : 0);
        dest.writeValue(semiMajorAxis);
        dest.writeValue(orbitalPeriod);
        dest.writeValue(orbitalEccentricity);
        dest.writeValue(orbitalInclination);
        dest.writeValue(argPeriapsis);
    }

    private class CelestialIterator implements Iterator<CelestialBody>
    {
        private final ArrayList<CelestialBody> directChildren = new ArrayList<>();

        private Iterator<CelestialBody> iterator;
        private CelestialBody nextBody;

        CelestialIterator(CelestialBody root)
        {
            directChildren.addAll(root.getSatellites());

            if (!directChildren.isEmpty()) {
                nextBody = directChildren.remove(0);
                iterator = nextBody.iterator();
            }
        }

        @Override
        public boolean hasNext()
        {
            return (nextBody != null);
        }

        @Override
        public CelestialBody next()
        {
            if (nextBody == null) {
                throw new NoSuchElementException("No more satellites.");
            }

            CelestialBody body = nextBody;

            if (iterator.hasNext()) {
                nextBody = iterator.next();
            } else if (!directChildren.isEmpty()) {
                nextBody = directChildren.remove(0);
                iterator = nextBody.iterator();
            } else {
                nextBody = null;
            }

            return body;
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException("Currently cannot remove bodies " +
                                                    "during iteration.");
        }
    }
}
