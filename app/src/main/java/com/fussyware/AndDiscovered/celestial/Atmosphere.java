package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 11/7/15.
 */
public enum Atmosphere
{
    Oxygen(0),
    Carbon_Dioxide(1),
    Ammonia(2),
    Argon(3),
    Nitrogen(4),
    Iron(5),
    Silicon(6),
    Sulfur_Dioxide(7),
    Water(8),
    Methane(9),
    Hydrogen(10),
    Neon(11),
    Helium(12),
    Unknown(-1);

    public final int value;

    Atmosphere(int value)
    {
        this.value = value;
    }

    public static Atmosphere getAtmosphere(int value)
    {
        for (Atmosphere type : Atmosphere.values()) {
            if (type.value == value) {
                return type;
            }
        }

        return Unknown;
    }
}
