package com.fussyware.AndDiscovered.celestial;

import android.os.Parcel;
import android.support.annotation.NonNull;

import java.security.InvalidParameterException;

/**
 * Created by wes on 11/7/15.
 */
public abstract class AsteroidBody extends CelestialBody
{
    private final Mineral type;

    private Double moonMasses;

    public AsteroidBody(@NonNull SystemInfo system,
                        @NonNull Mineral type,
                        @NonNull ScanLevel scanLevel,
                        boolean firstDiscovered,
                        CelestialBody parent,
                        String name,
                        Double distance,
                        Double mass,
                        Double orbitalPeriod,
                        Double semiMajorAxis,
                        Double orbitalEccentricity,
                        Double orbitalInclination,
                        Double argPeriapsis)
    {
        super(system, scanLevel, firstDiscovered, parent);

        if (type == Mineral.Unknown) {
            throw new InvalidParameterException("A valid asteroid type must be specified during creation.");
        }

        this.type = type;

        this.name =  (name == null) ? "" : name;
        this.scanLevel = scanLevel;
        this.distance = distance;
        this.moonMasses = mass;
        this.orbitalPeriod = orbitalPeriod;
        this.semiMajorAxis = semiMajorAxis;
        this.orbitalEccentricity = orbitalEccentricity;
        this.orbitalInclination = orbitalInclination;
        this.argPeriapsis = argPeriapsis;
    }

    protected AsteroidBody(@NonNull SystemInfo system,
                           @NonNull Mineral type,
                           CelestialBody parent)
    {
        super(system, ScanLevel.Level_2, false, parent);

        this.type = type;
    }

    protected AsteroidBody(Parcel parcel)
    {
        super(parcel);

        type = Mineral.getMineral(parcel.readInt());
        moonMasses = (Double) parcel.readValue(null);
    }

    public Mineral getType()
    {
        return type;
    }

    public Double getMoonMass()
    {
        return moonMasses;
    }

    public void setMoonMass(double mass)
    {
        if (this.moonMasses != mass) {
            this.moonMasses = mass;
            updateMoonMass(mass);
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeInt(type.value);
        dest.writeValue(moonMasses);
    }

    @Override
    public SatelliteCategory getSatelliteCategory()
    {
        return SatelliteCategory.Asteroid;
    }

    protected abstract void updateMoonMass(double mass);
}
