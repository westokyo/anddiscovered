package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 1/6/16.
 */
public enum VolcanoType
{
    No_Volcanism(0),
    Silicate_Magma(1),
    Silicate_Vapour_Geysers(2),
    Iron_Magma(3),
    Water_Geyser(4),
    Unknown(-1);

    public final int value;

    VolcanoType(int value)
    {
        this.value = value;
    }

    public static VolcanoType getVolcanoType(int value)
    {
        for (VolcanoType type : VolcanoType.values()) {
            if (type.value == value) {
                return type;
            }
        }

        return Unknown;
    }


}
