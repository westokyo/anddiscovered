package com.fussyware.AndDiscovered.celestial;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by wes on 12/7/15.
 */
public abstract class Ring implements Parcelable
{
    private CelestialBody parent;
    private int level;
    private Mineral type;
    private RingQuality quality;

    private Double mass;
    private Double semiMajorAxis;
    private Double innerRadius;
    private Double outerRadius;

    protected Ring(@NonNull CelestialBody parent, int level)
    {
        this.parent = parent;
        this.level = level;

        type = Mineral.Unknown;
        quality = RingQuality.Unknown;
    }

    protected Ring(@NonNull CelestialBody parent,
                   int level,
                   Mineral type,
                   RingQuality quality,
                   Double mass,
                   Double semiMajorAxis,
                   Double innerRadius,
                   Double outerRadius)
    {
        this.parent = parent;
        this.level = level;
        this.type = (type == null) ? Mineral.Unknown : type;
        this.quality = (quality == null) ? RingQuality.Unknown : quality;
        this.mass = mass;
        this.semiMajorAxis = semiMajorAxis;
        this.innerRadius = innerRadius;
        this.outerRadius = outerRadius;
    }

    protected Ring(Parcel parcel)
    {
        this.parent = parcel.readParcelable(CelestialBody.class.getClassLoader());
        this.level = parcel.readInt();
        this.type = Mineral.getMineral(parcel.readInt());
        this.quality = RingQuality.getRingQuality(parcel.readInt());
        this.mass = (Double) parcel.readValue(null);
        this.semiMajorAxis = (Double) parcel.readValue(null);
        this.innerRadius = (Double) parcel.readValue(null);
        this.outerRadius = (Double) parcel.readValue(null);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeParcelable(parent, 0);
        dest.writeInt(level);
        dest.writeInt(type.value);
        dest.writeInt(quality.value);
        dest.writeValue(mass);
        dest.writeValue(semiMajorAxis);
        dest.writeValue(innerRadius);
        dest.writeValue(outerRadius);
    }

    public CelestialBody getParent()
    {
        return parent;
    }

    public void setParent(CelestialBody parent)
    {
        this.parent = parent;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    public Mineral getMineralType()
    {
        return type;
    }

    public void setMineralType(Mineral type)
    {
        this.type = type;
    }

    public RingQuality getQuality()
    {
        return quality;
    }

    public void setQuality(RingQuality quality)
    {
        this.quality = quality;
    }

    public Double getMass()
    {
        return mass;
    }

    public void setMass(Double mass)
    {
        this.mass = mass;
    }

    public Double getSemiMajorAxis()
    {
        return semiMajorAxis;
    }

    public void setSemiMajorAxis(Double semiMajorAxis)
    {
        this.semiMajorAxis = semiMajorAxis;
    }

    public Double getInnerRadius()
    {
        return innerRadius;
    }

    public void setInnerRadius(Double innerRadius)
    {
        this.innerRadius = innerRadius;
    }

    public Double getOuterRadius()
    {
        return outerRadius;
    }

    public void setOuterRadius(Double outerRadius)
    {
        this.outerRadius = outerRadius;
    }

    @NonNull
    public abstract Object getId();

    protected abstract void updateParent(CelestialBody body);
    protected abstract void updateLevel(int level);
    protected abstract void updateType(Mineral type);
    protected abstract void updateQuality(RingQuality quality);
    protected abstract void updateMass(Double mass);
    protected abstract void updateSemiMajorAxis(Double semiMajorAxis);
    protected abstract void updateInnerRadius(Double innerRadius);
    protected abstract void updateOuterRadius(Double outerRadius);
}
