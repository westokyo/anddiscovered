package com.fussyware.AndDiscovered.celestial;

import android.os.Parcel;
import android.support.annotation.NonNull;

import java.security.InvalidParameterException;

/**
 * Created by wes on 11/7/15.
 */
public abstract class PlanetBody extends CelestialBody
{
    private final PlanetType type;

    private Double mass;
    private Double radius;
    private Double surfaceTemp;
    private Double surfacePressure;
    private VolcanoType volcanism = VolcanoType.Unknown;
    private Atmosphere atmosphereType = Atmosphere.Unknown;
    private Double rotationPeriod;
    private Boolean tidalLocked;
    private Double axisTilt;
    private boolean terraformable;

    public PlanetBody(@NonNull SystemInfo system,
                      @NonNull PlanetType type,
                      @NonNull ScanLevel scanLevel,
                      boolean firstDiscovered,
                      CelestialBody parent,
                      String name,
                      Double distance,
                      boolean terraformable,
                      Double mass,
                      Double radius,
                      Double surfaceTemp,
                      Double surfacePressure,
                      VolcanoType volcanism,
                      Atmosphere atmosphereType,
                      Double orbitalPeriod,
                      Double semiMajorAxis,
                      Double orbitalEccentricity,
                      Double orbitalInclination,
                      Double argPeriapsis,
                      Double rotationPeriod,
                      Boolean tidalLocked,
                      Double axisTilt)
    {
        super(system, scanLevel, firstDiscovered, parent);

        if (type == PlanetType.Unknown) {
            throw new InvalidParameterException("A valid planet type must be specified during creation.");
        }

        this.type = type;

        if (atmosphereType == null) {
            atmosphereType = Atmosphere.Unknown;
        }

        if (volcanism == null) {
            volcanism = VolcanoType.Unknown;
        }

        this.name =  (name == null) ? "" : name;
        this.distance = distance;
        this.terraformable = terraformable;
        this.mass = mass;
        this.radius = radius;
        this.surfaceTemp = surfaceTemp;
        this.surfacePressure = surfacePressure;
        this.volcanism = volcanism;
        this.atmosphereType = atmosphereType;
        this.orbitalPeriod = orbitalPeriod;
        this.semiMajorAxis = semiMajorAxis;
        this.orbitalEccentricity = orbitalEccentricity;
        this.orbitalInclination = orbitalInclination;
        this.argPeriapsis = argPeriapsis;
        this.rotationPeriod = rotationPeriod;
        this.tidalLocked = tidalLocked;
        this.axisTilt = axisTilt;
    }

    protected PlanetBody(@NonNull SystemInfo system,
                         @NonNull PlanetType type,
                         CelestialBody parent)
    {
        super(system, ScanLevel.Level_2, false, parent);

        this.type = type;
    }

    protected PlanetBody(Parcel parcel)
    {
        super(parcel);

        type = PlanetType.getPlanetType(parcel.readInt());
        terraformable = (parcel.readInt() == 1);
        mass = (Double) parcel.readValue(null);
        radius = (Double) parcel.readValue(null);
        surfaceTemp = (Double) parcel.readValue(null);
        surfacePressure = (Double) parcel.readValue(null);
        volcanism = VolcanoType.getVolcanoType(parcel.readInt());
        atmosphereType = Atmosphere.getAtmosphere(parcel.readInt());
        rotationPeriod = (Double) parcel.readValue(null);
        tidalLocked = (Boolean) parcel.readValue(null);
        axisTilt = (Double) parcel.readValue(null);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeInt(type.value());
        dest.writeInt(terraformable ? 1 : 0);
        dest.writeValue(mass);
        dest.writeValue(radius);
        dest.writeValue(surfaceTemp);
        dest.writeValue(surfacePressure);
        dest.writeInt(volcanism.value);
        dest.writeInt(atmosphereType.value);
        dest.writeValue(rotationPeriod);
        dest.writeValue(tidalLocked);
        dest.writeValue(axisTilt);
    }

    public PlanetType getType()
    {
        return type;
    }

    public boolean getTerraformable()
    {
        return terraformable;
    }

    public void setTerraformable(boolean terraformable)
    {
        this.terraformable = terraformable;
        updateTerraformable(terraformable);
    }

    public Double getMass()
    {
        return mass;
    }

    public void setMass(double mass)
    {
        if ((this.mass == null) || (this.mass != mass)) {
            this.mass = mass;
            updateMass(mass);
        }
    }

    public Double getRadius()
    {
        return radius;
    }

    public void setRadius(double radius)
    {
        if ((this.radius == null) || (this.radius != radius)) {
            this.radius = radius;
            updateRadius(radius);
        }
    }

    public Double getSurfaceTemp()
    {
        return surfaceTemp;
    }

    public void setSurfaceTemp(double surfaceTemp)
    {
        if ((this.surfaceTemp == null) || (this.surfaceTemp != surfaceTemp)) {
            this.surfaceTemp = surfaceTemp;
            updateSurfaceTemp(surfaceTemp);
        }
    }

    public Double getSurfacePressure()
    {
        return surfacePressure;
    }

    public void setSurfacePressure(double surfacePressure)
    {
        if ((this.surfacePressure == null) || (this.surfacePressure != surfacePressure)) {
            this.surfacePressure = surfacePressure;
            updateSurfacePressure(surfacePressure);
        }
    }

    public VolcanoType getVolcanism()
    {
        return volcanism;
    }

    public void setVolcanism(VolcanoType volcanism)
    {
        if (volcanism == null) {
            volcanism = VolcanoType.Unknown;
        }

        if (this.volcanism != volcanism) {
            this.volcanism = volcanism;
            updateVolcanism(volcanism);
        }
    }

    public Atmosphere getAtmosphereType()
    {
        return atmosphereType;
    }

    public void setAtmosphereType(Atmosphere atmosphereType)
    {
        if (atmosphereType == null) {
            atmosphereType = Atmosphere.Unknown;
        }

        if (this.atmosphereType != atmosphereType) {
            this.atmosphereType = atmosphereType;
            updateAtmosphereType(atmosphereType);
        }
    }

    public Double getRotationPeriod()
    {
        return rotationPeriod;
    }

    public void setRotationPeriod(double rotationPeriod)
    {
        if ((this.rotationPeriod == null) || (this.rotationPeriod != rotationPeriod)) {
            this.rotationPeriod = rotationPeriod;
            updateRotationPeriod(rotationPeriod);
        }
    }

    public Boolean getTidalLocked()
    {
        return tidalLocked;
    }

    public void setTidalLocked(boolean tidalLocked)
    {
        if ((this.tidalLocked == null) || (this.tidalLocked != tidalLocked)) {
            this.tidalLocked = tidalLocked;
            updateTidalLocked(tidalLocked);
        }
    }

    public Double getAxisTilt()
    {
        return axisTilt;
    }

    public void setAxisTilt(double axisTilt)
    {
        if ((this.axisTilt == null) || (this.axisTilt != axisTilt)) {
            this.axisTilt = axisTilt;
            updateAxisTilt(axisTilt);
        }
    }

    @Override
    public SatelliteCategory getSatelliteCategory()
    {
        return SatelliteCategory.Planet;
    }

    protected abstract void updateTerraformable(boolean terraformable);
    protected abstract void updateMass(double mass);
    protected abstract void updateRadius(double radius);
    protected abstract void updateSurfaceTemp(double surfaceTemp);
    protected abstract void updateSurfacePressure(double surfacePressure);
    protected abstract void updateVolcanism(VolcanoType volcanism);
    protected abstract void updateAtmosphereType(Atmosphere atmosphereType);
    protected abstract void updateRotationPeriod(double rotationPeriod);
    protected abstract void updateTidalLocked(boolean tidalLocked);
    protected abstract void updateAxisTilt(double axisTilt);
}
