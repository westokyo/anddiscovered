package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 8/30/15.
 */
public enum PlanetType
{
    Metal_Rich(0, "Metal Rich", 9000),
    High_Metallic(1, "High Metallic", 4100),
    Water_World(2, "Water World", 18000),
    Earth_Like(3, "Earth Like", 54000),
    Ammonia(4, "Ammonia", 33000),
    Gas_Giant_I(5, "Gas Giant I", 2100),
    Gas_Giant_II(6, "Gas Giant II", 7500),
    Gas_Giant_III(7, "Gas Giant III", 2200),
    Gas_Giant_IV(8, "Gas Giant IV", 2200),
    Gas_Giant_V(9, "Gas Giant V", 2200),
    Rocky(10, "Rocky", 600),
    Ice(11, "Ice", 600),
    Gas_Giant_Ammonia(12, "Gas Giant with Ammonia", 2200),
    Gas_Giant_Water(13, "Gas Giant with Water", 2200),
    Gas_Giant_Helium(14, "Gas Giant Helium-rich", 2200),
    Water_Giant(15, "Water Giant", 2200),
    Unknown(-1, "NA", -1);

    public final int value;
    public final int credits;

    // TODO: Get rid of name and make the application use R.string.
    public final String name;

    PlanetType(int value, String name, int credits)
    {
        this.value = value;
        this.credits = credits;
        this.name = name;
    }

    public static PlanetType getPlanetType(String value)
    {
        for (PlanetType t : PlanetType.values()) {
            if (t.name.equals(value)) {
                return t;
            }
        }

        return PlanetType.Unknown;
    }

    public static PlanetType getPlanetType(int value)
    {
        for (PlanetType t : PlanetType.values()) {
            if (t.value == value) {
                return t;
            }
        }

        return PlanetType.Unknown;
    }

    public int value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return name;
    }

}
