package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 11/8/15.
 */
public enum ScanLevel
{
    Level_2(0),
    Level_3(1),
    Unknown(-1);

    public final int value;

    ScanLevel(int value)
    {
        this.value = value;
    }

    public static ScanLevel getScanLevel(int value)
    {
        for (ScanLevel level : ScanLevel.values()) {
            if (level.value == value) {
                return level;
            }
        }

        return Unknown;
    }
}
