package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 11/7/15.
 */
public enum Mineral
{
    Iron(0),
    Gold(1),
    Unknown(-1);

    public final int value;

    Mineral(int value) {
        this.value = value;
    }

    public static Mineral getMineral(int value)
    {
        for (Mineral type : Mineral.values()) {
            if (type.value == value) {
                return type;
            }
        }

        return Unknown;
    }

}
