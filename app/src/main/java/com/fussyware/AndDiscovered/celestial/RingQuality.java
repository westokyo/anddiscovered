package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 12/7/15.
 */
public enum RingQuality
{
    Depleted(0),
    Low(1),
    Common(2),
    Major(3),
    Pristine(4),
    Unknown(-1);

    public final int value;

    RingQuality(int value)
    {
        this.value = value;
    }

    public static RingQuality getRingQuality(int type)
    {
        for (RingQuality ring : RingQuality.values()) {
            if (ring.value == type) {
                return ring;
            }
        }

        return Unknown;
    }
}
