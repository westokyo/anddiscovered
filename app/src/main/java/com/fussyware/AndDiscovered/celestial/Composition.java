package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 12/8/15.
 */
public enum Composition
{
    Ice(0),
    Rock(1),
    Metal(2),
    Unknown(-1);

    public final int value;

    Composition(int value)
    {
        this.value = value;
    }

    public static Composition getComposition(int value)
    {
        for (Composition type : Composition.values()) {
            if (type.value == value) {
                return type;
            }
        }

        return Composition.Unknown;
    }
}
