package com.fussyware.AndDiscovered.celestial;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wboyd on 7/7/15.
 */
public abstract class DistanceInfo implements Parcelable
{
    private final String first;
    private String second;
    private double distance;

    protected final Object mutex = new Object();

    public DistanceInfo(String first, String second, double distance)
    {
        this.first = first;
        this.second = second;
        this.distance = (distance < 0) ? -distance : distance;
    }

    protected DistanceInfo(Parcel parcel)
    {
        first = parcel.readString();
        second = parcel.readString();
        distance = parcel.readDouble();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }

        if (o == null || !(o instanceof DistanceInfo)) {
            return false;
        }

        DistanceInfo that = (DistanceInfo) o;

        if (Double.compare(that.distance, distance) != 0) {
            return false;
        }
        return first.equals(that.first) &&
               !(second != null ? !second.equals(that.second) : that.second != null);

    }

    @Override
    public int hashCode()
    {
        int result;
        long temp;
        result = first.hashCode();
        result = 31 * result + (second != null ? second.hashCode() : 0);
        temp = Double.doubleToLongBits(distance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String getFirstSystem()
    {
        return first;
    }

    public String getSecondSystem()
    {
        synchronized (mutex) {
            return second;
        }
    }

    public void setSecondSystem(String system)
    {
        synchronized (mutex) {
            second = system;
        }
    }

    public double getDistance()
    {
        synchronized (mutex) {
            return distance;
        }
    }

    public void setDistance(double distance)
    {
        synchronized (mutex) {
            this.distance = (distance < 0) ? -distance : distance;
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "distance=" + distance +
                ", first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(first);
        dest.writeString(second);
        dest.writeDouble(distance);
    }
}
