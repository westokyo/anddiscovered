package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 11/7/15.
 */
public enum SatelliteCategory
{
    Star(0),
    Planet(1),
    Asteroid(2),
    Unknown(-1);

    public final int value;

    SatelliteCategory(int value)
    {
        this.value = value;
    }

    public static SatelliteCategory getSatelliteCategory(int value)
    {
        for (SatelliteCategory category : SatelliteCategory.values()) {
            if (value == category.value) {
                return category;
            }
        }

        return SatelliteCategory.Unknown;
    }
}
