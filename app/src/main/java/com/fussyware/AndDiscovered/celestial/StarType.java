package com.fussyware.AndDiscovered.celestial;

/**
 * Created by wes on 8/29/15.
 */
public enum StarType
{
    A(0, "A", 2400),
    AeBe(1, "AeBe", 2400),
    B(2, "B", 2400),
    C(3, "C", 2400),
    F(4, "F", 2400),
    G(5, "G", 2400),
    K(6, "K", 2400),
    L(7, "L", 2400),
    M(8, "M", 2400),
    O(9, "O", 2400),
    S(10, "S", 2400),
    T(11, "T", 2400),
    TT(12, "TT", 2400),
    W(13, "W", 3000),
    Y(14, "Y", 2400),
    White_Dwarf(15, "White Dwarf", 22600),
    Neutron(16, "Neutron", 36000),
    Black_Hole(17, "Black Hole", 38000),
    MS(18, "MS", 2400),
    Unknown(-1, "NA", -1);

    public final int value;
    public final int credits;

    // TODO: Get rid of name. The applications should use a R.string value for translation.
    public final String name;

    StarType(int value, String name, int credits)
    {
        this.value = value;
        this.credits = credits;
        this.name = name;
    }

    public static StarType getStarType(String value)
    {
        for (StarType t : StarType.values()) {
            if (t.name.equals(value)) {
                return t;
            }
        }

        return StarType.Unknown;
    }

    public static StarType getStarType(int value)
    {
        for (StarType t : StarType.values()) {
            if (t.value == value) {
                return t;
            }
        }

        return StarType.Unknown;
    }

    public int value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
