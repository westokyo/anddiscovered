package com.fussyware.AndDiscovered.edutils;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by wes on 6/25/15.
 */
public abstract class DebouncedTextWatcher implements TextWatcher
{
    private Timer timer = new Timer();
    private int debounceTimeMs;
    private DebounceTimerTask task = null;

    private final Object mutex = new Object();

    public DebouncedTextWatcher(int debounceTimeMs)
    {
        this.debounceTimeMs = debounceTimeMs;
    }

    public abstract void debouncedTextChanged(Editable s);

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        synchronized (mutex) {
            if (null != task) {
                task.cancel();
                timer.purge();
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s)
    {
        synchronized (mutex) {
            task = new DebounceTimerTask(s);
        }

        timer.schedule(task, debounceTimeMs);
    }

    private class DebounceTimerTask extends TimerTask
    {
        private Editable s;

        public DebounceTimerTask(Editable s)
        {
            this.s = s;
        }

        @Override
        public void run()
        {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                debouncedTextChanged(s);
            } else {
                new Handler(Looper.getMainLooper()).post(this);
            }
        }
    }
}
