package com.fussyware.AndDiscovered.edutils;

import android.view.View;
import android.widget.EditText;

/**
 * Created by wes on 8/9/15.
 */
public class SelectAllOnFocusChangeListener implements View.OnFocusChangeListener {
    private static final String LOG_NAME = SelectAllOnFocusChangeListener.class.getSimpleName();

    private String defaultText = "";

    public SelectAllOnFocusChangeListener()
    {
        this (null);
    }

    public SelectAllOnFocusChangeListener(String defaultText)
    {
        if (defaultText != null) {
            this.defaultText = defaultText;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus)
    {
        if (v instanceof EditText) {
            final EditText text = (EditText) v;
            final String editText = (text.getText() == null) ? "" : text.getText().toString();

            if (hasFocus && (defaultText.isEmpty() || defaultText.equals(editText))) {
                text.selectAll();
            }
        }
    }
}
