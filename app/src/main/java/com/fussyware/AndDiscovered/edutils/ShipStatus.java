package com.fussyware.AndDiscovered.edutils;

/**
 * Created by wes on 6/14/15.
 */
public enum ShipStatus
{
    Unknown("unknown"),
    Cruising("cruising"),
    Supercruise("Supercruise"),
    NormalFlight("NormalFlight"),
    ProvingGround("ProvingGround");

    private String value;

    ShipStatus(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value;
    }

    public static ShipStatus getShipStatus(String value)
    {
        for (ShipStatus item : ShipStatus.values()) {
            if (item.value.equals(value)) {
                return item;
            }
        }

        return Unknown;
    }
}
