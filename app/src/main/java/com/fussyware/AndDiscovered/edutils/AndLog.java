package com.fussyware.AndDiscovered.edutils;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

/**
 * Created by wes on 2/8/16.
 */
public final class AndLog
{
    public static int d(String tag, String msg)
    {
        Crashlytics.log(String.format("DEBUG> %s: %s", tag, msg));
        return Log.d(tag, msg);
    }

    public static int d(String tag, String msg, Throwable tr)
    {
        Crashlytics.log(String.format("DEBUG> %s: %s", tag, msg));
//        Crashlytics.logException(tr);
        return Log.d(tag, msg, tr);
    }

    public static int v(String tag, String msg)
    {
        Crashlytics.log(String.format("VERBOSE> %s: %s", tag, msg));
        return Log.v(tag, msg);
    }

    public static int v(String tag, String msg, Throwable tr)
    {
        Crashlytics.log(String.format("VERBOSE> %s: %s", tag, msg));
//        Crashlytics.logException(tr);
        return Log.v(tag, msg, tr);
    }

    public static int i(String tag, String msg)
    {
        Crashlytics.log(String.format("INFO> %s: %s", tag, msg));
        return Log.i(tag, msg);
    }

    public static int i(String tag, String msg, Throwable tr)
    {
        Crashlytics.log(String.format("INFO> %s: %s", tag, msg));
//        Crashlytics.logException(tr);
        return Log.i(tag, msg, tr);
    }

    public static int e(String tag, String msg)
    {
        Crashlytics.log(String.format("ERROR> %s: %s", tag, msg));
        return Log.e(tag, msg);
    }

    public static int e(String tag, String msg, Throwable tr)
    {
        Crashlytics.log(String.format("ERROR> %s: %s", tag, msg));
//        Crashlytics.logException(tr);
        return Log.e(tag, msg, tr);
    }

    public static int w(String tag, String msg)
    {
        Crashlytics.log(String.format("WARN> %s: %s", tag, msg));
        return Log.w(tag, msg);
    }

    public static int w(String tag, String msg, Throwable tr)
    {
        Crashlytics.log(String.format("WARN> %s: %s", tag, msg));
//        Crashlytics.logException(tr);
        return Log.w(tag, msg, tr);
    }
}
