package com.fussyware.AndDiscovered.eddiscovery;

import android.util.JsonWriter;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by wes on 6/14/15.
 */
public abstract class DiscoveryMessage
{
    protected DiscoveryType type;
    protected String serviceName;

    protected DiscoveryMessage(DiscoveryType type)
    {
        this(type, null);
    }

    protected DiscoveryMessage(DiscoveryType type, String serviceName)
    {
        this.type = type;
        this.serviceName = serviceName;
    }

    public DiscoveryType getType()
    {
        return type;
    }

    public String getServiceName()
    {
        return serviceName;
    }

    protected abstract void fillJSON(JsonWriter writer) throws IOException;

    public String getJSON()
    {
        try {
            StringWriter sw = new StringWriter();
            JsonWriter writer = new JsonWriter(sw);

            writer.beginObject();
            writer.name("type").value(type.toString());

            if (null != serviceName) {
                writer.name("name").value(serviceName);
            }

            fillJSON(writer);
            writer.endObject();
            writer.flush();

            String ret = sw.toString();
            writer.close();

            return ret;
        } catch (IOException e) {
            return "";
        }
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("Type [");
        sb.append(type.toString());
        sb.append("]");

        if (null != serviceName) {
            sb.append(", Name [");
            sb.append(serviceName);
            sb.append("]");
        }

        return sb.toString();
    }
}
