package com.fussyware.AndDiscovered.eddiscovery;

import android.util.JsonReader;
import android.util.JsonWriter;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by wes on 6/14/15.
 */
public class AnnounceMessage extends DiscoveryMessage
{
    private String hostname;
    private int port;
    private int httpPort;

    public AnnounceMessage(String serviceName, String hostname, int port, int httpPort)
    {
        super(DiscoveryType.Announce, serviceName);

        this.hostname = hostname;
        this.port = port;
        this.httpPort = httpPort;
    }

    static DiscoveryMessage getMessage(String json)
    {
        try {
            StringReader sr = new StringReader(json);
            JsonReader reader = new JsonReader(sr);

            DiscoveryType _type = null;
            String _serviceName = null;
            String _ipv4 = null;
            int _port = -1;
            int _httpPort = -1;

            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();

                switch (name) {
                    case "type":
                        name = reader.nextString();

                        if (name.equals(DiscoveryType.Announce.toString())) {
                            _type = DiscoveryType.Announce;
                        }
                        break;
                    case "name":
                        _serviceName = reader.nextString();
                        break;
                    case "ipv4":
                        _ipv4 = reader.nextString();
                        break;
                    case "port":
                        _port = reader.nextInt();
                        break;
                    case "http_port":
                        _httpPort = reader.nextInt();
                        break;
                }
            }
            reader.endObject();
            reader.close();

            if ((null == _type) ||
                (null == _serviceName) ||
                (null == _ipv4) ||
                (_port == -1)) {
                return null;
            } else {
                return new AnnounceMessage(_serviceName, _ipv4, _port, _httpPort);
            }
        } catch (IOException e) {
            return null;
        }
    }

    public String getHostname()
    {
        return hostname;
    }

    public int getPort()
    {
        return port;
    }

    public int getHttpPort()
    {
        return httpPort;
    }

    @Override
    protected void fillJSON(JsonWriter writer) throws IOException
    {
        writer.name("ipv4").value(hostname);
        writer.name("port").value(port);
        writer.name("http_port").value(httpPort);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("AnnounceMessage: {");
        sb.append(super.toString());
        sb.append(", IPv4 [");
        sb.append(hostname);
        sb.append("], Port [");
        sb.append(port);
        sb.append("], HTTP Port [");
        sb.append(httpPort);
        sb.append("]}");

        return sb.toString();
    }
}
