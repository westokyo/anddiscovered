package com.fussyware.AndDiscovered.eddiscovery;

/**
 * Created by wes on 6/14/15.
 */
public interface DiscoveryListener
{
    void discoveryAnnounce(AnnounceMessage message);
    void discoveryQuery(QueryMessage message);
}
