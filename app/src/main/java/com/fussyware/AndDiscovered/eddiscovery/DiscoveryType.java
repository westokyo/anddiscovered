package com.fussyware.AndDiscovered.eddiscovery;

/**
 * Created by wes on 6/14/15.
 */
public enum DiscoveryType
{
    Query("Query"),
    Announce("Announce");

    private String value;

    DiscoveryType(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value;
    }
}
