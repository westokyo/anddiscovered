package com.fussyware.AndDiscovered.eddiscovery;

import com.fussyware.AndDiscovered.edutils.AndLog;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by wes on 6/14/15.
 */
public class DiscoveryService implements Runnable
{
    private static final String LOG_NAME = DiscoveryService.class.getSimpleName();

    private final String hostname;
    private final int port;
    private int ttl;

    private MulticastSocket socket;

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final ArrayList<DiscoveryListener> listeners = new ArrayList<>();

    public DiscoveryService(String hostname, int port)
    {
        this.hostname = hostname;
        this.port = port;
        this.ttl = 1;
    }

    public DiscoveryService(String hostname, int port, int ttl)
    {
        this.hostname = hostname;
        this.port = port;
        this.ttl = ttl;
    }

    public void addListener(DiscoveryListener listener)
    {
        if (null == listener) {
            throw new InvalidParameterException("No valid listener specified.");
        }

        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public boolean isRunning()
    {
        return running.get();
    }

    public void setTtl(int ttl)
    {
        this.ttl = ttl;
    }

    public void send(DiscoveryMessage message) throws IOException
    {
        if (running.get()) {
            try {
                byte[] b = message.getJSON().getBytes();
                DatagramPacket pack = new DatagramPacket(b,
                                                         b.length,
                                                         InetAddress.getByName(hostname),
                                                         port);

                socket.send(pack);
            } catch (InterruptedIOException ignored) {
            }
        } else {
            throw new IOException("Discovery service is not running.");
        }
    }

    public void start() throws IOException
    {
        if (running.compareAndSet(false, true)) {
            socket = new MulticastSocket(port);
            socket.setTimeToLive(ttl);
            socket.setReuseAddress(true);
            socket.setSoTimeout(500);
            socket.joinGroup(InetAddress.getByName(hostname));

            new Thread(this).start();
        }
    }

    public void stop()
    {
        if (running.compareAndSet(true, false)) {
            socket.close();
            socket = null;
        }
    }

    private void post(DiscoveryMessage message)
    {
        synchronized (listeners) {
            for (DiscoveryListener listener : listeners) {
                if (message instanceof AnnounceMessage) {
                    listener.discoveryAnnounce((AnnounceMessage) message);
                } else if (message instanceof  QueryMessage) {
                    listener.discoveryQuery((QueryMessage) message);
                }
            }
        }
    }

    @Override
    public void run()
    {
        try {
            while (isRunning()) {
                try {
                    DatagramPacket pack = new DatagramPacket(new byte[1024],
                                                             1024,
                                                             new InetSocketAddress(hostname, port));

                    socket.receive(pack);

                    if (pack.getLength() > 0) {
                        DiscoveryMessage msg = DiscoveryMessageFactory.getMessage(new String(pack.getData()));
                        post(msg);
                    } else {
                        stop();
                    }
                } catch (InterruptedIOException ignored) {
                }
            }
        } catch (Exception e) {
            AndLog.d("DiscoveryService", "Failed with IOException: " + e.getMessage());
        }

        stop();
    }
}
