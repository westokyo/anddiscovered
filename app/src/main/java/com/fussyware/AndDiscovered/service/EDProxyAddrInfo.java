package com.fussyware.AndDiscovered.service;

/**
 * Created by wes on 5/29/16.
 */
public class EDProxyAddrInfo
{
    public final String hostname;
    public final int port;
    public final int httpPort;

    EDProxyAddrInfo(String hostname, int port, int httpPort)
    {
        this.hostname = hostname;
        this.port = port;
        this.httpPort = httpPort;
    }

    @Override
    public String toString()
    {
        return "EDProxyAddrInfo{" +
               "hostname='" + hostname +
               ", port=" + port +
               ", httpPort=" + httpPort +
               "}";
    }
}
