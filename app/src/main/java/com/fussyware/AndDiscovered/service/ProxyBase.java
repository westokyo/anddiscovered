package com.fussyware.AndDiscovered.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.eddiscovery.AnnounceMessage;
import com.fussyware.AndDiscovered.eddiscovery.DiscoveryListener;
import com.fussyware.AndDiscovered.eddiscovery.DiscoveryService;
import com.fussyware.AndDiscovered.eddiscovery.QueryMessage;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.preference.PreferenceTag;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by wes on 5/29/16.
 */
public abstract class ProxyBase extends Service
{
    private static final String TAG = ProxyBase.class.getSimpleName();

    public static final String BROADCAST_DISCOVERY_STARTED = "EDProxyService.DiscoveryStarted";
    public static final String BROADCAST_DISCOVERY_SUCCESS = "EDProxyService.DiscoverySuccess";
    public static final String BROADCAST_DISCOVERY_FAILED = "EDProxyService.DiscoveryFailed";
    public static final String BROADCAST_CONNECTED = "EDProxyService.Connected";
    public static final String BROADCAST_DISCONNECTED = "EDProxyService.Disconnected";
    public static final String BROADCAST_NEW_SYSTEM = "EDProxyService.NewSystem";
    public static final String BROADCAST_NEW_DISTANCE = "EDProxyService.NewDistance";
    public static final String BROADCAST_IMAGE_EVENT = "EDProxyService.ImageEvent";
    public static final String BROADCAST_SYSTEM_UPDATED = "EDProxyService.SystemUpdated";

    public static final String EXTRA_HOSTNAME = "Hostname";
    public static final String EXTRA_PORT = "Port";
    public static final String EXTRA_IMAGE_URL = "ImageUrl";
    public static final String EXTRA_IMAGE_INFO = "image_info";
    public static final String EXTRA_IMAGE_PATH = "ImagePath";

    static final int HEARTBEAT_TIMEOUT = 30;
    static final int[] backoff = { 0, 0, 2000, 5000, 10000, 30000, 60000, 120000 };

    protected CmdrDbHelper dbHelper;

    protected LocalBroadcastManager broadcastManager;
    protected String lastStateBroadcast = ProxyBase.BROADCAST_DISCONNECTED;

    protected SharedPreferences preferences;

    private final EDProxyBinder binder = new EDProxyBinder(this);

    private ProxyRunnable proxyRunner;

    @Override
    public void onCreate()
    {
        super.onCreate();

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        dbHelper = CmdrDbHelper.getInstance();

        proxyRunner = getProxyRunner();
        proxyRunner.start();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        proxyRunner.shutdown();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return binder;
    }

    public void resendState()
    {
        Intent intent = new Intent(lastStateBroadcast);

        if (lastStateBroadcast.equals(ProxyBase.BROADCAST_DISCOVERY_SUCCESS) ||
            lastStateBroadcast.equals(ProxyBase.BROADCAST_CONNECTED)) {
            EDProxyAddrInfo addrInfo = proxyRunner.getAddrInfo();

            intent.putExtra(ProxyBase.EXTRA_HOSTNAME, addrInfo.hostname);
            intent.putExtra(ProxyBase.EXTRA_PORT, addrInfo.port);
        }

        broadcastManager.sendBroadcast(intent);
    }

    public void sendKeys(String keys) throws IOException
    {
        proxyRunner.sendKeys(keys);
    }

    protected boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return (activeNetworkInfo != null &&
                activeNetworkInfo.isConnected() &&
                ((activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) ||
                 (activeNetworkInfo.getType() == ConnectivityManager.TYPE_ETHERNET)));
    }

    protected abstract ProxyRunnable getProxyRunner();

    protected double computeDistance(Position p0, Position p1)
    {
        double x = p1.x - p0.x;
        double y = p1.y - p0.y;
        double z = p1.z - p0.z;

        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    protected void sendDiscoveryStartedIntent()
    {
        broadcastManager.sendBroadcast(new Intent(ProxyBase.BROADCAST_DISCOVERY_STARTED));
        lastStateBroadcast = ProxyBase.BROADCAST_DISCOVERY_STARTED;
    }

    protected void sendHostIntent(String action, String hostname, int port)
    {
        Intent intent = new Intent(action);
        intent.putExtra(ProxyBase.EXTRA_HOSTNAME, hostname);
        intent.putExtra(ProxyBase.EXTRA_PORT, port);

        broadcastManager.sendBroadcast(intent);

        lastStateBroadcast = action;
    }

    protected void sendDiscoveryFailedIntent()
    {
        broadcastManager.sendBroadcast(new Intent(ProxyBase.BROADCAST_DISCOVERY_FAILED));
        lastStateBroadcast = ProxyBase.BROADCAST_DISCOVERY_FAILED;
    }

    protected void sendClientStateChanged(String action)
    {
        broadcastManager.sendBroadcast(new Intent(action));
        lastStateBroadcast = action;
    }

    protected void sendNewSystem(CmdrSystemInfo system)
    {
        Intent intent = new Intent(ProxyBase.BROADCAST_NEW_SYSTEM);
        intent.putExtra("system_name", system.getSystem());
        intent.putExtra("system", system);

        broadcastManager.sendBroadcast(intent);
    }

    protected void sendNewDistance(DistanceInfo distance)
    {
        Intent intent = new Intent(ProxyBase.BROADCAST_NEW_DISTANCE);
        intent.putExtra("distance_info", distance);

        broadcastManager.sendBroadcast(intent);
    }

    protected void sendSystemUpdated()
    {
        Intent intent = new Intent(ProxyBase.BROADCAST_SYSTEM_UPDATED);

        broadcastManager.sendBroadcast(intent);
    }

    public static class EDProxyBinder extends Binder
    {
        private final ProxyBase base;

        EDProxyBinder(ProxyBase base)
        {
            this.base = base;
        }

        public ProxyBase getService()
        {
            return base;
        }
    }

    abstract class ProxyRunnable implements Runnable
    {
        protected final AtomicBoolean running = new AtomicBoolean(false);

        protected DiscoveryService edDiscovery;
        protected EDProxyAddrInfo addrInfo;
        protected final Object discoveryCond = new Object();

        private Thread handlerThread;

        ProxyRunnable()
        {
            edDiscovery = new DiscoveryService("239.45.99.98", 45551);
            edDiscovery.addListener(new DiscoveryListener()
            {
                @Override
                public void discoveryAnnounce(AnnounceMessage message)
                {
                    AndLog.d(TAG, "Discovery got message: " + message);
                    if (message.getServiceName().equals("edproxy")) {
                        synchronized (discoveryCond) {
                            addrInfo = new EDProxyAddrInfo(message.getHostname(),
                                                           message.getPort(),
                                                           message.getHttpPort());
                            discoveryCond.notify();
                        }
                    }
                }

                @Override
                public void discoveryQuery(QueryMessage message)
                {

                }
            });
        }

        protected abstract String getName();
        protected abstract int getDefaultPort();

        abstract void sendKeys(String keys) throws IOException;

        protected EDProxyAddrInfo getAddrInfo()
        {
            return addrInfo;
        }

        public boolean isRunning()
        {
            return running.get();
        }

        public void start()
        {
            if (running.compareAndSet(false, true)) {
                int ttl;

                try {
                    ttl = Integer.valueOf(preferences.getString(PreferenceTag.DISCOVERY_TTL, "1"));
                } catch (Exception e) {
                    String __tmp = preferences.getString(PreferenceTag.DISCOVERY_TTL, "1");
                    AndLog.e(TAG, "Failed to convert TTL from preferences. [" + __tmp + "]", e);
                    ttl = 1;
                }

                edDiscovery.setTtl(ttl);

                handlerThread = new Thread(this, getName());
                handlerThread.start();
            }
        }

        protected abstract void handleShutdown();

        public void shutdown()
        {
            if (running.compareAndSet(true, false)) {
                synchronized (discoveryCond) {
                    discoveryCond.notifyAll();
                }

                handleShutdown();

                try {
                    handlerThread.join();
                } catch (InterruptedException ignored) {

                }
            }
        }

        protected void discover()
        {
            boolean found = false;

            addrInfo = null;

            while (running.get() && isNetworkAvailable() && !found) {
                if (addrInfo == null) {
                    synchronized (discoveryCond) {
                        int tryCount = 0;

                        WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                        WifiManager.MulticastLock multicastLock = wifi.createMulticastLock("EDProxyDiscovery");

                        sendDiscoveryStartedIntent();
                        multicastLock.acquire();

                        try {
                            edDiscovery.start();

                            while (running.get() &&
                                   edDiscovery.isRunning() &&
                                   (addrInfo == null) &&
                                   (tryCount++ < 30)) {
                                try {
                                    edDiscovery.send(new QueryMessage("edproxy"));
                                    discoveryCond.wait(1000);
                                } catch (InterruptedException | IOException ignored) {
                                }
                            }
                        } catch (IOException ignored) {
                            addrInfo = null;
                        }

                        edDiscovery.stop();
                        multicastLock.release();
                    }
                }

                if ((addrInfo == null) ||
                    (addrInfo.hostname == null) ||
                    (addrInfo.hostname.isEmpty()) ||
                    (addrInfo.port == -1)) {
                    addrInfo = null;
                    sendDiscoveryFailedIntent();

                    if (preferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
                        synchronized (discoveryCond) {
                            try { discoveryCond.wait(10000); }
                            catch (InterruptedException ignored) {}
                        }
                    } else {
                        found = true;
                        addrInfo = new EDProxyAddrInfo(preferences.getString(PreferenceTag.IPADDR,
                                                                             ""),
                                                       getDefaultPort(),
                                                       getDefaultPort());
                    }
                } else {
                    found = true;
                    sendHostIntent(ProxyBase.BROADCAST_DISCOVERY_SUCCESS,
                                   addrInfo.hostname,
                                   addrInfo.port);
                }
            }
        }

    }
}
