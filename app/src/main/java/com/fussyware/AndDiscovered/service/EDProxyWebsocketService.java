package com.fussyware.AndDiscovered.service;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbContract;
import com.fussyware.AndDiscovered.eddatabase.CmdrDistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.eddatabase.SystemImageInfo;
import com.fussyware.AndDiscovered.edproxy.GetDistancesMessage;
import com.fussyware.AndDiscovered.edproxy.GetDistancesResultMessage;
import com.fussyware.AndDiscovered.edproxy.ImageMessage;
import com.fussyware.AndDiscovered.edproxy.InitMessage;
import com.fussyware.AndDiscovered.edproxy.ProxyMessage;
import com.fussyware.AndDiscovered.edproxy.ProxyMessageFactory;
import com.fussyware.AndDiscovered.edproxy.ProxyMessageType;
import com.fussyware.AndDiscovered.edproxy.SendKeysMessage;
import com.fussyware.AndDiscovered.edproxy.StarMapUpdatedMessage;
import com.fussyware.AndDiscovered.edproxy.SystemMessage;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.edutils.ShipStatus;
import com.fussyware.AndDiscovered.preference.PreferenceTag;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by wes on 3/8/16.
 */
public class EDProxyWebsocketService extends ProxyBase
{
    private static final String TAG = EDProxyWebsocketService.class.getSimpleName();

    private static final String WEBSOCKET_URL_FORMAT = "ws://%s:%d/v1";

    private static final int DEFAULT_WEBSOCKET_CONNECT_TIMEOUT = 500;
    private static final int DEFAULT_EDPROXY_HTTP_PORT = 8097;

    private final ProxyHandler proxyHandler = new ProxyHandler();

    @Override
    protected ProxyRunnable getProxyRunner()
    {
        return proxyHandler;
    }

    private class ProxyHandler extends ProxyRunnable
    {
        private final WebSocketFactory websocketFactory;
        private WebSocket webSocket;

        private final Lock connectionStateLock = new ReentrantLock();
        private final Condition disconnectCondition = connectionStateLock.newCondition();

        private boolean websocketConnected;

        ProxyHandler()
        {
            websocketFactory = new WebSocketFactory().setConnectionTimeout(DEFAULT_WEBSOCKET_CONNECT_TIMEOUT);
        }

        @Override
        protected String getName()
        {
            return TAG;
        }

        @Override
        protected int getDefaultPort()
        {
            return DEFAULT_EDPROXY_HTTP_PORT;
        }

        @Override
        protected void handleShutdown()
        {
            try {
                connectionStateLock.lock();
                if (websocketConnected) {
                    /* This should send the disconnected event
                     * to the websocket handler.
                     */
                    webSocket.disconnect();
                }
            } finally {
                connectionStateLock.unlock();
            }

        }

        @Override
        void sendKeys(String keys) throws IOException
        {
            SendKeysMessage message = new SendKeysMessage(keys);

            try {
                connectionStateLock.lock();
                if (websocketConnected) {
                    webSocket.sendText(message.getJSON());
                }
            } finally {
                connectionStateLock.unlock();
            }
        }

        private void sendProxyInit()
        {
            ArrayList<ProxyMessageType> list = new ArrayList<>();
            list.add(ProxyMessageType.System);
            list.add(ProxyMessageType.Image);

            InitMessage message = new InitMessage(new Date(),
                                                  ProxyBase.HEARTBEAT_TIMEOUT,
                                                  dbHelper.getLastProxyMessage(),
                                                  list);

            webSocket.sendText(message.getJSON());
        }

        @Override
        public void run()
        {
            int backoff_pos = 0;

            while (isRunning()) {
                if (!isNetworkAvailable()) {
                    synchronized (discoveryCond) {
                        try {
                            discoveryCond.wait(10000);
                        } catch (InterruptedException ignored) {
                        }
                    }

                    continue;
                }

                if (preferences.getBoolean(PreferenceTag.DISCOVERY_ENABLED, true)) {
                    discover();
                } else if (!preferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
                    addrInfo = new EDProxyAddrInfo(preferences.getString(PreferenceTag.IPADDR, ""),
                                                   DEFAULT_EDPROXY_HTTP_PORT,
                                                   DEFAULT_EDPROXY_HTTP_PORT);
                } else {
                    // Bail out. We will be started later once someone puts in an IP.
                    return;
                }

                if (isRunning() && (addrInfo != null)) {
                    try {
                        AndLog.d(TAG, "backoff pos: " + backoff_pos + ", time: " + backoff[backoff_pos]);
                        Thread.sleep(backoff[backoff_pos]);

                        backoff_pos++;
                        if (backoff_pos == ProxyBase.backoff.length) {
                            backoff_pos = ProxyBase.backoff.length - 1;
                        }
                    } catch (InterruptedException ignored) {
                        AndLog.e(TAG, "Received interrupted error", ignored);
                    }

                    try {
                        String __tmp = String.format(Locale.US,
                                                     WEBSOCKET_URL_FORMAT,
                                                     addrInfo.hostname,
                                                     addrInfo.httpPort);

                        websocketConnected = false;

                        webSocket = websocketFactory.createSocket(__tmp);
                        webSocket.addListener(new WebsocketHandler());
                        webSocket.setPingInterval(HEARTBEAT_TIMEOUT * 1000);

                        try {
                            connectionStateLock.lock();
                            websocketConnected = true;
                            webSocket.connect();
                        } catch (WebSocketException e) {
                            websocketConnected = false;
                            throw e;
                        } finally {
                            connectionStateLock.unlock();
                        }

                        AndLog.d(TAG, "reset backoff pos");
                        backoff_pos = 0;

                        sendProxyInit();
                        sendHostIntent(ProxyBase.BROADCAST_CONNECTED,
                                       addrInfo.hostname,
                                       addrInfo.httpPort);

                        while (isRunning() && websocketConnected) {
                            try {
                                connectionStateLock.lock();
                                try {
                                    disconnectCondition.await();
                                } catch (InterruptedException ignored) {
                                }
                            } finally {
                                connectionStateLock.unlock();
                            }
                        }
                    } catch (WebSocketException | IOException e) {
                        AndLog.e(TAG, "Error with the websocket", e);
                    } finally {
                        AndLog.d(TAG, "Event out disconnect.");
                        addrInfo = null;
                        sendClientStateChanged(ProxyBase.BROADCAST_DISCONNECTED);
                    }
                } else {
                    sendDiscoveryFailedIntent();
                }
            }
        }

        private class WebsocketHandler extends WebSocketAdapter
        {
            private Timer heartbeatTimer = new Timer();

            @Override
            public void onDisconnected(WebSocket websocket,
                                       WebSocketFrame serverCloseFrame,
                                       WebSocketFrame clientCloseFrame,
                                       boolean closedByServer) throws Exception
            {
                try {
                    AndLog.d(TAG, "Websocket was disconnected: " + closedByServer);
                    connectionStateLock.lock();
                    heartbeatTimer.cancel();
                    websocketConnected = false;
                    disconnectCondition.signal();
                } finally {
                    connectionStateLock.unlock();
                }
            }

            @Override
            public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception
            {
                heartbeatTimer.cancel();
                heartbeatTimer.schedule(new HeartbeatTimerTask(), HEARTBEAT_TIMEOUT * 2);
            }

            @Override
            public void onTextMessage(WebSocket websocket, String text) throws Exception
            {
                AndLog.d(TAG, "Received new message: " + text);

                try {
                    ProxyMessage message = ProxyMessageFactory.getMessage(new JSONObject(text));

                    if (null != message) {
                        switch (message.getType()) {
                            case Init:
                                break;
                            case System:
                                handleSystemMessage((SystemMessage) message);
                                break;
                            case Image:
                                handleImageMessage((ImageMessage) message);
                                break;
                            case Import:
                                break;
                            case StarMapUpdated:
                                handleStarMapUpdatedMessage((StarMapUpdatedMessage) message);
                                break;
                            case GetDistancesResult:
                                handleDistanceResponseMessage((GetDistancesResultMessage) message);
                                break;
                            default:
                                break;
                        }
                    }
                } catch (JSONException e) {
                    AndLog.e(TAG, "Failed parsing the websocket text frame as a JSON object.", e);
                }
            }

            @Override
            public void onTextMessageError(WebSocket websocket,
                                           WebSocketException cause,
                                           byte[] data) throws Exception
            {
                AndLog.e(TAG, "An error occurred parsing a text frame from the server.", cause);
            }

            @Override
            public void onSendError(WebSocket websocket,
                                    WebSocketException cause,
                                    WebSocketFrame frame) throws Exception
            {
                AndLog.e(TAG, "Error sending frame to the server.", cause);
            }

            @Override
            public void onUnexpectedError(WebSocket websocket, WebSocketException cause) throws
                                                                                         Exception
            {
                AndLog.e(TAG, "An unexpected websocket error has occurred.", cause);
            }

            private void handleSystemMessage(SystemMessage message)
            {

                /* Block out all CQC systems from making it into the list. */
                if (!message.getSystem().equals("Training") &&
                    (message.getShipStatus() != ShipStatus.ProvingGround)) {
                    CmdrSystemInfo systemInfo = dbHelper.getSystem(message.getSystem());
                    if (systemInfo == null) {
                        systemInfo = dbHelper.createSystem(message.getSystem(),
                                                           message.getSystemPosition());

                        sendNewSystem(systemInfo);
                    } else if ((message.getSystemPosition() != null) &&
                               (systemInfo.getPosition() != message.getSystemPosition())) {
                        systemInfo.setPosition(message.getSystemPosition());
                    }

                    CmdrDistanceInfo distanceInfo = dbHelper.getLastDistance();
                    if (distanceInfo == null) {
                        dbHelper.createDistance(message.getSystem(), message.getDate());
                    } else if (!distanceInfo.getFirstSystem()
                                            .equalsIgnoreCase(message.getSystem()) &&
                               (distanceInfo.getSecondSystem() == null)) {
                        SQLiteDatabase db = dbHelper.getWritableDatabase();

                        try {
                            double distance = -1;

                            /* If we have coordinates from both systems then we can just
                             * compute the distance.
                             */
                            if (message.getSystemPosition() != null) {
                                SystemInfo si = dbHelper.getSystem(distanceInfo.getFirstSystem());

                                if ((si != null) && (si.getPosition() != null)) {
                                    distance = computeDistance(si.getPosition(),
                                                               message.getSystemPosition());
                                }
                            }

                            /* We could not find the distance in EDSM. Look to see if we have in
                             * our own database.
                             */
                            if (distance == -1) {
                                DistanceInfo di = dbHelper.getDistance(distanceInfo.getFirstSystem(),
                                                                       message.getSystem());
                                if ((di != null) && (di.getDistance() > 0.0)) {
                                    distance = di.getDistance();
                                }
                            }

                            if (distance == -1) {
                                for (SystemMessage.SystemDistance dist : message.getDistances()) {
                                    if (distanceInfo.getFirstSystem().equalsIgnoreCase(dist.name) &&
                                        dist.distance != 0.0) {
                                        distance = dist.distance;
                                        break;
                                    }
                                }
                            }

                            db.beginTransaction();

                            distanceInfo.setSecondSystem(message.getSystem());

                            if (distance != -1) {
                                distanceInfo.setDistance(distance);
                                distanceInfo.setRemoteUpdated(true);
                            }

                            db.setTransactionSuccessful();
                        } finally {
                            db.endTransaction();
                        }

                        sendNewDistance(dbHelper.createDistance(message.getSystem(),
                                                                message.getDate()));
                    }
                }

                dbHelper.setLastProxyMessage(new Date(message.getDate().getTime() + 1000));
            }

            private void handleImageMessage(ImageMessage message)
            {
                AndLog.d(TAG, "Handle image message: " + message);
                if (preferences.getBoolean(PreferenceTag.ALLOW_IMAGES, false)) {
                    Intent intent = new Intent(ProxyBase.BROADCAST_IMAGE_EVENT);

                    try {
                        String path = message.getImageUrl().getPath();
                        path = URLDecoder.decode(path, "UTF-8");

                        String filename = new File(path).getName();
                        filename = filename.substring(0, "yyyy-mm-dd_hh-mm-ss".length());

                        AndLog.d(TAG, "system name: " + filename);

                        CmdrSystemInfo info = dbHelper.getCurrentSystem();
                        SystemImageInfo imageInfo = info.createImageInfo(path);
                        intent.putExtra("image_info", imageInfo);
                    } catch (UnsupportedEncodingException e) {
                        AndLog.e(TAG, "Failed decoding the URL string name.", e);
                    }

                    intent.putExtra(ProxyBase.EXTRA_IMAGE_URL, message.getImageUrl().toString());

                    broadcastManager.sendBroadcast(intent);
                }
                AndLog.d(TAG, "Done with image.");
            }

            private void handleStarMapUpdatedMessage(StarMapUpdatedMessage message)
            {
                GetDistancesMessage distancesMessage = new GetDistancesMessage();

                String[] columns = {
                        CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM,
                        CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                        };
                String selection = CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE + "=?";
                String[] selectionArgs = { "0.0" };

                Cursor cursor = dbHelper.getReadableDatabase().query(CmdrDbContract.CmdrDistances.TABLE_NAME,
                                                                     columns,
                                                                     selection,
                                                                     selectionArgs,
                                                                     CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM +
                                                                     "," +
                                                                     CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                                                                     null,
                                                                     null);

                try {
                    while (cursor.moveToNext()) {
                        String _first = (cursor.isNull(0) ? "" : cursor.getString(0));
                        String _second = (cursor.isNull(1) ? "" : cursor.getString(1));

                        if (!_first.isEmpty() && !_second.isEmpty()) {
                            distancesMessage.add(new GetDistancesMessage.DistanceRequest(_first,
                                                                                         _second));

                            /* Limit the number of distances to 50 per request.
                             * This is to alleviate Edproxy and the responsiveness of the app.
                             */
                            if (distancesMessage.size() >= 50) {
                                webSocket.sendText(distancesMessage.getJSON());

                                distancesMessage = new GetDistancesMessage();
                            }
                        }
                    }

                    if (distancesMessage.size() > 0) {
                        webSocket.sendText(distancesMessage.getJSON());
                    }
                } finally {
                    cursor.close();
                }
            }

            private void handleDistanceResponseMessage(GetDistancesResultMessage message)
            {
                List<GetDistancesResultMessage.DistanceResponse> list = message.getDistances();
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                AndLog.d(TAG, "Distance response: " + message.toString());

                for (GetDistancesResultMessage.DistanceResponse response : list) {
                    String selection = CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM + " IN (?,?) AND " +
                                       CmdrDbContract.CmdrDistances.COLUMN_NAME_TO + " IN (?,?)";
                    String[] selectionArgs = { response.first, response.second,
                                               response.first, response.second };

                    ContentValues values = new ContentValues();
                    values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE, response.distance);
                    values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED, true);

                    db.update(CmdrDbContract.CmdrDistances.TABLE_NAME,
                              values,
                              selection,
                              selectionArgs);
                }

                if (list.size() > 0) {
                    sendSystemUpdated();
                }
            }

            private class HeartbeatTimerTask extends TimerTask
            {

                @Override
                public void run()
                {
                    try {
                        connectionStateLock.lock();
                        AndLog.e(TAG, "A ping/pong was missed! Disconnect the websocket.");
                        webSocket.disconnect();
                    } finally {
                        connectionStateLock.unlock();
                    }
                }
            }
        }
    }
}
