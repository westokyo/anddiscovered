package com.fussyware.AndDiscovered.edproxy;

import android.support.annotation.NonNull;
import android.util.JsonWriter;

import java.io.IOException;
import java.util.Date;

/**
 * Created by wes on 12/13/15.
 */
public class SendKeysMessage extends ProxyMessage
{
    private final String keys;

    public SendKeysMessage(@NonNull String keys)
    {
        super(ProxyMessageType.SendKeys, new Date());
        this.keys = keys;
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {
        json.name("Keys").value(keys);
    }
}
