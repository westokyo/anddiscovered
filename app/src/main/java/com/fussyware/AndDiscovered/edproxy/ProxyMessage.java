package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by wes on 6/14/15.
 */
public abstract class ProxyMessage
{
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT,
                                                                               Locale.US);

    private static final String TAG = ProxyMessage.class.getSimpleName();

    protected ProxyMessageType type;
    protected Date date;

    protected ProxyMessage(ProxyMessageType type, Date date)
    {
        this.type = type;
        this.date = date;
    }

    public ProxyMessageType getType()
    {
        return type;
    }

    public Date getDate()
    {
        return date;
    }

    protected abstract void fillJSON(JsonWriter json) throws IOException;

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("Type [");
        sb.append(type.toString());
        sb.append("], Date [");
        sb.append(DATE_FORMATTER.format(date));
        sb.append("]");

        return sb.toString();
    }

    private void processJSON(JsonWriter json) throws IOException
    {
        json.beginObject();
        json.name("Type").value(type.toString());
        json.name("Date").value(DATE_FORMATTER.format(date));

        fillJSON(json);

        json.endObject();
        json.flush();
    }

    public String getJSON()
    {
        JsonWriter json = null;

        try {
            StringWriter sw = new StringWriter();

            json = new JsonWriter(sw);
            processJSON(json);

            return sw.toString();
        } catch (IOException e) {
            return "";
        } finally {
            if (null != json) {
                try {
                    json.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void writeJSON(JsonWriter json) throws IOException
    {
        processJSON(json);
    }
}
