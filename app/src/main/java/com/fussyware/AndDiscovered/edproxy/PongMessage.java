package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

/**
 * Created by wes on 10/3/15.
 */
public class PongMessage extends ProxyMessage
{
    public PongMessage()
    {
        super(ProxyMessageType.Pong, new Date());
    }

    private PongMessage(Date date)
    {
        super(ProxyMessageType.Pong, date);
    }

    static PongMessage getMessage(ProxyMessageType messageType, Date date, JSONObject object)
    {
        return (messageType == ProxyMessageType.Pong) ? new PongMessage(date) : null;
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {

    }
}
