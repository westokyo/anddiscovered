package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wes on 12/13/15.
 */
public class GetDistancesResultMessage extends ProxyMessage
{
    private static final String TAG = GetDistancesResultMessage.class.getSimpleName();

    private final ArrayList<DistanceResponse> distances = new ArrayList<>();

    public GetDistancesResultMessage()
    {
        super(ProxyMessageType.GetDistancesResult, new Date());
    }

    GetDistancesResultMessage(Date date, JSONObject object) throws JSONException
    {
        super(ProxyMessageType.GetDistancesResult, date);

        if (object.has("Distances")) {
            JSONArray jarray = object.getJSONArray("Distances");

            for (int i = 0; i < jarray.length(); i++) {
                JSONObject o = jarray.getJSONObject(i);

                distances.add(new DistanceResponse(o.getString("sys1"),
                                                   o.getString("sys2"),
                                                   o.getDouble("distance")));
            }
        }
    }

    public List<DistanceResponse> getDistances()
    {
        return distances;
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {
        if (distances.size() > 0) {
            json.name("Distances");
            json.beginArray();
            for (DistanceResponse item : distances) {
                json.beginObject();
                json.name("sys1").value(item.first);
                json.name("sys2").value(item.second);
                json.name("distance").value(item.distance);
                json.endObject();
            }
            json.endArray();
        }
    }

    @Override
    public String toString()
    {
        return "GetDistancesResultMessage{" +
               "distances=" + distances +
               '}';
    }

    static GetDistancesResultMessage getMessage(ProxyMessageType messageType, Date date, JSONObject object)
    {
        if (messageType == ProxyMessageType.GetDistancesResult) {
            try {
                return new GetDistancesResultMessage(date, object);
            } catch (JSONException e) {
                Log.e(TAG, "Failed parsing JSON distance result message.", e);
            }
        }

        return null;
    }

    public static class DistanceResponse
    {
        public final String first;
        public final String second;
        public final double distance;

        DistanceResponse(String first, String second, double distance)
        {
            this.first = first;
            this.second = second;
            this.distance = distance;
        }

        @Override
        public String toString()
        {
            return "DistanceResponse{" +
                   "first='" + first + '\'' +
                   ", second='" + second + '\'' +
                   ", distance=" + distance +
                   '}';
        }
    }
}
