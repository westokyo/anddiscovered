package com.fussyware.AndDiscovered.edproxy;

import com.fussyware.AndDiscovered.edutils.AndLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by wes on 6/15/15.
 */
public class ProxyMessageFactory
{
    private static final String LOG_NAME = ProxyMessageFactory.class.getSimpleName();

    public static ProxyMessage getMessage(JSONObject object)
    {
        ProxyMessage value = null;

        try {
            ProxyMessageType messageType = ProxyMessageType.getMessageType(object.getString("Type"));
            Date date = getDate(object.getString("Date"));

            switch (messageType) {
                case Init:
                    break;
                case Import:
                    break;
                case Heartbeat:
                    break;
                case Ping:
                    break;
                case Pong:
                    value = PongMessage.getMessage(messageType, date, object);
                    break;
                case System:
                    value = SystemMessage.getMessage(messageType, date, object);
                    break;
                case Image:
                    value = ImageMessage.getMessage(messageType, date, object);
                    break;
                case SendKeys:
                    break;
                case StarMapUpdated:
                    value = StarMapUpdatedMessage.getMessage(messageType, date, object);
                    break;
                case GetDistancesResult:
                    value = GetDistancesResultMessage.getMessage(messageType, date, object);
                    break;
            }
        } catch (JSONException e) {
            AndLog.e("ProxyMessageFactory", "Failed to retrieve Type from JSON.");
        }

        return value;
    }

    private static Date getDate(String value)
    {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(value);
        } catch (ParseException e) {
            return new Date();
        }
    }
}
