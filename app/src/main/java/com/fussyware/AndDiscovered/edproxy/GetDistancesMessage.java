package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by wes on 12/13/15.
 */
public class GetDistancesMessage extends ProxyMessage
{
    private final HashSet<DistanceRequest> distances = new HashSet<>();

    public GetDistancesMessage()
    {
        super(ProxyMessageType.GetDistances, new Date());
    }

    public void add(DistanceRequest distance)
    {
        distances.add(distance);
    }

    public int size()
    {
        return distances.size();
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {
        if (distances.size() > 0) {
            json.name("Distances");
            json.beginArray();
            for (DistanceRequest item : distances) {
                json.beginObject();
                json.name("sys1").value(item.first);
                json.name("sys2").value(item.second);
                json.endObject();
            }
            json.endArray();
        }
    }

    public static class DistanceRequest
    {
        final String first;
        final String second;

        private final int hash;

        public DistanceRequest(String first, String second)
        {
            this.first = first;
            this.second = second;

            if (first.compareToIgnoreCase(second) <=0) {
                hash = (first + second).hashCode();
            } else {
                hash = (second + first).hashCode();
            }
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            DistanceRequest that = (DistanceRequest) o;

            return first.equalsIgnoreCase(that.first) && second.equalsIgnoreCase(that.second) ||
                   first.equalsIgnoreCase(that.second) && second.equalsIgnoreCase(that.first);
        }

        @Override
        public int hashCode()
        {
            return hash;
        }
    }
}
