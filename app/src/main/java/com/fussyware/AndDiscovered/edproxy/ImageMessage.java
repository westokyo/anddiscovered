package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Created by wes on 9/15/15.
 */
public class ImageMessage extends ProxyMessage
{
    private final URL imageUrl;

    public ImageMessage(Date date, URL imageUrl)
    {
        super(ProxyMessageType.Image, date);

        this.imageUrl = imageUrl;
    }

    static ImageMessage getMessage(ProxyMessageType messageType, Date date, JSONObject object)
    {
        if (messageType != ProxyMessageType.Image) {
            return null;
        }

        try {
            URL url = new URL(object.getString("ImageUrl"));
            return new ImageMessage(date, url);
        } catch (JSONException e) {
            return null;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public URL getImageUrl()
    {
        return imageUrl;
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {
        json.name("ImageUrl").value(imageUrl.toString());
    }
}
