package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;
import android.util.Log;

import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.edutils.ShipStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wes on 6/14/15.
 */
public class SystemMessage extends ProxyMessage
{
    private static final String TAG = SystemMessage.class.getSimpleName();

    private String system;
    private int bodies;
    private ShipStatus status;
    private Position position;
    private Position systemPosition;

    private final ArrayList<SystemDistance> distanceList = new ArrayList<>();

    private SystemMessage(Date date, JSONObject object) throws JSONException
    {
        super(ProxyMessageType.System, date);

        this.system = object.getString("System");
        this.bodies = object.getInt("Bodies");
        this.status = ShipStatus.getShipStatus(object.getString("Status"));
        this.position = getGenericPosition(object.getJSONArray("Position"));

        if (object.has("SystemCoord"))
        {
            this.systemPosition = getGenericPosition(object.getJSONArray("SystemCoord"));
        }

        if (object.has("Distances")) {
            JSONArray list = object.getJSONArray("Distances");

            for (int i = 0; i < list.length(); i++) {
                JSONObject o = list.getJSONObject(i);

                distanceList.add(new SystemDistance(o));
            }
        }
    }

    public SystemMessage(Date date, String system, int numBodies, ShipStatus status, Position position)
    {
        super(ProxyMessageType.System, date);

        this.system = system;
        this.bodies = numBodies;
        this.status = status;
        this.position = position;
    }

    private Position getGenericPosition(JSONArray array) throws JSONException
    {
        return new Position(array.getDouble(0),
                            array.getDouble(1),
                            array.getDouble(2));
    }

    public String getSystem()
    {
        return system;
    }

    public int getNumBodies()
    {
        return bodies;
    }

    public ShipStatus getShipStatus()
    {
        return status;
    }

    public Position getPosition()
    {
        return position;
    }

    public Position getSystemPosition()
    {
        return systemPosition;
    }

    public List<SystemDistance> getDistances()
    {
        return distanceList;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("SystemMessage {");
        sb.append(super.toString());
        sb.append(", System [");
        sb.append(system);
        sb.append("], Bodies [");
        sb.append(bodies);
        sb.append("], Position [");
        sb.append(position.toString());
        sb.append("], Ship Status [");
        sb.append(status.toString());
        sb.append("]}");

        return sb.toString();
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {
        json.name("System").value(system);
        json.name("Bodies").value(bodies);
        json.name("Status").value(status.toString());

        json.name("Position");
        json.beginArray();
        json.value(Double.toString(position.x));
        json.value(Double.toString(position.y));
        json.value(Double.toString(position.z));
        json.endArray();
    }

    static SystemMessage getMessage(ProxyMessageType messageType, Date date, JSONObject object)
    {
        if (messageType == ProxyMessageType.System) {
            try {
                return new SystemMessage(date, object);
            } catch (JSONException e) {
                Log.e(TAG, "Failed parsing JSON system message.", e);
            }
        }

        return null;
    }

    public static class SystemDistance
    {
        public final String name;
        public final Position coordinates;
        public final double distance;

        SystemDistance(JSONObject object) throws JSONException
        {
            name = object.getString("name");
            distance = object.getDouble("distance");

            if (object.has("coords")) {
                JSONArray list = object.getJSONArray("coords");

                double x = list.getDouble(0);
                double y = list.getDouble(1);
                double z = list.getDouble(2);

                coordinates = new Position(x, y, z);
            } else {
                coordinates = null;
            }
        }
    }
}
