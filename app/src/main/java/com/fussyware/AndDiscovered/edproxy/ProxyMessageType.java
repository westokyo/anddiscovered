package com.fussyware.AndDiscovered.edproxy;

import android.support.annotation.NonNull;

/**
 * Created by wes on 6/14/15.
 */
public enum ProxyMessageType
{
    Init("Init"),
    System("System"),
    Image("Image"),
    Import("Import"),
    Heartbeat("Heartbeat"),
    Ping("Ping"),
    Pong("Pong"),
    SendKeys("SendKeys"),
    StarMapUpdated("StarMapUpdated"),
    GetDistances("GetDistances"),
    GetDistancesResult("GetDistancesResult");

    private final String value;

    ProxyMessageType(String value)
    {
        this.value = value;
    }

    public String value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value;
    }

    public static ProxyMessageType getMessageType(@NonNull String value)
    {
        for (ProxyMessageType type : ProxyMessageType.values()) {
            if (type.toString().equals(value)) {
                return type;
            }
        }

        return ProxyMessageType.System;
    }
}
