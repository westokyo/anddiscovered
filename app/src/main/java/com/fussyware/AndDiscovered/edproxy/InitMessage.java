package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by wes on 6/15/15.
 */
public class InitMessage extends ProxyMessage
{
    private final Date startTime;
    private final List<ProxyMessageType> registerList;
    private final Integer heartbeat;

    public InitMessage(Date date, Date startTime, List<ProxyMessageType> registerList)
    {
        this(date, null, startTime, registerList);
    }
    public InitMessage(Date date, Integer heartbeatTimeout, Date startTime, List<ProxyMessageType> registerList)
    {
        super(ProxyMessageType.Init, date);

        this.heartbeat = heartbeatTimeout;
        this.startTime = startTime;
        this.registerList = registerList;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public List<ProxyMessageType> getRegisterList()
    {
        return registerList;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("InitMessage {");
        sb.append(super.toString());

        if (heartbeat != null) {
            sb.append(", Heartbeat [");
            sb.append(heartbeat);
            sb.append("]");
        }

        sb.append(", Start Time [");

        if (null == startTime) {
            sb.append("now");
        } else {
            sb.append(DATE_FORMATTER.format(startTime));
        }

        sb.append("], Register List [");
        for (ProxyMessageType type : registerList) {
            sb.append(type.toString());
            sb.append(", ");
        }
        sb.append("]}");

        return sb.toString();
    }

    @Override
    public void fillJSON(JsonWriter json) throws IOException
    {
        if (heartbeat != null) {
            json.name("Heartbeat").value(this.heartbeat.intValue());
        }

        if (null == startTime) {
            json.name("StartTime").value("now");
        } else {
            json.name("StartTime").value(ProxyMessage.DATE_FORMATTER.format(startTime));
        }

        if (!registerList.isEmpty()) {
            json.name("Register");

            json.beginArray();
            for (ProxyMessageType value : registerList) {
                json.value(value.toString());
            }
            json.endArray();
        }
    }
}
