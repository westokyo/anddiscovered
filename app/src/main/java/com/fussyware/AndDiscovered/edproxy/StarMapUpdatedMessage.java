package com.fussyware.AndDiscovered.edproxy;

import android.util.JsonWriter;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

/**
 * Created by wes on 10/3/15.
 */
public class StarMapUpdatedMessage extends ProxyMessage
{
    public StarMapUpdatedMessage()
    {
        super(ProxyMessageType.StarMapUpdated, new Date());
    }

    private StarMapUpdatedMessage(Date date)
    {
        super(ProxyMessageType.StarMapUpdated, date);
    }

    static StarMapUpdatedMessage getMessage(ProxyMessageType messageType, Date date, JSONObject object)
    {
        return (messageType == ProxyMessageType.StarMapUpdated) ? new StarMapUpdatedMessage(date) : null;
    }

    @Override
    protected void fillJSON(JsonWriter json) throws IOException
    {

    }
}
