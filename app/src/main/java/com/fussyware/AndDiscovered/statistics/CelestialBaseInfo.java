package com.fussyware.AndDiscovered.statistics;

import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;

/**
 * Created by wes on 1/12/16.
 */
public abstract class CelestialBaseInfo
{
    protected boolean firstDiscovered;
    protected String name;
    protected Double distance;
    protected ScanLevel scanLevel;
    protected Double semiMajorAxis;
    protected Double orbitalPeriod;
    protected Double orbitalEccentricity;
    protected Double orbitalInclination;
    protected Double argPeriapsis;

    CelestialBaseInfo(@NonNull ScanLevel scanLevel, boolean firstDiscovered)
    {
        this.scanLevel = scanLevel;
        this.firstDiscovered = firstDiscovered;
    }

    public Double getDistance()
    {
        return distance;
    }

    public ScanLevel getScanLevel()
    {
        return scanLevel;
    }

    public Double getSemiMajorAxis()
    {
        return semiMajorAxis;
    }

    public Double getOrbitalPeriod()
    {
        return orbitalPeriod;
    }

    public Double getOrbitalEccentricity()
    {
        return orbitalEccentricity;
    }

    public Double getOrbitalInclination()
    {
        return orbitalInclination;
    }

    public Double getArgPeriapsis()
    {
        return argPeriapsis;
    }

    public String getName()
    {
        return (name == null) ? "" : name;
    }

    public boolean isFirstDiscovered()
    {
        return firstDiscovered;
    }

    public abstract SatelliteCategory getSatelliteCategory();
}
