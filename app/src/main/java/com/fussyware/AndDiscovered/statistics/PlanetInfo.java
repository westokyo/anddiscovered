package com.fussyware.AndDiscovered.statistics;

import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.Atmosphere;
import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.VolcanoType;

import java.security.InvalidParameterException;

/**
 * Created by wes on 1/12/16.
 */
public class PlanetInfo extends CelestialBaseInfo
{

    private final PlanetType type;

    private Double mass;
    private Double radius;
    private Double surfaceTemp;
    private Double surfacePressure;
    private VolcanoType volcanism = VolcanoType.Unknown;
    private Atmosphere atmosphereType = Atmosphere.Unknown;
    private Double rotationPeriod;
    private Boolean tidalLocked;
    private Double axisTilt;
    private boolean terraformable;

    public PlanetInfo(@NonNull PlanetType type,
                      @NonNull ScanLevel scanLevel,
                      boolean firstDiscovered,
                      String name,
                      Double distance,
                      boolean terraformable,
                      Double mass,
                      Double radius,
                      Double surfaceTemp,
                      Double surfacePressure,
                      VolcanoType volcanism,
                      Atmosphere atmosphereType,
                      Double orbitalPeriod,
                      Double semiMajorAxis,
                      Double orbitalEccentricity,
                      Double orbitalInclination,
                      Double argPeriapsis,
                      Double rotationPeriod,
                      Boolean tidalLocked,
                      Double axisTilt)
    {
        super(scanLevel, firstDiscovered);

        if (type == PlanetType.Unknown) {
            throw new InvalidParameterException("A valid planet type must be specified during creation.");
        }

        this.type = type;

        if (atmosphereType == null) {
            atmosphereType = Atmosphere.Unknown;
        }

        if (volcanism == null) {
            volcanism = VolcanoType.Unknown;
        }

        this.name =  (name == null) ? "" : name;
        this.distance = distance;
        this.terraformable = terraformable;
        this.mass = mass;
        this.radius = radius;
        this.surfaceTemp = surfaceTemp;
        this.surfacePressure = surfacePressure;
        this.volcanism = volcanism;
        this.atmosphereType = atmosphereType;
        this.orbitalPeriod = orbitalPeriod;
        this.semiMajorAxis = semiMajorAxis;
        this.orbitalEccentricity = orbitalEccentricity;
        this.orbitalInclination = orbitalInclination;
        this.argPeriapsis = argPeriapsis;
        this.rotationPeriod = rotationPeriod;
        this.tidalLocked = tidalLocked;
        this.axisTilt = axisTilt;
    }

    public PlanetType getType()
    {
        return type;
    }

    public boolean isTerraformable()
    {
        return terraformable;
    }

    public Double getMass()
    {
        return mass;
    }

    public Double getRadius()
    {
        return radius;
    }

    public Double getSurfaceTemp()
    {
        return surfaceTemp;
    }

    public Double getSurfacePressure()
    {
        return surfacePressure;
    }

    public VolcanoType getVolcanism()
    {
        return volcanism;
    }

    public Atmosphere getAtmosphereType()
    {
        return atmosphereType;
    }

    public Double getRotationPeriod()
    {
        return rotationPeriod;
    }

    public Boolean getTidalLocked()
    {
        return tidalLocked;
    }
    public Double getAxisTilt()
    {
        return axisTilt;
    }

    @Override
    public SatelliteCategory getSatelliteCategory()
    {
        return SatelliteCategory.Planet;
    }

}
