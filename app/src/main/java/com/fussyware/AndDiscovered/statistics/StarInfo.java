package com.fussyware.AndDiscovered.statistics;

import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.StarType;

import java.security.InvalidParameterException;

/**
 * Created by wes on 1/12/16.
 */
public class StarInfo extends CelestialBaseInfo
{
    private final StarType type;

    private Double age;
    private Double mass;
    private Double radius;
    private Double surfaceTemp;

    public StarInfo(@NonNull StarType type,
                    @NonNull ScanLevel scanLevel,
                    boolean firstDiscovered,
                    String name,
                    Double distance,
                    Double age,
                    Double mass,
                    Double radius,
                    Double surfaceTemp,
                    Double orbitalPeriod,
                    Double semiMajorAxis,
                    Double orbitalEccentricity,
                    Double orbitalInclination,
                    Double argPeriapsis)
    {
        super(scanLevel, firstDiscovered);

        if (type == StarType.Unknown) {
            throw new InvalidParameterException("A valid star type must be specified during creation.");
        }

        this.type = type;
        this.name =  (name == null) ? "" : name;
        this.distance = distance;
        this.age = age;
        this.mass = mass;
        this.radius = radius;
        this.surfaceTemp = surfaceTemp;
        this.orbitalPeriod = orbitalPeriod;
        this.semiMajorAxis = semiMajorAxis;
        this.orbitalEccentricity = orbitalEccentricity;
        this.orbitalInclination = orbitalInclination;
        this.argPeriapsis = argPeriapsis;
    }

    public StarType getType()
    {
        return type;
    }

    public Double getAge()
    {
        return age;
    }

    public Double getMass()
    {
        return mass;
    }

    public Double getRadius()
    {
        return radius;
    }

    public Double getSurfaceTemp()
    {
        return surfaceTemp;
    }

    @Override
    public SatelliteCategory getSatelliteCategory()
    {
        return SatelliteCategory.Star;
    }
}
