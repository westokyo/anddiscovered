package com.fussyware.AndDiscovered.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 9/3/15.
 */
public class TextPickerDialogFragment extends DialogFragment
{
    public interface OnTextChangedListener
    {
        void onDone(String tag, String text);
    }

    private static final String LOG_NAME = TextPickerDialogFragment.class.getSimpleName();

    private OnTextChangedListener listener;

    private String[] content;
    private int defaultValue;
    private NumberPicker picker;

    public static TextPickerDialogFragment newInstance(String title,
                                                       String summary,
                                                       String[] content,
                                                       String defaultValue)
    {
        TextPickerDialogFragment fragment = new TextPickerDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putStringArray("content", content);
        bundle.putString("title", (title == null) ? "" : title);
        bundle.putString("summary", (summary == null) ? "" : summary);

        int i;
        for (i = 0; i < content.length; i++) {
            if (defaultValue.equals(content[i])) {
                break;
            }
        }

        bundle.putInt("default", i);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnTextChangedListener) {
                listener = (OnTextChangedListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnTextChangedListener) {
                listener = (OnTextChangedListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle bundle = getArguments();

        String title = bundle.getString("title");
        String summary = bundle.getString("summary");

        content = bundle.getStringArray("content");
        defaultValue = bundle.getInt("default");

        if (!title.isEmpty()) {
            Dialog dialog = getDialog();
            dialog.setTitle(title);
        }

        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.textpicker_dialog_fragment, container, false);

        if (layout != null) {
            TextView summaryText = (TextView) layout.findViewById(R.id.SummaryText);
            Button doneButton = (Button) layout.findViewById(R.id.DoneButton);
            Button cancelButton = (Button) layout.findViewById(R.id.CancelButton);

            if (summary.isEmpty()) {
                layout.removeView(summaryText);
            } else {
                summaryText.setText(summary);
            }

            cancelButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    dismiss();
                }
            });

            doneButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (listener != null) {
                        int value = picker.getValue();

                        if (value != defaultValue) {
                            listener.onDone(getTag(), content[value]);
                        }
                    }

                    dismiss();
                }
            });

            picker = (NumberPicker) layout.findViewById(R.id.TextPicker);
            picker.setMinValue(0);
            picker.setMaxValue(content.length - 1);
            picker.setDisplayedValues(content);
            picker.setValue(defaultValue);
        }

        return layout;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        listener = null;
    }
}
