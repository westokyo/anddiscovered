package com.fussyware.AndDiscovered.dialog;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.method.NumberKeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.edproxy.ProxyConnectionState;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.service.EDProxyWebsocketService;
import com.fussyware.AndDiscovered.service.ProxyBase;

import java.io.IOException;

/**
 * Created by wes on 9/2/15.
 */
public class TextDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, String text);
    }

    private static final String LOG_NAME = TextDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;

    private String initialContent;
    private EditText contentText;

    private ProxyBase proxyService;

    private final ServiceConnection proxyConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            proxyService = ((ProxyBase.EDProxyBinder) service).getService();
            proxyService.resendState();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            proxyService = null;
        }
    };

    public static TextDialogFragment newInstance(String title,
                                                 String summary,
                                                 String content,
                                                 int inputType)
    {
        return TextDialogFragment.newInstance(title,
                                              summary,
                                              content,
                                              inputType,
                                              false);
    }

    public static TextDialogFragment newInstance(String title,
                                                 String summary,
                                                 String content,
                                                 int inputType,
                                                 boolean enableSend)
    {
        Bundle bundle = new Bundle();
        bundle.putString("content", content);
        bundle.putString("title", (title == null) ? "" : title);
        bundle.putString("summary", (summary == null) ? "" : summary);
        bundle.putInt("inputType", inputType);
        bundle.putBoolean("enable_send", enableSend);

        TextDialogFragment tdf = new TextDialogFragment();
        tdf.setArguments(bundle);

        return tdf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getActivity().bindService(new Intent(getContext().getApplicationContext(),
                                             EDProxyWebsocketService.class),
                                  proxyConnection,
                                  Context.BIND_AUTO_CREATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle bundle = getArguments();

        String title = bundle.getString("title");
        String summary = bundle.getString("summary");

        int inputType = bundle.getInt("inputType");

        initialContent = bundle.getString("content");

        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        if ((title != null) && !title.isEmpty()) {
            Dialog dialog = getDialog();
            dialog.setTitle(title);
        }

        View layout = inflater.inflate(R.layout.text_dialog_layout,
                                       container,
                                       false);

        if (layout != null) {
            TextView summaryText = (TextView) layout.findViewById(R.id.SummaryText);
            Button sendButton = (Button) layout.findViewById(R.id.send_system_button);

            final Button doneButton = (Button) layout.findViewById(R.id.DoneButton);
            Button cancelButton = (Button) layout.findViewById(R.id.CancelButton);

            contentText = (EditText) layout.findViewById(R.id.ContentText);

            if ((summary == null) || summary.isEmpty()) {
                summaryText.setVisibility(View.GONE);
            } else {
                summaryText.setText(summary);
            }

            if (bundle.getBoolean("enable_send", false)) {
                sendButton.setVisibility(View.VISIBLE);
                sendButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        ProxyConnectionState state = ProxyConnectionState.getInstance();

                        if (state.getState() == ProxyConnectionState.ConnectionState.CONNECTED) {
                            try {
                                String sendText = contentText.getText().toString();

                                proxyService.sendKeys(sendText);

                                String success = getString(R.string.send_system_success_toast);

                                Toast.makeText(getActivity(),
                                               String.format(success, sendText),
                                               Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                AndLog.e(LOG_NAME, "Failed to send text to Edproxy", e);
                                Toast.makeText(getActivity(),
                                               R.string.send_system_failed_ioexception_toast,
                                               Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(),
                                           R.string.send_system_failed_not_connected_toast,
                                           Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            if ((inputType & InputType.TYPE_NUMBER_FLAG_SIGNED) != 0) {
                contentText.setKeyListener(new NumberKeyListener() {
                    @Override
                    protected char[] getAcceptedChars()
                    {
                        return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
                    }

                    @Override
                    public int getInputType()
                    {
                        return InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_CLASS_NUMBER;
                    }
                });
            } else if ((inputType & InputType.TYPE_NUMBER_FLAG_DECIMAL) != 0) {
                contentText.setKeyListener(new NumberKeyListener() {
                    @Override
                    protected char[] getAcceptedChars()
                    {
                        return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'};
                    }

                    @Override
                    public int getInputType()
                    {
                        return InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER;
                    }
                });
            }

            contentText.setInputType(inputType);
            contentText.setText(initialContent);
            contentText.setOnEditorActionListener(new TextView.OnEditorActionListener()
            {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
                {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        doneButton.callOnClick();
                    }

                    return false;
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    dismiss();
                }
            });

            doneButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    String currentContent = contentText.getText().toString();

                    if (listener != null) {
                        if (!currentContent.equals(initialContent)) {
                            listener.onDone(getTag(), currentContent);
                        }
                    }

                    dismiss();
                }
            });
        }

        return layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnDoneListener) {
                listener = (OnDoneListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnDoneListener) {
                listener = (OnDoneListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        getActivity().unbindService(proxyConnection);
    }
}
