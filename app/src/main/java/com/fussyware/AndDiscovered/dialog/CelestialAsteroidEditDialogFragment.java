package com.fussyware.AndDiscovered.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.AsteroidBody;

/**
 * Created by wes on 11/20/15.
 */
public class CelestialAsteroidEditDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, Bundle changedItems);
    }

    public static final String TAG_MOONMASSES = "CelestialAsteroidEditDialogFragment.moonmasses";

    private static final String TAG_ORIGINAL_BUNDLE = "CelestialAsteroidEditDialogFragment.bundle";

    private static final String LOG_NAME = CelestialAsteroidEditDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;

    private Bundle originalBundle;
    private Bundle savedState;

    private EditText moonMassesText;

    private Button doneButton;

    public static CelestialAsteroidEditDialogFragment newInstance(@NonNull final AsteroidBody body)
    {
        Bundle bundle = new Bundle();

        bundle.putString(TAG_MOONMASSES,
                         (body.getMoonMass() == null) ?
                         "" :
                         String.format("%1.4f", body.getMoonMass()));

        CelestialAsteroidEditDialogFragment df = new CelestialAsteroidEditDialogFragment();
        df.setArguments(bundle);

        return df;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            originalBundle = savedState = getArguments();
        } else {
            originalBundle = savedInstanceState.getBundle(TAG_ORIGINAL_BUNDLE);
            savedState = savedInstanceState;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        getDialog().setTitle("Update Details");
        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        View layout = inflater.inflate(R.layout.cb_dialog_asteroid_layout,
                                       container,
                                       false);

        createViews(layout);
        createButtons(layout);

        if (savedState != null) {
            populateViews(savedState);
            savedState = null;
        }

        return layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnDoneListener) {
                listener = (OnDoneListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnDoneListener) {
                listener = (OnDoneListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putBundle(TAG_ORIGINAL_BUNDLE, originalBundle);
        outState.putString(TAG_MOONMASSES, moonMassesText.getText().toString());
    }

    private void createButtons(View layout)
    {
        final Button cancel = (Button) layout.findViewById(R.id.CancelButton);
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        doneButton = (Button) layout.findViewById(R.id.DoneButton);
        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (listener != null) {
                    Bundle bundle = new Bundle();

                    if (!moonMassesText.getText()
                                       .toString()
                                       .equals(originalBundle.getString(TAG_MOONMASSES))) {
                        String value = moonMassesText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_MOONMASSES, Double.valueOf(value));
                    }

                    listener.onDone(getTag(), bundle);
                }

                dismiss();
            }
        });
    }

    private void createViews(View layout)
    {
        moonMassesText = (EditText) layout.findViewById(R.id.cb_asteroid_moonmasses_value);
        moonMassesText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doneButton.callOnClick();
                }

                return false;
            }
        });
    }

    private void populateViews(Bundle bundle)
    {
        moonMassesText.setText(bundle.getString(TAG_MOONMASSES));
    }
}
