package com.fussyware.AndDiscovered.dialog;

import android.app.Activity;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RadioGroup;

import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarType;

/**
 * Created by wes on 9/22/15.
 */
public class EditCelestialTypeDialogFragment extends DialogFragment
{
    public interface onDoneListener
    {
        void onDone(String tag, String text);
    }

    public static String ARGS_DEFAULT_VALUE = EditCelestialTypeDialogFragment.class.getSimpleName() + "defaultValue";

    private static final String LOG_NAME = EditCelestialTypeDialogFragment.class.getSimpleName();

    private String[] starContent;
    private String[] planetContent;

    View topLayout;
    NumberPicker starPicker;
    NumberPicker planetPicker;
    RadioGroup radioGroup;

    onDoneListener listener;

    public static EditCelestialTypeDialogFragment newInstance(String defaultValue)
    {
        Bundle bundle = new Bundle();
        bundle.putString(ARGS_DEFAULT_VALUE, defaultValue);

        EditCelestialTypeDialogFragment fragment = new EditCelestialTypeDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof onDoneListener) {
            listener = (onDoneListener) activity;
        } else {
            throw new ClassCastException("Activity did not implement done listener.");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        String defaultValue;

        if (super.getArguments() != null) {
            Bundle bundle = super.getArguments();
            defaultValue = bundle.getString(ARGS_DEFAULT_VALUE);
        } else if (savedInstanceState != null) {
            defaultValue = savedInstanceState.getString(ARGS_DEFAULT_VALUE);
        } else {
            defaultValue = "NA";
        }

        View layout = topLayout = inflater.inflate(R.layout.photo_edit_type_dialog_layout,
                                                   container,
                                                   false);

        getDialog().setTitle(R.string.photo_edit_celestial_type);

        Button cancelButton = (Button) layout.findViewById(R.id.CancelButton);
        Button doneButton = (Button) layout.findViewById(R.id.DoneButton);

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (listener != null) {
                    if (radioGroup.getCheckedRadioButtonId() == R.id.radio_planet_type) {
                        listener.onDone(getTag(), planetContent[planetPicker.getValue()]);
                    } else {
                        listener.onDone(getTag(), starContent[starPicker.getValue()]);
                    }
                }

                dismiss();
            }
        });

        int defaultId = R.id.radio_planet_type;
        int defaultStarPosition = 0;
        int defaultPlanetPosition = 0;

        starContent = new String[StarType.values().length - 1];
        StarType[] starTypes = StarType.values();
        for (int i = 0; i < starTypes.length; i++) {
            if (starTypes[i] != StarType.Unknown) {
                starContent[i] = starTypes[i].toString();

                if (defaultValue.equalsIgnoreCase(starContent[i])) {
                    defaultId = R.id.radio_stellar_type;
                    defaultStarPosition = i;
                }
            }
        }

        planetContent = new String[PlanetType.values().length - 1];
        PlanetType[] planetTypes = PlanetType.values();
        for (int i = 0; i < planetTypes.length; i++) {
            if (planetTypes[i] != PlanetType.Unknown) {
                planetContent[i] = planetTypes[i].toString();

                if (defaultValue.equalsIgnoreCase(planetContent[i])) {
                    defaultId = R.id.radio_planet_type;
                    defaultPlanetPosition = i;
                }
            }
        }

        starPicker = (NumberPicker) layout.findViewById(R.id.picker_stellar_type);
        starPicker.setMinValue(0);
        starPicker.setMaxValue(starContent.length - 1);
        starPicker.setDisplayedValues(starContent);
        starPicker.setValue(defaultStarPosition);

        planetPicker = (NumberPicker) layout.findViewById(R.id.picker_planet_type);
        planetPicker.setMinValue(0);
        planetPicker.setMaxValue(planetContent.length - 1);
        planetPicker.setDisplayedValues(planetContent);
        planetPicker.setValue(defaultPlanetPosition);

        radioGroup = (RadioGroup) layout.findViewById(R.id.radio_group_celestial_type);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch (checkedId) {
                    case R.id.radio_planet_type:
                        starPicker.setVisibility(View.GONE);
                        planetPicker.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radio_stellar_type:
                        starPicker.setVisibility(View.VISIBLE);
                        planetPicker.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }
        });
        radioGroup.check(defaultId);

        return layout;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        if (radioGroup.getCheckedRadioButtonId() == R.id.radio_planet_type) {
            outState.putString(ARGS_DEFAULT_VALUE, planetContent[planetPicker.getValue()]);
        } else {
            outState.putString(ARGS_DEFAULT_VALUE, starContent[starPicker.getValue()]);
        }
    }
}
