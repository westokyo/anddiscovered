package com.fussyware.AndDiscovered.dialog;

import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.CelestialIconAdapter;
import com.fussyware.AndDiscovered.celestial.StarType;

/**
 * Created by wes on 12/7/15.
 */
public class ChooseMainStarDialog extends DialogFragment
{
    public interface OnDismissListener
    {
        void onDone(String tag, StarType type);
        void onMainStarCancel();
    }

    private static final String LOG_NAME = ChooseMainStarDialog.class.getSimpleName();

    private StarType mainStar;
    private View selected;
    private Button doneButton;
    private OnDismissListener listener;

    public static ChooseMainStarDialog newInstance()
    {
        return new ChooseMainStarDialog();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof OnDismissListener) {
            listener = (OnDismissListener) activity;
        } else {
            throw new ClassCastException("OnDismissListener not implemented.");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        getDialog().setTitle(R.string.select_main_star_title);

        View layout = inflater.inflate(R.layout.mainstar_selection_dialog_layout,
                                       container,
                                       false);

        setCancelable(false);

        TypedArray typedArray = getResources().obtainTypedArray(R.array.star_icons);

        CelestialIconAdapter adapter = new CelestialIconAdapter(getActivity(), typedArray);
        adapter.setItemClickListener(new CelestialIconAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(View view, int position)
            {
                mainStar = StarType.getStarType(position);

                if (selected != null) {
                    selected.setSelected(false);
                }

                selected = view;
                selected.setSelected(true);

                if (!doneButton.isEnabled()) {
                    doneButton.setEnabled(true);
                }
            }
        });

        typedArray.recycle();

        RecyclerView gridView = (RecyclerView) layout.findViewById(R.id.selection_icon_grid);
        gridView.setAdapter(adapter);

        doneButton = (Button) layout.findViewById(R.id.DoneButton);
        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();

                if (listener != null) {
                    listener.onDone(getTag(), mainStar);
                }
            }
        });

        Button cancelButton = (Button) layout.findViewById(R.id.CancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();

                if (listener != null) {
                    listener.onMainStarCancel();
                }
            }
        });

        return layout;
    }
}
