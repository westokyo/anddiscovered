package com.fussyware.AndDiscovered.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.PlanetBody;

/**
 * Created by wes on 11/20/15.
 */
public class CelestialPlanetEditDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, Bundle changedItems);
    }

    public static final String TAG_TERRAFORM = "CelestialPlanetEditDialogFragment.terraform";
    public static final String TAG_MASS = "CelestialPlanetEditDialogFragment.mass";
    public static final String TAG_RADIUS = "CelestialPlanetEditDialogFragment.radius";
    public static final String TAG_SURFACETEMP = "CelestialPlanetEditDialogFragment.surfacetemp";
    public static final String TAG_SURFACEPRESSURE = "CelestialPlanetEditDialogFragment.surfacepressure";
    public static final String TAG_VOLCANISM = "CelestialPlanetEditDialogFragment.volcanism";
    public static final String TAG_ATMOSPHERETYPE = "CelestialPlanetEditDialogFragment.atmospheretype";
    public static final String TAG_ROTATIONPERIOD = "CelestialPlanetEditDialogFragment.rotationperiod";
    public static final String TAG_TIDALOCKED = "CelestialPlanetEditDialogFragment.tidallocked";
    public static final String TAG_AXISTILT = "CelestialPlanetEditDialogFragment.axistilt";

    private static final String TAG_ORIGINAL_BUNDLE = "CelestialPlanetEditDialogFragment.bundle";

    private static final String LOG_NAME = CelestialPlanetEditDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;

    private Bundle originalBundle;
    private Bundle savedState;

    private EditText massText;
    private EditText radiusText;
    private EditText surfaceTempText;
    private EditText surfacePressureText;
    private EditText rotationText;
    private EditText axisTiltText;

    private Spinner atmosphereTypeSpinner;
    private Spinner volcansimSpinner;
    private CheckBox terraformCheck;
    private CheckBox tidalLockedCheck;
    private Button doneButton;

    public static CelestialPlanetEditDialogFragment newInstance(@NonNull final PlanetBody body)
    {
        Bundle bundle = new Bundle();

        bundle.putString(TAG_MASS,
                         (body.getMass() == null) ?
                         "" :
                         String.format("%1.4f", body.getMass()));

        bundle.putString(TAG_RADIUS,
                         (body.getRadius() == null) ?
                         "" :
                         String.format("%1.2f", body.getRadius()));

        bundle.putString(TAG_SURFACETEMP,
                         (body.getSurfaceTemp() == null) ?
                         "" :
                         String.format("%1.2f", body.getSurfaceTemp()));

        bundle.putString(TAG_SURFACEPRESSURE,
                         (body.getSurfacePressure() == null) ?
                         "" :
                         String.format("%1.2f", body.getSurfacePressure()));

        bundle.putString(TAG_ROTATIONPERIOD,
                         (body.getRotationPeriod() == null) ?
                         "" :
                         String.format("%1.2f", body.getRotationPeriod()));

        bundle.putString(TAG_AXISTILT,
                         (body.getAxisTilt() == null) ?
                         "" :
                         String.format("%1.2f", body.getAxisTilt()));

        bundle.putInt(TAG_ATMOSPHERETYPE, body.getAtmosphereType().value);
        bundle.putInt(TAG_VOLCANISM, body.getVolcanism().value);

        bundle.putBoolean(TAG_TERRAFORM, body.getTerraformable());
        bundle.putBoolean(TAG_TIDALOCKED,
                          ((body.getTidalLocked() != null) && body.getTidalLocked()));

        CelestialPlanetEditDialogFragment df = new CelestialPlanetEditDialogFragment();
        df.setArguments(bundle);

        return df;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            originalBundle = savedState = getArguments();
        } else {
            originalBundle = savedInstanceState.getBundle(TAG_ORIGINAL_BUNDLE);
            savedState = savedInstanceState;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        getDialog().setTitle("Update Details");
        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        View layout = inflater.inflate(R.layout.cb_dialog_planetary_layout,
                                       container,
                                       false);

        createViews(layout);
        createButtons(layout);

        if (savedState != null) {
            populateViews(savedState);
            savedState = null;
        }

        return layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnDoneListener) {
                listener = (OnDoneListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnDoneListener) {
                listener = (OnDoneListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putBundle(TAG_ORIGINAL_BUNDLE, originalBundle);

        outState.putString(TAG_MASS, massText.getText().toString());
        outState.putString(TAG_RADIUS, radiusText.getText().toString());
        outState.putString(TAG_SURFACETEMP, surfaceTempText.getText().toString());
        outState.putString(TAG_SURFACEPRESSURE, surfacePressureText.getText().toString());
        outState.putString(TAG_ROTATIONPERIOD, rotationText.getText().toString());
        outState.putString(TAG_AXISTILT, axisTiltText.getText().toString());

        outState.putInt(TAG_ATMOSPHERETYPE, atmosphereTypeSpinner.getSelectedItemPosition());
        outState.putInt(TAG_VOLCANISM, volcansimSpinner.getSelectedItemPosition());

        outState.putBoolean(TAG_TERRAFORM, terraformCheck.isChecked());
        outState.putBoolean(TAG_TIDALOCKED, tidalLockedCheck.isChecked());
    }

    private void createButtons(View layout)
    {
        final Button cancel = (Button) layout.findViewById(R.id.CancelButton);
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        doneButton = (Button) layout.findViewById(R.id.DoneButton);
        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (listener != null) {
                    Bundle bundle = new Bundle();

                    if (!massText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_MASS))) {
                        String value = massText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_MASS, Double.valueOf(value));
                    }

                    if (!radiusText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_RADIUS))) {
                        String value = radiusText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_RADIUS, Double.valueOf(value));
                    }

                    if (!surfaceTempText.getText()
                                   .toString()
                                   .equals(originalBundle.getString(TAG_SURFACETEMP))) {
                        String value = surfaceTempText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_SURFACETEMP, Double.valueOf(value));
                    }

                    if (!surfacePressureText.getText()
                                .toString()
                                .equals(originalBundle.getString(TAG_SURFACEPRESSURE))) {
                        String value = surfacePressureText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_SURFACEPRESSURE, Double.valueOf(value));
                    }

                    if (!rotationText.getText()
                                        .toString()
                                        .equals(originalBundle.getString(TAG_ROTATIONPERIOD))) {
                        String value = rotationText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_ROTATIONPERIOD, Double.valueOf(value));
                    }

                    if (!axisTiltText.getText()
                                         .toString()
                                         .equals(originalBundle.getString(TAG_AXISTILT))) {
                        String value = axisTiltText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_AXISTILT, Double.valueOf(value));
                    }

                    if (atmosphereTypeSpinner.getSelectedItemPosition() !=
                        originalBundle.getInt(TAG_ATMOSPHERETYPE)) {
                        bundle.putInt(TAG_ATMOSPHERETYPE,
                                      atmosphereTypeSpinner.getSelectedItemPosition());
                    }

                    if (terraformCheck.isChecked() != originalBundle.getBoolean(TAG_TERRAFORM)) {
                        bundle.putBoolean(TAG_TERRAFORM, terraformCheck.isChecked());
                    }

                    if (volcansimSpinner.getSelectedItemPosition() != originalBundle.getInt(TAG_VOLCANISM)) {
                        bundle.putInt(TAG_VOLCANISM, volcansimSpinner.getSelectedItemPosition());
                    }

                    if (tidalLockedCheck.isChecked() != originalBundle.getBoolean(TAG_TIDALOCKED)) {
                        bundle.putBoolean(TAG_TIDALOCKED, tidalLockedCheck.isChecked());
                    }

                    listener.onDone(getTag(), bundle);
                }

                dismiss();
            }
        });
    }

    private void createViews(View layout)
    {
        massText = (EditText) layout.findViewById(R.id.cb_planet_mass_value);
        radiusText = (EditText) layout.findViewById(R.id.cb_planet_radius_value);
        surfaceTempText = (EditText) layout.findViewById(R.id.cb_planet_surfacetemp_value);
        surfacePressureText = (EditText) layout.findViewById(R.id.cb_planet_surfacepressure_value);
        rotationText = (EditText) layout.findViewById(R.id.cb_planet_rotationperiod_value);
        axisTiltText = (EditText) layout.findViewById(R.id.cb_planet_axistilt_value);
        axisTiltText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doneButton.callOnClick();
                }

                return false;
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                                                             R.array.atmosphere_types,
                                                                             android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        atmosphereTypeSpinner = (Spinner) layout.findViewById(R.id.cb_planet_atmospheretype_value);
        atmosphereTypeSpinner.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(getActivity(),
                                                  R.array.volcano_types,
                                                  android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        volcansimSpinner = (Spinner) layout.findViewById(R.id.cb_planet_volcanism_value);
        volcansimSpinner.setAdapter(adapter);

        terraformCheck = (CheckBox) layout.findViewById(R.id.cb_planet_terraformable_value);
        tidalLockedCheck = (CheckBox) layout.findViewById(R.id.cb_planet_tidallocked_value);
    }

    private void populateViews(Bundle bundle)
    {
        massText.setText(bundle.getString(TAG_MASS));
        radiusText.setText(bundle.getString(TAG_RADIUS));
        surfaceTempText.setText(bundle.getString(TAG_SURFACETEMP));
        surfacePressureText.setText(bundle.getString(TAG_SURFACEPRESSURE));
        rotationText.setText(bundle.getString(TAG_ROTATIONPERIOD));
        axisTiltText.setText(bundle.getString(TAG_AXISTILT));

        atmosphereTypeSpinner.setSelection(bundle.getInt(TAG_ATMOSPHERETYPE));
        volcansimSpinner.setSelection(bundle.getInt(TAG_VOLCANISM));

        terraformCheck.setChecked(bundle.getBoolean(TAG_TERRAFORM));
        tidalLockedCheck.setChecked(bundle.getBoolean(TAG_TIDALOCKED));
    }
}
