package com.fussyware.AndDiscovered.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.CheckListAdapter;

/**
 * Created by wes on 9/5/15.
 */
public class CheckDialogFragment extends DialogFragment
{
    private static final String LOG_NAME = CheckDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;
    private NumberPicker countPicker;
    private CheckListAdapter listAdapter;

    private Bundle bundle;

    public static CheckDialogFragment newInstance(String title,
                                                  String summary,
                                                  int defaultCount,
                                                  CheckListItem[] listItems)
    {
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("summary", summary);
        bundle.putInt("count", defaultCount);
        bundle.putParcelableArray("items", listItems);

        CheckDialogFragment fragment = new CheckDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof OnDoneListener) {
            listener = (OnDoneListener) activity;
        } else {
            throw new ClassCastException("Activity did not implement OnDoneListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        String title;
        String summary;
        int count;
        final CheckListItem[] items;

        bundle = getArguments();
        if (bundle == null) {
            bundle = (savedInstanceState == null) ? null : savedInstanceState.getBundle("check_list_bundle");

            if (bundle == null) {
                throw new IllegalArgumentException("No Bundle of arguments defined.");
            }
        }

        title = bundle.getString("title");
        summary = bundle.getString("summary");

        count = bundle.getInt("count");
        items = (CheckListItem[]) bundle.getParcelableArray("items");

        if (!title.isEmpty()) {
            Dialog dialog = getDialog();
            dialog.setTitle(title);
        }

        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.check_list_dialog_layout, container, false);

        if (layout != null) {
            TextView summaryText = (TextView) layout.findViewById(R.id.SummaryText);
            Button doneButton = (Button) layout.findViewById(R.id.DoneButton);
            Button cancelButton = (Button) layout.findViewById(R.id.CancelButton);

            if (summary.isEmpty()) {
                layout.removeView(summaryText);
            } else {
                summaryText.setText(summary);
            }

            cancelButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    dismiss();
                }
            });

            doneButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (listener != null) {
                        listener.onDone(getTag(), countPicker.getValue(), listAdapter.getItems());
                    }

                    dismiss();
                }
            });

            countPicker = (NumberPicker) layout.findViewById(R.id.CountNumberPicker);
            countPicker.setMinValue(0);
            countPicker.setMaxValue(255);
            countPicker.setValue(count);

            listAdapter = new CheckListAdapter(items);

            ListView checkList = (ListView) layout.findViewById(R.id.StringCheckList);
            checkList.setAdapter(listAdapter);
            checkList.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                {
                    CheckListItem item = (CheckListItem) listAdapter.getItem(position);
                    item.setChecked(!item.isChecked());
                    listAdapter.notifyDataSetChanged();
                }
            });
        }

        return layout;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        listener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        bundle.putInt("count", countPicker.getValue());
        bundle.putParcelableArray("items", listAdapter.getItems());

        outState.putBundle("check_list_bundle", bundle);
    }

    public interface OnDoneListener
    {
        void onDone(String tag, int count, CheckListItem[] items);
    }

    public static class CheckListItem implements Parcelable
    {
        public final String name;
        private boolean checked;

        public CheckListItem(String name, boolean checked)
        {
            this.name = name;
            this.checked = checked;
        }

        private CheckListItem(Parcel in)
        {
            this.name = in.readString();
            this.checked = (in.readInt() == 1);
        }

        public void setChecked(boolean checked)
        {
            this.checked = checked;
        }

        public boolean isChecked()
        {
            return this.checked;
        }

        @Override
        public String toString()
        {
            return "CheckListItem {" +
                   "name='" + name + '\'' +
                   ", checked=" + checked +
                   '}';
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeString(name);
            dest.writeInt((checked) ? 1 : 0);
        }

        public static final Parcelable.Creator<CheckListItem> CREATOR =
                new Parcelable.Creator<CheckListItem>() {
                    @Override
                    public CheckListItem createFromParcel(Parcel in) {
                        return new CheckListItem(in);
                    }

                    @Override
                    public CheckListItem[] newArray(int size) {
                        return new CheckListItem[size];
                    }
                };
    }
}
