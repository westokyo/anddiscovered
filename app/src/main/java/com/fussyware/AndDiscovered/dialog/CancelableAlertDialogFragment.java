package com.fussyware.AndDiscovered.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 9/22/15.
 */
public class CancelableAlertDialogFragment extends DialogFragment
{
    private int title;

    private OnAlertListener listener;

    public interface OnAlertListener
    {
        void onPositive(String tag);
        void onNegative(String tag);
    }

    public static CancelableAlertDialogFragment newInstance(int titleId)
    {
        Bundle bundle = new Bundle();
        bundle.putInt("CancelableAlertDialogFragment.title", titleId);

        CancelableAlertDialogFragment fragment = new CancelableAlertDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnAlertListener) {
                listener = (OnAlertListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnAlertListener) {
                listener = (OnAlertListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        int iconId = android.R.drawable.ic_dialog_alert;

        if (getArguments() != null) {
            Bundle bundle = getArguments();

            title = bundle.getInt("CancelableAlertDialogFragment.title", 0);
        } else if (savedInstanceState != null) {
            title = savedInstanceState.getInt("CancelableAlertDialogFragment.title", 0);
        }

        return new AlertDialog
                .Builder(getActivity())
                .setIcon(iconId)
                .setTitle(title)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (listener != null) {
                            listener.onPositive(getTag());
                        }
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (listener != null) {
                            listener.onNegative(getTag());
                        }
                    }
                })
                .create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("CancelableAlertDialogFragment.title", title);
    }
}
