package com.fussyware.AndDiscovered.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 9/22/15.
 */
public class OkAlertDialogFragment extends DialogFragment
{
    private int titleId;
    private int messageId;

    public static OkAlertDialogFragment newInstance(int titleId, int messageId)
    {
        Bundle bundle = new Bundle();
        bundle.putInt("title", titleId);
        bundle.putInt("message", messageId);

        OkAlertDialogFragment fragment = new OkAlertDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("title", titleId);
        outState.putInt("message", messageId);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        int iconId = android.R.drawable.ic_dialog_alert;

        if (getArguments() != null) {
            Bundle bundle = getArguments();

            titleId = bundle.getInt("title");
            messageId = bundle.getInt("message");
        } else if (savedInstanceState != null) {
            titleId = savedInstanceState.getInt("title");
            messageId = savedInstanceState.getInt("message");
        }

        return new AlertDialog
                .Builder(getActivity())
                .setIcon(iconId)
                .setTitle(titleId)
                .setMessage(messageId)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                    }
                })
                .create();
    }
}
