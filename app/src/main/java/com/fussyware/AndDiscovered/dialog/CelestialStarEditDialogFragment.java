package com.fussyware.AndDiscovered.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarBody;

/**
 * Created by wes on 11/20/15.
 */
public class CelestialStarEditDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, Bundle changedItems);
    }

    public static final String TAG_AGE = "CelestialStarEditDialogFragment.age";
    public static final String TAG_MASS = "CelestialStarEditDialogFragment.mass";
    public static final String TAG_RADIUS = "CelestialStarEditDialogFragment.radius";
    public static final String TAG_SURFACETEMP = "CelestialStarEditDialogFragment.surfacetemp";

    private static final String TAG_ORIGINAL_BUNDLE = "CelestialStarEditDialogFragment.bundle";

    private static final String LOG_NAME = CelestialStarEditDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;

    private Bundle originalBundle;
    private Bundle savedState;

    private EditText ageText;
    private EditText massText;
    private EditText radiusText;
    private EditText tempText;

    private Button doneButton;

    public static CelestialStarEditDialogFragment newInstance(@NonNull final StarBody body)
    {
        Bundle bundle = new Bundle();

        bundle.putString(TAG_AGE,
                         (body.getAge() == null) ?
                         "" :
                         String.format("%1.2f", body.getAge()));

        bundle.putString(TAG_MASS,
                         (body.getMass() == null) ?
                         "" :
                         String.format("%1.4f", body.getMass()));

        bundle.putString(TAG_RADIUS,
                         (body.getRadius() == null) ?
                         "" :
                         String.format("%1.4f", body.getRadius()));

        bundle.putString(TAG_SURFACETEMP,
                         (body.getSurfaceTemp() == null) ?
                         "" :
                         String.format("%1.2f", body.getSurfaceTemp()));

        CelestialStarEditDialogFragment df = new CelestialStarEditDialogFragment();
        df.setArguments(bundle);

        return df;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            originalBundle = savedState = getArguments();
        } else {
            originalBundle = savedInstanceState.getBundle(TAG_ORIGINAL_BUNDLE);
            savedState = savedInstanceState;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        getDialog().setTitle("Update Details");
        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        View layout = inflater.inflate(R.layout.cb_dialog_star_layout,
                                       container,
                                       false);

        createViews(layout);
        createButtons(layout);

        if (savedState != null) {
            populateViews(savedState);
            savedState = null;
        }

        return layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnDoneListener) {
                listener = (OnDoneListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnDoneListener) {
                listener = (OnDoneListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putBundle(TAG_ORIGINAL_BUNDLE, originalBundle);

        outState.putString(TAG_AGE, ageText.getText().toString());
        outState.putString(TAG_MASS, massText.getText().toString());
        outState.putString(TAG_RADIUS, radiusText.getText().toString());
        outState.putString(TAG_SURFACETEMP, tempText.getText().toString());
    }

    private void createButtons(View layout)
    {
        final Button cancel = (Button) layout.findViewById(R.id.CancelButton);
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        doneButton = (Button) layout.findViewById(R.id.DoneButton);
        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (listener != null) {
                    Bundle bundle = new Bundle();

                    if (!ageText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_AGE))) {
                        String value = ageText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_AGE, Double.valueOf(value));
                    }

                    if (!massText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_MASS))) {
                        String value = massText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_MASS, Double.valueOf(value));
                    }

                    if (!radiusText.getText()
                                   .toString()
                                   .equals(originalBundle.getString(TAG_RADIUS))) {
                        String value = radiusText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_RADIUS, Double.valueOf(value));
                    }

                    if (!tempText.getText()
                                         .toString()
                                         .equals(originalBundle.getString(TAG_SURFACETEMP))) {
                        String value = tempText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_SURFACETEMP, Double.valueOf(value));
                    }

                    listener.onDone(getTag(), bundle);
                }

                dismiss();
            }
        });
    }

    private void createViews(View layout)
    {
        ageText = (EditText) layout.findViewById(R.id.cb_star_age_value);
        massText = (EditText) layout.findViewById(R.id.cb_star_mass_value);
        radiusText = (EditText) layout.findViewById(R.id.cb_star_radius_value);
        tempText = (EditText) layout.findViewById(R.id.cb_star_surface_temp_value);
        tempText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doneButton.callOnClick();
                }

                return false;
            }
        });
    }

    private void populateViews(Bundle bundle)
    {
        ageText.setText(bundle.getString(TAG_AGE));
        massText.setText(bundle.getString(TAG_MASS));
        radiusText.setText(bundle.getString(TAG_RADIUS));
        tempText.setText(bundle.getString(TAG_SURFACETEMP));
    }
}
