package com.fussyware.AndDiscovered.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.CelestialBody;

/**
 * Created by wes on 11/20/15.
 */
public class CelestialCommonEditDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, Bundle changedItems);
    }

    public static final String TAG_DISTANCE = "CelestialCommonEditDialogFragment.distance";
    public static final String TAG_SEMIAXIS = "CelestialCommonEditDialogFragment.semiaxis";
    public static final String TAG_PERIOD = "CelestialCommonEditDialogFragment.period";
    public static final String TAG_ECC = "CelestialCommonEditDialogFragment.ecc";
    public static final String TAG_INCLINE = "CelestialCommonEditDialogFragment.incline";
    public static final String TAG_PERIAPSIS = "CelestialCommonEditDialogFragment.periapsis";

    private static final String TAG_ORIGINAL_BUNDLE = "CelestialCommonEditDialogFragment.bundle";

    private static final String LOG_NAME = CelestialCommonEditDialogFragment.class.getSimpleName();

    private OnDoneListener listener = null;

    private Bundle originalBundle;
    private Bundle savedState;

    private EditText distanceText;
    private EditText semiAxisText;
    private EditText periodText;
    private EditText eccText;
    private EditText inclinationText;
    private EditText argPeriapsisText;

    private Button doneButton;

    public static CelestialCommonEditDialogFragment newInstance(@NonNull final CelestialBody body)
    {
        Bundle bundle = new Bundle();

        bundle.putString(TAG_DISTANCE,
                         (body.getDistance() == null) ?
                         "" :
                         String.format("%1.2f", body.getDistance()));

        bundle.putString(TAG_SEMIAXIS,
                         (body.getSemiMajorAxis() == null) ?
                         "" :
                         String.format("%1.4f", body.getSemiMajorAxis()));

        bundle.putString(TAG_PERIOD,
                         (body.getOrbitalPeriod() == null) ?
                         "" :
                         String.format("%1.2f", body.getOrbitalPeriod()));

        bundle.putString(TAG_ECC,
                         (body.getOrbitalEccentricity() == null) ?
                         "" :
                         String.format("%1.4f", body.getOrbitalEccentricity()));

        bundle.putString(TAG_INCLINE,
                         (body.getOrbitalInclination() == null) ?
                         "" :
                         String.format("%1.2f", body.getOrbitalInclination()));

        bundle.putString(TAG_PERIAPSIS,
                         (body.getArgPeriapsis() == null) ?
                         "" :
                         String.format("%1.2f", body.getArgPeriapsis()));

        CelestialCommonEditDialogFragment df = new CelestialCommonEditDialogFragment();
        df.setArguments(bundle);

        return df;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            originalBundle = savedState = getArguments();
        } else {
            originalBundle = savedInstanceState.getBundle(TAG_ORIGINAL_BUNDLE);
            savedState = savedInstanceState;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        getDialog().setTitle("Update Details");
        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        View layout = inflater.inflate(R.layout.cb_dialog_common_layout,
                                       container,
                                       false);

        createViews(layout);
        createButtons(layout);

        if (savedState != null) {
            populateViews(savedState);
            savedState = null;
        }

        return layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnDoneListener) {
                listener = (OnDoneListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnDoneListener) {
                listener = (OnDoneListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putBundle(TAG_ORIGINAL_BUNDLE, originalBundle);

        outState.putString(TAG_DISTANCE, distanceText.getText().toString());
        outState.putString(TAG_SEMIAXIS, semiAxisText.getText().toString());
        outState.putString(TAG_PERIOD, periodText.getText().toString());
        outState.putString(TAG_ECC, eccText.getText().toString());
        outState.putString(TAG_INCLINE, inclinationText.getText().toString());
        outState.putString(TAG_PERIAPSIS, argPeriapsisText.getText().toString());
    }

    private void createButtons(View layout)
    {
        final Button cancel = (Button) layout.findViewById(R.id.CancelButton);
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        doneButton = (Button) layout.findViewById(R.id.DoneButton);
        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (listener != null) {
                    Bundle bundle = new Bundle();

                    if (!distanceText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_DISTANCE))) {
                        String value = distanceText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_DISTANCE, Double.valueOf(value));
                    }

                    if (!semiAxisText.getText()
                                     .toString()
                                     .equals(originalBundle.getString(TAG_SEMIAXIS))) {
                        String value = semiAxisText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_SEMIAXIS, Double.valueOf(value));
                    }

                    if (!periodText.getText()
                                   .toString()
                                   .equals(originalBundle.getString(TAG_PERIOD))) {
                        String value = periodText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_PERIOD, Double.valueOf(value));
                    }

                    if (!eccText.getText()
                                .toString()
                                .equals(originalBundle.getString(TAG_ECC))) {
                        String value = eccText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_ECC, Double.valueOf(value));
                    }

                    if (!inclinationText.getText()
                                        .toString()
                                        .equals(originalBundle.getString(TAG_INCLINE))) {
                        String value = inclinationText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_INCLINE, Double.valueOf(value));
                    }

                    if (!argPeriapsisText.getText()
                                         .toString()
                                         .equals(originalBundle.getString(TAG_PERIAPSIS))) {
                        String value = argPeriapsisText.getText().toString();

                        if (value.isEmpty()) {
                            value = "0.0";
                        }

                        bundle.putDouble(TAG_PERIAPSIS, Double.valueOf(value));
                    }

                    listener.onDone(getTag(), bundle);
                }

                dismiss();
            }
        });
    }

    private void createViews(View layout)
    {
        distanceText = (EditText) layout.findViewById(R.id.cb_distance_value);
        semiAxisText = (EditText) layout.findViewById(R.id.cb_semi_axis_value);
        periodText = (EditText) layout.findViewById(R.id.cb_orbperiod_value);
        eccText = (EditText) layout.findViewById(R.id.cb_orbeccentricity_value);
        inclinationText = (EditText) layout.findViewById(R.id.cb_orbinclination_value);
        argPeriapsisText = (EditText) layout.findViewById(R.id.cb_argperiapsis_value);
        argPeriapsisText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doneButton.callOnClick();
                }

                return false;
            }
        });
    }

    private void populateViews(Bundle bundle)
    {
        distanceText.setText(bundle.getString(TAG_DISTANCE));
        semiAxisText.setText(bundle.getString(TAG_SEMIAXIS));
        periodText.setText(bundle.getString(TAG_PERIOD));
        eccText.setText(bundle.getString(TAG_ECC));
        inclinationText.setText(bundle.getString(TAG_INCLINE));
        argPeriapsisText.setText(bundle.getString(TAG_PERIAPSIS));
    }
}
