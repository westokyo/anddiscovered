package com.fussyware.AndDiscovered.dialog;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbContract;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by wes on 1/20/16.
 */
public class DateSystemPickerDialogFragment extends DialogFragment
{
    public interface OnDoneListener
    {
        void onDone(String tag, String system, Date date);
        void onCancel(String tag);
    }

    private final static String TAG = DateSystemPickerDialogFragment.class.getSimpleName();

    private final CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();

    private OnDoneListener listener;

    private Button doneButton;
    private Button backButton;

    private DatePicker datePicker;
    private ListView listView;

    private SystemAdapter adapter;

    private Date minDate;
    private Date maxDate;
    private Date currentDate;

    public static DateSystemPickerDialogFragment newInstance(Date minDateTime,
                                                             Date maxDateTime,
                                                             Date startDateTime)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("min_date", minDateTime);
        bundle.putSerializable("max_date", maxDateTime);
        bundle.putSerializable("start_date", startDateTime);

        DateSystemPickerDialogFragment fragment = new DateSystemPickerDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnDoneListener) {
                listener = (OnDoneListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnDoneListener) {
                listener = (OnDoneListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle == null) {
                minDate = new Date(0);
                maxDate = new Date();
                currentDate = maxDate;
            } else {
                minDate = (Date) bundle.getSerializable("min_date");
                maxDate = (Date) bundle.getSerializable("max_date");
                currentDate = (Date) bundle.getSerializable("start_date");
            }
        } else {
            minDate = (Date) savedInstanceState.getSerializable("min_date");
            maxDate = (Date) savedInstanceState.getSerializable("max_date");
            currentDate = (Date) savedInstanceState.getSerializable("current_date");
        }

        adapter = new SystemAdapter();
        adapter.setTime(currentDate);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.stat_system_picker_dialog_layout,
                                       container,
                                       false);

        getDialog().setTitle(R.string.date_picker_dialog_title);


        createButtonViews(layout);
        createMainViews(layout);

        return layout;
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();

        adapter.close();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putSerializable("min_date", minDate);
        outState.putSerializable("max_date", maxDate);
        outState.putSerializable("current_date", currentDate);
    }

    private void createButtonViews(View layout)
    {
        Button cancelButton = (Button) layout.findViewById(R.id.CancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                listener.onCancel(getTag());
                dismiss();
            }
        });

        backButton = (Button) layout.findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                backButton.setEnabled(false);

                doneButton.setEnabled(true);
                doneButton.setText(R.string.dialog_next);

                listView.setVisibility(View.GONE);
                datePicker.setVisibility(View.VISIBLE);
            }
        });

        doneButton = (Button) layout.findViewById(R.id.DoneButton);
        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (doneButton.getText().toString().equals(getString(R.string.dialog_next))) {
                    Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                                                              datePicker.getMonth(),
                                                              datePicker.getDayOfMonth());
                    currentDate = calendar.getTime();
                    adapter.setTime(currentDate);

                    datePicker.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);

                    doneButton.setText(R.string.dialog_done);
                    backButton.setEnabled(true);
                    doneButton.setEnabled(false);
                } else {
                    ItemInfo info = adapter.getActiveItem();
                    listener.onDone(getTag(), info.system, info.date);
                    dismiss();
                }
            }
        });
    }

    private void createMainViews(View layout)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);

        datePicker = (DatePicker) layout.findViewById(R.id.stat_date_picker);
        datePicker.setMinDate(minDate.getTime());
        datePicker.setMaxDate(maxDate.getTime());
        datePicker.init(calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH),
                        null);

        listView = (ListView) layout.findViewById(R.id.stat_system_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                adapter.setActive(adapter.getItem(position), view);
                doneButton.setEnabled(true);
            }
        });
    }

    private static class ItemInfo implements Parcelable
    {
        public static final Creator<ItemInfo> CREATOR = new Creator<ItemInfo>()
        {
            @Override
            public ItemInfo createFromParcel(Parcel in)
            {
                return new ItemInfo(in);
            }

            @Override
            public ItemInfo[] newArray(int size)
            {
                return new ItemInfo[size];
            }
        };

        final String system;
        final Date date;

        ItemInfo(String system, Date date)
        {
            this.system = system;
            this.date = date;
        }

        private ItemInfo(Parcel parcel)
        {
            system = parcel.readString();
            date = (Date) parcel.readSerializable();
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            ItemInfo itemInfo = (ItemInfo) o;

            return system.equals(itemInfo.system) && date.equals(itemInfo.date);
        }

        @Override
        public int hashCode()
        {
            int result = system.hashCode();
            result = 31 * result + date.hashCode();
            return result;
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeString(system);
            dest.writeSerializable(date);
        }
    }

    private class SystemAdapter extends BaseAdapter
    {
        private final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("HH:mm:ss", Locale.US);
        private final CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();

        private Cursor cursor;

        private ItemInfo activeInfo;
        private View activeView;

        public void close()
        {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }

            activeInfo = null;
            activeView = null;
        }

        public void setActive(ItemInfo info, View view)
        {
            if (activeView != null) {
                activeView.setActivated(false);
            }

            activeInfo = info;
            view.setActivated(true);
        }

        public ItemInfo getActiveItem()
        {
            return activeInfo;
        }

        public void setTime(Date date)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            Calendar startTime = new GregorianCalendar(calendar.get(Calendar.YEAR),
                                                       calendar.get(Calendar.MONTH),
                                                       calendar.get(Calendar.DAY_OF_MONTH));
            Calendar endTime = new GregorianCalendar(calendar.get(Calendar.YEAR),
                                                     calendar.get(Calendar.MONTH),
                                                     calendar.get(Calendar.DAY_OF_MONTH),
                                                     23,
                                                     59,
                                                     59);


            String[] columns = {
                    CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM,
                    CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE,
            };

            String selection = CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE + " BETWEEN ? AND ?";
            String[] selectionArgs = {
                    Long.toString(startTime.getTimeInMillis() / 1000),
                    Long.toString(endTime.getTimeInMillis() / 1000)
            };
            String orderBy = "";
            orderBy += CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE;
            orderBy += " DESC";

            if (cursor != null) {
                cursor.close();
            }

            if (activeView != null) {
                activeView.setActivated(false);
                activeView = null;
            }

            activeInfo = null;

            cursor = dbHelper.getReadableDatabase()
                             .query(CmdrDbContract.CmdrDistances.TABLE_NAME,
                                    columns,
                                    selection,
                                    selectionArgs,
                                    null,
                                    null,
                                    orderBy);

            notifyDataSetChanged();
        }

        @Override
        public int getCount()
        {
            return (cursor == null) ? 0 : cursor.getCount();
        }

        @Override
        public ItemInfo getItem(int position)
        {
            ItemInfo info = null;

            if ((cursor != null) && cursor.moveToPosition(position)) {
                info = new ItemInfo(cursor.getString(0), new Date(cursor.getLong(1) * 1000));
            }

            return info;
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                convertView = inflater.inflate(R.layout.stat_system_picker_dialog_list_item_layout,
                                               parent,
                                               false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            ItemInfo info = getItem(position);

            if (info == null) {
                viewHolder.systemView.setText(R.string.unknown);
                viewHolder.dateView.setText(R.string.unknown_time);
            } else {
                viewHolder.systemView.setText(info.system);
                viewHolder.dateView.setText(DATE_FORMATTER.format(info.date));

                if (activeView != null) {
                    activeView.setActivated(false);
                    activeView = null;
                }

                if (info.equals(activeInfo)) {
                    convertView.setActivated(true);
                    activeView = convertView;
                }
            }

            return convertView;
        }

        private class ViewHolder
        {
            final TextView systemView;
            final TextView dateView;

            ViewHolder(View layout)
            {
                systemView = (TextView) layout.findViewById(R.id.date_picker_system_item);
                dateView = (TextView) layout.findViewById(R.id.date_picker_date_item);
            }
        }
    }
}
