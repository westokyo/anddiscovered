 package com.fussyware.AndDiscovered;

 import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fussyware.AndDiscovered.activity.BaseActivity;
import com.fussyware.AndDiscovered.activity.SystemInfoActivity;
import com.fussyware.AndDiscovered.adapter.FilterListAdapter;
import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.fragment.FilterListFragment;
import com.fussyware.AndDiscovered.fragment.FragmentTag;
import com.fussyware.AndDiscovered.fragment.SystemListFragment;
import com.fussyware.AndDiscovered.preference.PreferenceTag;
import com.fussyware.AndDiscovered.service.EDProxyWebsocketService;

import java.util.ArrayList;
import java.util.HashMap;

 public class AndDiscoveredActivity
         extends BaseActivity
         implements SystemListFragment.SystemSelectedListener,
                    FilterListFragment.OnSystemClickListener
{
    private static final String LOG_NAME = AndDiscoveredActivity.class.getSimpleName();
    private static final int RESULT_SYSTEM_INFO = 101;

    private CmdrDbHelper dbHelper;
    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;

    private SystemListFragment listFragment;
    private FilterListFragment filterFragment;

    private boolean isFilterShowing;
    private FilterDialog filterDialog;

    private OnUpgradeReceiver upgradeReceiver;

    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        dbHelper = CmdrDbHelper.getInstance();

        prefListener = new SharedPreferences.OnSharedPreferenceChangeListener()
        {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
            {
                Intent intent = new Intent(getApplicationContext(),
                                           EDProxyWebsocketService.class);

                switch (key) {
                    case PreferenceTag.DISCOVERY_TTL: {
                        if (sharedPreferences.getString(key, "").isEmpty()) {
                            boolean enabled = sharedPreferences.getBoolean(PreferenceTag.DISCOVERY_ENABLED,
                                                                           true);

                            stopService(intent);

                            if (enabled) {
                                startService(intent);
                            } else {
                                if (sharedPreferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
//                                    DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.no_ipaddr_title,
//                                                                                                R.string.no_ipaddr_message);
//                                    fragment.show(getFragmentManager(), FragmentTag.main_activity.no_ipaddr_dialog);
                                } else {
                                    startService(intent);
                                }
                            }
                        }

                        break;
                    }
                    case PreferenceTag.IPADDR: {
                        if (!sharedPreferences.getString(key, "").isEmpty()) {
                            stopService(intent);
                            startService(intent);
                        } else {
                            boolean enabled = sharedPreferences.getBoolean(PreferenceTag.DISCOVERY_ENABLED,
                                                                           true);

                            if (enabled) {
                                startService(intent);
                            } else {
//                                DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.no_ipaddr_title,
//                                                                                            R.string.no_ipaddr_message);
//                                fragment.show(getFragmentManager(),
//                                              FragmentTag.main_activity.no_ipaddr_dialog);
                            }
                        }

                        break;
                    }
                    case PreferenceTag.DISCOVERY_ENABLED: {
                        boolean enabled = sharedPreferences.getBoolean(key, true);

                        if (enabled) {
                            stopService(intent);
                            startService(intent);
                        } else {
                            stopService(intent);

                            if (sharedPreferences.getString(PreferenceTag.IPADDR, "").isEmpty()) {
//                                DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.no_ipaddr_title,
//                                                                                            R.string.no_ipaddr_message);
//                                fragment.show(getFragmentManager(), FragmentTag.main_activity.no_ipaddr_dialog);
                            } else {
                                startService(intent);
                            }
                        }
                    }
                    default:
                        break;
                }
            }
        };

        preferences.registerOnSharedPreferenceChangeListener(prefListener);

        setContentView(R.layout.main);

        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int oldVersion = preferences.getInt("version_code", 3);

            onUpgrade(oldVersion, pinfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            AndLog.e(LOG_NAME, "Failed to get the package information.", e);
        }

        filterDialog = new FilterDialog();

        listFragment = (SystemListFragment) getSupportFragmentManager().findFragmentByTag(FragmentTag.system_list_fragment.tag);
        filterFragment = (FilterListFragment) getSupportFragmentManager().findFragmentByTag(FragmentTag.filter_list_fragment.tag);

        if (listFragment == null) {
            listFragment = new SystemListFragment();
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_holder, listFragment, FragmentTag.system_list_fragment.tag)
                    .commit();
        } else {
            isFilterShowing = savedInstanceState.getBoolean("is_filter_showing");
        }

        upgradeReceiver = new OnUpgradeReceiver();
        registerReceiver(upgradeReceiver,
                         new IntentFilter("android.intent.action.MY_PACKAGE_REPLACED"));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_filter_showing", isFilterShowing);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        unregisterReceiver(upgradeReceiver);

        preferences.unregisterOnSharedPreferenceChangeListener(prefListener);
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        if (intent.hasExtra("modified_list")) {
            String[] list = intent.getStringArrayExtra("modified_list");
            for (String item : list) {
                AndLog.d(LOG_NAME, "Modified: " + item);
            }

            listFragment.invalidate();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if ((resultCode == RESULT_OK) && (requestCode == RESULT_SYSTEM_INFO)) {
            String[] list = data.getStringArrayExtra("modified_list");
            for (String item : list) {
                AndLog.d(LOG_NAME, "Modified: " + item);
            }

            listFragment.invalidate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.search_icon_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if ((item.getItemId() == android.R.id.home) &&
            drawerLayout.isDrawerVisible(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);

            return true;
        } else {
            switch (item.getItemId()) {
                case R.id.action_search:
                    if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                        drawerLayout.closeDrawer(GravityCompat.END);
                    } else {
                        drawerLayout.openDrawer(GravityCompat.END);
                    }
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        if (!onDrawerBackPressed()) {
            if (drawerLayout.isDrawerVisible(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END);
            } else {
                if (isFilterShowing) {
                    isFilterShowing = false;
                    getSupportFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    @Override
    protected void onNewSystemEntered(SystemInfo system, DistanceInfo distance)
    {
        super.onNewSystemEntered(system, distance);

        listFragment.invalidate();
    }

    @Override
    protected void onUpdateSystems()
    {
        super.onUpdateSystems();

        listFragment.invalidate();
    }

    @Override
    public void onSystemSelected(String system, View view, int position, long id)
    {
        AndLog.d(LOG_NAME, "System Selected: " + system);

        CmdrSystemInfo info = dbHelper.getSystem(system);

        Intent intent = new Intent(getApplicationContext(), SystemInfoActivity.class);
        intent.putExtra("system", info);

        startActivityForResult(intent, RESULT_SYSTEM_INFO);
    }

    @Override
    public void onSystemClick(SystemInfo system)
    {
        Intent intent = new Intent(getApplicationContext(), SystemInfoActivity.class);
        intent.putExtra("system", system);

        startActivityForResult(intent, RESULT_SYSTEM_INFO);
    }

    private class OnUpgradeReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            switch (intent.getAction()) {
                case Intent.ACTION_MY_PACKAGE_REPLACED:
                    try {
                        PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        int oldVersion = preferences.getInt("version_code", 3);

                        onUpgrade(oldVersion, pinfo.versionCode);
                    } catch (PackageManager.NameNotFoundException e) {
                        AndLog.e(LOG_NAME, "Failed to get the package information.", e);
                    }

                    break;
                default:
                    break;
            }
        }
    }

    private void onUpgrade(int oldVersion, int newVersion)
    {
        AndLog.d(LOG_NAME,
                 "oldVersion: " +
                 Integer.toString(oldVersion) +
                 ", newVersion: " +
                 Integer.toString(newVersion));

        if (oldVersion != newVersion) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                                               .edit();

            switch (oldVersion) {
                case 2:
                case 3:
                    editor.putString(PreferenceTag.CMDR_NAME, dbHelper.getCmdrName());
                    editor.putInt(PreferenceTag.DISPLAY_DAYS, dbHelper.getDisplayDays());
                    editor.putString(PreferenceTag.ROUTE, dbHelper.getRouteDestination());
                    break;
                default:
                    break;
            }

            editor.putInt("version_code", newVersion);
            editor.apply();
        }
    }

    private class FilterDialog
    {
        private static final int SYSTEM_NAME  = (1 << 0);
        private static final int PLANET_TYPES = (1 << 1);
        private static final int STAR_TYPES   = (1 << 2);

        private static final String ALL_SQL = "SELECT CmdrSystems.* FROM CmdrSystems, (SELECT CmdrSystems._id FROM CmdrSystems, Stars, (SELECT SystemId, BodyId FROM Satellites WHERE SystemId IN (SELECT _id FROM CmdrSystems WHERE System LIKE ?) AND CategoryId=0) AS Banana WHERE Stars._id=Banana.BodyId AND Stars.Type IN (STAR_LIST) AND Banana.SystemId=CmdrSystems._id GROUP BY Banana.SystemId) AS StarInfo, (SELECT CmdrSystems._id FROM CmdrSystems, Planets, (SELECT SystemId, BodyId FROM Satellites WHERE SystemId IN (SELECT _id FROM CmdrSystems WHERE System LIKE ?) AND CategoryId=1) AS Banana WHERE Planets._id=Banana.BodyId AND Planets.Type IN (PLANET_LIST) AND Banana.SystemId=CmdrSystems._id GROUP BY Banana.SystemId) AS PlanetInfo WHERE CmdrSystems._id=StarInfo._id AND CmdrSystems._id=PlanetInfo._id ORDER BY CmdrSystems._id DESC";
        private static final String NAME_PLANET_SQL = "SELECT CmdrSystems.* FROM CmdrSystems, Planets, (SELECT SystemId, BodyId FROM Satellites WHERE SystemId IN (SELECT _id FROM CmdrSystems WHERE System LIKE ?) AND CategoryId=1) AS Banana WHERE Planets._id=Banana.BodyId AND Planets.Type IN (PLANET_LIST) AND Banana.SystemId=CmdrSystems._id GROUP BY Banana.SystemId ORDER BY CmdrSystems._id DESC";
        private static final String NAME_STAR_SQL = "SELECT CmdrSystems.* FROM CmdrSystems, Stars, (SELECT SystemId, BodyId FROM Satellites WHERE SystemId IN (SELECT _id FROM CmdrSystems WHERE System LIKE ?) AND CategoryId=0) AS Banana WHERE Stars._id=Banana.BodyId AND Stars.Type IN (STAR_LIST) AND Banana.SystemId=CmdrSystems._id GROUP BY Banana.SystemId ORDER BY CmdrSystems._id DESC";
        private static final String NAME_SQL = "SELECT * FROM CmdrSystems WHERE System LIKE ? ORDER BY CmdrSystems._id DESC";
        private static final String PLANET_STAR_SQL = "SELECT CmdrSystems.* FROM CmdrSystems, (SELECT * FROM CmdrSystems, Planets, Satellites WHERE CmdrSystems._id=Satellites.SystemId AND Satellites.CategoryId=1 AND Planets._id=BodyId AND Planets.Type IN (PLANET_LIST)) AS PlanetInfo, (SELECT * FROM CmdrSystems, Stars, Satellites WHERE CmdrSystems._id=Satellites.SystemId AND Satellites.CategoryId=0 AND Stars._id=BodyId AND Stars.Type IN (STAR_LIST)) AS StarInfo WHERE CmdrSystems._id=StarInfo._id AND CmdrSystems._id=PlanetInfo._id GROUP BY CmdrSystems._id ORDER BY CmdrSystems._id DESC";
        private static final String PLANET_SQL = "SELECT CmdrSystems.* FROM CmdrSystems, Planets, Satellites WHERE CmdrSystems._id=Satellites.SystemId AND Satellites.CategoryId=1 AND Planets._id=Satellites.BodyId AND Planets.Type IN (PLANET_LIST) GROUP BY CmdrSystems._id ORDER BY CmdrSystems._id DESC";
        private static final String STAR_SQL = "SELECT CmdrSystems.* FROM CmdrSystems, Stars, Satellites WHERE CmdrSystems._id=Satellites.SystemId AND Satellites.CategoryId=0 AND Stars._id=Satellites.BodyId AND Stars.Type IN (STAR_LIST) GROUP BY CmdrSystems._id ORDER BY CmdrSystems._id DESC";

        private EditText systemNameText;

        private final ArrayList<Integer> planetCheckboxList = new ArrayList<>();
        private final ArrayList<Integer> starCheckboxList = new ArrayList<>();

        private final HashMap<Integer, String> sqlMap = new HashMap<>();

        FilterDialog()
        {
            sqlMap.put((SYSTEM_NAME  | PLANET_TYPES | STAR_TYPES), ALL_SQL);
            sqlMap.put((SYSTEM_NAME  | PLANET_TYPES), NAME_PLANET_SQL);
            sqlMap.put((SYSTEM_NAME  | STAR_TYPES), NAME_STAR_SQL);
            sqlMap.put((PLANET_TYPES | STAR_TYPES), PLANET_STAR_SQL);
            sqlMap.put(SYSTEM_NAME, NAME_SQL);
            sqlMap.put(PLANET_TYPES, PLANET_SQL);
            sqlMap.put(STAR_TYPES, STAR_SQL);

            systemNameText = (EditText) findViewById(R.id.filter_sysname_text);

            LayoutInflater inflater = getLayoutInflater();

            final LinearLayout planetList = (LinearLayout) findViewById(R.id.filter_planet_type_layout);

            TypedArray planetNames = getResources().obtainTypedArray(R.array.planet_types);
            for (int i = 0; i < planetNames.length(); i++) {
                View layout = inflater.inflate(R.layout.filter_cb_checkbox_layout,
                                               planetList,
                                               false);

                TextView tv = (TextView) layout.findViewById(R.id.filter_checkbox_name_text);
                tv.setText(planetNames.getString(i));

                CheckBox box = (CheckBox) layout.findViewById(R.id.filter_checkbox);
                box.setTag(i);

                layout.setTag(box);
                layout.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CheckBox box = (CheckBox) v.getTag();
                        Integer i = (Integer) box.getTag();
                        boolean listContains = planetCheckboxList.contains(i);

                        box.toggle();

                        if (box.isChecked()) {
                            if (!listContains) {
                                planetCheckboxList.add(i);
                            }
                        } else if (listContains) {
                            planetCheckboxList.remove(i);
                        }
                    }
                });

                planetList.addView(layout);
            }
            planetNames.recycle();

            final LinearLayout starLayout = (LinearLayout) findViewById(R.id.filter_star_type_layout);

            TypedArray starNames = getResources().obtainTypedArray(R.array.star_types);
            for (int i = 0; i < starNames.length(); i++) {
                View layout = inflater.inflate(R.layout.filter_cb_checkbox_layout,
                                               starLayout,
                                               false);

                TextView tv = (TextView) layout.findViewById(R.id.filter_checkbox_name_text);
                tv.setText(starNames.getString(i));

                CheckBox box = (CheckBox) layout.findViewById(R.id.filter_checkbox);
                box.setTag(i);

                layout.setTag(box);
                layout.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        CheckBox box = (CheckBox) v.getTag();
                        Integer i = (Integer) box.getTag();
                        boolean listContains = starCheckboxList.contains(i);

                        box.toggle();

                        if (box.isChecked()) {
                            if (!listContains) {
                                starCheckboxList.add(i);
                            }
                        } else if (listContains) {
                            starCheckboxList.remove(i);
                        }
                    }
                });

                starLayout.addView(layout);
            }
            starNames.recycle();

            Button searchButton = (Button) findViewById(R.id.filter_search_button);
            searchButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    performSearch();
                }
            });
        }

        /** This is going to get ugly. I do not know of a "simple easy" way to
         * do any of this. The conditions are just to 'different'.
         */
        private void performSearch()
        {
            String systemName = systemNameText.getText().toString() + "%";

            boolean nameEmpty = systemName.isEmpty();
            boolean planetEmpty = planetCheckboxList.isEmpty();
            boolean starEmpty = starCheckboxList.isEmpty();

            Cursor cursor = null;

            int score = 0;
            int argListSize = 0;

            if (!systemNameText.getText().toString().isEmpty()) {
                score |= SYSTEM_NAME;
                argListSize++;
            }

            if (!planetCheckboxList.isEmpty()) {
                score |= PLANET_TYPES;
            }

            if (!starCheckboxList.isEmpty()) {
                score |= STAR_TYPES;
            }

            if ((score & (SYSTEM_NAME | PLANET_TYPES | STAR_TYPES)) == (SYSTEM_NAME | PLANET_TYPES | STAR_TYPES)) {
                // We use the system name twice in this situation.
                argListSize++;
            }

            if (score == 0) {
                AndLog.d(LOG_NAME, "Score is zero. WTF?");
                if (isFilterShowing) {
                    isFilterShowing = false;
                    drawerLayout.closeDrawer(GravityCompat.END);
                    getSupportFragmentManager().popBackStack();
                }
            } else {
                String sql = sqlMap.get(score);
                String[] sqlArgs = new String[argListSize];

                if ((score & SYSTEM_NAME) != 0) {
                    sqlArgs[0] = systemName;

                    if ((score & (PLANET_TYPES | STAR_TYPES)) == (PLANET_TYPES | STAR_TYPES)) {
                        sqlArgs[1] = systemName;
                    }
                }

                if ((score & PLANET_TYPES) != 0) {
                    String planets = "";

                    for (int i = 0; i < planetCheckboxList.size(); i++) {
                        planets += planetCheckboxList.get(i).toString();

                        if ((i + 1) < planetCheckboxList.size()) {
                            planets += ",";
                        }
                    }

                    sql = sql.replace("PLANET_LIST", planets);
                }

                if ((score & STAR_TYPES) != 0) {
                    String stars = "";

                    for (int i = 0; i < starCheckboxList.size(); i++) {
                        stars += starCheckboxList.get(i).toString();

                        if ((i + 1) < starCheckboxList.size()) {
                            stars += ",";
                        }
                    }

                    sql = sql.replace("STAR_LIST", stars);
                }

                drawerLayout.closeDrawer(GravityCompat.END);

                FilterListAdapter.FilterCursor filterCursor = new FilterListAdapter.FilterCursor(sql,
                                                                                                 sqlArgs);
                if (isFilterShowing) {
                    filterFragment.setFilter(filterCursor);
                } else {
                    if (filterFragment == null) {
                        filterFragment = new FilterListFragment();
                    }

                    isFilterShowing = true;
                    filterFragment.setArguments(FilterListFragment.createArguments(filterCursor));

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_holder,
                                     filterFragment,
                                     FragmentTag.filter_list_fragment.tag)
                            .addToBackStack(FilterListFragment.class.getCanonicalName())
                            .commit();
                }
            }

//            Log.d(LOG_NAME, "sql: " + sql);
//            for (int i = 0; i < sqlArgs.length; i++) {
//                Log.d(LOG_NAME, "args: " + sqlArgs[i]);
//            }

//            cursor = dbHelper.getReadableDatabase().rawQuery(sql, sqlArgs);
//
//            try {
//                while (cursor.moveToNext()) {
//                    Log.d(LOG_NAME, String.format("%d, %s", cursor.getLong(0), cursor.getString(1)));
//                }
//            } finally {
//                cursor.close();
//            }
        }
    }
}
