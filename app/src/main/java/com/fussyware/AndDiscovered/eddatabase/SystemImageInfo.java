package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.StarType;

import java.util.UnknownFormatConversionException;

/**
 * Created by wes on 9/16/15.
 */
public class SystemImageInfo implements Parcelable
{
    private static final String LOG_NAME = SystemImageInfo.class.getSimpleName();

    private final SQLiteDatabase db;
    private final long id;
    private final long systemId;
    private final String system;
    private String celestialName;
    private StarType starType = null;
    private PlanetType planetType = null;
    private double distance;
    private final String imagePath;

    private final Object mutex = new Object();

    SystemImageInfo(SQLiteDatabase db,
                    long id,
                    long systemId,
                    String systemName,
                    String celestialName,
                    String celestialType,
                    Double distance,
                    String imagePath)
    {
        this.db = db;
        this.id = id;
        this.systemId = systemId;
        this.system = systemName;
        this.imagePath = imagePath;

        this.celestialName = (celestialName == null) ? "" : celestialName;
        this.distance = (distance == null) ? 0.0 : distance;

        if (celestialType != null) {
            if (celestialType.startsWith("star_")) {
                starType = StarType.getStarType(celestialType.substring("star_".length()));
            } else if (celestialType.startsWith("planet_")) {
                planetType = PlanetType.getPlanetType(celestialType.substring("planet_".length()));
            }
        }
    }

    private SystemImageInfo(Parcel parcel)
    {
        db = CmdrDbHelper.getInstance().getWritableDatabase();

        id = parcel.readLong();
        systemId = parcel.readLong();
        system = parcel.readString();
        celestialName = parcel.readString();

        int value = parcel.readInt();
        if (value != -1) {
            starType = StarType.getStarType(value);
        }

        value = parcel.readInt();
        if (value != -1) {
            planetType = PlanetType.getPlanetType(value);
        }

        distance = parcel.readDouble();
        imagePath = parcel.readString();
    }

    public long getId()
    {
        return id;
    }

    public String getSystem()
    {
        return system;
    }

    public String getImagePath()
    {
        return imagePath;
    }

    public double getDistance()
    {
        synchronized (mutex) {
            return distance;
        }
    }

    public void setDistance(double distance)
    {
        synchronized (mutex) {
            if (this.distance != distance) {
                String selection = CmdrDbContract.Images._ID + "=?";
                String[] selectionArgs = { Long.toString(id) };

                ContentValues values = new ContentValues();
                values.put(CmdrDbContract.Images.COLUMN_NAME_DISTANCE_FROM_MAINSTAR, distance);

                db.update(CmdrDbContract.Images.TABLE_NAME,
                          values,
                          selection,
                          selectionArgs);

                this.distance = distance;
            }
        }
    }

    public String getCelestialName()
    {
        synchronized (mutex) {
            return celestialName;
        }
    }

    public void setCelestialName(String name)
    {
        if (name != null) {
            synchronized (mutex) {
                if (!name.equalsIgnoreCase(celestialName)) {
                    String selection = CmdrDbContract.Images._ID + "=?";
                    String[] selectionArgs = {Long.toString(id)};

                    ContentValues values = new ContentValues();
                    values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_NAME, name);

                    db.update(CmdrDbContract.Images.TABLE_NAME,
                              values,
                              selection,
                              selectionArgs);

                    this.celestialName = name;
                }
            }
        }
    }

    public boolean isStar()
    {
        synchronized (mutex) {
            return (starType != null);
        }
    }

    public boolean isPlanet()
    {
        synchronized (mutex) {
            return (planetType != null);
        }
    }

    public StarType getStarType()
    {
        synchronized (mutex) {
            if (starType == null) {
                throw new UnknownFormatConversionException("No star type specified.");
            }

            return starType;
        }
    }

    public void setStarType(StarType type)
    {
        synchronized (mutex) {
            if (this.starType != type) {
                String selection = CmdrDbContract.Images._ID + "=?";
                String[] selectionArgs = { Long.toString(id) };

                ContentValues values = new ContentValues();
                values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_TYPE, "star_" + type.toString());

                db.update(CmdrDbContract.Images.TABLE_NAME,
                          values,
                          selection,
                          selectionArgs);

                this.starType = type;
                this.planetType = null;
            }
        }
    }

    public void setPlanetType(PlanetType type)
    {
        synchronized (mutex) {
            if (this.planetType != type) {
                String selection = CmdrDbContract.Images._ID + "=?";
                String[] selectionArgs = { Long.toString(id) };

                ContentValues values = new ContentValues();
                values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_TYPE,
                           "planet_" + type.toString());

                db.update(CmdrDbContract.Images.TABLE_NAME,
                          values,
                          selection,
                          selectionArgs);

                this.planetType = type;
                this.starType = null;
            }
        }
    }

    public PlanetType getPlanetType()
    {
        synchronized (mutex) {
            if (planetType == null) {
                throw new UnknownFormatConversionException("No planet type specified.");
            }

            return planetType;
        }
    }

    public void delete()
    {
        synchronized (mutex) {
            String where = CmdrDbContract.Images._ID + "=?";
            String[] whereArgs = new String[] {Long.toString(id)};
            db.delete(CmdrDbContract.Images.TABLE_NAME, where, whereArgs);
        }
    }

    @Override
    public String toString()
    {
        return "SystemImageInfo{" +
               "id=" + id +
               ", systemId=" + systemId +
               ", system='" + system + '\'' +
               ", celestialName='" + celestialName + '\'' +
               ", starType=" + starType +
               ", planetType=" + planetType +
               ", distance=" + distance +
               ", imagePath='" + imagePath + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SystemImageInfo imageInfo = (SystemImageInfo) o;

        if (id != imageInfo.id) {
            return false;
        }
        return systemId == imageInfo.systemId && imagePath.equals(imageInfo.imagePath);

    }

    @Override
    public int hashCode()
    {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (systemId ^ (systemId >>> 32));
        result = 31 * result + imagePath.hashCode();
        return result;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(id);
        dest.writeLong(systemId);
        dest.writeString(system);
        dest.writeString(celestialName);
        dest.writeInt((starType == null) ? -1 : starType.value());
        dest.writeInt((planetType == null) ? -1 : planetType.value());
        dest.writeDouble(distance);
        dest.writeString(imagePath);
    }

    public static final Parcelable.Creator<SystemImageInfo> CREATOR = new Parcelable.Creator<SystemImageInfo>() {

        @Override
        public SystemImageInfo createFromParcel(Parcel source)
        {
            return new SystemImageInfo(source);
        }

        @Override
        public SystemImageInfo[] newArray(int size)
        {
            return new SystemImageInfo[size];
        }
    };
}
