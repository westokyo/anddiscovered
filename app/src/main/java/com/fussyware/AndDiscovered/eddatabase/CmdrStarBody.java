package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.Ring;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.celestial.StarType;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by wes on 11/17/15.
 */
public class CmdrStarBody extends StarBody
{
    public static final Creator<CmdrStarBody> CREATOR = new Creator<CmdrStarBody>()
    {
        @Override
        public CmdrStarBody createFromParcel(Parcel source)
        {
            return new CmdrStarBody(source);
        }

        @Override
        public CmdrStarBody[] newArray(int size)
        {
            return new CmdrStarBody[size];
        }
    };

    private static final Object createMutex = new Object();

    private final SQLiteDatabase db;
    private final Long id;

    CmdrStarBody(@NonNull SQLiteDatabase db,
                 @NonNull CmdrSystemInfo system,
                 @NonNull StarType type,
                 @NonNull ScanLevel scanLevel,
                 long id,
                 boolean firstDiscovered,
                 CelestialBody parent,
                 String name,
                 Double distance,
                 Double age,
                 Double mass,
                 Double radius,
                 Double surfaceTemp,
                 Double orbitalPeriod,
                 Double semiMajorAxis,
                 Double orbitalEccentricity,
                 Double orbitalInclination,
                 Double argPeriapsis)
    {
        super(system,
              type,
              scanLevel,
              firstDiscovered,
              parent,
              name,
              distance,
              age,
              mass,
              radius,
              surfaceTemp,
              orbitalPeriod,
              semiMajorAxis,
              orbitalEccentricity,
              orbitalInclination,
              argPeriapsis);

        if (db.isReadOnly()) {
            throw new InvalidParameterException("A valid writable database must be provided.");
        }

        this.db = db;
        this.id = id;
    }

    private CmdrStarBody(@NonNull SQLiteDatabase db,
                         @NonNull CmdrSystemInfo system,
                         CelestialBody parent,
                         long id,
                         @NonNull StarType type)
    {
        super(system, type, parent);

        this.db = db;
        this.id = id;
    }

    private CmdrStarBody(Parcel parcel)
    {
        super(parcel);

        db = CmdrDbHelper.getInstance().getWritableDatabase();
        id = parcel.readLong();
    }

    public static StarBody create(@NonNull CmdrSystemInfo system,
                                  @NonNull StarType type)
    {
        return CmdrStarBody.create(system, type, null);
    }

    public static StarBody create(@NonNull CmdrSystemInfo system,
                                  @NonNull StarType type,
                                  CelestialBody parent)
    {
        if (type == StarType.Unknown) {
            throw new InvalidParameterException("A valid star type must be specified during creation.");
        }

        synchronized (createMutex) {
            SQLiteDatabase db = CmdrDbHelper.getInstance().getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(CmdrDbContract.Stars.COLUMN_NAME_TYPE, type.value());
            values.put(CmdrDbContract.Stars.COLUMN_NAME_SCANLEVEL, ScanLevel.Level_2.value);
            values.put(CmdrDbContract.Stars.COLUMN_NAME_FIRST_DISCOVERED, 0);

            long id = db.insert(CmdrDbContract.Stars.TABLE_NAME, null, values);

            values.clear();

            values.put(CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID, system.getRowId());
            values.put(CmdrDbContract.Satellites.COLUMN_NAME_BODYID, id);
            values.put(CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
                       SatelliteCategory.Star.value);

            if (parent != null) {
                long _id = -1;

                if (parent.getId() instanceof Long) {
                    _id = (Long) parent.getId();
                }

                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTID, _id);
                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID,
                           parent.getSatelliteCategory().value);
            }

            db.insert(CmdrDbContract.Satellites.TABLE_NAME, null, values);

            return new CmdrStarBody(db, system, parent, id, type);
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        CmdrStarBody starBody = (CmdrStarBody) o;

        return getId().equals(starBody.getId());

    }

    @Override
    public int hashCode()
    {
        int result = super.hashCode();
        result = 31 * result + getId().hashCode();
        return result;
    }

    @NonNull
    @Override
    public Object getId()
    {
        return id;
    }

    @Override
    public List<CelestialBody> getSatellites()
    {
        return CmdrDbHelper.getInstance()
                           .getSatellitesOf((CmdrSystemInfo) getSystem(), this);
    }

    @Override
    public boolean hasSatellites()
    {
        return CmdrDbHelper.getInstance()
                           .hasSatellites(((CmdrSystemInfo) getSystem()).getRowId(),
                                          id,
                                          getSatelliteCategory());
    }

    @Override
    public boolean hasRings()
    {
        return CmdrDbHelper.getInstance().hasRings(this);
    }

    @Override
    public List<Ring> getRings()
    {
        return CmdrDbHelper.getInstance().getRingsOf(this);
    }

    @Override
    public void add(int level)
    {
        CmdrRing.create(this, level);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeLong(id);
    }

    @Override
    public void delete()
    {
        synchronized (createMutex) {
            List<CelestialBody> bodies = getSatellites();

            /** First delete all of this Bodies Satellites. */
            for (CelestialBody body : bodies) {
                body.delete();
            }

            /** Now delete this Satellite. */
            Long satId = CmdrDbHelper.getInstance()
                                     .getSatelliteId(((CmdrSystemInfo) getSystem()).getRowId(),
                                                     id,
                                                     getSatelliteCategory());

            if (satId != -1) {
                String selection = CmdrDbContract.Satellites._ID + "=?";
                String[] selectionArgs = { satId.toString() };

                db.delete(CmdrDbContract.Satellites.TABLE_NAME, selection, selectionArgs);

                selection = CmdrDbContract.Stars._ID + "=?";
                selectionArgs = new String[] { id.toString() };

                db.delete(CmdrDbContract.Stars.TABLE_NAME, selection, selectionArgs);
            }
        }
    }

    @Override
    protected void updateFirstDisocovered(boolean firstDiscovered)
    {
        ContentValues values = new ContentValues();

        values.put(CmdrDbContract.Stars.COLUMN_NAME_FIRST_DISCOVERED, firstDiscovered);
        write(values);
    }

    @Override
    protected void updateParent(CelestialBody body)
    {
        Long satId = CmdrDbHelper.getInstance()
                                 .getSatelliteId(((CmdrSystemInfo) getSystem()).getRowId(),
                                                 id,
                                                 getSatelliteCategory());

        if (satId != -1L) {
            ContentValues values = new ContentValues();
            if (parent == null) {
                values.putNull(CmdrDbContract.Satellites.COLUMN_NAME_PARENTID);
                values.putNull(CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID);
            } else {
                long _id = -1;

                if (parent.getId() instanceof Long) {
                    _id = (Long) parent.getId();
                }

                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTID, _id);
                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID,
                           parent.getSatelliteCategory().value);
            }

            String selection = CmdrDbContract.Satellites._ID + "=?";
            String[] selectionArgs = { satId.toString() };

            db.update(CmdrDbContract.Satellites.TABLE_NAME, values, selection, selectionArgs);
        }
    }

    @Override
    protected void updateAge(double age)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_AGE, age);

        write(values);
    }

    @Override
    protected void updateMass(double mass)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_MASS, mass);

        write(values);
    }

    @Override
    protected void updateRadius(double radius)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_RADIUS, radius);

        write(values);
    }

    @Override
    protected void updateSurfaceTemp(double surfaceTemp)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_SURFACETEMP, surfaceTemp);

        write(values);
    }

    @Override
    protected void updateName(String name)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_NAME, name);

        write(values);
    }

    @Override
    protected void updateDistance(double distance)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_DISTANCE, distance);

        write(values);
    }

    @Override
    protected void updateScanLevel(ScanLevel scanLevel)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_SCANLEVEL, scanLevel.value);

        write(values);
    }

    @Override
    protected void updateSemiMajorAxis(double semiMajorAxis)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_SEMIMAJORAXIS, semiMajorAxis);

        write(values);
    }

    @Override
    protected void updateOrbitalPeriod(double orbitalPeriod)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_ORBITALPERIOD, orbitalPeriod);

        write(values);
    }

    @Override
    protected void updateOrbitalEccentricity(double orbitalEccentricity)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_ORBITALECCENTRICITY, orbitalEccentricity);

        write(values);
    }

    @Override
    protected void updateOrbitalInclination(double orbitalInclination)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_ORBITALINCLINATION, orbitalInclination);

        write(values);
    }

    @Override
    protected void updateArgPeriapsis(double argPeriapsis)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Stars.COLUMN_NAME_ARGPERIAPSIS, argPeriapsis);

        write(values);
    }

    private void write(ContentValues values)
    {
        String selection = CmdrDbContract.Stars._ID + "=?";
        String[] selectionArgs = { id.toString() };

        db.update(CmdrDbContract.Stars.TABLE_NAME, values, selection, selectionArgs);
    }

}
