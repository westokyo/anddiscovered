package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wboyd on 7/7/15.
 */
public class CmdrSystemInfo extends SystemInfo
{
    public static final Parcelable.Creator<CmdrSystemInfo> CREATOR = new Creator<CmdrSystemInfo>()
    {
        @Override
        public CmdrSystemInfo createFromParcel(Parcel source)
        {
            return new CmdrSystemInfo(source);
        }

        @Override
        public CmdrSystemInfo[] newArray(int size)
        {
            return new CmdrSystemInfo[size];
        }
    };

    private final SQLiteDatabase db;
    private final long id;
    private final AtomicInteger trilat_req;
    private String note;
    private Long mainStar;

    private final Object mutex = new Object();

    CmdrSystemInfo(SQLiteDatabase db,
                   long id,
                   String system,
                   Position xyz,
                   int trilat_systems_required,
                   String note)
    {
        super(system, xyz);

        this.db = db;
        this.id = id;
        this.trilat_req = new AtomicInteger(trilat_systems_required);
        this.note = note;

        mainStar = null;
    }

    public CmdrSystemInfo(SQLiteDatabase db,
                   long id,
                   String system,
                   Position xyz,
                   int trilat_systems_required,
                   String note,
                   Long mainStar)
    {
        super(system, xyz);

        this.db = db;
        this.id = id;
        this.trilat_req = new AtomicInteger(trilat_systems_required);
        this.note = note;
        this.mainStar = mainStar;
    }

    private CmdrSystemInfo(Parcel parcel)
    {
        super(parcel);

        db = CmdrDbHelper.getInstance().getWritableDatabase();
        id = parcel.readLong();
        trilat_req = new AtomicInteger(parcel.readInt());
        note = parcel.readString();
        mainStar = (Long) parcel.readValue(null);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeLong(id);
        dest.writeInt(trilat_req.get());
        dest.writeString(note);
        dest.writeValue(mainStar);
    }

    public long getRowId()
    {
        return id;
    }

    @Override
    public void setPosition(Position xyz)
    {
        if ((xyz != null) && (getPosition() != xyz)) {
            super.setPosition(xyz);

            String selection = CmdrDbContract.CmdrSystems._ID + "=?";
            String[] selectionArgs = {Long.toString(id)};

            ContentValues values = new ContentValues();
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD, xyz.x);
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD, xyz.y);
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD, xyz.z);

            db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                      values,
                      selection,
                      selectionArgs);
        }
    }

    public int getMinSystemsRequiredForTrilat()
    {
        return trilat_req.get();
    }

    public void setMinSystemsRequiredForTrilat(int required)
    {
        int req = trilat_req.get();

        if (req != required) {
            trilat_req.set(required);
            updateTrilatDb();
        }
    }

    public void incrementMinSystemsRequiredForTrilat()
    {
        trilat_req.incrementAndGet();
        updateTrilatDb();
    }

    public String getNote()
    {
        synchronized (mutex) {
            return note;
        }
    }

    public void setNote(String note)
    {
        if (note == null) note = "";

        synchronized (mutex) {
            if (this.note.equals(note)) return;

            this.note = note;
        }

        String selection = CmdrDbContract.CmdrSystems._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_NOTE, note);

        db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                  values,
                  selection,
                  selectionArgs);
    }

    @Override
    public List<CelestialBody> getSatellites()
    {
        String[] columns = {
                CmdrDbContract.Satellites.COLUMN_NAME_BODYID,
                CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
        };

        String selection;

        selection  = CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_PARENTID + " IS NULL";

        String[] selectionArgs = { Long.toString(id) };

        ArrayList<CelestialBody> rootList = new ArrayList<>();

        Cursor cursor = db.query(CmdrDbContract.Satellites.TABLE_NAME,
                                    columns,
                                    selection,
                                    selectionArgs,
                                    null,
                                    null,
                                    null);
        try {
            while (cursor.moveToNext()) {
                CelestialBody body = null;
                SatelliteCategory satCat = SatelliteCategory.getSatelliteCategory(cursor.getInt(1));
                CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();

                switch (satCat) {
                    case Star:
                        body = dbHelper.getStarSatellite(this, null, cursor.getLong(0));
                        break;
                    case Planet:
                        body = dbHelper.getPlanetSatellite(this, null, cursor.getLong(0));
                        break;
                    case Asteroid:
                        body = dbHelper.getAsteroidSatellite(this, null, cursor.getLong(0));
                        break;
                    case Unknown:
                        break;
                }

                if (body != null) {
                    rootList.add(body);
                }
            }
        } finally {
            cursor.close();
        }

        return rootList;
    }

    public StarBody getMainStar()
    {
        synchronized (mutex) {
            return CmdrDbHelper.getInstance().getStarSatellite(this, null, mainStar);
        }
    }

    public void setMainStar(StarBody body)
    {
        synchronized (mutex) {
            String selection = CmdrDbContract.CmdrSystems._ID + "=?";
            String[] selectionArgs = { Long.toString(id) };

            ContentValues values = new ContentValues();

            if (body == null) {
                values.putNull(CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR);
                mainStar = null;
            } else {
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR, (Long) body.getId());
                mainStar = (Long) body.getId();
            }

            db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                      values,
                      selection,
                      selectionArgs);
        }
    }

    public SystemImageInfo[] getSystemImages()
    {
        synchronized (mutex) {
            return CmdrDbHelper.getInstance().getSystemImages(id, getSystem());
        }
    }

    public SystemImageInfo getSystemImage(String celestialName)
    {
        if (celestialName == null) {
            throw new NullPointerException("Celestial name may not be null");
        }

        synchronized (mutex) {
            SystemImageInfo[] imageList = CmdrDbHelper.getInstance().getSystemImages(id, getSystem());

            for (SystemImageInfo info : imageList) {
                if (celestialName.equalsIgnoreCase(info.getCelestialName())) {
                    return info;
                }
            }
        }

        return null;
    }

    public SystemImageInfo createImageInfo(String urlPath)
    {
        if ((urlPath == null) || urlPath.isEmpty()) {
            throw new IllegalArgumentException("Invalid image path name specified.");
        }

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Images.COLUMN_NAME_SYSTEMID, this.id);
        values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_NAME, "");
        values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_TYPE, "");
        values.put(CmdrDbContract.Images.COLUMN_NAME_DISTANCE_FROM_MAINSTAR, 0.0);
        values.put(CmdrDbContract.Images.COLUMN_NAME_URL_PATH, urlPath);

        long id = db.insert(CmdrDbContract.Images.TABLE_NAME,
                            null,
                            values);

        SystemImageInfo info = new SystemImageInfo(db,
                                                   id,
                                                   this.id,
                                                   getSystem(),
                                                   "",
                                                   "",
                                                   0.0,
                                                   urlPath);

        return info;
    }

    public boolean deleteImageInfo(SystemImageInfo info)
    {
        synchronized (mutex) {
            info.delete();
        }

        return true;
    }

    private void updateTrilatDb()
    {
        String selection = CmdrDbContract.CmdrSystems._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_TRILAT_SYSTEMS, trilat_req.get());

        db.update(CmdrDbContract.CmdrSystems.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }
}
