package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.Atmosphere;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.Mineral;
import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.Ring;
import com.fussyware.AndDiscovered.celestial.RingQuality;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.celestial.VolcanoType;
import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.statistics.PlanetInfo;
import com.fussyware.AndDiscovered.statistics.StarInfo;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by wes on 6/15/15.
 */
public class CmdrDbHelper extends SQLiteAssetHelper
{
    private static final String LOG_NAME = CmdrDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 50;
    private static final String DATABASE_NAME = "Commander.db";

    private static CmdrDbHelper singleton = null;

    private String cmdrName = null;
    private String route = null;
    private Date lastProxyTime = null;
    private int numDays = -1;

    private final SharedPreferences preferences;

    private final Object mutex = new Object();

    public CmdrDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static synchronized void init(Context context)
    {
        if (singleton == null) {
            singleton = new CmdrDbHelper(context);
        }
    }

    public static synchronized CmdrDbHelper getInstance()
    {
        if (null == singleton) {
            throw new IllegalStateException("CMDR Database was not initnialized.");
        }

        return singleton;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        super.onUpgrade(db, oldVersion, newVersion);

        UpgradeDatabase upgrade = null;

        switch (newVersion) {
            case 50:
                upgrade = new UpgradeDatabase50(db);
                break;
            default:
                break;
        }

        if (upgrade != null) {
            upgrade.upgrade();
        }
    }

    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);

        db.enableWriteAheadLogging();
    }

    public CmdrSystemInfo createSystem(String system)
    {
        return this.createSystem(system, null, null);
    }

    public CmdrSystemInfo createSystem(String system, Position xyz)
    {
        return this.createSystem(system, xyz, null);
    }

    public CmdrSystemInfo createSystem(String system,
                                       Position xyz,
                                       StarBody mainStar)
    {
        if ((system == null) || system.isEmpty()) {
            throw new IllegalArgumentException("Invalid system name specified.");
        }

        if (containsSystem(system)) {
            return getSystem(system);
        } else {
            ContentValues values = new ContentValues();
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM, system);
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_NOTE, "");
            values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_TRILAT_SYSTEMS,
                       CmdrDbContract.CmdrSystems.DEFAULT_NUM_TRILAT_SYSTEMS);

            if (xyz == null) {
                values.putNull(CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD);
                values.putNull(CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD);
                values.putNull(CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD);
            } else {
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD, xyz.x);
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD, xyz.y);
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD, xyz.z);
            }

            if (mainStar == null) {
                values.putNull(CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR);
            } else {
                values.put(CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR,
                           (Long) mainStar.getId());
            }

            long id = getWritableDatabase().insert(CmdrDbContract.CmdrSystems.TABLE_NAME,
                                                   null,
                                                   values);

            return new CmdrSystemInfo(getWritableDatabase(),
                                      id,
                                      system,
                                      xyz,
                                      CmdrDbContract.CmdrSystems.DEFAULT_NUM_TRILAT_SYSTEMS,
                                      "",
                                      (mainStar == null) ? null : (Long) mainStar.getId());
        }
    }

    public boolean containsSystem(String system)
    {
        String[] columns = {
                CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM,
        };

        String selection = CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM + "=?";
        String[] selectionArgs = { system };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrSystems.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            cursor.moveToFirst();
            return !cursor.isAfterLast();
        } finally {
            cursor.close();
        }
    }

    public CmdrSystemInfo getSystem(String system)
    {
        String[] columns = {
                CmdrDbContract.CmdrSystems._ID,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_TRILAT_SYSTEMS,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_NOTE,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR,
        };

        String selection = CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM + "=?";
        String[] selectionArgs = { system };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrSystems.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            CmdrSystemInfo info = null;

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                Position xyz = null;

                if (!cursor.isNull(2) && !cursor.isNull(3) && !cursor.isNull(4)) {
                    xyz = new Position(cursor.getDouble(2), cursor.getDouble(3), cursor.getDouble(4));
                }

                long id = cursor.getLong(0);
                Long mainStar = cursor.isNull(7) ? null : cursor.getLong(7);

                info = new CmdrSystemInfo(getWritableDatabase(),
                                          id,
                                          cursor.getString(1),
                                          xyz,
                                          cursor.getInt(5),
                                          cursor.getString(6),
                                          mainStar);
            }

            return info;
        } finally {
            cursor.close();
        }
    }

    public List<CmdrSystemInfo> getSystemsWithPosition()
    {
        return getSystemsWithPosition(0);
    }

    public List<CmdrSystemInfo> getSystemsWithPosition(int limit)
    {
        String[] columns = {
                CmdrDbContract.CmdrSystems._ID,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_TRILAT_SYSTEMS,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_NOTE,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR,
        };

        String selection = CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD + " IS NOT NULL";
        String orderBy = CmdrDbContract.CmdrSystems._ID + " DESC";
        String limitCount = (limit > 0) ? Integer.toString(limit) : null;

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrSystems.TABLE_NAME,
                columns, selection, null,
                null, null, orderBy, limitCount);

        ArrayList<CmdrSystemInfo> list = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                Position position = new Position(cursor.getDouble(2),
                                                 cursor.getDouble(3),
                                                 cursor.getDouble(4));

                long id = cursor.getLong(0);
                String system = cursor.getString(1);
                Long mainStar = cursor.isNull(7) ? null : cursor.getLong(7);

                list.add(new CmdrSystemInfo(getWritableDatabase(),
                                            id,
                                            system,
                                            position,
                                            cursor.getInt(5),
                                            cursor.getString(6),
                                            mainStar));
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    public List<CmdrSystemInfo> getSystemsWithNoPosition()
    {
        String[] columns = {
                CmdrDbContract.CmdrSystems._ID,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_YCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_ZCOORD,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_TRILAT_SYSTEMS,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_NOTE,
                CmdrDbContract.CmdrSystems.COLUMN_NAME_MAIN_STAR,
        };

        String selection = CmdrDbContract.CmdrSystems.COLUMN_NAME_XCOORD + " IS NULL";

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrSystems.TABLE_NAME,
                                                    columns, selection, null,
                                                    null, null, null);

        ArrayList<CmdrSystemInfo> list = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                long id = cursor.getLong(0);
                String system = cursor.getString(1);
                Long mainStar = cursor.isNull(7) ? null : cursor.getLong(7);

                list.add(new CmdrSystemInfo(getWritableDatabase(),
                                            id,
                                            system,
                                            null,
                                            cursor.getInt(5),
                                            cursor.getString(6),
                                            mainStar));
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    public SystemImageInfo createImageInfo(String system, String imagePath)
    {
        CmdrSystemInfo systemInfo = getSystem(system);

        if ((system == null) || system.isEmpty()) {
            throw new IllegalArgumentException("Invalid system name specified.");
        }

        if ((imagePath == null) || imagePath.isEmpty()) {
            throw new IllegalArgumentException("Invalid image path name specified.");
        }

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Images.COLUMN_NAME_SYSTEMID, systemInfo.getRowId());
        values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_NAME, "");
        values.put(CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_TYPE, "");
        values.put(CmdrDbContract.Images.COLUMN_NAME_DISTANCE_FROM_MAINSTAR, 0.0);
        values.put(CmdrDbContract.Images.COLUMN_NAME_URL_PATH, imagePath);

        long id = getWritableDatabase().insert(CmdrDbContract.Images.TABLE_NAME,
                                               null,
                                               values);

        return new SystemImageInfo(getWritableDatabase(),
                                   id,
                                   systemInfo.getRowId(),
                                   system,
                                   "",
                                   "",
                                   0.0,
                                   imagePath);
    }

    public CmdrDistanceInfo createDistance(String from, Date logDate)
    {
        return createDistance(from, null, null, logDate);
    }

    public CmdrDistanceInfo createDistance(String from, String to, Double distance, Date logDate)
    {
        if ((from == null) || from.isEmpty()) {
            throw new IllegalArgumentException("Invalid beginning system specified.");
        }

        if (logDate == null) {
            throw new IllegalArgumentException("Invalid log entry date specified.");
        }

        if (distance == null) {
            distance = 0.0;
        }

        distance = (distance < 0.0) ? -distance : distance;

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM, from);
        values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE, distance);
        values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE, logDate.getTime() / 1000);
        values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED, false);

        if ((to != null) && !to.isEmpty()) {
            values.put(CmdrDbContract.CmdrDistances.COLUMN_NAME_TO, to);
        } else {
            values.putNull(CmdrDbContract.CmdrDistances.COLUMN_NAME_TO);
        }

        long id = getWritableDatabase().insert(CmdrDbContract.CmdrDistances.TABLE_NAME,
                                               null,
                                               values);

        return new CmdrDistanceInfo(getWritableDatabase(),
                                id,
                                from,
                                to,
                                distance,
                                logDate,
                                false,
                                false);
    }

    public CmdrDistanceInfo createReferenceDistance(String from, String to, Double distance)
    {
        if ((from == null) || from.isEmpty()) {
            throw new IllegalArgumentException("Invalid beginning system specified.");
        }

        if ((to == null) || to.isEmpty()) {
            throw new IllegalArgumentException("Invalid ending system specified.");
        }

        if (distance == null) {
            distance = 0.0;
        }

        distance = (distance < 0.0) ? -distance : distance;

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_FROM, from);
        values.put(CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_DISTANCE, distance);
        values.put(CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_TO, to);

        long id = getWritableDatabase().insert(CmdrDbContract.CmdrReferenceDistances.TABLE_NAME,
                                               null,
                                               values);

        return new CmdrDistanceInfo(getWritableDatabase(),
                id,
                from,
                to,
                distance,
                null,
                false,
                true);
    }

    public CmdrDistanceInfo getDistance(String s1, String s2)
    {
        String[] columns = {
                CmdrDbContract.CmdrDistances._ID,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED
        };

        String selection = CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM + " IN (?,?) AND " +
                CmdrDbContract.CmdrDistances.COLUMN_NAME_TO + " IN (?,?)";
        String[] selectionArgs = { s1, s2, s1, s2 };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrDistances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            CmdrDistanceInfo info = null;

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                Date creationDate = new Date(cursor.getLong(4) * 1000);
                info = new CmdrDistanceInfo(getWritableDatabase(),
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getDouble(3),
                        creationDate,
                        (cursor.getInt(5) == 1),
                        false);
            }

            return info;
        } finally {
            cursor.close();
        }
    }

    public CmdrDistanceInfo getReferenceDistance(String s1, String s2)
    {
        String[] columns = {
                CmdrDbContract.CmdrReferenceDistances._ID,
                CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_FROM,
                CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_TO,
                CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_DISTANCE
        };

        String selection = CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_FROM + " =? AND " +
                CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_TO + " =?";
        String[] selectionArgs = { s1, s2 };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrReferenceDistances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            CmdrDistanceInfo info = null;

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                info = new CmdrDistanceInfo(getWritableDatabase(),
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getDouble(3),
                        null,
                        false,
                        true);
            }

            return info;
        } finally {
            cursor.close();
        }
    }

    public List<CmdrDistanceInfo> getDistances(String system, boolean duplicatesAllowed)
    {
        ArrayList<CmdrDistanceInfo> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.CmdrDistances._ID,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED
        };

        String selection = CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM + " =? OR " +
                CmdrDbContract.CmdrDistances.COLUMN_NAME_TO + " =?";
        String[] selectionArgs = { system, system };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrDistances.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        try {
            while (cursor.moveToNext()) {
                String s1 = cursor.getString(1);
                String s2 = cursor.getString(2);

                CmdrDistanceInfo info;
                Date creationDate = new Date(cursor.getLong(4) * 1000);

                if (system.equalsIgnoreCase(s1)) {
                    info = new CmdrDistanceInfo(getWritableDatabase(),
                                            cursor.getLong(0),
                                            s1,
                                            s2,
                                            cursor.getDouble(3),
                                            creationDate,
                                            (cursor.getInt(5) == 1),
                                            false);
                } else {
                    info = new CmdrDistanceInfo(getWritableDatabase(),
                                            cursor.getLong(0),
                                            s2,
                                            s1,
                                            cursor.getDouble(3),
                                            creationDate,
                                            (cursor.getInt(5) == 1),
                                            false);
                }

                if (duplicatesAllowed) {
                    list.add(info);
                } else if (!list.contains(info)) {
                    list.add(info);
                }
            }

            return list;
        } finally {
            cursor.close();
        }
    }

    public List<CmdrDistanceInfo> getReferenceDistances(String system)
    {
        ArrayList<CmdrDistanceInfo> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.CmdrReferenceDistances._ID,
                CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_FROM,
                CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_TO,
                CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_DISTANCE,
        };

        String selection = CmdrDbContract.CmdrReferenceDistances.COLUMN_NAME_FROM + " =?";
        String[] selectionArgs = { system };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrReferenceDistances.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);

        try {
            while (cursor.moveToNext()) {
                CmdrDistanceInfo info = new CmdrDistanceInfo(getWritableDatabase(),
                                                             cursor.getLong(0),
                                                             cursor.getString(1),
                                                             cursor.getString(2),
                                                             cursor.getDouble(3),
                                                             null,
                                                             false,
                                                             true);

                list.add(info);
            }

            return list;
        } finally {
            cursor.close();
        }
    }

    public CmdrSystemInfo getCurrentSystem()
    {
        CmdrDistanceInfo distanceInfo = getLastDistance();
        return (distanceInfo == null) ? null : getSystem(distanceInfo.getFirstSystem());
    }

    public List<CmdrDistanceInfo> getDistances(int limitCount, int offset, String order)
    {
        String[] columns = {
                CmdrDbContract.CmdrDistances._ID,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED
        };

        String orderBy = String.format("%s %s", CmdrDbContract.CmdrDistances._ID, order);

        String limit = null;
        if (limitCount > 0) {
            limit = Integer.toString(limitCount);

            if (offset > 0) {
                limit += " OFFSET " + Integer.toString(offset);
            }
        }

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrDistances.TABLE_NAME,
                                                    columns,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    orderBy,
                                                    limit);

        ArrayList<CmdrDistanceInfo> list = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                String second = cursor.isNull(2) ? null : cursor.getString(2);
                Date creationDate = new Date(cursor.getLong(4) * 1000);

                list.add(new CmdrDistanceInfo(getWritableDatabase(),
                                              cursor.getLong(0),
                                              cursor.getString(1),
                                              second,
                                              cursor.getDouble(3),
                                              creationDate,
                                              (cursor.getInt(5) == 1),
                                              false));
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    public CmdrDistanceInfo getFirstDistance()
    {
        List<CmdrDistanceInfo> dist = getDistances(1, 0, "ASC");
        return (dist.isEmpty()) ? null : dist.get(0);
    }

    public CmdrDistanceInfo getLastDistance()
    {
        List<CmdrDistanceInfo> dist = getDistances(1, 0, "DESC");
        return (dist.isEmpty()) ? null : dist.get(0);
    }

    public List<CmdrDistanceInfo> getEmptyDistances()
    {
        ArrayList<CmdrDistanceInfo> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.CmdrDistances._ID,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED
        };
        String selection = CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE + "=?";
        String[] selectionArgs = { "0.0" };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrDistances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM + "," + CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                                                    null,
                                                    null);

        try {
            while (cursor.moveToNext()) {
                String _first = (cursor.isNull(1) ? "" : cursor.getString(1));
                String _second = (cursor.isNull(2) ? "" : cursor.getString(2));

                if (!_first.isEmpty() && !_second.isEmpty()) {
                    boolean found = false;

                    for (CmdrDistanceInfo info : list) {
                        if (info.getFirstSystem().equalsIgnoreCase(_second) &&
                            info.getSecondSystem().equalsIgnoreCase(_first)) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        Date creationDate = new Date(cursor.getLong(3) * 1000);

                        list.add(new CmdrDistanceInfo(getWritableDatabase(),
                                                      cursor.getLong(0),
                                                      _first,
                                                      _second,
                                                      0.0,
                                                      creationDate,
                                                      (cursor.getInt(4) == 1),
                                                      false));
                    }
                }
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    public List<CmdrDistanceInfo> getUploadRequiredDistances()
    {
        ArrayList<CmdrDistanceInfo> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.CmdrDistances._ID,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_TO,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE,
                CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED
        };

        String selection = "";
        selection += CmdrDbContract.CmdrDistances.COLUMN_NAME_REMOTE_UPDATED;
        selection += "=? AND ";
        selection += CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE;
        selection += "!=?";

        String[] selectionArgs = { "0", "0.0" };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.CmdrDistances.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            while (cursor.moveToNext()) {
                Date creationDate = new Date(cursor.getLong(4) * 1000);

                list.add(new CmdrDistanceInfo(getWritableDatabase(),
                                              cursor.getLong(0),
                                              cursor.getString(1),
                                              cursor.getString(2),
                                              cursor.getDouble(3),
                                              creationDate,
                                              (cursor.getInt(5) == 1),
                                              false));
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    public int getDisplayDays()
    {
        synchronized (mutex) {
            if (numDays == -1) {
//                String[] columns = {
//                        CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG,
//                        CmdrDbContract.DbInfo.COLUMN_NAME_VALUE
//                };
//
//                String selection = CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
//                String[] selectionArgs = { CmdrDbContract.DbInfo.CONFIG_NAME_DISPLAY_DAYS };
//
//                Cursor c = getReadableDatabase().query(CmdrDbContract.DbInfo.TABLE_NAME, columns, selection, selectionArgs, null, null, null);
//
//                try {
//                    c.moveToFirst();
//                    numDays = (c.isAfterLast() ? -1 : Integer.valueOf(c.getString(1)));
//                } finally {
//                    c.close();
//                }
                numDays = 365;
            }

            return preferences.getInt("pref_display_days", numDays);
        }
    }

    public void setDisplayDays(int days)
    {
        String selection = CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
        String[] selectionArgs = { CmdrDbContract.DbInfo.CONFIG_NAME_DISPLAY_DAYS };

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.DbInfo.COLUMN_NAME_VALUE, Integer.toString(days));

        getWritableDatabase().update(CmdrDbContract.DbInfo.TABLE_NAME,
                                     values,
                                     selection,
                                     selectionArgs);

        synchronized (mutex) {
            numDays = days;
        }
    }

    public String getCmdrName()
    {
        synchronized (mutex) {
            if (cmdrName == null) {
                String[] columns = {
                        CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG,
                        CmdrDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {CmdrDbContract.DbInfo.CONFIG_NAME_CMDRNAME};

                Cursor c = getReadableDatabase().query(CmdrDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();

                    cmdrName = (c.isAfterLast() ? "" : c.getString(1));
                } finally {
                    c.close();
                }
            }

            return preferences.getString("pref_cmdr_name", cmdrName);
        }
    }

    public Date getLastProxyMessage()
    {
        synchronized (mutex) {
            if (lastProxyTime == null) {
                String[] columns = {
                        CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG,
                        CmdrDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {CmdrDbContract.DbInfo.CONFIG_NAME_LAST_PROXY_MESSAGE};

                Cursor c = getReadableDatabase().query(CmdrDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();
                    lastProxyTime = CmdrDbContract.DATE_FORMATTER.parse(c.getString(1));
                } catch (ParseException e) {
                    lastProxyTime = new Date(0);
                } finally {
                    c.close();
                }
            }

            return lastProxyTime;
        }
    }

    public void setLastProxyMessage(Date lastProxyMessage)
    {
        String selection = CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
        String[] selectionArgs = {CmdrDbContract.DbInfo.CONFIG_NAME_LAST_PROXY_MESSAGE};

        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.DbInfo.COLUMN_NAME_VALUE, CmdrDbContract.DATE_FORMATTER.format(lastProxyMessage));

        getWritableDatabase().update(CmdrDbContract.DbInfo.TABLE_NAME,
                                     values,
                                     selection,
                                     selectionArgs);

        synchronized (mutex) {
            lastProxyTime = lastProxyMessage;
        }
    }

    public String getRouteDestination()
    {
        synchronized (mutex) {
            if (route == null) {
                String[] columns = {
                    CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG,
                    CmdrDbContract.DbInfo.COLUMN_NAME_VALUE
                };

                String selection = CmdrDbContract.DbInfo.COLUMN_NAME_CONFIG + "=?";
                String[] selectionArgs = {CmdrDbContract.DbInfo.CONFIG_NAME_ROUTE};

                Cursor c = getReadableDatabase().query(CmdrDbContract.DbInfo.TABLE_NAME,
                                                       columns,
                                                       selection,
                                                       selectionArgs,
                                                       null,
                                                       null,
                                                       null);

                try {
                    c.moveToFirst();
                    route = (c.isAfterLast() ? "" : c.getString(1));
                } finally {
                    c.close();
                }
            }

            return preferences.getString("pref_route", route);
        }
    }

    Long getSatelliteId(Long systemId, Long bodyId, SatelliteCategory category)
    {
        Long ret = -1L;

        String[] columns = {
                CmdrDbContract.Satellites._ID
        };

        String selection;
        selection = CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_BODYID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID + "=?";

        String[] selectionArgs = {
                systemId.toString(),
                bodyId.toString(),
                Long.toString(category.value)
        };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.Satellites.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null,
                                                    "1");

        try {
            if (cursor.moveToNext()) {
                ret =  cursor.getLong(0);
            }
        } finally {
            cursor.close();
        }

        return ret;
    }

    List<CelestialBody> getSatellitesOf(@NonNull CmdrSystemInfo system,
                                        @NonNull CelestialBody parent)
    {
        ArrayList<CelestialBody> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.Satellites.COLUMN_NAME_BODYID,
                CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
        };

        String selection;
        selection = CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_PARENTID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID + "=?";

        String[] selectionArgs = {
                Long.toString(system.getRowId()),
                Long.toString((Long) parent.getId()),
                Long.toString(parent.getSatelliteCategory().value)
        };

        Cursor cursor = getWritableDatabase().query(CmdrDbContract.Satellites.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            while (cursor.moveToNext()) {
                CelestialBody body = null;
                SatelliteCategory satCat = SatelliteCategory.getSatelliteCategory(cursor.getInt(1));

                switch (satCat) {
                    case Star:
                        body = getStarSatellite(system, parent, cursor.getLong(0));
                        break;
                    case Planet:
                        body = getPlanetSatellite(system, parent, cursor.getLong(0));
                        break;
                    case Asteroid:
                        body = getAsteroidSatellite(system, parent, cursor.getLong(0));
                        break;
                    case Unknown:
                        break;
                }

                if (body != null) {
                    list.add(body);
                }
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    public CmdrStarBody getStarSatellite(@NonNull CmdrSystemInfo system,
                                         CelestialBody parent,
                                         Long id)
    {
        if (id == null) return null;

        String[] columns = {
                CmdrDbContract.Stars._ID,
                CmdrDbContract.Stars.COLUMN_NAME_TYPE,
                CmdrDbContract.Stars.COLUMN_NAME_SCANLEVEL,
                CmdrDbContract.Stars.COLUMN_NAME_NAME,
                CmdrDbContract.Stars.COLUMN_NAME_DISTANCE,
                CmdrDbContract.Stars.COLUMN_NAME_AGE,
                CmdrDbContract.Stars.COLUMN_NAME_MASS,
                CmdrDbContract.Stars.COLUMN_NAME_RADIUS,
                CmdrDbContract.Stars.COLUMN_NAME_SURFACETEMP,
                CmdrDbContract.Stars.COLUMN_NAME_ORBITALPERIOD,
                CmdrDbContract.Stars.COLUMN_NAME_SEMIMAJORAXIS,
                CmdrDbContract.Stars.COLUMN_NAME_ORBITALECCENTRICITY,
                CmdrDbContract.Stars.COLUMN_NAME_ORBITALINCLINATION,
                CmdrDbContract.Stars.COLUMN_NAME_ARGPERIAPSIS,
                CmdrDbContract.Stars.COLUMN_NAME_FIRST_DISCOVERED,
        };

        String selection = CmdrDbContract.Stars._ID + "=?";
        String[] selectionArgs = { id.toString() };

        /** We are going to make an assumption here that
         * may kill us in the future. However, there
         * *should NEVER* be a body that is IN the Satellites
         * list AND NOT in the Body detailed table.
         */
        Cursor cursor = getWritableDatabase().query(CmdrDbContract.Stars.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null,
                                                    "1");

        CmdrStarBody body = null;

        try {
            cursor.moveToFirst();

            if (!cursor.isAfterLast()) {
                StarType type = StarType.getStarType(cursor.getInt(1));
                ScanLevel level = ScanLevel.getScanLevel(cursor.getInt(2));

                body = new CmdrStarBody(getWritableDatabase(),
                                        system,
                                        type,
                                        // Star Type
                                        level,
                                        // Scan Level
                                        cursor.getLong(0),
                                        // ID
                                        (cursor.getInt(14) == 1),
                                        parent,
                                        (cursor.isNull(3) ? "" : cursor.getString(3)),
                                        // Name
                                        (cursor.isNull(4) ? null : cursor.getDouble(4)),
                                        // Distance
                                        (cursor.isNull(5) ? null : cursor.getDouble(5)),
                                        // Age
                                        (cursor.isNull(6) ? null : cursor.getDouble(6)),
                                        // Mass
                                        (cursor.isNull(7) ? null : cursor.getDouble(7)),
                                        // Radius
                                        (cursor.isNull(8) ? null : cursor.getDouble(8)),
                                        // Surface Temp
                                        (cursor.isNull(9) ? null : cursor.getDouble(9)),
                                        // Orbital Period
                                        (cursor.isNull(10) ? null : cursor.getDouble(10)),
                                        // Semi Major Axis
                                        (cursor.isNull(11) ? null : cursor.getDouble(11)),
                                        // Orbital Eccentricity
                                        (cursor.isNull(12) ? null : cursor.getDouble(12)),
                                        // Orbital Inclination
                                        (cursor.isNull(13) ?
                                         null :
                                         cursor.getDouble(13)));  // Arg of Periapsis
            }
        } finally {
            cursor.close();
        }

        return body;
    }

    boolean hasSatellites(long systemId, long id, SatelliteCategory category)
    {
        String[] columns = {
                CmdrDbContract.Satellites.COLUMN_NAME_BODYID,
                CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
        };

        String selection;
        selection = CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_PARENTID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID + "=?";

        String[] selectionArgs = {
                Long.toString(systemId),
                Long.toString(id),
                Long.toString(category.value)
        };

        Cursor cursor = getWritableDatabase().query(CmdrDbContract.Satellites.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        try {
            cursor.moveToFirst();
            return !cursor.isAfterLast();
        } finally {
            cursor.close();
        }
    }

    public CmdrPlanetBody getPlanetSatellite(@NonNull CmdrSystemInfo system,
                                             CelestialBody parent,
                                             long id)
    {
        String[] columns = {
                CmdrDbContract.Planets._ID,
                CmdrDbContract.Planets.COLUMN_NAME_TYPE,
                CmdrDbContract.Planets.COLUMN_NAME_SCANLEVEL,
                CmdrDbContract.Planets.COLUMN_NAME_NAME,
                CmdrDbContract.Planets.COLUMN_NAME_DISTANCE,
                CmdrDbContract.Planets.COLUMN_NAME_MASS,
                CmdrDbContract.Planets.COLUMN_NAME_RADIUS,
                CmdrDbContract.Planets.COLUMN_NAME_SURFACETEMP,
                CmdrDbContract.Planets.COLUMN_NAME_SURFACEPRESSURE,
                CmdrDbContract.Planets.COLUMN_NAME_VOLCANISM,
                CmdrDbContract.Planets.COLUMN_NAME_ATMOSPHERETYPE,
                CmdrDbContract.Planets.COLUMN_NAME_ORBITALPERIOD,
                CmdrDbContract.Planets.COLUMN_NAME_SEMIMAJORAXIS,
                CmdrDbContract.Planets.COLUMN_NAME_ORBITALECCENTRICITY,
                CmdrDbContract.Planets.COLUMN_NAME_ORBITALINCLINATION,
                CmdrDbContract.Planets.COLUMN_NAME_ARGPERIAPSIS,
                CmdrDbContract.Planets.COLUMN_NAME_ROTATIONPERIOD,
                CmdrDbContract.Planets.COLUMN_NAME_TIDALLOCKED,
                CmdrDbContract.Planets.COLUMN_NAME_AXISTILT,
                CmdrDbContract.Planets.COLUMN_NAME_FIRST_DISCOVERED,
                CmdrDbContract.Planets.COLUMN_NAME_TERRAFORMABLE
        };

        String selection = CmdrDbContract.Planets._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        /** We are going to make an assumption here that
         * may kill us in the future. However, there
         * *should NEVER* be a body that is IN the Satellites
         * list AND NOT in the Body detailed table.
         */
        Cursor cursor = getWritableDatabase().query(CmdrDbContract.Planets.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null,
                                                    "1");

        CmdrPlanetBody body = null;

        try {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                PlanetType type = PlanetType.getPlanetType(cursor.getInt(1));
                Atmosphere atmosphere = cursor.isNull(10) ?
                                        Atmosphere.Unknown :
                                        Atmosphere.getAtmosphere(cursor.getInt(10));
                VolcanoType volcano = cursor.isNull(9) ? VolcanoType.Unknown : VolcanoType.getVolcanoType(cursor.getInt(9));
                ScanLevel level = ScanLevel.getScanLevel(cursor.getInt(2));

                body = new CmdrPlanetBody(getWritableDatabase(),
                                          system,
                                          type, // Planet Type
                                          level, // Scan Level
                                          (cursor.getInt(19) == 1),
                                          cursor.getLong(0), // ID
                                          parent,
                                          (cursor.isNull(3) ? "" : cursor.getString(3)), // Name
                                          (cursor.isNull(4) ? null : cursor.getDouble(4)), // Distance
                                          (cursor.getInt(20) == 1),
                                          (cursor.isNull(5) ? null : cursor.getDouble(5)), // Mass
                                          (cursor.isNull(6) ? null : cursor.getDouble(6)), // Radius
                                          (cursor.isNull(7) ? null : cursor.getDouble(7)), // Surface Temp
                                          (cursor.isNull(8) ? null : cursor.getDouble(8)), // Surface Pressure
                                          volcano, // Volcanism
                                          atmosphere, // Atmosphere
                                          (cursor.isNull(11) ? null : cursor.getDouble(11)), // Orbital Period
                                          (cursor.isNull(12) ? null : cursor.getDouble(12)), // Semi Major Axis
                                          (cursor.isNull(13) ? null : cursor.getDouble(13)), // Orbital Eccentricity
                                          (cursor.isNull(14) ? null : cursor.getDouble(14)), // Orbital Inclination
                                          (cursor.isNull(15) ? null : cursor.getDouble(15)), // Arg of Periapsis
                                          (cursor.isNull(16) ? null : cursor.getDouble(16)), // Rotation Period
                                          (cursor.isNull(17) ? null : (cursor.getInt(17) != 0)), // Tidal Locked
                                          (cursor.isNull(18) ? null : cursor.getDouble(18)));    // Axis Tilt
            }
        } finally {
            cursor.close();
        }

        return body;
    }

    public CmdrAsteroidBody getAsteroidSatellite(@NonNull SystemInfo system,
                                                 CelestialBody parent,
                                                 long id)
    {
        String[] columns = {
                CmdrDbContract.Asteroids._ID,
                CmdrDbContract.Asteroids.COLUMN_NAME_TYPE,
                CmdrDbContract.Asteroids.COLUMN_NAME_SCANLEVEL,
                CmdrDbContract.Asteroids.COLUMN_NAME_NAME,
                CmdrDbContract.Asteroids.COLUMN_NAME_DISTANCE,
                CmdrDbContract.Asteroids.COLUMN_NAME_MOONMASSES,
                CmdrDbContract.Asteroids.COLUMN_NAME_ORBITALPERIOD,
                CmdrDbContract.Asteroids.COLUMN_NAME_SEMIMAJORAXIS,
                CmdrDbContract.Asteroids.COLUMN_NAME_ORBITALECCENTRICITY,
                CmdrDbContract.Asteroids.COLUMN_NAME_ORBITALINCLINATION,
                CmdrDbContract.Asteroids.COLUMN_NAME_ARGPERIAPSIS,
                CmdrDbContract.Stars.COLUMN_NAME_FIRST_DISCOVERED,
        };

        String selection = CmdrDbContract.Asteroids._ID + "=?";
        String[] selectionArgs = { Long.toString(id) };

        /** We are going to make an assumption here that
         * may kill us in the future. However, there
         * *should NEVER* be a body that is IN the Satellites
         * list AND NOT in the Body detailed table.
         */
        Cursor cursor = getWritableDatabase().query(CmdrDbContract.Asteroids.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null,
                                                    "1");

        CmdrAsteroidBody body;

        try {
            cursor.moveToFirst();
            Mineral type = Mineral.getMineral(cursor.getInt(1));
            ScanLevel level = ScanLevel.getScanLevel(cursor.getInt(2));

            body = new CmdrAsteroidBody(getWritableDatabase(),
                                        system,
                                        type,              // Mineral Type
                                        level,             // Scan Level
                                        (cursor.getInt(11) == 1),
                                        cursor.getLong(0), // ID
                                        parent,
                                        (cursor.isNull(3) ? "" : cursor.getString(3)),  // Name
                                        (cursor.isNull(4) ? null : cursor.getDouble(4)),  // Distance
                                        (cursor.isNull(5) ? null : cursor.getDouble(5)),  // Moon Masses
                                        (cursor.isNull(6) ? null : cursor.getDouble(6)),  // Orbital Period
                                        (cursor.isNull(7) ? null : cursor.getDouble(7)),  // Semi Major Axis
                                        (cursor.isNull(8) ? null : cursor.getDouble(8)),  // Orbital Eccentricity
                                        (cursor.isNull(9) ? null : cursor.getDouble(9)),  // Orbital Inclination
                                        (cursor.isNull(10) ? null : cursor.getDouble(10))); // Arg of Periapsis
        } finally {
            cursor.close();
        }

        return body;
    }

    boolean hasRings(CelestialBody body)
    {
        String[] columns = {
                CmdrDbContract.Rings._ID
        };
        String selection;
        selection = CmdrDbContract.Rings.COLUMN_NAME_BODYID + "=? AND ";
        selection +=  CmdrDbContract.Rings.COLUMN_NAME_CATEGORYID + "=?";

        String[] selectionArgs = {
                body.getId().toString(),
                String.format("%d", body.getSatelliteCategory().value)
        };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.Asteroids.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null,
                                                    "1");

        try {
            return cursor.moveToNext();
        } finally {
            cursor.close();
        }
    }

    List<Ring> getRingsOf(CelestialBody body)
    {
        ArrayList<Ring> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.Rings._ID,
                CmdrDbContract.Rings.COLUMN_NAME_LEVEL,
                CmdrDbContract.Rings.COLUMN_NAME_TYPE,
                CmdrDbContract.Rings.COLUMN_NAME_QUALITY,
                CmdrDbContract.Rings.COLUMN_NAME_MASS,
                CmdrDbContract.Rings.COLUMN_NAME_SEMIMAJORAXIS,
                CmdrDbContract.Rings.COLUMN_NAME_INNERRADIUS,
                CmdrDbContract.Rings.COLUMN_NAME_OUTERRADIUS
        };
        String selection;
        selection = CmdrDbContract.Rings.COLUMN_NAME_BODYID + "=? AND ";
        selection +=  CmdrDbContract.Rings.COLUMN_NAME_CATEGORYID + "=?";

        String[] selectionArgs = {
                body.getId().toString(),
                String.format("%d", body.getSatelliteCategory().value)
        };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.Asteroids.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);
        try {
            while (cursor.moveToNext()) {
                Mineral mineral;

                if (cursor.isNull(2)) {
                    mineral = Mineral.Unknown;
                } else {
                    mineral = Mineral.getMineral(cursor.getInt(2));
                }

                RingQuality quality;

                if (cursor.isNull(3)) {
                    quality = RingQuality.Unknown;
                } else {
                    quality = RingQuality.getRingQuality(cursor.getInt(3));
                }

                CmdrRing ring = new CmdrRing(getWritableDatabase(),
                                             cursor.getLong(0),
                                             body,
                                             cursor.getInt(1),
                                             mineral,
                                             quality,
                                             cursor.isNull(4) ? null : cursor.getDouble(4),
                                             cursor.isNull(5) ? null : cursor.getDouble(5),
                                             cursor.isNull(6) ? null : cursor.getDouble(6),
                                             cursor.isNull(7) ? null : cursor.getDouble(7));

                list.add(ring);
            }

            Collections.sort(list, new Comparator<Ring>()
            {
                @Override
                public int compare(Ring lhs, Ring rhs)
                {
                    Integer i_lhs = lhs.getLevel();
                    Integer i_rhs = rhs.getLevel();

                    return i_lhs.compareTo(i_rhs);
                }
            });
        } finally {
            cursor.close();
        }

        return list;
    }

    public SystemImageInfo[] getSystemImages()
    {
        String sql;

        sql = "SELECT ";
        sql += CmdrDbContract.Images.TABLE_NAME + "." + CmdrDbContract.Images._ID + ",";
        sql += CmdrDbContract.Images.COLUMN_NAME_SYSTEMID + ",";
        sql += CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_NAME + ",";
        sql += CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_TYPE + ",";
        sql += CmdrDbContract.Images.COLUMN_NAME_DISTANCE_FROM_MAINSTAR + ",";
        sql += CmdrDbContract.Images.COLUMN_NAME_URL_PATH + ",";
        sql += CmdrDbContract.CmdrSystems.COLUMN_NAME_SYSTEM;

        sql += " FROM " + CmdrDbContract.Images.TABLE_NAME;
        sql += " INNER JOIN ";
        sql += CmdrDbContract.CmdrSystems.TABLE_NAME;
        sql += " ON ";
        sql += CmdrDbContract.Images.TABLE_NAME + "." + CmdrDbContract.Images.COLUMN_NAME_SYSTEMID;
        sql += " = ";
        sql += CmdrDbContract.CmdrSystems.TABLE_NAME + "." + CmdrDbContract.CmdrSystems._ID;

        sql += " GROUP BY ";
        sql += CmdrDbContract.Images.COLUMN_NAME_SYSTEMID;
        sql += " ORDER BY ";
        sql += CmdrDbContract.Images.COLUMN_NAME_SYSTEMID;
        sql += " ASC;";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        ArrayList<SystemImageInfo> list = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                SystemImageInfo info = new SystemImageInfo(getWritableDatabase(),
                                                           cursor.getLong(0),
                                                           cursor.getLong(1),
                                                           cursor.getString(6),
                                                           cursor.getString(2),
                                                           cursor.getString(3),
                                                           cursor.getDouble(4),
                                                           cursor.getString(5));

                list.add(info);
            }
        } finally {
            cursor.close();
        }


        return list.toArray(new SystemImageInfo[list.size()]);
    }

    public SystemImageInfo[] getSystemImages(long systemId, String systemName)
    {
        String columns[] = {
                CmdrDbContract.Images._ID,
                CmdrDbContract.Images.COLUMN_NAME_SYSTEMID,
                CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_NAME,
                CmdrDbContract.Images.COLUMN_NAME_CELESTIAL_TYPE,
                CmdrDbContract.Images.COLUMN_NAME_DISTANCE_FROM_MAINSTAR,
                CmdrDbContract.Images.COLUMN_NAME_URL_PATH
        };

        String selection = CmdrDbContract.Images.COLUMN_NAME_SYSTEMID + "=?";
        String[] selectionArgs = { Long.toString(systemId) };

        Cursor cursor = getReadableDatabase().query(CmdrDbContract.Images.TABLE_NAME,
                                                    columns,
                                                    selection,
                                                    selectionArgs,
                                                    null,
                                                    null,
                                                    null);

        ArrayList<SystemImageInfo> list = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                SystemImageInfo info = new SystemImageInfo(getWritableDatabase(),
                                                           cursor.getLong(0),
                                                           cursor.getLong(1),
                                                           systemName,
                                                           cursor.getString(2),
                                                           cursor.getString(3),
                                                           cursor.getDouble(4),
                                                           cursor.getString(5));

                list.add(info);
            }
        } finally {
            cursor.close();
        }


        return list.toArray(new SystemImageInfo[list.size()]);
    }

    public void createStatViews(Date from, Date to)
    {
        if (from == null) {
            from = new Date(0);
        }

        if (to == null) {
            to = new Date();
        }

        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("DROP VIEW IF EXISTS StatStars");
        db.execSQL("DROP VIEW IF EXISTS StatPlanets");
        db.execSQL("DROP VIEW IF EXISTS StatSystems");

        String sql = "CREATE VIEW StatSystems AS SELECT CmdrSystems.* FROM CmdrSystems, (SELECT FromSystem FROM CmdrDistances WHERE CreateDate BETWEEN %d AND %d GROUP BY FromSystem) AS DistSys WHERE DistSys.FromSystem=CmdrSystems.System";

        db.execSQL(String.format(sql, from.getTime() / 1000, to.getTime() / 1000));
        db.execSQL("CREATE VIEW StatStars AS SELECT * FROM Stars, (SELECT BodyId FROM Satellites, StatSystems WHERE Satellites.SystemId=StatSystems._id AND Satellites.CategoryId=0) AS TmpSats WHERE Stars._id=TmpSats.BodyId");
        db.execSQL("CREATE VIEW StatPlanets AS SELECT * FROM Planets, (SELECT BodyId FROM Satellites, StatSystems WHERE Satellites.SystemId=StatSystems._id AND Satellites.CategoryId=1) AS TmpSats WHERE Planets._id=TmpSats.BodyId");
    }

    public int getStatSystemCount()
    {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM StatSystems", null);

        try {
            return cursor.getCount();
        } finally {
            cursor.close();
        }
    }

    public List<PlanetInfo> getStatPlanets()
    {
        String[] columns = {
                CmdrDbContract.Planets._ID,
                CmdrDbContract.Planets.COLUMN_NAME_TYPE,
                CmdrDbContract.Planets.COLUMN_NAME_SCANLEVEL,
                CmdrDbContract.Planets.COLUMN_NAME_NAME,
                CmdrDbContract.Planets.COLUMN_NAME_DISTANCE,
                CmdrDbContract.Planets.COLUMN_NAME_MASS,
                CmdrDbContract.Planets.COLUMN_NAME_RADIUS,
                CmdrDbContract.Planets.COLUMN_NAME_SURFACETEMP,
                CmdrDbContract.Planets.COLUMN_NAME_SURFACEPRESSURE,
                CmdrDbContract.Planets.COLUMN_NAME_VOLCANISM,
                CmdrDbContract.Planets.COLUMN_NAME_ATMOSPHERETYPE,
                CmdrDbContract.Planets.COLUMN_NAME_ORBITALPERIOD,
                CmdrDbContract.Planets.COLUMN_NAME_SEMIMAJORAXIS,
                CmdrDbContract.Planets.COLUMN_NAME_ORBITALECCENTRICITY,
                CmdrDbContract.Planets.COLUMN_NAME_ORBITALINCLINATION,
                CmdrDbContract.Planets.COLUMN_NAME_ARGPERIAPSIS,
                CmdrDbContract.Planets.COLUMN_NAME_ROTATIONPERIOD,
                CmdrDbContract.Planets.COLUMN_NAME_TIDALLOCKED,
                CmdrDbContract.Planets.COLUMN_NAME_AXISTILT,
                CmdrDbContract.Planets.COLUMN_NAME_FIRST_DISCOVERED,
                CmdrDbContract.Planets.COLUMN_NAME_TERRAFORMABLE
        };

        /** We are going to make an assumption here that
         * may kill us in the future. However, there
         * *should NEVER* be a body that is IN the Satellites
         * list AND NOT in the Body detailed table.
         */
        Cursor cursor = getReadableDatabase().query("StatPlanets",
                                                    columns,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null);

        ArrayList<PlanetInfo> list = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                PlanetType type = PlanetType.getPlanetType(cursor.getInt(1));
                Atmosphere atmosphere = cursor.isNull(10) ?
                                        Atmosphere.Unknown :
                                        Atmosphere.getAtmosphere(cursor.getInt(10));
                VolcanoType volcano = cursor.isNull(9) ? VolcanoType.Unknown : VolcanoType.getVolcanoType(cursor.getInt(9));
                ScanLevel level = ScanLevel.getScanLevel(cursor.getInt(2));

                list.add(new PlanetInfo(type, // Planet Type
                                        level, // Scan Level
                                        (cursor.getInt(19) == 1), // First Discovered
                                        (cursor.isNull(3) ? "" : cursor.getString(3)), // Name
                                        (cursor.isNull(4) ? null : cursor.getDouble(4)), // Distance
                                        (cursor.getInt(20) == 1), // Terraform-able
                                        (cursor.isNull(5) ? null : cursor.getDouble(5)), // Mass
                                        (cursor.isNull(6) ? null : cursor.getDouble(6)), // Radius
                                        (cursor.isNull(7) ? null : cursor.getDouble(7)), // Surface Temp
                                        (cursor.isNull(8) ? null : cursor.getDouble(8)), // Surface Pressure
                                        volcano, // Volcanism
                                        atmosphere, // Atmosphere
                                        (cursor.isNull(11) ? null : cursor.getDouble(11)), // Orbital Period
                                        (cursor.isNull(12) ? null : cursor.getDouble(12)), // Semi Major Axis
                                        (cursor.isNull(13) ? null : cursor.getDouble(13)), // Orbital Eccentricity
                                        (cursor.isNull(14) ? null : cursor.getDouble(14)), // Orbital Inclination
                                        (cursor.isNull(15) ? null : cursor.getDouble(15)), // Arg of Periapsis
                                        (cursor.isNull(16) ? null : cursor.getDouble(16)), // Rotation Period
                                        (cursor.isNull(17) ? null : (cursor.getInt(17) != 0)), // Tidal Locked
                                        (cursor.isNull(18) ? null : cursor.getDouble(18))));    // Axis Tilt
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    public List<StarInfo> getStatStars()
    {
        String[] columns = {
                CmdrDbContract.Stars._ID,
                CmdrDbContract.Stars.COLUMN_NAME_TYPE,
                CmdrDbContract.Stars.COLUMN_NAME_SCANLEVEL,
                CmdrDbContract.Stars.COLUMN_NAME_NAME,
                CmdrDbContract.Stars.COLUMN_NAME_DISTANCE,
                CmdrDbContract.Stars.COLUMN_NAME_AGE,
                CmdrDbContract.Stars.COLUMN_NAME_MASS,
                CmdrDbContract.Stars.COLUMN_NAME_RADIUS,
                CmdrDbContract.Stars.COLUMN_NAME_SURFACETEMP,
                CmdrDbContract.Stars.COLUMN_NAME_ORBITALPERIOD,
                CmdrDbContract.Stars.COLUMN_NAME_SEMIMAJORAXIS,
                CmdrDbContract.Stars.COLUMN_NAME_ORBITALECCENTRICITY,
                CmdrDbContract.Stars.COLUMN_NAME_ORBITALINCLINATION,
                CmdrDbContract.Stars.COLUMN_NAME_ARGPERIAPSIS,
                CmdrDbContract.Stars.COLUMN_NAME_FIRST_DISCOVERED,
        };

        Cursor cursor = getReadableDatabase().query("StatStars",
                                                    columns,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null);

        ArrayList<StarInfo> list = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                StarType type = StarType.getStarType(cursor.getInt(1));
                ScanLevel level = ScanLevel.getScanLevel(cursor.getInt(2));

                list.add(new StarInfo(type, // Star Type
                                        level, // Scan Level
                                        (cursor.getInt(14) == 1), // First Discovered
                                        (cursor.isNull(3) ? "" : cursor.getString(3)), // Name
                                        (cursor.isNull(4) ? null : cursor.getDouble(4)), // Distance
                                        (cursor.isNull(5) ? null : cursor.getDouble(5)), // Age
                                        (cursor.isNull(6) ? null : cursor.getDouble(6)), // Mass
                                        (cursor.isNull(7) ? null : cursor.getDouble(7)), // Radius
                                        (cursor.isNull(8) ? null : cursor.getDouble(8)), // Surface Temp
                                        (cursor.isNull(9) ? null : cursor.getDouble(9)), // Orbital Period
                                        (cursor.isNull(10) ? null : cursor.getDouble(10)), // Semi Major Axis
                                        (cursor.isNull(11) ? null : cursor.getDouble(11)), // Orbital Eccentricity
                                        (cursor.isNull(12) ? null : cursor.getDouble(12)), // Orbital Inclination
                                        (cursor.isNull(13) ? null : cursor.getDouble(13))));  // Arg of Periapsis
            }
        } finally {
            cursor.close();
        }

        return list;
    }
}
