package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fussyware.AndDiscovered.celestial.Atmosphere;
import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.celestial.VolcanoType;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.fussyware.AndDiscovered.edutils.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wes on 12/9/15.
 */
class UpgradeDatabase50 extends UpgradeDatabase
{
    private static final String LOG_NAME = UpgradeDatabase50.class.getSimpleName();

    UpgradeDatabase50(SQLiteDatabase db)
    {
        super(db);
    }

    @Override
    void upgrade()
    {
        correctMainStar();
        expandBodies();
    }

    /** The "number" of bodies from the main system table
     * is not going to be equal to the number of bodies
     * in the planet/star tables. We don't actually have
     * all the information to recreate properly. However,
     * we *do* know how many bodies there should be. We can
     * be conservative and expand out to the "cheapest"
     * body in the category. This way we can get an idea of
     * how much we have earned.
     */
    private void expandBodies()
    {
        final int[] starWeights = {
                30, 30, 20, 30, 20, 20,
                 1,  1, 20,  1, 20, 30,
                20, 30, 20, 50, 50, 50
        };

        final int[] planetWeights = {
                30,  1, 50, 50, 50,
                20, 30, 20, 20, 30,
                 1,  1, 40, 40, 40,
                40
        };

        String masterSql = "SELECT * FROM CmdrSystems";
        String pastSql = "SELECT StellarBodies, PlanetaryBodies FROM CmdrSystems_v49";

        Cursor masterCursor = db.rawQuery(masterSql, null);
        Cursor pastCursor = db.rawQuery(pastSql, null);

        try {
            while (masterCursor.moveToNext() && pastCursor.moveToNext()) {
                int starBodyCount = pastCursor.getInt(0);
                int planetBodyCount = pastCursor.getInt(1);

                Long starId = (masterCursor.isNull(7) ? null : masterCursor.getLong(7));
                Position xyz = null;

                if (!masterCursor.isNull(2) &&
                    !masterCursor.isNull(3) &&
                    !masterCursor.isNull(4)) {
                    xyz = new Position(masterCursor.getDouble(2),
                                       masterCursor.getDouble(3),
                                       masterCursor.getDouble(4));
                }

                CmdrSystemInfo info = new CmdrSystemInfo(db,
                                                         masterCursor.getLong(0),
                                                         masterCursor.getString(1),
                                                         xyz,
                                                         masterCursor.getInt(5),
                                                         masterCursor.getString(6),
                                                         starId);

                List<CmdrStarBody> starList = getStarSatellitesOf(db, info);
                int starDelta = starBodyCount - starList.size();

//                Log.d(LOG_NAME,
//                      String.format("id [%d], list: %d, count: %d, delta: %d",
//                                    info.getRowId(),
//                                    starList.size(),
//                                    starBodyCount,
//                                    starDelta));
                if (starDelta > 0) {
                    StarType credits = StarType.Black_Hole;

                    for (CmdrStarBody starBody : starList) {
                        StarType type = starBody.getType();

                        int wieghtList = type.credits * starWeights[type.value];
                        int wieghtMin = credits.credits * starWeights[credits.value];

                        if (wieghtList < wieghtMin) {
                            credits = type;
                        }
                    }

                    for (int i = 0; i < starDelta; i++) {
                        createStar(info, credits);
                    }
                }

                List<CmdrPlanetBody> planetList = getPlanetSatellitesOf(db, info);
                int planetDelta = planetBodyCount - planetList.size();

                if (planetDelta > 0) {
                    PlanetType credits = PlanetType.Earth_Like;

                    for (CmdrPlanetBody planetBody : planetList) {
                        PlanetType type = planetBody.getType();

                        int wieghtList = type.credits * planetWeights[type.value];
                        int wieghtMin = credits.credits * planetWeights[credits.value];

                        if (wieghtList < wieghtMin) {
                            credits = type;
                        }
                    }

                    for (int i = 0; i < planetDelta; i++) {
                        createPlanet(info, credits);
                    }
                }
            }
        } finally {
            masterCursor.close();
            pastCursor.close();
        }
    }

    private void correctMainStar()
    {
        final int conversion[] = {
                -1,
                0,  1,  2,  3,
                4,  5,  6,  7,
                8,  9, 10, 11,
                12, 13, 14, 15,
                16, 17, -1
        };

        String masterSql = "SELECT * FROM CmdrSystems";
        String pastSql = "SELECT MainStar FROM CmdrSystems_v49";

        Cursor masterCursor = db.rawQuery(masterSql, null);
        Cursor pastCursor = db.rawQuery(pastSql, null);

        try {
            while (masterCursor.moveToNext() && pastCursor.moveToNext()) {
                int starType = conversion[pastCursor.getInt(0)];

                Long starId = (masterCursor.isNull(7) ? null : masterCursor.getLong(7));

                if ((starId == null) && (starType != -1)) {
                    AndLog.d(LOG_NAME,
                             "Star type: " +
                             starType +
                             ", id: " +
                             starId +
                             ", system id: " +
                             masterCursor.getLong(0));

                    /** We have a failure to import in the main star. The
                     * user did NOT enter the main star into the Stars list.
                     */
                    Position xyz = null;

                    if (!masterCursor.isNull(2) &&
                        !masterCursor.isNull(3) &&
                        !masterCursor.isNull(4)) {
                        xyz = new Position(masterCursor.getDouble(2),
                                           masterCursor.getDouble(3),
                                           masterCursor.getDouble(4));
                    }

                    CmdrSystemInfo info = new CmdrSystemInfo(db,
                                                             masterCursor.getLong(0),
                                                             masterCursor.getString(1),
                                                             xyz,
                                                             masterCursor.getInt(5),
                                                             masterCursor.getString(6),
                                                             null);

                    AndLog.d(LOG_NAME, "Setting main star: " + info.getRowId());
                    info.setMainStar(createStar(info,
                                                StarType.getStarType(starType)));
                }
            }
        } finally {
            masterCursor.close();
            pastCursor.close();
        }
    }

    private List<CmdrStarBody> getStarSatellitesOf(SQLiteDatabase db, CmdrSystemInfo system)
    {
        ArrayList<CmdrStarBody> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.Satellites.COLUMN_NAME_BODYID,
        };

        String selection;
        selection = CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID + "=?";

        String[] selectionArgs = {
                Long.toString(system.getRowId()),
                Long.toString(SatelliteCategory.Star.value)
        };

        Cursor satCursor = db.query(CmdrDbContract.Satellites.TABLE_NAME,
                                    columns,
                                    selection,
                                    selectionArgs,
                                    null,
                                    null,
                                    null);

        try {
            columns = new String[] {
                    CmdrDbContract.Stars._ID,
                    CmdrDbContract.Stars.COLUMN_NAME_TYPE,
                    CmdrDbContract.Stars.COLUMN_NAME_SCANLEVEL,
                    CmdrDbContract.Stars.COLUMN_NAME_NAME,
                    CmdrDbContract.Stars.COLUMN_NAME_DISTANCE,
                    CmdrDbContract.Stars.COLUMN_NAME_AGE,
                    CmdrDbContract.Stars.COLUMN_NAME_MASS,
                    CmdrDbContract.Stars.COLUMN_NAME_RADIUS,
                    CmdrDbContract.Stars.COLUMN_NAME_SURFACETEMP,
                    CmdrDbContract.Stars.COLUMN_NAME_ORBITALPERIOD,
                    CmdrDbContract.Stars.COLUMN_NAME_SEMIMAJORAXIS,
                    CmdrDbContract.Stars.COLUMN_NAME_ORBITALECCENTRICITY,
                    CmdrDbContract.Stars.COLUMN_NAME_ORBITALINCLINATION,
                    CmdrDbContract.Stars.COLUMN_NAME_ARGPERIAPSIS,
                    CmdrDbContract.Stars.COLUMN_NAME_FIRST_DISCOVERED,
            };
            selection = CmdrDbContract.Stars._ID + "=?";

            while (satCursor.moveToNext()) {
                selectionArgs = new String[] {Long.toString(satCursor.getLong(0))};

                Cursor cursor = db.query(CmdrDbContract.Stars.TABLE_NAME,
                                         columns,
                                         selection,
                                         selectionArgs,
                                         null,
                                         null,
                                         null,
                                         "1");
                try {
                    cursor.moveToNext();

                    StarType type = StarType.getStarType(cursor.getInt(1));
                    ScanLevel level = ScanLevel.getScanLevel(cursor.getInt(2));

                    CmdrStarBody body = new CmdrStarBody(db,
                                                         system,
                                                         type, // Star Type
                                                         level, // Scan Level
                                                         cursor.getLong(0), // ID
                                                         (cursor.getInt(14) == 1),
                                                         null,
                                                         (cursor.isNull(3) ? "" : cursor.getString(3)),// Name
                                                         (cursor.isNull(4) ? null : cursor.getDouble(4)),// Distance
                                                         (cursor.isNull(5) ? null : cursor.getDouble(5)),// Age
                                                         (cursor.isNull(6) ? null : cursor.getDouble(6)),// Mass
                                                         (cursor.isNull(7) ? null : cursor.getDouble(7)),// Radius
                                                         (cursor.isNull(8) ? null : cursor.getDouble(8)),// Surface Temp
                                                         (cursor.isNull(9) ? null : cursor.getDouble(9)),// Orbital Period
                                                         (cursor.isNull(10) ? null : cursor.getDouble(10)),// Semi Major Axis
                                                         (cursor.isNull(11) ? null : cursor.getDouble(11)),// Orbital Eccentricity
                                                         (cursor.isNull(12) ? null : cursor.getDouble(12)),// Orbital Inclination
                                                         (cursor.isNull(13) ? null : cursor.getDouble(13)));  // Arg of Periapsis

                    list.add(body);
                } finally {
                    cursor.close();
                }
            }
        } finally {
            satCursor.close();
        }

        return list;
    }

    private List<CmdrPlanetBody> getPlanetSatellitesOf(SQLiteDatabase db, CmdrSystemInfo system)
    {
        ArrayList<CmdrPlanetBody> list = new ArrayList<>();

        String[] columns = {
                CmdrDbContract.Satellites.COLUMN_NAME_BODYID,
        };

        String selection;
        selection = CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID + "=?";
        selection += " AND ";
        selection += CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID + "=?";

        String[] selectionArgs = {
                Long.toString(system.getRowId()),
                Long.toString(SatelliteCategory.Planet.value)
        };

        Cursor satCursor = db.query(CmdrDbContract.Satellites.TABLE_NAME,
                                    columns,
                                    selection,
                                    selectionArgs,
                                    null,
                                    null,
                                    null);

        try {
            columns = new String[] {
                    CmdrDbContract.Planets._ID,
                    CmdrDbContract.Planets.COLUMN_NAME_TYPE,
                    CmdrDbContract.Planets.COLUMN_NAME_SCANLEVEL,
                    CmdrDbContract.Planets.COLUMN_NAME_NAME,
                    CmdrDbContract.Planets.COLUMN_NAME_DISTANCE,
                    CmdrDbContract.Planets.COLUMN_NAME_MASS,
                    CmdrDbContract.Planets.COLUMN_NAME_RADIUS,
                    CmdrDbContract.Planets.COLUMN_NAME_SURFACETEMP,
                    CmdrDbContract.Planets.COLUMN_NAME_SURFACEPRESSURE,
                    CmdrDbContract.Planets.COLUMN_NAME_VOLCANISM,
                    CmdrDbContract.Planets.COLUMN_NAME_ATMOSPHERETYPE,
                    CmdrDbContract.Planets.COLUMN_NAME_ORBITALPERIOD,
                    CmdrDbContract.Planets.COLUMN_NAME_SEMIMAJORAXIS,
                    CmdrDbContract.Planets.COLUMN_NAME_ORBITALECCENTRICITY,
                    CmdrDbContract.Planets.COLUMN_NAME_ORBITALINCLINATION,
                    CmdrDbContract.Planets.COLUMN_NAME_ARGPERIAPSIS,
                    CmdrDbContract.Planets.COLUMN_NAME_ROTATIONPERIOD,
                    CmdrDbContract.Planets.COLUMN_NAME_TIDALLOCKED,
                    CmdrDbContract.Planets.COLUMN_NAME_AXISTILT,
                    CmdrDbContract.Planets.COLUMN_NAME_FIRST_DISCOVERED,
                    CmdrDbContract.Planets.COLUMN_NAME_TERRAFORMABLE
            };

            selection = CmdrDbContract.Planets._ID + "=?";

            while (satCursor.moveToNext()) {
                selectionArgs = new String[] {Long.toString(satCursor.getLong(0))};

                Cursor cursor = db.query(CmdrDbContract.Planets.TABLE_NAME,
                                         columns,
                                         selection,
                                         selectionArgs,
                                         null,
                                         null,
                                         null,
                                         "1");

                try {
                    cursor.moveToNext();

                    PlanetType type = PlanetType.getPlanetType(cursor.getInt(1));
                    Atmosphere atmosphere = cursor.isNull(10) ?
                                            Atmosphere.Unknown :
                                            Atmosphere.getAtmosphere(cursor.getInt(10));
                    VolcanoType volcano = cursor.isNull(9) ? VolcanoType.Unknown : VolcanoType.getVolcanoType(
                            cursor.getInt(9));
                    ScanLevel level = ScanLevel.getScanLevel(cursor.getInt(2));

                    CmdrPlanetBody body = new CmdrPlanetBody(db,
                                                             system,
                                                             type, // Planet Type
                                                             level, // Scan Level
                                                             (cursor.getInt(19) == 1),
                                                             cursor.getLong(0),// ID
                                                             null,
                                                             (cursor.isNull(3) ? "" : cursor.getString(3)),// Name
                                                             (cursor.isNull(4) ? null : cursor.getDouble(4)),// Distance
                                                             false, // Terraformable
                                                             (cursor.isNull(5) ? null : cursor.getDouble(5)),// Mass
                                                             (cursor.isNull(6) ? null : cursor.getDouble(6)),// Radius
                                                             (cursor.isNull(7) ? null : cursor.getDouble(7)),// Surface Temp
                                                             (cursor.isNull(8) ? null : cursor.getDouble(8)),// Surface Pressure
                                                             volcano,// Volcanism
                                                             atmosphere,// Atmosphere
                                                             (cursor.isNull(11) ? null : cursor.getDouble(11)),// Orbital Period
                                                             (cursor.isNull(12) ? null : cursor.getDouble(12)),// Semi Major Axis
                                                             (cursor.isNull(13) ? null : cursor.getDouble(13)),// Orbital Eccentricity
                                                             (cursor.isNull(14) ? null : cursor.getDouble(14)),// Orbital Inclination
                                                             (cursor.isNull(15) ? null : cursor.getDouble(15)),// Arg of Periapsis
                                                             (cursor.isNull(16) ? null : cursor.getDouble(16)),// Rotation Period
                                                             (cursor.isNull(17) ? null : (cursor.getInt(17) != 0)),// Tidal Locked
                                                             (cursor.isNull(18) ? null : cursor.getDouble(18)));    // Axis Tilt

                    list.add(body);
                } finally {
                    cursor.close();
                }
            }
        } finally {
            satCursor.close();
        }

        return list;
    }

    private CmdrStarBody createStar(CmdrSystemInfo info, StarType starType)
    {
        ContentValues values = new ContentValues();

        values.put(CmdrDbContract.Stars.COLUMN_NAME_TYPE, starType.value);
        values.put(CmdrDbContract.Stars.COLUMN_NAME_SCANLEVEL, ScanLevel.Level_2.value);
        values.put(CmdrDbContract.Stars.COLUMN_NAME_FIRST_DISCOVERED, false);

        long id = db.insert(CmdrDbContract.Stars.TABLE_NAME, null, values);

        values.clear();

        values.put(CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID,
                   info.getRowId());
        values.put(CmdrDbContract.Satellites.COLUMN_NAME_BODYID, id);
        values.put(CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
                   SatelliteCategory.Star.value);

        db.insert(CmdrDbContract.Satellites.TABLE_NAME, null, values);

        return new CmdrStarBody(db,
                                info,
                                starType,
                                ScanLevel.Level_2,
                                id,
                                false,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null);
    }

    private CmdrPlanetBody createPlanet(CmdrSystemInfo info, PlanetType planetType)
    {
        ContentValues values = new ContentValues();

        values.put(CmdrDbContract.Planets.COLUMN_NAME_TYPE, planetType.value);
        values.put(CmdrDbContract.Planets.COLUMN_NAME_SCANLEVEL, ScanLevel.Level_2.value);
        values.put(CmdrDbContract.Planets.COLUMN_NAME_FIRST_DISCOVERED, false);

        long id = db.insert(CmdrDbContract.Planets.TABLE_NAME, null, values);

        values.clear();

        values.put(CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID, info.getRowId());
        values.put(CmdrDbContract.Satellites.COLUMN_NAME_BODYID, id);
        values.put(CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
                   SatelliteCategory.Planet.value);

        db.insert(CmdrDbContract.Satellites.TABLE_NAME, null, values);

        return new CmdrPlanetBody(db,
                                  info,
                                  planetType,
                                  ScanLevel.Level_2,
                                  false,
                                  id,
                                  null,
                                  null,
                                  null,
                                  false,
                                  null,
                                  null,
                                  null,
                                  null,
                                  VolcanoType.Unknown,
                                  Atmosphere.Unknown,
                                  null,
                                  null,
                                  null,
                                  null,
                                  null,
                                  null,
                                  null,
                                  null);
    }
}
