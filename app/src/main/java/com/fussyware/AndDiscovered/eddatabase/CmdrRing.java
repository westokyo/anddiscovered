package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.Mineral;
import com.fussyware.AndDiscovered.celestial.Ring;
import com.fussyware.AndDiscovered.celestial.RingQuality;

import java.security.InvalidParameterException;

/**
 * Created by wes on 12/7/15.
 */
public class CmdrRing extends Ring
{
    public static final Creator<CmdrRing> CREATOR = new Creator<CmdrRing>()
    {
        @Override
        public CmdrRing createFromParcel(Parcel source)
        {
            return new CmdrRing(source);
        }

        @Override
        public CmdrRing[] newArray(int size)
        {
            return new CmdrRing[size];
        }
    };

    private static final Object createMutex = new Object();

    private final Long id;
    private final SQLiteDatabase db;

    CmdrRing(SQLiteDatabase db,
             @NonNull Long id,
             @NonNull CelestialBody parent,
             int level,
             Mineral type,
             RingQuality quality,
             Double mass,
             Double semiMajorAxis,
             Double innerRadius, Double outerRadius)
    {
        super(parent, level, type, quality, mass, semiMajorAxis, innerRadius, outerRadius);

        this.id = id;
        this.db = db;
    }

    private CmdrRing(SQLiteDatabase db,
                     @NonNull Long id,
                     @NonNull CelestialBody parent,
                     int level)
    {
        super(parent, level);

        this.id = id;
        this.db = db;
    }

    private CmdrRing(Parcel parcel)
    {
        super(parcel);

        this.db = CmdrDbHelper.getInstance().getWritableDatabase();
        this.id = parcel.readLong();
    }

    public static Ring create(@NonNull CelestialBody parent, int level)
    {
        if (level < 0) {
            throw new InvalidParameterException("Ring level must start at zero.");
        }

        synchronized (createMutex) {
            SQLiteDatabase db = CmdrDbHelper.getInstance().getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(CmdrDbContract.Rings.COLUMN_NAME_BODYID, (Long) parent.getId());
            values.put(CmdrDbContract.Rings.COLUMN_NAME_CATEGORYID,
                       parent.getSatelliteCategory().value);
            values.put(CmdrDbContract.Rings.COLUMN_NAME_LEVEL, level);

            long id = db.insert(CmdrDbContract.Rings.TABLE_NAME, null, values);

            return new CmdrRing(db, id, parent, level);
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeLong(id);
    }

    @NonNull
    @Override
    public Object getId()
    {
        return id;
    }

    @Override
    protected void updateParent(CelestialBody body)
    {
        ContentValues values = new ContentValues();

        values.put(CmdrDbContract.Rings.COLUMN_NAME_BODYID,
                   (Long) body.getId());
        values.put(CmdrDbContract.Rings.COLUMN_NAME_CATEGORYID,
                   body.getSatelliteCategory().value);

        write(values);
    }

    @Override
    protected void updateLevel(int level)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Rings.COLUMN_NAME_LEVEL, level);

        write(values);
    }

    @Override
    protected void updateType(Mineral type)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Rings.COLUMN_NAME_TYPE, type.value);

        write(values);
    }

    @Override
    protected void updateQuality(RingQuality quality)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Rings.COLUMN_NAME_QUALITY, quality.value);

        write(values);
    }

    @Override
    protected void updateMass(Double mass)
    {
        ContentValues values = new ContentValues();

        if (mass == null) {
            values.putNull(CmdrDbContract.Rings.COLUMN_NAME_MASS);
        } else {
            values.put(CmdrDbContract.Rings.COLUMN_NAME_MASS, mass);
        }

        write(values);
    }

    @Override
    protected void updateSemiMajorAxis(Double semiMajorAxis)
    {
        ContentValues values = new ContentValues();

        if (semiMajorAxis == null) {
            values.putNull(CmdrDbContract.Rings.COLUMN_NAME_MASS);
        } else {
            values.put(CmdrDbContract.Rings.COLUMN_NAME_MASS, semiMajorAxis);
        }

        write(values);
    }

    @Override
    protected void updateInnerRadius(Double innerRadius)
    {
        ContentValues values = new ContentValues();

        if (innerRadius == null) {
            values.putNull(CmdrDbContract.Rings.COLUMN_NAME_MASS);
        } else {
            values.put(CmdrDbContract.Rings.COLUMN_NAME_MASS, innerRadius);
        }

        write(values);
    }

    @Override
    protected void updateOuterRadius(Double outerRadius)
    {
        ContentValues values = new ContentValues();

        if (outerRadius == null) {
            values.putNull(CmdrDbContract.Rings.COLUMN_NAME_MASS);
        } else {
            values.put(CmdrDbContract.Rings.COLUMN_NAME_MASS, outerRadius);
        }

        write(values);
    }

    private void write(ContentValues values)
    {
        String selection = CmdrDbContract.Rings._ID + "=?";
        String[] selectionArgs = { id.toString() };

        db.update(CmdrDbContract.Rings.TABLE_NAME, values, selection, selectionArgs);
    }

}
