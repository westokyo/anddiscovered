package com.fussyware.AndDiscovered.eddatabase;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by wes on 12/9/15.
 */
abstract class UpgradeDatabase
{
    protected SQLiteDatabase db;

    UpgradeDatabase(SQLiteDatabase db)
    {
        this.db = db;
    }

    abstract void upgrade();
}
