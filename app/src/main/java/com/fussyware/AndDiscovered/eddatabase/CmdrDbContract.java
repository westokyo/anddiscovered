package com.fussyware.AndDiscovered.eddatabase;

import android.provider.BaseColumns;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by wes on 6/15/15.
 */
public final class CmdrDbContract
{
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT,
                                                                               Locale.US);

    static {
        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static abstract class CmdrSystems implements BaseColumns
    {
        public static final String TABLE_NAME = "CmdrSystems";
        public static final String COLUMN_NAME_SYSTEM = "System";
        public static final String COLUMN_NAME_XCOORD = "xCoord";
        public static final String COLUMN_NAME_YCOORD = "yCoord";
        public static final String COLUMN_NAME_ZCOORD = "zCoord";
        public static final String COLUMN_NAME_TRILAT_SYSTEMS = "TrilatSystems";
        public static final String COLUMN_NAME_NOTE = "Note";
        public static final String COLUMN_NAME_MAIN_STAR = "MainStar";

        //public static final String COLUMN_NAME_ = "";
        public static final int DEFAULT_NUM_TRILAT_SYSTEMS = 1;
    }

    public static abstract class CmdrDistances implements BaseColumns
    {
        public static final String TABLE_NAME = "CmdrDistances";
        public static final String COLUMN_NAME_FROM = "FromSystem";
        public static final String COLUMN_NAME_TO = "ToSystem";
        public static final String COLUMN_NAME_DISTANCE = "Distance";
        public static final String COLUMN_NAME_CREATEDATE = "CreateDate";
        public static final String COLUMN_NAME_REMOTE_UPDATED = "RemoteUpdated";
    }

    public static abstract class CmdrReferenceDistances implements BaseColumns
    {
        public static final String TABLE_NAME = "CmdrRefDistances";
        public static final String COLUMN_NAME_FROM = "FromSystem";
        public static final String COLUMN_NAME_TO = "ToSystem";
        public static final String COLUMN_NAME_DISTANCE = "Distance";
    }

    public static abstract class Satellites implements BaseColumns
    {
        public static final String TABLE_NAME = "Satellites";
        public static final String COLUMN_NAME_SYSTEMID = "SystemId";
        public static final String COLUMN_NAME_BODYID = "BodyId";
        public static final String COLUMN_NAME_CATEGORYID = "CategoryId";
        public static final String COLUMN_NAME_PARENTID = "ParentId";
        public static final String COLUMN_NAME_PARENTCATEGORYID = "ParentCategoryId";
    }

    public static abstract class Stars implements BaseColumns
    {
        public static final String TABLE_NAME = "Stars";
        public static final String COLUMN_NAME_TYPE = "Type";
        public static final String COLUMN_NAME_SCANLEVEL = "ScanLevel";
        public static final String COLUMN_NAME_FIRST_DISCOVERED = "FirstDiscovered";
        public static final String COLUMN_NAME_NAME = "Name";
        public static final String COLUMN_NAME_DISTANCE = "Distance";
        public static final String COLUMN_NAME_AGE = "Age";
        public static final String COLUMN_NAME_MASS = "Mass";
        public static final String COLUMN_NAME_RADIUS = "Radius";
        public static final String COLUMN_NAME_SURFACETEMP = "SurfaceTemp";
        public static final String COLUMN_NAME_ORBITALPERIOD = "OrbitalPeriod";
        public static final String COLUMN_NAME_SEMIMAJORAXIS = "SemiMajorAxis";
        public static final String COLUMN_NAME_ORBITALECCENTRICITY = "OrbitalEccentricity";
        public static final String COLUMN_NAME_ORBITALINCLINATION = "OrbitalInclination";
        public static final String COLUMN_NAME_ARGPERIAPSIS = "ArgPeriapsis";
    }

    public static abstract class Planets implements BaseColumns
    {
        public static final String TABLE_NAME = "Planets";
        public static final String COLUMN_NAME_TYPE = "Type";
        public static final String COLUMN_NAME_SCANLEVEL = "ScanLevel";
        public static final String COLUMN_NAME_FIRST_DISCOVERED = "FirstDiscovered";
        public static final String COLUMN_NAME_NAME = "Name";
        public static final String COLUMN_NAME_DISTANCE = "Distance";
        public static final String COLUMN_NAME_TERRAFORMABLE = "Terraformable";
        public static final String COLUMN_NAME_MASS = "Mass";
        public static final String COLUMN_NAME_RADIUS = "Radius";
        public static final String COLUMN_NAME_SURFACETEMP = "SurfaceTemp";
        public static final String COLUMN_NAME_SURFACEPRESSURE = "SurfacePressure";
        public static final String COLUMN_NAME_VOLCANISM = "Volcanism";
        public static final String COLUMN_NAME_ATMOSPHERETYPE = "AtmosphereType";
        public static final String COLUMN_NAME_ORBITALPERIOD = "OrbitalPeriod";
        public static final String COLUMN_NAME_SEMIMAJORAXIS = "SemiMajorAxis";
        public static final String COLUMN_NAME_ORBITALECCENTRICITY = "OrbitalEccentricity";
        public static final String COLUMN_NAME_ORBITALINCLINATION = "OrbitalInclination";
        public static final String COLUMN_NAME_ARGPERIAPSIS = "ArgPeriapsis";
        public static final String COLUMN_NAME_ROTATIONPERIOD = "RotationPeriod";
        public static final String COLUMN_NAME_TIDALLOCKED = "TidalLocked";
        public static final String COLUMN_NAME_AXISTILT = "AxisTilt";
    }

    public static abstract class Asteroids implements BaseColumns
    {
        public static final String TABLE_NAME = "Asteroids";
        public static final String COLUMN_NAME_TYPE = "Type";
        public static final String COLUMN_NAME_SCANLEVEL = "ScanLevel";
        public static final String COLUMN_NAME_FIRST_DISCOVERED = "FirstDiscovered";
        public static final String COLUMN_NAME_NAME = "Name";
        public static final String COLUMN_NAME_DISTANCE = "Distance";
        public static final String COLUMN_NAME_MOONMASSES = "MoonMasses";
        public static final String COLUMN_NAME_ORBITALPERIOD = "OrbitalPeriod";
        public static final String COLUMN_NAME_SEMIMAJORAXIS = "SemiMajorAxis";
        public static final String COLUMN_NAME_ORBITALECCENTRICITY = "OrbitalEccentricity";
        public static final String COLUMN_NAME_ORBITALINCLINATION = "OrbitalInclination";
        public static final String COLUMN_NAME_ARGPERIAPSIS = "ArgPeriapsis";
    }

    public static abstract class Rings implements BaseColumns
    {
        public static final String TABLE_NAME = "Rings";
        public static final String COLUMN_NAME_BODYID = "BodyId";
        public static final String COLUMN_NAME_CATEGORYID = "CategoryId";
        public static final String COLUMN_NAME_TYPE = "Type";
        public static final String COLUMN_NAME_QUALITY = "Quality";
        public static final String COLUMN_NAME_LEVEL = "Level";
        public static final String COLUMN_NAME_MASS = "Mass";
        public static final String COLUMN_NAME_SEMIMAJORAXIS = "SemiMajorAxis";
        public static final String COLUMN_NAME_INNERRADIUS = "InnerRadius";
        public static final String COLUMN_NAME_OUTERRADIUS = "OuterRadius";
    }

    public static abstract class Images implements BaseColumns
    {
        public static final String TABLE_NAME = "Images";
        public static final String COLUMN_NAME_SYSTEMID = "SystemId";
        public static final String COLUMN_NAME_CELESTIAL_NAME = "CelestialName";
        public static final String COLUMN_NAME_CELESTIAL_TYPE = "CelestialType";
        public static final String COLUMN_NAME_DISTANCE_FROM_MAINSTAR = "DistanceFromMainStar";
        public static final String COLUMN_NAME_URL_PATH = "UrlPath";
    }

    public static abstract class DbInfo implements BaseColumns
    {
        public static final String TABLE_NAME = "DbInfo";
        public static final String COLUMN_NAME_CONFIG = "ConfigName";
        public static final String COLUMN_NAME_VALUE = "Value";

        public static final String CONFIG_NAME_DBVERSION = "DbVersion";
        public static final String CONFIG_NAME_CMDRNAME = "CmdrName";
        public static final String CONFIG_NAME_DISPLAY_DAYS = "DisplayDays";
        public static final String CONFIG_NAME_LAST_PROXY_MESSAGE = "LastProxyMessage";
        public static final String CONFIG_NAME_ROUTE = "RouteDestination";
    }

    @Deprecated
    public static abstract class StarsDeprecated implements BaseColumns
    {
        public static final String TABLE_NAME = "Stars";
        public static final String COLUMN_NAME_SYSTEMID = "SystemId";
        public static final String COLUMN_NAME_STAR = "Star";
    }

    @Deprecated
    public static abstract class PlanetsDeprecated implements BaseColumns
    {
        public static final String TABLE_NAME = "Planets";
        public static final String COLUMN_NAME_SYSTEMID = "SystemId";
        public static final String COLUMN_NAME_PLANET = "Planet";
    }
}
