package com.fussyware.AndDiscovered.eddatabase;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;

import com.fussyware.AndDiscovered.celestial.AsteroidBody;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.Mineral;
import com.fussyware.AndDiscovered.celestial.Ring;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.SystemInfo;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by wes on 11/17/15.
 */
public class CmdrAsteroidBody extends AsteroidBody
{
    public static final Creator<CmdrAsteroidBody> CREATOR = new Creator<CmdrAsteroidBody>()
    {
        @Override
        public CmdrAsteroidBody createFromParcel(Parcel source)
        {
            return new CmdrAsteroidBody(source);
        }

        @Override
        public CmdrAsteroidBody[] newArray(int size)
        {
            return new CmdrAsteroidBody[size];
        }
    };

    private static final Object createMutex = new Object();

    private final SQLiteDatabase db;
    private final Long id;

    CmdrAsteroidBody(@NonNull SQLiteDatabase db,
                     @NonNull SystemInfo system,
                     @NonNull Mineral type,
                     @NonNull ScanLevel scanLevel,
                     boolean firstDiscovered,
                     long id,
                     CelestialBody parent,
                     String name,
                     Double distance,
                     Double mass,
                     Double orbitalPeriod,
                     Double semiMajorAxis,
                     Double orbitalEccentricity,
                     Double orbitalInclination, Double argPeriapsis)
    {
        super(system,
              type,
              scanLevel,
              firstDiscovered,
              parent,
              name,
              distance,
              mass,
              orbitalPeriod,
              semiMajorAxis,
              orbitalEccentricity,
              orbitalInclination,
              argPeriapsis);

        if (db.isReadOnly()) {
            throw new InvalidParameterException("A valid writable database must be provided.");
        }

        this.db = db;
        this.id = id;
    }

    private CmdrAsteroidBody(@NonNull SQLiteDatabase db,
                             @NonNull SystemInfo system,
                             @NonNull Mineral type,
                             long id,
                             CelestialBody parent)
    {
        super(system, type, parent);

        this.db = db;
        this.id = id;
    }

    private CmdrAsteroidBody(Parcel parcel)
    {
        super(parcel);

        db = CmdrDbHelper.getInstance().getWritableDatabase();
        id = parcel.readLong();
    }

    public static AsteroidBody create(@NonNull CmdrSystemInfo system,
                                      @NonNull Mineral type)
    {
        return CmdrAsteroidBody.create(system, type, null);
    }

    public static AsteroidBody create(@NonNull CmdrSystemInfo system,
                                      @NonNull Mineral type,
                                      CelestialBody parent)
    {
        if (type == Mineral.Unknown) {
            throw new InvalidParameterException("A valid asteroid type must be specified during creation.");
        }

        synchronized (createMutex) {
            SQLiteDatabase db = CmdrDbHelper.getInstance().getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(CmdrDbContract.Asteroids.COLUMN_NAME_TYPE, type.value);
            values.put(CmdrDbContract.Asteroids.COLUMN_NAME_SCANLEVEL, ScanLevel.Level_2.value);
            values.put(CmdrDbContract.Asteroids.COLUMN_NAME_FIRST_DISCOVERED, 0);

            long id = db.insert(CmdrDbContract.Asteroids.TABLE_NAME, null, values);

            values.clear();

            values.put(CmdrDbContract.Satellites.COLUMN_NAME_SYSTEMID, system.getRowId());
            values.put(CmdrDbContract.Satellites.COLUMN_NAME_BODYID, id);
            values.put(CmdrDbContract.Satellites.COLUMN_NAME_CATEGORYID,
                       SatelliteCategory.Asteroid.value);

            if (parent != null) {
                long _id = -1;

                if (parent.getId() instanceof Long) {
                    _id = (Long) parent.getId();
                }

                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTID, _id);
                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID,
                           parent.getSatelliteCategory().value);
            }

            db.insert(CmdrDbContract.Satellites.TABLE_NAME, null, values);

            return new CmdrAsteroidBody(db, system, type, id, parent);
        }
    }

    @NonNull
    @Override
    public Object getId()
    {
        return id;
    }

    @Override
    public List<CelestialBody> getSatellites()
    {
        return CmdrDbHelper.getInstance()
                           .getSatellitesOf((CmdrSystemInfo) getSystem(), this);
    }

    @Override
    public boolean hasSatellites()
    {
        return CmdrDbHelper.getInstance()
                           .hasSatellites(((CmdrSystemInfo) getSystem()).getRowId(),
                                          id,
                                          getSatelliteCategory());
    }

    @Override
    public boolean hasRings()
    {
        return CmdrDbHelper.getInstance().hasRings(this);
    }

    @Override
    public List<Ring> getRings()
    {
        return CmdrDbHelper.getInstance().getRingsOf(this);
    }

    @Override
    public void add(int level)
    {
        CmdrRing.create(this, level);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeLong(id);
    }

    @Override
    public void delete()
    {
        synchronized (createMutex) {
            List<CelestialBody> bodies = getSatellites();

            /** First delete all of this Bodies Satellites. */
            for (CelestialBody body : bodies) {
                body.delete();
            }

            /** Now delete this Satellite. */
            Long satId = CmdrDbHelper.getInstance()
                                     .getSatelliteId(((CmdrSystemInfo) getSystem()).getRowId(),
                                                     id,
                                                     getSatelliteCategory());

            if (satId != -1) {
                String selection = CmdrDbContract.Satellites._ID + "=?";
                String[] selectionArgs = { satId.toString() };

                db.delete(CmdrDbContract.Satellites.TABLE_NAME, selection, selectionArgs);

                selection = CmdrDbContract.Asteroids._ID + "=?";
                selectionArgs = new String[] { id.toString() };

                db.delete(CmdrDbContract.Asteroids.TABLE_NAME, selection, selectionArgs);
            }
        }
    }

    @Override
    protected void updateFirstDisocovered(boolean firstDiscovered)
    {
        ContentValues values = new ContentValues();

        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_FIRST_DISCOVERED, firstDiscovered);
        write(values);
    }

    @Override
    protected void updateParent(CelestialBody body)
    {
        Long satId = CmdrDbHelper.getInstance()
                                 .getSatelliteId(((CmdrSystemInfo) getSystem()).getRowId(),
                                                 id,
                                                 getSatelliteCategory());

        if (satId != -1L) {
            ContentValues values = new ContentValues();
            if (parent == null) {
                values.putNull(CmdrDbContract.Satellites.COLUMN_NAME_PARENTID);
                values.putNull(CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID);
            } else {
                long _id = -1;

                if (parent.getId() instanceof Long) {
                    _id = (Long) parent.getId();
                }

                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTID, _id);
                values.put(CmdrDbContract.Satellites.COLUMN_NAME_PARENTCATEGORYID,
                           parent.getSatelliteCategory().value);
            }

            String selection = CmdrDbContract.Satellites._ID + "=?";
            String[] selectionArgs = { satId.toString() };

            db.update(CmdrDbContract.Satellites.TABLE_NAME, values, selection, selectionArgs);
        }
    }

    @Override
    protected void updateMoonMass(double mass)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_MOONMASSES, mass);

        write(values);
    }

    @Override
    protected void updateName(String name)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_NAME, name);

        write(values);
    }

    @Override
    protected void updateDistance(double distance)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_DISTANCE, distance);

        write(values);
    }

    @Override
    protected void updateScanLevel(ScanLevel scanLevel)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Planets.COLUMN_NAME_SCANLEVEL, scanLevel.value);

        write(values);
    }

    @Override
    protected void updateSemiMajorAxis(double semiMajorAxis)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_SEMIMAJORAXIS, semiMajorAxis);

        write(values);
    }

    @Override
    protected void updateOrbitalPeriod(double orbitalPeriod)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_ORBITALPERIOD, orbitalPeriod);

        write(values);
    }

    @Override
    protected void updateOrbitalEccentricity(double orbitalEccentricity)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_ORBITALECCENTRICITY, orbitalEccentricity);

        write(values);
    }

    @Override
    protected void updateOrbitalInclination(double orbitalInclination)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_ORBITALINCLINATION, orbitalInclination);

        write(values);
    }

    @Override
    protected void updateArgPeriapsis(double argPeriapsis)
    {
        ContentValues values = new ContentValues();
        values.put(CmdrDbContract.Asteroids.COLUMN_NAME_ARGPERIAPSIS, argPeriapsis);

        write(values);
    }

    private void write(ContentValues values)
    {
        String selection = CmdrDbContract.Asteroids._ID + "=?";
        String[] selectionArgs = { id.toString() };

        db.update(CmdrDbContract.Asteroids.TABLE_NAME, values, selection, selectionArgs);
    }
}
