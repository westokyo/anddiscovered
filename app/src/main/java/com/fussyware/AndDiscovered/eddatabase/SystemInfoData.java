package com.fussyware.AndDiscovered.eddatabase;

import com.fussyware.AndDiscovered.edutils.Position;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by wes on 12/7/15.
 */
class SystemInfoData
{
    private final String system;

    private Long id;
    private String note;
    private Long mainStar;
    private Position xyz;
    private int trilat;

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    SystemInfoData(Long id,
                   String name,
                   Position position,
                   String note,
                   Long mainStar,
                   int trilat)
    {
        this.id = id;
        this.system = name;
        this.xyz = position;
        this.note = note;
        this.mainStar = mainStar;
        this.trilat = trilat;
    }

    Long getId()
    {
        try {
            readLock.lock();
            return id;
        } finally {
            readLock.unlock();
        }
    }

    String getSystem()
    {
        try {
            readLock.lock();
            return system;
        } finally {
            readLock.unlock();
        }
    }

    String getNote()
    {
        try {
            readLock.lock();
            return note;
        } finally {
            readLock.unlock();
        }
    }

    Long getMainStar()
    {
        try {
            readLock.lock();
            return mainStar;
        } finally {
            readLock.unlock();
        }
    }

    Position getXyz()
    {
        try {
            readLock.lock();
            return xyz;
        } finally {
            readLock.unlock();
        }
    }

    int getTrilat()
    {
        try {
            readLock.lock();
            return trilat;
        } finally {
            readLock.unlock();
        }
    }

    public void setId(Long id)
    {
        try {
            writeLock.lock();
            this.id = id;
        } finally {
            writeLock.unlock();
        }
    }

    public void setNote(String note)
    {
        try {
            writeLock.lock();
            this.note = note;
        } finally {
            writeLock.unlock();
        }
    }

    public void setMainStar(Long mainStar)
    {
        try {
            writeLock.lock();
            this.mainStar = mainStar;
        } finally {
            writeLock.unlock();
        }
    }

    public void setXyz(Position xyz)
    {
        try {
            writeLock.lock();
            this.xyz = xyz;
        } finally {
            writeLock.unlock();
        }
    }

    public void setTrilat(int trilat)
    {
        this.trilat = trilat;
    }
}
