package com.fussyware.AndDiscovered;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.edproxy.ProxyConnectionState;

import java.io.File;

import io.fabric.sdk.android.Fabric;

/**
 * Created by wes on 9/28/15.
 */
public class AndDiscoveredApplication extends Application
{
    private CmdrDbHelper dbHelper;
    private ProxyConnectionState proxyState;

    @Override
    public void onCreate()
    {
        super.onCreate();

        Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build());

        if (dbHelper == null) {
            // Init and hold a reference so it does not get cleaned up.
            CmdrDbHelper.init(this);
            dbHelper = CmdrDbHelper.getInstance();
        }

        if (proxyState == null) {
            ProxyConnectionState.init(this);
            proxyState = ProxyConnectionState.getInstance();
        }

        deleteEDSMDatabase();
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();

        dbHelper.close();

        dbHelper = null;
        proxyState = null;
    }

    private void deleteEDSMDatabase()
    {
        File databaseDir = new File(getApplicationInfo().dataDir, "databases");
        File db = new File(databaseDir, "edsm.db");
        File db_shm = new File(databaseDir, "edsm.db-shm");
        File db_wal = new File(databaseDir, "edsm.db-wal");

        if (db.exists()) {
            db.delete();
        }

        if (db_shm.exists()) {
            db_shm.delete();
        }

        if (db_wal.exists()) {
            db_wal.delete();
        }
    }
}
