package com.fussyware.AndDiscovered.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;
import com.fussyware.AndDiscovered.fragment.FilterListFragment;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by wes on 12/18/15.
 */
public class FilterListAdapter extends BaseAdapter
{
    private static final String LOG_NAME = FilterListFragment.class.getSimpleName();

    private final String[] starTypes;
    private final String unknownPosition;

    private final CmdrDbHelper dbHelper;
    private Cursor dbCursor;

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    private FilterCursor filter;

    public FilterListAdapter(Context context, FilterCursor filter)
    {
        dbHelper = CmdrDbHelper.getInstance();
        starTypes = context.getResources().getStringArray(R.array.star_types);
        unknownPosition = context.getResources().getString(R.string.UnknownPosition);

        this.filter = filter;

        new FilterAsyncTask().execute(filter);
    }

    public void notifyDatabaseChanged()
    {
        setFilter(filter);
    }

    public void setFilter(FilterCursor filter)
    {
        try {
            readLock.lock();
            this.filter = filter;
            new FilterAsyncTask().execute(this.filter);
        } finally {
            readLock.unlock();
        }
    }

    public FilterCursor getFilter()
    {
        try {
            readLock.lock();
            return filter;
        } finally {
            readLock.unlock();
        }
    }

    public void close()
    {
        try {
            writeLock.lock();
            dbCursor.close();
            dbCursor = null;
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public int getCount()
    {
        try {
            readLock.lock();
            return (dbCursor == null) ? 0 : dbCursor.getCount();
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public CmdrSystemInfo getItem(int position)
    {
        CmdrSystemInfo info = null;

        try {
            readLock.lock();
            if (dbCursor != null) {
                dbCursor.moveToPosition(position);

                Position xyz = null;
                Long mainStar = dbCursor.isNull(7) ? null : dbCursor.getLong(7);

                if (!dbCursor.isNull(2) && !dbCursor.isNull(3) && !dbCursor.isNull(4)) {
                    xyz = new Position(dbCursor.getDouble(2),
                                       dbCursor.getDouble(3),
                                       dbCursor.getDouble(4));
                }

                info = new CmdrSystemInfo(dbHelper.getWritableDatabase(),
                                          dbCursor.getLong(0),
                                          dbCursor.getString(1),
                                          xyz,
                                          dbCursor.getInt(5),
                                          dbCursor.getString(6),
                                          mainStar);
            }
        } finally {
            readLock.unlock();
        }

        return info;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.filter_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CmdrSystemInfo info = getItem(position);

        if (info != null) {
            viewHolder.systemName.setText(info.getSystem());
            viewHolder.journal.setText(info.getNote());

            StarBody mainStar = info.getMainStar();

            if (mainStar == null) {
                viewHolder.mainStar.setText("Unknown");
            } else {
                viewHolder.mainStar.setText(starTypes[mainStar.getType().value]);
            }

            viewHolder.position.setText((info.getPosition() == null) ?
                                        unknownPosition :
                                        info.getPosition().toString());
        }

        return convertView;
    }

    private static class ViewHolder
    {
        public final TextView systemName;
        public final TextView journal;
        public final TextView position;
        public final TextView mainStar;

        public ViewHolder(View layout)
        {
            systemName = (TextView) layout.findViewById(R.id.system_text);
            journal = (TextView) layout.findViewById(R.id.journal_text);
            position = (TextView) layout.findViewById(R.id.position_text);
            mainStar = (TextView) layout.findViewById(R.id.main_star_text);
        }
    }

    public static class FilterCursor implements Parcelable
    {
        public static final Creator<FilterCursor> CREATOR = new Creator<FilterCursor>()
        {
            @Override
            public FilterCursor createFromParcel(Parcel in)
            {
                return new FilterCursor(in);
            }

            @Override
            public FilterCursor[] newArray(int size)
            {
                return new FilterCursor[size];
            }
        };

        private final String sql;
        private final String[] args;

        public FilterCursor(String sql, String[] args)
        {
            this.sql = sql;
            this.args = args;
        }

        private FilterCursor(Parcel parcel)
        {
            sql = parcel.readString();
            args = parcel.createStringArray();
        }

        public Cursor getCursor()
        {
            return CmdrDbHelper.getInstance()
                               .getReadableDatabase()
                               .rawQuery(sql, args);
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeString(sql);
            dest.writeStringArray(args);
        }
    }

    private class FilterAsyncTask extends AsyncTask<FilterCursor, Void, Cursor>
    {
        @Override
        protected void onPostExecute(Cursor cursor)
        {
            try {
                writeLock.lock();
                if (dbCursor != null) {
                    dbCursor.close();
                    dbCursor = null;
                }

                dbCursor = cursor;
                notifyDataSetChanged();
            } finally {
                writeLock.unlock();
            }
        }

        @Override
        protected Cursor doInBackground(FilterCursor... params)
        {
            return params[0].getCursor();
        }
    }
}
