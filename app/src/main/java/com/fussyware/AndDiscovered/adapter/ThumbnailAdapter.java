package com.fussyware.AndDiscovered.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fussyware.AndDiscovered.edproxy.ProxyConnectionState;
import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.eddatabase.SystemImageInfo;
import com.fussyware.AndDiscovered.edutils.AndLog;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wes on 9/18/15.
 */
public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.ViewHolder>
{
    public static final int USE_CELESTIAL_NAME = 1;
    public static final int USE_SYSTEM_NAME = 2;

    private static final String LOG_NAME = ThumbnailAdapter.class.getSimpleName();

    private final ArrayList<SystemImageInfo> imageList = new ArrayList<>();
    private final ArrayList<Integer> selectedList = new ArrayList<>();

    private final Context context;
    private final int useName;

    private OnItemClickListener clickListener;
    private OnItemLongClickListener longClickListener;

    public ThumbnailAdapter(Context context)
    {
        this(context, USE_CELESTIAL_NAME);
    }

    public ThumbnailAdapter(Context context, int useName)
    {
        this.context = context;
        this.useName = useName;
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        this.clickListener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener)
    {
        this.longClickListener = listener;
    }

    public void add(SystemImageInfo imageInfo)
    {
        synchronized (imageList) {
            imageList.add(imageInfo);
            notifyItemInserted(imageList.size() - 1);
        }
    }

    public void update(SystemImageInfo imageInfo)
    {
        synchronized (imageList) {
            int index = imageList.indexOf(imageInfo);

            if (index != -1) {
                notifyItemChanged(index);
            }
        }
    }

    public boolean remove(SystemImageInfo imageInfo)
    {
        boolean ret;

        synchronized (imageList) {
            int index = imageList.indexOf(imageInfo);
            ret = imageList.remove(imageInfo);

            if (ret) {
                synchronized (selectedList) {
                    if (selectedList.contains(index)) {
                        selectedList.remove(Integer.valueOf(index));
                    }
                }

                notifyItemRemoved(index);
            }
        }

        return ret;
    }

    public void setImageList(List<SystemImageInfo> list)
    {
        synchronized (imageList) {
            imageList.clear();
            imageList.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void setSelected(boolean selected, int position)
    {
        synchronized (selectedList) {
            if (selectedList.contains(position)) {
                if (!selected) {
                    selectedList.remove(Integer.valueOf(position));
                    notifyItemChanged(position);
                }
            } else if (selected) {
                selectedList.add(position);
                notifyItemChanged(position);
            }
        }
    }

    public void clearSelected()
    {
        synchronized (selectedList) {
            while (selectedList.size() > 0) {
                int position = selectedList.get(0);
                selectedList.remove(0);
                notifyItemChanged(position);
            }
        }
    }
    public List<SystemImageInfo> getSelected()
    {
        ArrayList<SystemImageInfo> list = new ArrayList<>();

        synchronized (selectedList) {
            for (Integer value : selectedList) {
                list.add(imageList.get(value));
            }
        }

        return list;
    }

    public int getSelectedCount()
    {
        synchronized (selectedList) {
            return selectedList.size();
        }
    }

    public boolean isSelected(int position)
    {
        synchronized (selectedList) {
            return selectedList.contains(position);
        }
    }

    public List<SystemImageInfo> getItems()
    {
        synchronized (imageList) {
            return imageList;
        }
    }

    public SystemImageInfo getItem(int position)
    {
        synchronized (imageList) {
            return imageList.get(position);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater
                                      .from(parent.getContext())
                                      .inflate(R.layout.image_listview_item, parent, false),
                              this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        SystemImageInfo info = getItem(position);

        synchronized (selectedList) {
            holder.itemView.setSelected(selectedList.contains(position));

            if (holder.itemView.isSelected()) {
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.ed_light_orange));
            } else {
                holder.itemView.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            }
        }

        if (useName == USE_CELESTIAL_NAME) {
            if (info.getCelestialName().isEmpty()) {
                holder.textView.setVisibility(View.GONE);
            } else {
                holder.textView.setVisibility(View.VISIBLE);
                holder.textView.setText(info.getCelestialName());
            }
        } else {
            holder.textView.setVisibility(View.VISIBLE);
            holder.textView.setText(info.getSystem());
        }

        holder.position = position;

        try {
            AndLog.d(LOG_NAME, "Retrieve image URL: " + ProxyConnectionState.getInstance()
                                                                            .generateImageURL(info.getImagePath()).toString());
            Picasso.with(context)
                   .load(ProxyConnectionState
                                 .getInstance()
                                 .generateImageURL(info.getImagePath()).toString())
                   .resize(context.getResources().getInteger(R.integer.thumbnail_width), context.getResources().getInteger(R.integer.thumbnail_height))
                   .placeholder(R.drawable.ic_placeholder)
                   .error(R.drawable.ic_placeholder_error)
                   .centerCrop()
                   .into(holder.imageView);
        } catch (IllegalStateException e) {
            AndLog.d(LOG_NAME, "No connection was established with the Proxy", e);
            Picasso.with(context)
                   .load(R.drawable.ic_placeholder_error)
                   .resize(context.getResources().getInteger(R.integer.thumbnail_width), context.getResources().getInteger(R.integer.thumbnail_height))
                   .into(holder.imageView);
        } catch (MalformedURLException e) {
            AndLog.d(LOG_NAME, "Failed to generate the image URL.", e);
            Picasso.with(context)
                   .load(R.drawable.ic_placeholder_error)
                   .resize(context.getResources().getInteger(R.integer.thumbnail_width), context.getResources().getInteger(R.integer.thumbnail_height))
                   .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount()
    {
        synchronized (imageList) {
            return imageList.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public final TextView textView;
        public final ImageView imageView;
        int position;

        public ViewHolder(View view, ThumbnailAdapter adapter) {
            super(view);

            textView = (TextView) view.findViewById(R.id.ImageName);
            imageView = (ImageView) view.findViewById(R.id.ImageThumbnail);

            ClickListener cl = new ClickListener(imageView, adapter);

            view.setOnClickListener(cl);
            textView.setOnClickListener(cl);
            imageView.setOnClickListener(cl);

            LongClickListener lcl = new LongClickListener(imageView, adapter);

            view.setOnLongClickListener(lcl);
            textView.setOnLongClickListener(lcl);
            imageView.setOnLongClickListener(lcl);
        }

        private class ClickListener implements View.OnClickListener
        {
            private final View view;
            private final ThumbnailAdapter adapter;

            ClickListener(View view, ThumbnailAdapter adapter)
            {
                this.view = view;
                this.adapter = adapter;
            }

            @Override
            public void onClick(View v)
            {
                if (adapter.clickListener != null) {
                    adapter.clickListener.onItemClick(itemView, view, position);
                }
            }
        }

        private class LongClickListener implements View.OnLongClickListener
        {
            private final View view;
            private final ThumbnailAdapter adapter;

            LongClickListener(View view, ThumbnailAdapter adapter)
            {
                this.view = view;
                this.adapter = adapter;
            }


            @Override
            public boolean onLongClick(View v)
            {
                return (adapter.longClickListener != null &&
                        adapter.longClickListener.onItemLongClick(itemView, view, position));

            }
        }
    }

    public interface OnItemClickListener
    {
        void onItemClick(View parent, View view, int position);
    }

    public interface OnItemLongClickListener
    {
        boolean onItemLongClick(View parent, View view, int position);
    }
}
