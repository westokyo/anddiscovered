package com.fussyware.AndDiscovered.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbContract;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;

/**
 * Created by wes on 6/20/15.
 */
public class SystemListCursorAdapter extends BaseCursorAdapter
{
    public static final int DATE_HEADER_VIEW = 0;
    public static final int SYSTEM_ENTRY_VIEW = 1;

    private static final String LOG_NAME = SystemListCursorAdapter.class.getSimpleName();
    private static final DecimalFormat distanceFormatter = new DecimalFormat("0.00");

    private Cursor dbCursor;
    private Date dateRange = new Date(0);

    private final String unknownPosition;

    private final ArrayList<DateHeader> dateList = new ArrayList<>();

    static {
        distanceFormatter.setRoundingMode(RoundingMode.HALF_UP);
    }

    public SystemListCursorAdapter(Context context)
    {
        unknownPosition = context.getResources().getString(R.string.UnknownPosition);

        notifyDatabaseChanged();
    }

    public void notifyDatabaseChanged()
    {
        new DbAsyncTask().execute();
    }

    public void destroy()
    {
        try {
            lock.writeLock().lock();
            if (dbCursor != null) {
                dbCursor.close();
                dbCursor = null;
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    private String getSQLStatement()
    {
        String sql = "SELECT ";
        sql += CmdrDbContract.CmdrDistances._ID;
        sql += ", ";
        sql += CmdrDbContract.CmdrDistances.COLUMN_NAME_FROM;
        sql += ", ";
        sql += CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE;
        sql += ", ";
        sql += CmdrDbContract.CmdrDistances.COLUMN_NAME_DISTANCE;
        sql += " FROM ";
        sql += CmdrDbContract.CmdrDistances.TABLE_NAME;

        sql += " WHERE ";
        sql += CmdrDbContract.CmdrDistances.COLUMN_NAME_CREATEDATE;
        sql += ">=";

        try {
            lock.readLock().lock();
            sql += Long.toString(dateRange.getTime() / 1000);
        } finally {
            lock.readLock().unlock();
        }

        sql += " ORDER BY ";
        sql += CmdrDbContract.CmdrDistances._ID;
        sql += " DESC";

        return sql;
    }

    public void setDateRange(Date date)
    {
        try {
            lock.writeLock().lock();
            dateRange = date;
            notifyDatabaseChanged();
        } finally {
            lock.writeLock().unlock();
        }
    }

    public boolean isReady()
    {
        try {
            lock.readLock().lock();
            return (dbCursor != null);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public boolean areAllItemsEnabled()
    {
        return false;
    }

    @Override
    public boolean isEnabled(int position)
    {
        DateHeader header = getDateHeader(position);
        return ((header != null) && (header.position != position));
    }

    @Override
    public int getCount()
    {
        try {
            lock.readLock().lock();
            return (null != dbCursor) ? (dbCursor.getCount() + dateList.size()) : 0;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return 2;
    }

    @Override
    public int getItemViewType(int position)
    {
        DateHeader header = getDateHeader(position);
        return ((header != null) && (header.position == position)) ? SYSTEM_ENTRY_VIEW : DATE_HEADER_VIEW;
    }

    @Override
    public Item getItem(int position)
    {
        try {
            lock.readLock().lock();

            Item item = null;
            DateHeader header = getDateHeader(position);

            if (header != null) {
                if (header.position == position) {
                    item = header;
                } else {
                    HashMap<String, String> map = new HashMap<>();

                    dbCursor.moveToPosition(position - header.modifier);

                    String system = dbCursor.getString(1);

                    map.put("system", system);
                    map.put("date", CmdrDbContract.DATE_FORMATTER.format(new Date(dbCursor.getLong(2) * 1000)));

                    if (dbCursor.isNull(3)) {
                        map.put("distance", "0.00");
                    } else {
                        map.put("distance", distanceFormatter.format(dbCursor.getDouble(3)));
                    }

                    CmdrSystemInfo sinfo = dbHelper.getSystem(system);
                    if (sinfo == null) {
                        map.put("star", StarType.Unknown.toString());
                        map.put("position", unknownPosition);
                        map.put("notes", "");
                    } else {
                        StarType stype = (sinfo.getMainStar() == null ?
                                          StarType.Unknown :
                                          sinfo.getMainStar().getType());

                        map.put("star", stype.toString());
                        map.put("notes", sinfo.getNote());
                        map.put("position",
                                (sinfo.getPosition() == null) ?
                                unknownPosition :
                                sinfo.getPosition().toString());
                    }

                    item = new SystemEntryView(map);
                }
            }

            return item;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Item item = getItem(position);

        if (convertView == null) {
            convertView = item.getView(LayoutInflater.from(parent.getContext()),
                                       null,
                                       parent);
        } else {
            Object value = convertView.getTag();

            switch (item.getViewType()) {
                case DATE_HEADER_VIEW:
                    if (value instanceof ViewHolder) {
                        convertView = null;
                    }
                    break;
                case SYSTEM_ENTRY_VIEW:
                    if (!(value instanceof ViewHolder)) {
                        convertView = null;
                    }
                    break;
            }

            convertView = item.getView(LayoutInflater.from(parent.getContext()),
                                       convertView,
                                       parent);
        }

        return convertView;
    }

    private DateHeader getDateHeader(int position)
    {
        ListIterator<DateHeader> li = dateList.listIterator(dateList.size());
        while (li.hasPrevious()) {
            DateHeader date = li.previous();

            if (position >= date.position) {
                return date;
            }
        }

        return null;
    }

    public interface Item
    {
        int getViewType();
        View getView(LayoutInflater inflater,
                     View convertView,
                     ViewGroup parent);
    }

    public static class DateHeader implements Item
    {
        public final int position;
        public int modifier;
        public String date;

        DateHeader(int position, int modifier, String date)
        {
            this.position = position;
            this.modifier = modifier;
            this.date = date;
        }

        @Override
        public int getViewType()
        {
            return DATE_HEADER_VIEW;
        }

        @Override
        public View getView(LayoutInflater inflater, View convertView, ViewGroup parent)
        {
            TextView view;

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.cmd_system_listview_dateheader_item, parent, false);

                view = (TextView) convertView.findViewById(R.id.DateHeaderText);
                convertView.setTag(view);
            } else {
                view = (TextView) convertView.getTag();
            }

            view.setText(date);

            return convertView;
        }

        @Override
        public String toString()
        {
            return "DateHeader{" +
                   "position=" + position +
                   ", modifier=" + modifier +
                   ", date='" + date + '\'' +
                   '}';
        }
    }

    public static class SystemEntryView implements Item
    {
        public final HashMap<String, String> map;

        SystemEntryView(HashMap<String, String> map)
        {
            this.map = map;
        }

        @Override
        public int getViewType()
        {
            return SYSTEM_ENTRY_VIEW;
        }

        @Override
        public View getView(LayoutInflater inflater, View convertView, ViewGroup parent)
        {
            ViewHolder viewHolder;

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.cmdr_system_listview_item, parent, false);

                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if (map.size() > 0) {
                viewHolder.systemName.setText(map.get("system"));
                viewHolder.starType.setText(map.get("star"));
                viewHolder.lastSystemDistance.setText(map.get("distance"));
                viewHolder.position.setText(map.get("position"));
                viewHolder.notes.setText(map.get("notes"));
            }

            return convertView;
        }

        @Override
        public String toString()
        {
            return "SystemEntryView{" +
                   "map=" + map +
                   '}';
        }
    }

    private static class ViewHolder
    {
        final TextView systemName;
        final TextView starType;
        final TextView lastSystemDistance;
        final TextView position;
        final TextView notes;

        ViewHolder(View layout)
        {
            systemName = (TextView) layout.findViewById(R.id.SystemText);
            starType = (TextView) layout.findViewById(R.id.MainStarText);
            lastSystemDistance = (TextView) layout.findViewById(R.id.DistanceText);
            position = (TextView) layout.findViewById(R.id.PositionText);
            notes = (TextView) layout.findViewById(R.id.NotesSummaryText);
        }
    }

    private class DbAsyncTask extends AsyncTask<Void, Void, ArrayList<Object>>
    {
        @Override
        protected void onPostExecute(ArrayList<Object> list)
        {
            try {
                lock.writeLock().lock();
                if (list.get(0) instanceof Cursor) {
                    Cursor cursor = (Cursor) list.get(0);
                    if (null != dbCursor) {
                        dbCursor.close();
                    }

                    dbCursor = cursor;
                }

                if (list.get(1) instanceof ArrayList) {
                    ArrayList<DateHeader> dates = (ArrayList<DateHeader>) list.get(1);

                    if (!dateList.isEmpty()) {
                        dateList.clear();
                    }

                    dateList.addAll(dates);
                }

                notifyDataSetChanged();
            } finally {
                lock.writeLock().unlock();
            }
        }

        @Override
        protected ArrayList<Object> doInBackground(Void... params)
        {
            Cursor cursor = dbHelper.getReadableDatabase().rawQuery(getSQLStatement(), null);

            int modifier = 0;

            ArrayList<DateHeader> dates = new ArrayList<>();

            DateFormat formatter = SimpleDateFormat.getDateInstance();
            String previous = "";
            Date today = new Date();

            while (cursor.moveToNext()) {
                String current = formatter.format(new Date(cursor.getLong(2) * 1000));

                if (!current.equals(previous)) {
                    previous = current;

                    if (current.equals(formatter.format(today))) {
                        dates.add(new DateHeader(cursor.getPosition() + modifier, ++modifier, "Today"));
                    } else {
                        dates.add(new DateHeader(cursor.getPosition() + modifier, ++modifier, current));
                    }
                }
            }

            // Move the cursor back before the first entry
            cursor.moveToPosition(-1);

            ArrayList<Object> ret = new ArrayList<>();
            ret.add(cursor);
            ret.add(dates);

            return ret;
        }
    }
}
