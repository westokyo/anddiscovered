package com.fussyware.AndDiscovered.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.dialog.CheckDialogFragment;

/**
 * Created by wes on 9/5/15.
 */
public class CheckListAdapter extends BaseAdapter
{
    private static final String LOG_NAME = CheckListAdapter.class.getSimpleName();

    private CheckDialogFragment.CheckListItem[] items;

    public CheckListAdapter(CheckDialogFragment.CheckListItem[] items)
    {
        this.items = items;
    }

    @Override
    public int getCount()
    {
        return items.length;
    }

    public CheckDialogFragment.CheckListItem[] getItems()
    {
        return items;
    }

    @Override
    public Object getItem(int position)
    {
        return items[position];
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                                        .inflate(R.layout.check_list_dialog_item,
                                                 parent,
                                                 false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(items[position].name);
        holder.checkBox.setChecked(items[position].isChecked());

        return convertView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public final TextView name;
        public final CheckBox checkBox;

        public ViewHolder(View view)
        {
            super(view);

            this.name = (TextView) view.findViewById(R.id.ItemNameText);
            this.checkBox = (CheckBox) view.findViewById(R.id.ItemCheckBox);
        }
    }
}
