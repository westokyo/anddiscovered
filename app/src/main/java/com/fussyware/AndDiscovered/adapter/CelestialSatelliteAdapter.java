package com.fussyware.AndDiscovered.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.StarBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wes on 11/29/15.
 */
public class CelestialSatelliteAdapter extends RecyclerView.Adapter<CelestialSatelliteAdapter.ViewHolder>
{
    public interface SatelliteListener
    {
        void onSatelliteClick(int position, View view, CelestialBody body);
        boolean onSatelliteLongClick(int position, View view, CelestialBody body);

        /** A Drag event has occurred on an internal View. Report it back to the
         * holder of the Adapter.
         * @param view The View the DragEvent occurred <b>on</b>.
         * @param event The DragEvent detailing what is happening.
         * @param body The CelestialBody that is <em>being</em> effected.
         * @return True if the onDrag handled the event. Otherwise False.
         */
        boolean onSatelliteDrag(View view, DragEvent event, CelestialBody body);
    }

    private static final String LOG_NAME = CelestialSatelliteAdapter.class.getSimpleName();

    private final ArrayList<Item> itemList = new ArrayList<>();
    private final Resources resources;

    private SatelliteListener listener;
    private StarBody mainStar;

    public CelestialSatelliteAdapter(Context context)
    {
        this.resources = context.getResources();
    }

    public void setSatelliteClickListener(SatelliteListener listener)
    {
        this.listener = listener;
    }

    public void setMainStar(StarBody body)
    {
        mainStar = body;
    }

    public int indexOf(CelestialBody body)
    {
        if (body != null) {
            for (int i = 0; i < itemList.size(); i++) {
                if (itemList.get(i).body.equals(body)) {
                    return i;
                }
            }
        }

        return -1;
    }

    public void add(@NonNull CelestialBody body)
    {
        itemList.add(new Item(body));
        notifyItemInserted(itemList.size() - 1);
    }

    public void add(int position, @NonNull CelestialBody body)
    {
        itemList.add(position, new Item(body));
        notifyItemInserted(position);
    }

    public void addMainStar(@NonNull StarBody body)
    {
        mainStar = body;
        add(0, body);
    }

    public void remove(int position)
    {
        itemList.remove(position);
        notifyItemRemoved(position);
    }

    public void remove(CelestialBody body)
    {
        for (int i = 0; i < itemList.size(); i++) {
            if (itemList.get(i).body.equals(body)) {
                itemList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void update(List<CelestialBody> bodies)
    {
        if (bodies.size() != itemList.size()) {
            set(bodies);
        } else {
            for (int i = 0; i < bodies.size(); i++) {
                if (!itemList.get(i).body.equals(bodies.get(i))) {
                    itemList.set(i, new Item(bodies.get(i)));
                    notifyItemChanged(i);
                }
            }
        }

    }

    public void update(CelestialBody body)
    {
        for (int i = 0; i < itemList.size(); i++) {
            if (itemList.get(i).body.equals(body)) {
                itemList.set(i, new Item(body));
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void set(CelestialBody[] bodies)
    {
        itemList.clear();

        for (CelestialBody body : bodies) {
            itemList.add(new Item(body));
        }

        notifyDataSetChanged();
    }

    public void set(List<CelestialBody> bodies)
    {
        itemList.clear();

        for (CelestialBody body : bodies) {
            itemList.add(new Item(body));
        }

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater
                                      .from(parent.getContext())
                                      .inflate(R.layout.celestial_body_list_item_layout,
                                               parent,
                                               false),
                              this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Item item = itemList.get(position);

        holder.button.setBackground(resources.getDrawable(item.resourceId));
        holder.numSatellitesText.setText(String.format("%d", item.body.getSatellites().size()));

        holder.planetDetailScanned.setVisibility(View.GONE);
        holder.planetFirstDiscovered.setVisibility(View.GONE);
        holder.starDetailScanned.setVisibility(View.GONE);
        holder.starFirstDiscovered.setVisibility(View.GONE);
        holder.starMain.setVisibility(View.GONE);

        switch (item.body.getSatelliteCategory()) {
            case Star:
                if (item.body.getScanLevel() == ScanLevel.Level_3) {
                    holder.starDetailScanned.setVisibility(View.VISIBLE);
                }

                if (item.body.isFirstDiscovered()) {
                    holder.starFirstDiscovered.setVisibility(View.VISIBLE);
                }

                if (item.body.equals(mainStar)) {
                    holder.starMain.setVisibility(View.VISIBLE);
                }

                break;
            case Planet:
                if (item.body.getScanLevel() == ScanLevel.Level_3) {
                    holder.planetDetailScanned.setVisibility(View.VISIBLE);
                }

                if (item.body.isFirstDiscovered()) {
                    holder.planetFirstDiscovered.setVisibility(View.VISIBLE);
                }
                break;
            case Asteroid:
                break;
            case Unknown:
                break;
        }
    }

    @Override
    public int getItemCount()
    {
        return itemList.size();
    }

    private class Item
    {
        public final int resourceId;
        public final CelestialBody body;

        Item(CelestialBody body)
        {
            SatelliteCategory category = body.getSatelliteCategory();
            TypedArray resourceArray;
            int position = 0;

            switch (category)
            {
                case Star:
                    resourceArray = resources.obtainTypedArray(R.array.star_icons);
                    position = ((StarBody) body).getType().value;
                    break;
                case Planet:
                    resourceArray = resources.obtainTypedArray(R.array.planet_icons);
                    position = ((PlanetBody) body).getType().value;
                    break;
                case Asteroid:
//                    resourceArray = resources.obtainTypedArray(R.array.asteroid_icons);
//                    position = ((AsteroidBody) body).getType().value;
                    resourceArray = null;
                    break;
                default:
                    resourceArray = null;
                    break;
            }

            if (resourceArray != null) {
                this.resourceId = resourceArray.getResourceId(position, 0);
                resourceArray.recycle();
            } else {
                resourceId = 0;
            }

            this.body = body;
        }

        @Override
        public boolean equals(Object o)
        {
            return ((o != null) &&
                    (o.getClass() == body.getClass()) &&
                    body.equals(o));

        }

        @Override
        public int hashCode()
        {
            return body.hashCode();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public final Button button;
        public final TextView numSatellitesText;
        public final TextView starDetailScanned;
        public final TextView starFirstDiscovered;
        public final TextView starMain;
        public final TextView planetDetailScanned;
        public final TextView planetFirstDiscovered;

        private final View layout;

        public ViewHolder(View view,
                          final CelestialSatelliteAdapter adapter)
        {
            super(view);

            this.layout = view;

            this.numSatellitesText = (TextView) view.findViewById(R.id.num_satellites_text);
            this.starDetailScanned = (TextView) view.findViewById(R.id.detail_scanned_star_text);
            this.starFirstDiscovered = (TextView) view.findViewById(R.id.first_discover_star_text);
            this.starMain = (TextView) view.findViewById(R.id.main_star_text);
            this.planetDetailScanned = (TextView) view.findViewById(R.id.detail_scanned_planet_text);
            this.planetFirstDiscovered = (TextView) view.findViewById(R.id.first_discover_planet_text);

            this.button = (Button) view.findViewById(R.id.selection_icon_button);

            button.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (adapter.listener != null) {
                        int position = getLayoutPosition();

                        adapter.listener.onSatelliteClick(position, layout,
                                                          adapter.itemList.get(position).body);
                    }
                }
            });

            button.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View v)
                {
                    int position = getLayoutPosition();

                    return ((adapter.listener != null) &&
                            adapter.listener.onSatelliteLongClick(position, layout,
                                                                  adapter.itemList
                                                                          .get(position)
                                                                          .body));
                }
            });

//            view.setOnTouchListener(new View.OnTouchListener()
//            {
//                @Override
//                public boolean onTouch(View v, MotionEvent event)
//                {
//                    Log.d(LOG_NAME, event.toString());
//                    return false;
//                }
//            });

            view.setOnDragListener(new View.OnDragListener()
            {
                @Override
                public boolean onDrag(View v, DragEvent event)
                {
                    if (adapter.listener != null) {
                        int position = getLayoutPosition();

                        if (position != -1) {
                            boolean ret = adapter.listener.onSatelliteDrag(v,
                                                                           event,
                                                                           adapter.itemList
                                                                                   .get(position)
                                                                                   .body);

                            if (ret && (event.getAction() == DragEvent.ACTION_DROP)) {
                                /** We need to pull the position again just in case
                                 * the layout was changed from under our feet.
                                 */
                                position = getLayoutPosition();
                                adapter.notifyItemChanged(position);
                            }

                            return ret;
                        }
                    }

                    return false;
                }
            });
        }
    }
}
