package com.fussyware.AndDiscovered.adapter;

import android.os.AsyncTask;

import com.fussyware.AndDiscovered.celestial.DistanceInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wes on 12/15/15.
 */
public abstract class ReferenceDistanceSelector
{
    private static final String TAG = ReferenceDistanceSelector.class.getSimpleName();

    public interface OnPostExecuteListener
    {
        void onPostExecute(ArrayList<DistanceInfo> systemDistanceInfos);
    }

    protected final CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();

    private final AtomicInteger requestCount = new AtomicInteger(0);
    private OnPostExecuteListener listener;

    ReferenceDistanceSelector()
    {
    }

    public void setOnPostExecuteListener(OnPostExecuteListener listener)
    {
        this.listener = listener;
    }

    public void execute()
    {
        if (requestCount.getAndIncrement() == 0) {
            new InnerAsyncTask().execute();
        }
    }

    public abstract void close();

    protected void onPreExecute() {}
    protected void onPostExecute(ArrayList<DistanceInfo> distanceInfos) {}
    protected void onProgressUpdate() {}
    protected void onCancelled() {}
    protected abstract ArrayList<DistanceInfo> doInBackground();

    protected class InnerAsyncTask extends AsyncTask<Void, Void, ArrayList<DistanceInfo>>
    {
        @Override
        protected void onPreExecute()
        {
            ReferenceDistanceSelector.this.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<DistanceInfo> distanceInfos)
        {
            ReferenceDistanceSelector.this.onPostExecute(distanceInfos);

            if (listener != null) {
                listener.onPostExecute(distanceInfos);
            }

            if (requestCount.decrementAndGet() > 0) {
                new InnerAsyncTask().execute();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values)
        {
            ReferenceDistanceSelector.this.onProgressUpdate();
        }

        @Override
        protected void onCancelled()
        {
            ReferenceDistanceSelector.this.onCancelled();
        }

        @Override
        protected ArrayList<DistanceInfo> doInBackground(Void... params)
        {
            return ReferenceDistanceSelector.this.doInBackground();
        }
    }

}
