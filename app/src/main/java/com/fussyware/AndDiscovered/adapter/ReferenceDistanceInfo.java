package com.fussyware.AndDiscovered.adapter;

import android.os.Parcel;
import android.os.Parcelable;

import com.fussyware.AndDiscovered.celestial.DistanceInfo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wes on 1/19/16.
 */
public class ReferenceDistanceInfo extends DistanceInfo
{
    public static final Parcelable.Creator<ReferenceDistanceInfo> CREATOR = new Creator<ReferenceDistanceInfo>()
    {
        @Override
        public ReferenceDistanceInfo createFromParcel(Parcel source)
        {
            return new ReferenceDistanceInfo(source);
        }

        @Override
        public ReferenceDistanceInfo[] newArray(int size)
        {
            return new ReferenceDistanceInfo[size];
        }
    };

    private static final AtomicInteger creationReference = new AtomicInteger(0);

    private final int id;

    public ReferenceDistanceInfo(String first, String second)
    {
        super(first, second, 0.0);

        id = creationReference.incrementAndGet();
    }

    private ReferenceDistanceInfo(Parcel parcel)
    {
        super(parcel);
        id = parcel.readInt();
    }

    public int getId()
    {
        return id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        super.writeToParcel(dest, flags);

        dest.writeInt(id);
    }
}
