package com.fussyware.AndDiscovered.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fussyware.AndDiscovered.R;

import java.util.ArrayList;

/**
 * Created by wes on 11/29/15.
 */
public class CelestialIconAdapter extends RecyclerView.Adapter<CelestialIconAdapter.ViewHolder>
{
    public interface OnItemClickListener
    {
        void onItemClick(View view, int position);
    }

    public interface OnItemLongClickListener
    {
        boolean onItemLongClick(View view, int position);
    }

    private static final String LOG_NAME = CelestialIconAdapter.class.getSimpleName();

    private final ArrayList<Integer> iconList = new ArrayList<>();
    private final Resources resources;

    private OnItemClickListener clickListener;
    private OnItemLongClickListener longClickListener;

    public CelestialIconAdapter(Context context, TypedArray icons)
    {
        this.resources = context.getResources();

        set(icons);
    }

    public void setItemClickListener(OnItemClickListener listener)
    {
        this.clickListener = listener;
    }

    public void setItemLongClickListener(OnItemLongClickListener listener)
    {
        this.longClickListener = listener;
    }

    public void add(int iconId)
    {
        iconList.add(iconId);
        notifyItemInserted(iconList.size() - 1);
    }

    public void remove(int position)
    {
        iconList.remove(position);
        notifyItemRemoved(position);
    }

    public void set(int[] icons)
    {
        iconList.clear();

        for (int icon : icons) {
            iconList.add(icon);
        }

        notifyDataSetChanged();
    }

    public void set(TypedArray icons)
    {
        iconList.clear();

        for (int i = 0; i < icons.length(); i++) {
            iconList.add(icons.getResourceId(i, 0));
        }

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater
                                      .from(parent.getContext())
                                      .inflate(R.layout.selection_icon_item_layout,
                                               parent,
                                               false),
                              this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.button.setBackground(resources.getDrawable(iconList.get(position)));
    }

    @Override
    public int getItemCount()
    {
        return iconList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public final Button button;
        private final View view;

        public ViewHolder(View layout,
                          final CelestialIconAdapter adapter)
        {
            super(layout);

            this.view = layout;
            this.button = (Button) layout.findViewById(R.id.selection_icon_button);

            button.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (adapter.clickListener != null) {
                        adapter.clickListener.onItemClick(view, getLayoutPosition());
                    }
                }
            });

            button.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View v)
                {
                    return ((adapter.longClickListener != null) &&
                            adapter.longClickListener.onItemLongClick(view, getLayoutPosition()));
                }
            });

            layout.setOnDragListener(new View.OnDragListener()
            {
                @Override
                public boolean onDrag(View v, DragEvent event)
                {
                    switch (event.getAction()) {
                        case DragEvent.ACTION_DRAG_STARTED:
                            button.setEnabled(false);
                            return true;
                        case DragEvent.ACTION_DRAG_ENDED:
                            button.setEnabled(true);
                            return true;
                        default:
                            return false;
                    }
                }
            });
        }
    }
}
