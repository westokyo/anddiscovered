package com.fussyware.AndDiscovered.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wes on 8/30/15.
 */
public class StringListAdapter extends RecyclerView.Adapter<StringListAdapter.ViewHolder>
{
    private ArrayList<String> values;
    private int layoutId;

    public StringListAdapter(int layout, List<String> values)
    {
        this.values = new ArrayList<>(values);
        this.layoutId = layout;
    }

    @Override
    public int getItemCount()
    {
        return values.size();
    }

    public void setItems(List<String> values)
    {
        ArrayList<String> list = new ArrayList<>(values);

        if (!this.values.equals(list)) {
            this.values = list;
            notifyDataSetChanged();
        }
    }

    public List<String> getItems()
    {
        return Collections.unmodifiableList(values);
    }

    public String getItem(int position)
    {
        String value;

        if (position < values.size()) {
            value = values.get(position);
        } else {
            value = "";
        }

        return value;
    }

    @Override
    public StringListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(layoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.textView.setText(values.get(position));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textView;

        public ViewHolder(View view) {
            super(view);

            textView = (TextView) view.findViewById(R.id.HorizontalListText);
        }
    }
}
