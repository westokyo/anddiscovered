package com.fussyware.AndDiscovered.adapter;

import android.widget.BaseAdapter;

import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by wes on 6/2/15.
 */
public abstract class BaseCursorAdapter extends BaseAdapter
{
    private static final String LOG_NAME = BaseCursorAdapter.class.getSimpleName();

    protected CmdrDbHelper dbHelper;
    protected ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public BaseCursorAdapter()
    {
        dbHelper = CmdrDbHelper.getInstance();
    }

    public abstract void notifyDatabaseChanged();
}
