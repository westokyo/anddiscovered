package com.fussyware.AndDiscovered.preference;

/**
 * Created by wes on 9/30/15.
 */
public final class PreferenceTag
{
    public static final String CMDR_NAME = "pref_cmdr_name";
    public static final String DISPLAY_DAYS = "pref_display_days";
    public static final String ROUTE = "pref_route";
    public static final String ALLOW_IMAGES = "pref_allow_images";
    public static final String DISCOVERY_TTL = "pref_discovery_ttl";
    public static final String IPADDR = "pref_ipaddr";
    public static final String DISCOVERY_ENABLED = "pref_discovery_enabled";
    public static final String MAX_JUMP_DISTANCE = "pref_max_jump";
    public static final String STAY_AWAKE = "pref_stay_awake";

    private PreferenceTag() {}
}
