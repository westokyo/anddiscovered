package com.fussyware.AndDiscovered.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 8/31/15.
 */
public class NumberPickerPreference extends DialogPreference
{
    private static final String LOG_NAME = NumberPickerPreference.class.getSimpleName();
    private static final int DEFAULT_VALUE = 7;

    private NumberPicker picker;

    public NumberPickerPreference(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);

        setDialogLayoutResource(R.layout.numberpicker_pref_layout);
    }

    public NumberPickerPreference(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        setDialogLayoutResource(R.layout.numberpicker_pref_layout);
    }

    @Override
    protected void onBindDialogView(View view)
    {
        picker = (NumberPicker) view.findViewById(R.id.DisplayDaysPicker);
        picker.setMinValue(1);
        picker.setMaxValue(365);
        picker.setValue(getPersistedInt(DEFAULT_VALUE));

        if (view instanceof ViewGroup) {
            ((ViewGroup) view).setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        }

        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus();

        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult)
    {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            persistInt(picker.getValue());
        }

        picker = null;
    }

    @Override
    protected Parcelable onSaveInstanceState()
    {
        final Parcelable superState = super.onSaveInstanceState();

        // Check whether this Preference is persistent (continually saved)
        if (isPersistent()) {
            // No need to save instance state since it's persistent,
            // use superclass state
            return superState;
        }

        // Create instance of custom BaseSavedState
        final SavedState myState = new SavedState(superState);
        // Set the state's value with the class member that holds current
        // setting value
        myState.value = picker.getValue();

        return myState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        // Check whether we saved the state in onSaveInstanceState
        if (state == null || !state.getClass().equals(SavedState.class)) {
            // Didn't save the state, so call superclass
            super.onRestoreInstanceState(state);
            return;
        }

        // Cast state to custom BaseSavedState and pass to superclass
        SavedState myState = (SavedState) state;
        super.onRestoreInstanceState(myState.getSuperState());

        // Set this Preference's widget to reflect the restored state
        picker.setValue(myState.value);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index)
    {
        return a.getInteger(index, DEFAULT_VALUE);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue)
    {
        if (!restorePersistedValue) {
            persistInt((defaultValue == null) ? DEFAULT_VALUE : (Integer) defaultValue);
        }
    }

    private static class SavedState extends BaseSavedState {
        // Member that holds the setting's value
        // Change this data type to match the type saved by your Preference
        int value;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel source) {
            super(source);
            // Get the current preference's value
            value = source.readInt();  // Change this to read the appropriate data type
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            // Write the preference's value
            dest.writeInt(value);  // Change this to write the appropriate data type
        }

        // Standard creator object using an instance of this class
        public static final Parcelable.Creator<SavedState> CREATOR =
            new Parcelable.Creator<SavedState>() {

                public SavedState createFromParcel(Parcel in) {
                    return new SavedState(in);
                }

                public SavedState[] newArray(int size) {
                    return new SavedState[size];
                }
            };
    }
}
