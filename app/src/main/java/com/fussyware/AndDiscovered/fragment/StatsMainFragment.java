package com.fussyware.AndDiscovered.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.statistics.PlanetInfo;
import com.fussyware.AndDiscovered.statistics.StarInfo;

import java.util.HashMap;
import java.util.List;

/**
 * Created by wes on 1/12/16.
 */
public class StatsMainFragment extends BaseStatsFragment
{
    private static final String TAG = StatsMainFragment.class.getSimpleName();

    private final CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();

    private TextView earningsText;
    private TextView systemsText;
    private TextView starsText;
    private TextView planetsText;
    private TextView cbScannedText;
    private TextView dsScannedText;
    private TextView firstText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.stat_main_fragment_layout,
                                       container,
                                       false);

        earningsText = (TextView) layout.findViewById(R.id.stat_main_earnings);
        systemsText = (TextView) layout.findViewById(R.id.stat_main_systems_visited);
        starsText = (TextView) layout.findViewById(R.id.stat_main_stars_visited);
        planetsText = (TextView) layout.findViewById(R.id.stat_main_plantes_visited);
        cbScannedText = (TextView) layout.findViewById(R.id.stat_main_cb_scanned);
        dsScannedText = (TextView) layout.findViewById(R.id.stat_main_cb_ds_scanned);
        firstText = (TextView) layout.findViewById(R.id.stat_main_first);

        invalidated = true;

        return layout;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        onRefresh();
    }

    @Override
    public void onRefresh()
    {
        if (invalidated) {
            new RefreshTask().execute();
            invalidated = false;
        }
    }

    private class RefreshTask extends AsyncTask<Void, Void, HashMap<String, Long>>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            String loading = getString(R.string.loading);

            systemsText.setText(loading);
            planetsText.setText(loading);
            starsText.setText(loading);
            earningsText.setText(loading);
            cbScannedText.setText(loading);
            dsScannedText.setText(loading);
            firstText.setText(loading);
        }

        @Override
        protected void onPostExecute(HashMap<String, Long> map)
        {
            super.onPostExecute(map);

            systemsText.setText(String.format("%,d", map.get("system_count")));

            planetsText.setText(String.format("%,d", map.get("planet_count")));
            starsText.setText(String.format("%,d", map.get("star_count")));
            earningsText.setText(String.format("%,d Cr", map.get("earnings")));
            cbScannedText.setText(String.format("%,d", map.get("cb_scanned")));
            dsScannedText.setText(String.format("%,d", map.get("ds_scanned")));
            firstText.setText(String.format("%,d", map.get("first_discovered")));
        }

        @Override
        protected HashMap<String, Long> doInBackground(Void... params)
        {
            HashMap<String, Long> map = new HashMap<>();

            long cbScanned = 0;
            long dsScanned = 0;
            long firstDiscovered = 0;
            long earnings = 0;

            map.put("system_count", (long) dbHelper.getStatSystemCount());

            List<PlanetInfo> planetList = dbHelper.getStatPlanets();
            for (PlanetInfo info : planetList) {
                int credits = info.getType().credits;

                if (info.isTerraformable()) {
                    switch (info.getType()) {
                        case Rocky:
                            credits *= 50;
                            break;
                        case High_Metallic:
                            credits *= 7.3;
                            break;
                        case Water_World:
                            credits *= 2;
                            break;
                        default:
                            break;
                    }
                }

                if (info.getScanLevel() == ScanLevel.Level_3) {
                    dsScanned++;
                    credits *= 2;
                }

                if (info.isFirstDiscovered()) {
                    firstDiscovered++;
                    credits *= 1.5;
                }

                cbScanned++;
                earnings += credits;
            }

            List<StarInfo> starList = dbHelper.getStatStars();
            for (StarInfo info : starList) {
                int credits = info.getType().credits;

                if (info.getScanLevel() == ScanLevel.Level_3) {
                    dsScanned++;
                    credits *= 2;
                }

                if (info.isFirstDiscovered()) {
                    firstDiscovered++;
                    credits *= 1.5;
                }

                cbScanned++;
                earnings += credits;
            }

            map.put("planet_count", (long) planetList.size());
            map.put("star_count", (long) starList.size());
            map.put("earnings", earnings);
            map.put("cb_scanned", cbScanned);
            map.put("ds_scanned", dsScanned);
            map.put("first_discovered", firstDiscovered);

            return map;
        }
    }
}
