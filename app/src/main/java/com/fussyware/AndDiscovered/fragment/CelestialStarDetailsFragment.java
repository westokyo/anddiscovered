package com.fussyware.AndDiscovered.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.dialog.CelestialStarEditDialogFragment;

/**
 * Created by wes on 11/17/15.
 */
public class CelestialStarDetailsFragment
        extends Fragment
        implements CelestialStarEditDialogFragment.OnDoneListener
{
    private static final String LOG_NAME = CelestialStarDetailsFragment.class.getSimpleName();

    private final Object mutex = new Object();

    private StarBody celestialBody;

    private View mainView;

    private TextView ageText;
    private TextView massText;
    private TextView radiusText;
    private TextView surfaceTempText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                celestialBody = bundle.getParcelable("celestial_body");
            }
        } else {
            celestialBody = savedInstanceState.getParcelable("celestial_body");
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.cb_star_details_layout,
                                       container,
                                       false);

        createViews(mainView);

        if (celestialBody == null) {
            mainView.setVisibility(View.GONE);
        } else {
            setCelestialBody(celestialBody);
        }

        return mainView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial_body", celestialBody);
    }

    public void setCelestialBody(StarBody body)
    {
        synchronized (mutex) {
            celestialBody = body;

            ageText.setText((body.getAge() == null) ?
                            "" :
                            String.format("%1.2f", body.getAge()));
            massText.setText((body.getMass() == null) ?
                             "" :
                             String.format("%1.4f", body.getMass()));
            radiusText.setText((body.getRadius() == null) ?
                               "" :
                               String.format("%1.4f", body.getRadius()));
            surfaceTempText.setText((body.getSurfaceTemp() == null) ?
                                    "" :
                                    String.format("%1.2f", body.getSurfaceTemp()));

            mainView.setVisibility(View.VISIBLE);
        }
    }

    public void onDone(Bundle bundle)
    {
        for (String key : bundle.keySet()) {
            double dbl = bundle.getDouble(key);

            switch (key) {
                case CelestialStarEditDialogFragment.TAG_AGE:
                    celestialBody.setAge(dbl);
                    ageText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialStarEditDialogFragment.TAG_MASS:
                    celestialBody.setMass(dbl);
                    massText.setText(String.format("%1.4f", dbl));
                    break;
                case CelestialStarEditDialogFragment.TAG_RADIUS:
                    celestialBody.setRadius(dbl);
                    radiusText.setText(String.format("%1.4f", dbl));
                    break;
                case CelestialStarEditDialogFragment.TAG_SURFACETEMP:
                    celestialBody.setSurfaceTemp(dbl);
                    surfaceTempText.setText(String.format("%1.2f", dbl));
                    break;
                default:
                    break;
            }
        }
    }

    private void createViews(View layout)
    {
        mainView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CelestialStarEditDialogFragment df = CelestialStarEditDialogFragment.newInstance(celestialBody);
                df.show(getChildFragmentManager(), FragmentTag.celestial_info_fragment.celestial_star_dialog);
            }
        });

        ageText = (TextView) layout.findViewById(R.id.cb_star_age_value);
        massText = (TextView) layout.findViewById(R.id.cb_star_mass_value);
        radiusText = (TextView) layout.findViewById(R.id.cb_star_radius_value);
        surfaceTempText = (TextView) layout.findViewById(R.id.cb_star_surface_temp_value);
    }

    @Override
    public void onDone(String tag, Bundle changedItems)
    {
        onDone(changedItems);
    }
}
