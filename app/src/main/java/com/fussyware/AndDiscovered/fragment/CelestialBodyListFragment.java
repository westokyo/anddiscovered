package com.fussyware.AndDiscovered.fragment;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.CelestialSatelliteAdapter;
import com.fussyware.AndDiscovered.celestial.AsteroidBody;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.Mineral;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.PlanetType;
import com.fussyware.AndDiscovered.celestial.SatelliteCategory;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.dialog.CancelableAlertDialogFragment;
import com.fussyware.AndDiscovered.dialog.OkAlertDialogFragment;
import com.fussyware.AndDiscovered.dialog.TextPickerDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrAsteroidBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrPlanetBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrStarBody;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.view.ActiveViewPager;
import com.fussyware.AndDiscovered.view.SlidingTray;

import java.util.List;
import java.util.Stack;

/**
 * Created by wes on 9/30/15.
 */
public class CelestialBodyListFragment
        extends Fragment
        implements StarIconGridFragment.OnStarClickListeners,
                   PlanetIconGridFragment.OnPlanetClickListeners,
                   TextPickerDialogFragment.OnTextChangedListener,
                   CancelableAlertDialogFragment.OnAlertListener
{
    public interface OnCelestialBodyClickListener
    {
        void onCelestialBodyClick(CelestialBody body);
    }

    private static final String LOG_NAME = CelestialBodyListFragment.class.getSimpleName();

    private HolderFragment holderFragment;
    private CmdrSystemInfo selectedSystem;

    private View rootLayout;
    private Button rootButton;
    private ActiveViewPager viewPager;
    private SlidingTray slidingTray;

    private CelestialSatelliteAdapter satelliteAdapter;

    private ActionMode actionMode;
    private final LongClickActionBar actionCallback = new LongClickActionBar();

    private OnCelestialBodyClickListener listener;

    private final Object mutex = new Object();

    public static CelestialBodyListFragment newInstance(CmdrSystemInfo system)
    {
        Bundle bundle = new Bundle();
        bundle.putParcelable("system", system);

        CelestialBodyListFragment fragment = new CelestialBodyListFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnCelestialBodyClickListener) {
                listener = (OnCelestialBodyClickListener) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnCelestialBodyClickListener) {
                listener = (OnCelestialBodyClickListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        holderFragment = (HolderFragment) getFragmentManager().findFragmentByTag(HolderFragment.HOLDER_FRAGMENT);
        if (holderFragment == null) {
            holderFragment = new HolderFragment();
            holderFragment.setRetainInstance(true);
            getFragmentManager()
                    .beginTransaction()
                    .add(holderFragment, HolderFragment.HOLDER_FRAGMENT)
                    .commit();
        }

        CelestialSatelliteAdapter.SatelliteListener satelliteListener = new CelestialSatelliteAdapter.SatelliteListener()
        {
            @Override
            public void onSatelliteClick(int position, View view, CelestialBody body)
            {
                onCelestialBodyItemClick(position, view, body);
            }

            @Override
            public boolean onSatelliteLongClick(int position, View view, CelestialBody body)
            {
                return onCelestialBodyItemLongClick(position, view, body);
            }

            @Override
            public boolean onSatelliteDrag(View view, DragEvent event, CelestialBody body)
            {
                return onCelestialBodyDrag(view, event, body);
            }
        };

        satelliteAdapter = new CelestialSatelliteAdapter(getActivity().getApplicationContext());
        satelliteAdapter.setSatelliteClickListener(satelliteListener);

        if (savedInstanceState == null) {
            holderFragment.root = null;
            holderFragment.backStack.clear();

            Bundle bundle = getArguments();

            if (bundle != null) {
                selectedSystem = bundle.getParcelable("system");
            }
        } else {
            selectedSystem = savedInstanceState.getParcelable("system");
        }

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.celestial_body_list_layout,
                                       container,
                                       false);

        layout.setOnDragListener(new View.OnDragListener()
        {
            @Override
            public boolean onDrag(View v, DragEvent event)
            {
                return onCelestialBodyDrag(v, event, holderFragment.root);
            }
        });

        slidingTray = (SlidingTray) layout.findViewById(R.id.slide_up_panel);

        viewPager = (ActiveViewPager) layout.findViewById(R.id.content);
        viewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));

        rootLayout = layout.findViewById(R.id.root_body_layout);

        rootButton = (Button) layout.findViewById(R.id.root_body_button);
        rootButton.setText("", TextView.BufferType.SPANNABLE);
        rootButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onCelestialRootClick(v, holderFragment.root);
            }
        });

        rootButton.setOnDragListener(new View.OnDragListener()
        {
            @Override
            public boolean onDrag(View v, DragEvent event)
            {
                return onCelestialRootDrag(v, event, holderFragment.root);
            }
        });

        RecyclerView satView = (RecyclerView) layout.findViewById(R.id.celestial_body_list);
        satView.setAdapter(satelliteAdapter);

        if (selectedSystem != null) {
//            if (selectedSystem.getMainStar() == null) {
//                viewPager.setPagingEnabled(false);
//
//                DialogFragment df = ChooseMainStarDialog.newInstance();
//                df.show(getFragmentManager(),
//                        FragmentTag.celestial_body_list_fragment.choose_main_star_dialog);
//            }

            setCelestialRoot(holderFragment.root);
        }

        return layout;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        satelliteAdapter.setSatelliteClickListener(null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putParcelable("system", selectedSystem);
    }

    /**
     *
     * @return A valid screen was returned within the fragment. If there
     * was nothing to "pop" off the stack then return False.
     */
    public boolean onBackPressed()
    {
        if ((slidingTray != null) && slidingTray.isOpened()) {
            slidingTray.animateClose();

            return true;
        } else {
            synchronized (holderFragment.backStack) {
                if (!holderFragment.backStack.isEmpty()) {
                    CelestialBody body = null;

                    holderFragment.backStack.pop();

                    if (!holderFragment.backStack.isEmpty()) {
                        body = holderFragment.backStack.peek();
                    }

                    setCelestialRoot(body);

                    return true;
                }
            }

            return false;
        }
    }

    public void addMainStar(@NonNull StarBody body)
    {
        selectedSystem.setMainStar(body);
        satelliteAdapter.addMainStar(body);
//        viewPager.setPagingEnabled(true);
    }

    public void add(@NonNull CelestialBody body)
    {
        satelliteAdapter.add(body);
    }

    public void onBodyUpdated(CelestialBody body)
    {
        satelliteAdapter.update(body);
    }

    public void onDuplicate(int times)
    {
        CelestialBody parent = actionCallback.body.getParent();

        for (int i = 0; i < times; i++) {
            switch (actionCallback.body.getSatelliteCategory()) {
                case Star:
                    add(CmdrStarBody.create(selectedSystem,
                                            ((StarBody) actionCallback.body).getType(),
                                            parent));
                    break;

                case Planet:
                    add(CmdrPlanetBody.create(selectedSystem,
                                              ((PlanetBody) actionCallback.body).getType(),
                                              parent));
                    break;
                case Asteroid:
                    add(CmdrAsteroidBody.create(selectedSystem,
                                                ((AsteroidBody) actionCallback.body).getType(),
                                                parent));
                    break;
                case Unknown:
                    break;
            }
        }

        actionMode.finish();
    }

    public void onDelete()
    {
        if (actionCallback.body.equals(selectedSystem.getMainStar())) {
            selectedSystem.setMainStar(null);
        }

        remove(actionCallback.position);
        actionCallback.body.delete();
        actionMode.finish();

//        if (selectedSystem.getMainStar() == null) {
////            viewPager.setCurrentItem(0);
////            viewPager.setPagingEnabled(false);
//
////            if ((slidingTray != null) && slidingTray.isOpened()) {
////                slidingTray.animateClose();
////            }
//
//            AndLog.d(LOG_NAME, "Show the main star dialog from onDelete.");
//            DialogFragment df = ChooseMainStarDialog.newInstance();
//            df.show(getFragmentManager(),
//                    FragmentTag.celestial_body_list_fragment.choose_main_star_dialog);
//        }
    }

    public void remove(int position)
    {
        satelliteAdapter.remove(position);
    }

    public void setSystem(@NonNull CmdrSystemInfo systemInfo)
    {
        setSystem(systemInfo, null);
    }

    public void setSystem(@NonNull CmdrSystemInfo systemInfo, CelestialBody root)
    {
        synchronized (mutex) {
            selectedSystem = systemInfo;

            synchronized (holderFragment.backStack) {
                holderFragment.backStack.clear();
            }

            if (selectedSystem.getMainStar() == null) {
                viewPager.setCurrentItem(0);

                if ((slidingTray != null) && !slidingTray.isOpened()) {
                    slidingTray.animateOpen();
                }

//                viewPager.setPagingEnabled(false);

//                Log.d(LOG_NAME, "Show the main star dialog from Start.");
//                DialogFragment df = ChooseMainStarDialog.newInstance();
//                df.show(getFragmentManager(),
//                        FragmentTag.celestial_body_list_fragment.choose_main_star_dialog);
            }

            setCelestialRoot(root);
        }
    }

    public void setCelestialRoot(CelestialBody root)
    {
        synchronized (mutex) {
            holderFragment.root = root;
            setRootLayout(root);
            setBodies(selectedSystem, root);
        }
    }

    @Override
    public void onStarIconClick(int position, View view, StarType type)
    {
        StarBody body = CmdrStarBody.create(selectedSystem, type, holderFragment.root);

        if (selectedSystem.getMainStar() == null) {
            selectedSystem.setMainStar(body);
            addMainStar(body);
//            viewPager.setPagingEnabled(true);
        } else {
            add(body);
        }
    }

    @Override
    public void onStarLongClick(int position, View view, StarType type)
    {
        Intent intent = new Intent("star_selection_icon_drag");
        intent.putExtra("type", type.value);

        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_INTENT};
        ClipData.Item item = new ClipData.Item(intent);
        ClipData clipData = new ClipData(type.toString(), mimeTypes, item);

        View.DragShadowBuilder shadow = new View.DragShadowBuilder(view);

        if (slidingTray != null) {
            slidingTray.animateClose();
        }

        view.startDrag(clipData, shadow, null, 0);
    }

    @Override
    public void onPlanetIconClick(int position, View view, PlanetType type)
    {
        add(CmdrPlanetBody.create(selectedSystem, type, holderFragment.root));
    }

    @Override
    public void onPlanetLongClick(int position, View view, PlanetType type)
    {
        Intent intent = new Intent("planet_selection_icon_drag");
        intent.putExtra("type", type.value);

        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_INTENT};
        ClipData.Item item = new ClipData.Item(intent);
        ClipData clipData = new ClipData(type.toString(), mimeTypes, item);

        View.DragShadowBuilder shadow = new View.DragShadowBuilder(view);

        if (slidingTray != null) {
            slidingTray.animateClose();
        }

        view.startDrag(clipData, shadow, null, 0);
    }

    public void onCelestialRootClick(View view, CelestialBody body)
    {
        if (actionMode != null) {
            actionMode.finish();
        }

        if (listener != null) {
            listener.onCelestialBodyClick(body);
        }
    }

    public void onCelestialBodyItemClick(int position, View view, CelestialBody body)
    {
        if (actionMode != null) {
            actionMode.finish();
        }

        if (body.getSatellites().isEmpty()) {
            if (listener != null) {
                listener.onCelestialBodyClick(body);
            }
        } else {
            synchronized (holderFragment.backStack) {
                holderFragment.backStack.push(body);
            }

            setCelestialRoot(body);
        }
    }

    public boolean onCelestialBodyItemLongClick(int position, View view, CelestialBody body)
    {
        if (actionMode == null) {
            actionMode = getActivity().startActionMode(actionCallback);
        }

        actionCallback.setSelected(position, view, body);

        Intent intent = new Intent("registered_cb_drag");
        intent.putExtra("body", body);
        intent.putExtra("position", position);

        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_INTENT};
        ClipData.Item item = new ClipData.Item(intent);
        ClipData clipData = new ClipData(body.getId().toString(), mimeTypes, item);

        View.DragShadowBuilder shadow = new View.DragShadowBuilder(view);

        if ((slidingTray != null) &&
            (slidingTray.isOpened())) {
            slidingTray.animateClose();
        }

        view.startDrag(clipData, shadow, null, 0);

        return true;
    }

    //    private ExecutorService executorService = Executors.newScheduledThreadPool(1);
    //    private Future hoverFuture;
    //
    //    private class HoverTimeout implements Runnable
    //    {
    //        private static final int UP = 1;
    //        private static final int DOWN = 2;
    //
    //        private final CelestialBody body;
    //        private final int direction;
    //
    //        HoverTimeout(CelestialBody body, int direction)
    //        {
    //            this.body = body;
    //            this.direction = direction;
    //        }
    //
    //        @Override
    //        public void run()
    //        {
    //            if (Looper.myLooper() == Looper.getMainLooper()) {
    //                if (!backStackFragment.backStack.isEmpty()) {
    //                    onBackPressed();
    //                }
    //            } else {
    //                if (body != null) {
    //                    switch (direction) {
    //                        case UP:
    //                            new Handler(Looper.getMainLooper()).post(this);
    //                            break;
    //                        case DOWN:
    //                            break;
    //                        default:
    //                            break;
    //                    }
    //                }
    //            }
    //
    //        }
    //    }

    public boolean onCelestialRootDrag(View view, DragEvent event, CelestialBody body)
    {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                return event.getClipDescription()
                            .hasMimeType(ClipDescription.MIMETYPE_TEXT_INTENT);
            //            case DragEvent.ACTION_DRAG_ENTERED:
            //                Log.d(LOG_NAME, "Root was entered.");
            //                hoverFuture = executorService.submit(new HoverTimeout(body, HoverTimeout.UP));
            //                return true;
            //            case DragEvent.ACTION_DRAG_EXITED:
            //                Log.d(LOG_NAME, "Root was exited.");
            //                if (hoverFuture != null) {
            //                    hoverFuture.cancel(true);
            //                    hoverFuture = null;
            //                    Log.d(LOG_NAME, "Cancel hover timer.");
            //                }
            //
            //                return true;
            case DragEvent.ACTION_DROP: {
                //                if (hoverFuture != null) {
                //                    hoverFuture.cancel(true);
                //                    hoverFuture = null;
                //                    Log.d(LOG_NAME, "Cancel hover timer.");
                //                }

                Intent intent = event.getClipData().getItemAt(0).getIntent();

                switch (intent.getAction()) {
                    case "registered_cb_drag": {
                        intent.setExtrasClassLoader(CelestialBody.class.getClassLoader());

                        CelestialBody celestialBody = intent.getParcelableExtra("body");
                        int celestialPosition = intent.getIntExtra("position", -1);

                        celestialBody.setParent(body.getParent());
                        remove(celestialPosition);

                        if (actionMode != null) {
                            actionMode.finish();
                        }

                        return true;
                    }
                }
            }
            default:
                return false;
        }
    }

    public boolean onCelestialBodyDrag(View view, DragEvent event, CelestialBody body)
    {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                return event.getClipDescription()
                            .hasMimeType(ClipDescription.MIMETYPE_TEXT_INTENT);
            case DragEvent.ACTION_DROP: {
                Intent intent = event.getClipData().getItemAt(0).getIntent();

                switch (intent.getAction()) {
                    case "star_selection_icon_drag": {
                        StarType type = StarType.getStarType(intent.getIntExtra("type",
                                                                                StarType.Unknown.value));

                        StarBody celestialBody = CmdrStarBody.create(selectedSystem, type, body);

                        if (body == holderFragment.root) {
                            if (selectedSystem.getMainStar() == null) {
                                addMainStar(celestialBody);
                                selectedSystem.setMainStar(celestialBody);
                            } else {
                                add(celestialBody);
                            }
                        }

                        return true;
                    }
                    case "planet_selection_icon_drag": {
                        PlanetType type = PlanetType.getPlanetType(intent.getIntExtra("type",
                                                                                      PlanetType.Unknown.value));

                        CelestialBody celestialBody = CmdrPlanetBody.create(selectedSystem,
                                                                            type,
                                                                            body);

                        if (body == holderFragment.root) {
                            add(celestialBody);
                        }

                        return true;
                    }
                    case "asteroid_selection_icon_drag": {
                        Mineral type = Mineral.getMineral(intent.getIntExtra("type",
                                                                             Mineral.Unknown.value));

                        CelestialBody celestialBody = CmdrAsteroidBody.create(selectedSystem,
                                                                              type,
                                                                              body);

                        if (body == holderFragment.root) {
                            add(celestialBody);
                        }

                        return true;
                    }
                    case "registered_cb_drag": {
                        intent.setExtrasClassLoader(CelestialBody.class.getClassLoader());

                        CelestialBody celestialBody = intent.getParcelableExtra("body");
                        int celestialPosition = intent.getIntExtra("position", -1);

                        boolean isRoot = false;

                        /** This is a little bit of complex logic. Thus I am breaking
                         * it down into if/else statements. If there is a nice clean
                         * way to get the same result then fix this please!
                         *
                         * Basically we are trying to figure out if the View that
                         * was dropped onto the panel is the background or not. If it
                         * is AND we are at the same "level" then we don't actually want
                         * to do anything. This is because we are ALREADY a child of
                         * the root node.
                         */
                        if (body == null && celestialBody.getParent() == null) {
                            isRoot = true;
                        } else {
                            CelestialBody parent = celestialBody.getParent();

                            if (parent != null && parent.equals(body)) {
                                isRoot = true;
                            }
                        }

                        if (!isRoot && !celestialBody.equals(body)) {
                            if (celestialBody.equals(selectedSystem.getMainStar())) {
                                /** We cannot move the main star! */
                                DialogFragment df = OkAlertDialogFragment.newInstance(R.string.error_move_mainstar_title,
                                                                                      R.string.error_move_mainstar_summary);
                                df.show(getFragmentManager(),
                                        FragmentTag.celestial_body_list_fragment.error_main_star_dialog);
                            } else {
                                celestialBody.setParent(body);
                                remove(celestialPosition);

                                int index = satelliteAdapter.indexOf(body);
                                if (index != -1) {
                                    satelliteAdapter.notifyItemChanged(index);
                                }

                                if (actionMode != null) {
                                    actionMode.finish();
                                }

                                return true;
                            }
                        }

                        return false;
                    }
                    default:
                        return false;
                }
            }
            default:
                return false;
        }
    }

    private void setBodies(@NonNull CmdrSystemInfo systemInfo, CelestialBody root)
    {
        List<CelestialBody> bodyList;

        if (root == null) {
            bodyList = systemInfo.getSatellites();
            StarBody mainStar = systemInfo.getMainStar();

            /* Remove the main star from the list as this will
             * be added back in later as a special item.
             */
            if (mainStar != null) {
                for (int i = 0; i < bodyList.size(); i++) {
                    CelestialBody body = bodyList.get(i);

                    if ((body.getSatelliteCategory() == SatelliteCategory.Star) &&
                        body.getId().equals(mainStar.getId())) {
                        bodyList.remove(i);
                        break;
                    }
                }
            }

            satelliteAdapter.set(bodyList);

            if (mainStar != null) {
                satelliteAdapter.addMainStar(mainStar);
            }
        } else {
            bodyList = root.getSatellites();
            satelliteAdapter.set(bodyList);
        }
    }

    private void setRootLayout(CelestialBody root)
    {
        if (root == null) {
            rootLayout.setVisibility(View.GONE);
        } else {
            SatelliteCategory category = root.getSatelliteCategory();
            TypedArray resourceArray;
            int position = 0;

            switch (category)
            {
                case Star:
                    resourceArray = getResources().obtainTypedArray(R.array.star_icons);
                    position = ((StarBody) root).getType().value;
                    break;
                case Planet:
                    resourceArray = getResources().obtainTypedArray(R.array.planet_icons);
                    position = ((PlanetBody) root).getType().value;
                    break;
                case Asteroid:
                    //                    resourceArray = getResources().obtainTypedArray(R.array.asteroid_icons);
                    //                    position = ((AsteroidBody) root).getType().value;
                    resourceArray = null;
                    break;
                default:
                    resourceArray = null;
                    break;
            }

            if (resourceArray != null) {
                Drawable[] hoveredDrawables = new Drawable[2];
                Drawable iconDrawable = resourceArray.getDrawable(position);

                hoveredDrawables[0] = getResources().getDrawable(R.drawable.square_full_border_no_padding_primary_orange);
                hoveredDrawables[1] = iconDrawable;

                LayerDrawable layerDrawable = new LayerDrawable(hoveredDrawables);

                StateListDrawable drawable = new StateListDrawable();

                drawable.addState(new int[] { android.R.attr.state_drag_hovered },
                                  layerDrawable);
                drawable.addState(new int[0], iconDrawable);

                rootButton.setBackground(drawable);
                rootLayout.setVisibility(View.VISIBLE);

                resourceArray.recycle();
            }
        }
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.celestial_body_list_fragment.duplicate_dialog:
                onDuplicate(Integer.valueOf(text));
                break;
            default:
                break;
        }
    }

    /** The user has chosen to delete a celestial body.
     *
     * @param tag
     */
    @Override
    public void onPositive(String tag)
    {
        switch (tag) {
            case FragmentTag.celestial_body_list_fragment.delete_dialog:
                onDelete();
                break;
            default:
                break;
        }
    }

    @Override
    public void onNegative(String tag)
    {

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter
    {
        public ViewPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position) {
                case 0:
                    return new StarIconGridFragment();
                case 1:
                    return new PlanetIconGridFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount()
        {
            return 2;
        }
    }

    public static class HolderFragment extends Fragment
    {
        public static final String HOLDER_FRAGMENT = "celestial_body_list_fragment.holder_fragment";

        public CelestialBody root;

        public final Stack<CelestialBody> backStack = new Stack<>();
    }

    private class LongClickActionBar implements ActionMode.Callback
    {
        public int position;
        public View view;
        public CelestialBody body;

        private void setSelected(int position, View view, CelestialBody body)
        {
            if (this.view != null) {
                this.view.setSelected(false);
            }

            this.position = position;
            this.view = view;
            this.body = body;

            view.setSelected(true);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu)
        {
            mode.getMenuInflater().inflate(R.menu.celestial_body_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu)
        {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item)
        {
            switch (item.getItemId()) {
                case R.id.cb_delete_menu: {
                    DialogFragment df = CancelableAlertDialogFragment.newInstance(R.string.cb_delete_menu_title);
                    df.show(getChildFragmentManager(), FragmentTag.celestial_body_list_fragment.delete_dialog);

                    return true;
                }
                case R.id.cb_duplicate_menu: {
                    String[] values = new String[255];

                    for (int i = 0; i < 255; i++) {
                        values[i] = String.format("%d", i + 1);
                    }

                    TextPickerDialogFragment df = TextPickerDialogFragment.newInstance(getString(R.string.cb_duplicate_menu_title),
                                                                                       getString(R.string.cb_duplicate_menu_summary),
                                                                                       values,
                                                                                       "1");
                    df.show(getChildFragmentManager(), FragmentTag.celestial_body_list_fragment.duplicate_dialog);

                    return true;
                }
                case R.id.cb_edit_menu: {
                    if (listener != null) {
                        listener.onCelestialBodyClick(body);
                    }

                    mode.finish();

                    return true;
                }
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode)
        {
            view.setSelected(false);

            actionMode = null;
            view = null;
            body = null;
        }
    }
}
