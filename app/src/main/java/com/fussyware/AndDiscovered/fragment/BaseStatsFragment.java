package com.fussyware.AndDiscovered.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by wes on 1/23/16.
 */
public abstract class BaseStatsFragment extends Fragment
{
    protected volatile boolean invalidated;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        invalidated = true;
    }

    public abstract void onRefresh();

    public void invalidate()
    {
        invalidated = true;
    }
}
