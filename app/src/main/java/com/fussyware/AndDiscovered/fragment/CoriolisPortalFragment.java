package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 12/19/15.
 */
public class CoriolisPortalFragment extends PortalBaseFragment
{
    private static final String LOG_NAME = CoriolisPortalFragment.class.getSimpleName();

    @Override
    protected String getBaseUrl()
    {
        return "http://coriolis.io";
    }

    @Override
    protected String getAllowedHost()
    {
        return "coriolis.io";
    }

    @Override
    protected String getAllowedPath()
    {
        return null;
    }
}
