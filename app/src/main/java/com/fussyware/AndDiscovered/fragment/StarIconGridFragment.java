package com.fussyware.AndDiscovered.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.CelestialIconAdapter;
import com.fussyware.AndDiscovered.celestial.StarType;

/**
 * Created by wes on 11/29/15.
 */
public class StarIconGridFragment extends Fragment
{
    public interface OnStarClickListeners
    {
        void onStarIconClick(int position, View view, StarType type);
        void onStarLongClick(int position, View view, StarType type);
    }

    private static final String LOG_NAME = StarIconGridFragment.class.getSimpleName();

    private OnStarClickListeners listener;

    @Override
    public void onDetach()
    {
        super.onDetach();

        listener = null;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnStarClickListeners) {
                listener = (OnStarClickListeners) context;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        } else {
            if (fragment instanceof OnStarClickListeners) {
                listener = (OnStarClickListeners) fragment;
            } else {
                throw new ClassCastException("Activity does not implement click listeners.");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.selection_fragment_layout, container, false);

        ImageView imageView = (ImageView) layout.findViewById(R.id.pager_navigation_icons);
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.pager_two_left_on));

        TypedArray typedArray = getResources().obtainTypedArray(R.array.star_icons);

        CelestialIconAdapter adapter = new CelestialIconAdapter(getActivity(), typedArray);
        adapter.setItemClickListener(new ClickListener());
        adapter.setItemLongClickListener(new LongClickListener());

        typedArray.recycle();

        RecyclerView gridView = (RecyclerView) layout.findViewById(R.id.selection_icon_grid);
        gridView.setAdapter(adapter);

        return layout;
    }

    private class ClickListener implements CelestialIconAdapter.OnItemClickListener
    {

        @Override
        public void onItemClick(View view, int position)
        {
            listener.onStarIconClick(position, view, StarType.getStarType(position));
        }
    }

    private class LongClickListener implements CelestialIconAdapter.OnItemLongClickListener
    {

        @Override
        public boolean onItemLongClick(View view, int position)
        {
            listener.onStarLongClick(position, view, StarType.getStarType(position));
            return true;
        }
    }
}
