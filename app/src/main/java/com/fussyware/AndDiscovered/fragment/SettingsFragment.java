package com.fussyware.AndDiscovered.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 8/31/15.
 */
public class SettingsFragment extends PreferenceFragment
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
