package com.fussyware.AndDiscovered.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.dialog.CelestialCommonEditDialogFragment;

/**
 * Created by wes on 11/17/15.
 */
public class CelestialInfoCommonDetailsFragment
        extends Fragment
        implements CelestialCommonEditDialogFragment.OnDoneListener
{
    private static final String LOG_NAME = CelestialInfoCommonDetailsFragment.class.getSimpleName();

    private final Object mutex = new Object();

    private CelestialBody celestialBody;

    private View mainView;
    private TextView distanceText;
    private TextView semiAxisText;
    private TextView periodText;
    private TextView eccText;
    private TextView inclinationText;
    private TextView argPeriapsisText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                celestialBody = bundle.getParcelable("celestial_body");
            }
        } else {
            celestialBody = savedInstanceState.getParcelable("celestial_body");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.cb_common_details_layout, container, false);

        createViews(mainView);

        if (celestialBody != null) {
            setCelestialBody(celestialBody);
        }

        return mainView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial_body", celestialBody);
    }

    public void setCelestialBody(CelestialBody body)
    {
        synchronized (mutex) {
            celestialBody = body;

            if (body.getDistance() != null) {
                distanceText.setText(String.format("%1.2f", body.getDistance()));
            }

            if (body.getSemiMajorAxis() != null) {
                semiAxisText.setText(String.format("%1.4f", body.getSemiMajorAxis()));
            }

            if (body.getOrbitalPeriod() != null) {
                periodText.setText(String.format("%1.2f", body.getOrbitalPeriod()));
            }

            if (body.getOrbitalEccentricity() != null) {
                eccText.setText(String.format("%1.4f", body.getOrbitalEccentricity()));
            }

            if (body.getOrbitalInclination() != null) {
                inclinationText.setText(String.format("%1.2f", body.getOrbitalInclination()));
            }

            if (body.getArgPeriapsis() != null) {
                argPeriapsisText.setText(String.format("%1.2f", body.getArgPeriapsis()));
            }
        }
    }

    public void onDone(Bundle bundle)
    {
        for (String key : bundle.keySet()) {
            double dbl = bundle.getDouble(key, -1);

            switch (key) {
                case CelestialCommonEditDialogFragment.TAG_DISTANCE:
                    celestialBody.setDistance(dbl);
                    distanceText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialCommonEditDialogFragment.TAG_SEMIAXIS:
                    celestialBody.setSemiMajorAxis(dbl);
                    semiAxisText.setText(String.format("%1.4f", dbl));
                    break;
                case CelestialCommonEditDialogFragment.TAG_PERIOD:
                    celestialBody.setOrbitalPeriod(dbl);
                    periodText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialCommonEditDialogFragment.TAG_ECC:
                    celestialBody.setOrbitalEccentricity(dbl);
                    eccText.setText(String.format("%1.4f", dbl));
                    break;
                case CelestialCommonEditDialogFragment.TAG_INCLINE:
                    celestialBody.setOrbitalInclination(dbl);
                    inclinationText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialCommonEditDialogFragment.TAG_PERIAPSIS:
                    celestialBody.setArgPeriapsis(dbl);
                    argPeriapsisText.setText(String.format("%1.2f", dbl));
                    break;
                default:
                    break;
            }
        }
    }

    private void createViews(View layout)
    {
        mainView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CelestialCommonEditDialogFragment df = CelestialCommonEditDialogFragment.newInstance(celestialBody);
                df.show(getChildFragmentManager(), FragmentTag.celestial_info_fragment.celestial_common_dialog);
            }
        });

        distanceText = (TextView) layout.findViewById(R.id.cb_distance_value);
        semiAxisText = (TextView) layout.findViewById(R.id.cb_semi_axis_value);
        periodText = (TextView) layout.findViewById(R.id.cb_orbperiod_value);
        eccText = (TextView) layout.findViewById(R.id.cb_orbeccentricity_value);
        inclinationText = (TextView) layout.findViewById(R.id.cb_orbinclination_value);
        argPeriapsisText = (TextView) layout.findViewById(R.id.cb_argperiapsis_value);
    }

    @Override
    public void onDone(String tag, Bundle changedItems)
    {
        onDone(changedItems);
    }
}
