package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 9/6/15.
 */
public final class FragmentTag
{
    public static abstract class main_activity
    {
        public static final String tag = "main_activity";
        public static final String route_dialog = "main_activity.route_dialog";
        public static final String no_ipaddr_dialog = "main_activity.no_ipaddr_dialog";
    }

    public static abstract class system_list_fragment
    {
        public static final String tag = "system_list_fragment";
        public static final String no_items_dialog = "system_list_fragment.no_items_dialog";
    }

    public static abstract class filter_list_fragment
    {
        public static final String tag = "filter_list_fragment";
        public static final String no_items_dialog = "filter_list_fragment.no_items_dialog";
    }

    public static abstract class system_info_fragment
    {
        public static final String tag = "system_info_fragment";
        public static final String main_star_dialog = "system_info_fragment.main_star_dialog";
        public static final String star_check_list_dialog = "system_info_fragment.star_check_list_dialog";
        public static final String planet_check_list_dialog = "system_info_fragment.planet_check_list_dialog";
    }

    public static abstract class celestial_info_fragment
    {
        public static final String tag = "celestial_info_fragment";
        public static final String tag_header = "celestial_info_fragment.header";
        public static final String tag_common = "celestial_info_fragment.common";
        public static final String tag_planet = "celestial_info_fragment.planet";
        public static final String tag_star = "celestial_info_fragment.star";
        public static final String tag_asteroid = "celestial_info_fragment.asteroid";

        public static final String main_star_dialog = "celestial_info_fragment.main_star_dialog";
        public static final String star_check_list_dialog = "celestial_info_fragment.star_check_list_dialog";
        public static final String planet_check_list_dialog = "celestial_info_fragment.planet_check_list_dialog";
        public static final String celestial_name_dialog = "celestial_info_fragment.celestial_name_dialog";
        public static final String celestial_common_dialog = "celestial_info_fragment.celestial_common_dialog";
        public static final String celestial_star_dialog = "celestial_info_fragment.celestial_star_dialog";
        public static final String celestial_planet_dialog = "celestial_info_fragment.celestial_planet_dialog";
        public static final String celestial_asteroid_dialog = "celestial_info_fragment.celestial_asteroid_dialog";
    }

    public static abstract class celestial_body_list_fragment
    {
        public static final String tag = "celestial_body_list_fragment";
        public static final String no_satellites_dialog = "celestial_body_list_fragment.no_satellites_dialog";
        public static final String duplicate_dialog = "celestial_body_list_fragment.duplicate_dialog";
        public static final String delete_dialog = "celestial_body_list_fragment.delete_dialog";
        public static final String choose_main_star_dialog = "celestial_body_list_fragment.choose_main_star_dialog";
        public static final String error_main_star_dialog = "celestial_body_list_fragment.error_main_star_dialog";
    }

    public static abstract class distance_list_fragment
    {
        public static final String tag = "distance_list_fragment";
        public static final String distance_update_dialog = "distance_list_fragment.distance_update_dialog";
    }

    public static abstract class notes_fragment
    {
        public static final String tag = "notes_fragment";
    }

    public static abstract class settings_fragment
    {
        public static final String tag = "settings_fragment";
    }

    public static abstract class system_photo_gallery_fragment
    {
        public static final String tag = "system_photo_gallery_fragment";
        public static final String alert_delete_dialog = "system_photo_gallery_fragment.alert_delete_dialog";
        public static final String no_items_dialog = "system_photo_gallery_fragment.no_items_dialog";
    }

    public static abstract class photo_fullscreen_fragment
    {
        public static final String tag = "photo_fullscreen_fragment";
        public static final String edit_celestial_type_dialog = "photo_fullscreen_fragment.edit_celestial_type_dialog";
        public static final String edit_celestial_name_dialog = "photo_fullscreen_fragment.edit_celestial_name_dialog";
        public static final String edit_celestial_distance_dialog = "photo_fullscreen_fragment.edit_celestial_distance_dialog";
        public static final String alert_delete_dialog = "photo_fullscreen_fragment.alert_delete_dialog";
    }

    public static abstract class star_icon_grid_fragment
    {
        public static final String tag = "star_icon_grid_fragment";
    }

    private FragmentTag() {}
}
