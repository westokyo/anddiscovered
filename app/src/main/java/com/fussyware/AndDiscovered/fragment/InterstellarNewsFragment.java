package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 12/19/15.
 */
public class InterstellarNewsFragment extends PortalBaseFragment
{
    private static final String LOG_NAME = InterstellarNewsFragment.class.getSimpleName();

    @Override
    protected String getBaseUrl()
    {
        return "http://interstellarpress.blogspot.ca/";
    }

    @Override
    protected String getAllowedHost()
    {
        return "interstellarpress.blogspot.ca";
    }

    @Override
    protected String getAllowedPath()
    {
        return null;
    }
}
