package com.fussyware.AndDiscovered.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.CelestialBody;

import java.security.InvalidParameterException;

/**
 * Created by wes on 12/11/15.
 */
public class CelestialInfoParentFragment extends Fragment
{
    private static final String LOG_NAME = CelestialInfoParentFragment.class.getSimpleName();

    private CelestialInfoHeaderFragment headerFragment;
    private CelestialInfoCommonDetailsFragment commonFragment;
    private Fragment bodyFragment;
    private String fragmentTag;
    private CelestialBody celestialBody;

    public static CelestialInfoParentFragment newInstance(CelestialBody body)
    {
        Bundle bundle = new Bundle();
        bundle.putParcelable("celestial_body", body);

        CelestialInfoParentFragment fragment = new CelestialInfoParentFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                celestialBody = bundle.getParcelable("celestial_body");
            }
        } else {
            celestialBody = savedInstanceState.getParcelable("celestial_body");
        }

        if (celestialBody == null) {
            throw new InvalidParameterException("No Celestial Body specified.");
        }

        Bundle bundle = new Bundle();
        bundle.putParcelable("celestial_body", celestialBody);

        FragmentManager childManager = getChildFragmentManager();

        headerFragment = (CelestialInfoHeaderFragment) childManager.findFragmentByTag(FragmentTag.celestial_info_fragment.tag_header);
        if (headerFragment == null) {
            headerFragment = new CelestialInfoHeaderFragment();
        }

        commonFragment = (CelestialInfoCommonDetailsFragment) childManager.findFragmentByTag(FragmentTag.celestial_info_fragment.tag_common);
        if (commonFragment == null) {
            commonFragment = new CelestialInfoCommonDetailsFragment();
        }

        switch (celestialBody.getSatelliteCategory()) {
            case Star:
                fragmentTag = FragmentTag.celestial_info_fragment.tag_star;
                bodyFragment = childManager.findFragmentByTag(fragmentTag);
                if (bodyFragment == null) {
                    bodyFragment = new CelestialStarDetailsFragment();
                }
                break;
            case Planet:
                fragmentTag = FragmentTag.celestial_info_fragment.tag_planet;
                bodyFragment = childManager.findFragmentByTag(fragmentTag);
                if (bodyFragment == null) {
                    bodyFragment = new CelestialPlanetDetailsFragment();
                }
                break;
            case Asteroid:
                fragmentTag = FragmentTag.celestial_info_fragment.tag_asteroid;
                bodyFragment = childManager.findFragmentByTag(fragmentTag);
                if (bodyFragment == null) {
                    bodyFragment = new CelestialAsteroidDetailsFragment();
                }
                break;
            case Unknown:
                throw new InvalidParameterException("Bad celestial body specified.");
        }

        headerFragment.setArguments(bundle);
        commonFragment.setArguments(bundle);
        bodyFragment.setArguments(bundle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.cb_parent_fragment_layout,
                                       container,
                                       false);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.celestial_info_header_frame,
                         headerFragment,
                         FragmentTag.celestial_info_fragment.tag_header)
                .replace(R.id.cb_details_frame, bodyFragment, fragmentTag)
                .replace(R.id.cb_common_details_frame,
                         commonFragment,
                         FragmentTag.celestial_info_fragment.tag_common)
                .commit();

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial_body", celestialBody);
    }
}
