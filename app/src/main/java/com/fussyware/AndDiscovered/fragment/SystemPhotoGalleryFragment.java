package com.fussyware.AndDiscovered.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.activity.ImageFullscreenActivity;
import com.fussyware.AndDiscovered.adapter.ThumbnailAdapter;
import com.fussyware.AndDiscovered.dialog.CancelableAlertDialogFragment;
import com.fussyware.AndDiscovered.dialog.OkAlertDialogFragment;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.eddatabase.SystemImageInfo;
import com.fussyware.AndDiscovered.edutils.AndLog;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wes on 9/20/15.
 */
public class SystemPhotoGalleryFragment
        extends Fragment
        implements CancelableAlertDialogFragment.OnAlertListener
{
    private static final String LOG_NAME = SystemPhotoGalleryFragment.class.getSimpleName();
    private static final DecimalFormat distanceFormatter = new DecimalFormat("0.00");

    static {
        distanceFormatter.setRoundingMode(RoundingMode.HALF_UP);
    }

    private String systemName;
    private ThumbnailAdapter imageAdapter;

    private ActionMode mainActionMode;
    private final ActionMode.Callback mainActionCallback = new LongClickActionBar();

    // TODO: Need to move this to actual object instead of string.
    public static SystemPhotoGalleryFragment newInstance(String system)
    {
        Bundle bundle = new Bundle();
        bundle.putString("system", system);

        SystemPhotoGalleryFragment fragment = new SystemPhotoGalleryFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ThumbnailOnItemClickListener clickListener = new ThumbnailOnItemClickListener();

        imageAdapter = new ThumbnailAdapter(getActivity());
        imageAdapter.setOnItemClickListener(clickListener);
        imageAdapter.setOnItemLongClickListener(clickListener);

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                systemName = bundle.getString("system");
            }
        } else {
            systemName = savedInstanceState.getString("system");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {

        View layout = inflater.inflate(R.layout.system_photo_gallery_layout, container, false);

        RecyclerView recyclerView = (RecyclerView) layout.findViewById(R.id.photo_gallery_grid);
        recyclerView.setAdapter(imageAdapter);

        AndLog.d(LOG_NAME, "System: " + systemName);
        if (systemName != null) {
            setSystem(systemName);
        }

        return layout;
    }

    @Override
    public void onDestroyView()
    {
        if (mainActionMode != null) {
            mainActionMode.finish();
        }

        super.onDestroyView();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if (imageAdapter.getItemCount() == 0) {
            DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.system_photo_gallery_no_items_title,
                                                                        R.string.system_photo_gallery_no_items_message);
            fragment.show(getChildFragmentManager(), FragmentTag.system_photo_gallery_fragment.no_items_dialog);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("system", systemName);
    }

    public void setSystem(String system)
    {
        setSystem(CmdrDbHelper.getInstance().getSystem(system));
    }

    public void setSystem(CmdrSystemInfo system)
    {
        ArrayList<SystemImageInfo> list = new ArrayList<>();
        Collections.addAll(list, system.getSystemImages());
        AndLog.d(LOG_NAME, "image list: " + list);
        imageAdapter.setImageList(list);
    }

    public void add(SystemImageInfo imageInfo)
    {
        imageAdapter.add(imageInfo);
    }

    @Override
    public void onPositive(String tag)
    {
        List<SystemImageInfo> list = imageAdapter.getSelected();

        for (SystemImageInfo info : list) {
            imageAdapter.remove(info);
            info.delete();
        }

        mainActionMode.finish();
    }

    @Override
    public void onNegative(String tag)
    {
        // Ignored
    }

    private class ThumbnailOnItemClickListener implements ThumbnailAdapter.OnItemClickListener,
                                                          ThumbnailAdapter.OnItemLongClickListener
    {
        @Override
        public void onItemClick(View parent, View view, int position)
        {
            if (mainActionMode == null) {
                SystemImageInfo info = imageAdapter.getItem(position);
                Intent intent = new Intent(getActivity().getApplicationContext(),
                                           ImageFullscreenActivity.class);

                intent.putExtra("image_info", info);
                startActivity(intent);
            } else {
                imageAdapter.setSelected(!imageAdapter.isSelected(position), position);
            }
        }

        @Override
        public boolean onItemLongClick(View parent, View view, int position)
        {
            imageAdapter.setSelected(!imageAdapter.isSelected(position), position);

            if (mainActionMode == null) {
                mainActionMode = getActivity().startActionMode(mainActionCallback);
            }

            return true;
        }
    }

    private class LongClickActionBar implements ActionMode.Callback
    {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu)
        {
            mode.getMenuInflater().inflate(R.menu.photo_gallery_context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu)
        {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item)
        {
            switch (item.getItemId()) {
                case R.id.photo_delete_menu: {
                    if (imageAdapter.getSelectedCount() > 0) {
                        DialogFragment fragment = CancelableAlertDialogFragment.newInstance(R.string.photo_delete_title);
                        fragment.show(getChildFragmentManager(),
                                      FragmentTag.system_photo_gallery_fragment.alert_delete_dialog);
                    }
                    return true;
                }
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode)
        {
            mainActionMode = null;
            imageAdapter.clearSelected();
        }
    }
}
