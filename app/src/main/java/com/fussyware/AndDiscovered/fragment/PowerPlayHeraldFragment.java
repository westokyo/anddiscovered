package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 12/19/15.
 */
public class PowerPlayHeraldFragment extends PortalBaseFragment
{
    private static final String LOG_NAME = PowerPlayHeraldFragment.class.getSimpleName();

    @Override
    protected String getBaseUrl()
    {
        return "http://athrael.net/content/ed/pph/";
    }

    @Override
    protected String getAllowedHost()
    {
        return "athrael.net";
    }

    @Override
    protected String getAllowedPath()
    {
        return "/content/ed/pph";
    }
}
