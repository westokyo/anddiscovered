package com.fussyware.AndDiscovered.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.FilterListAdapter;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;

/**
 * Created by wes on 12/18/15.
 */
public class FilterListFragment extends Fragment
{
    public interface OnSystemClickListener
    {
        void onSystemClick(SystemInfo system);
    }

    private FilterListAdapter adapter;
    private OnSystemClickListener listener;

    public static Bundle createArguments(FilterListAdapter.FilterCursor filter)
    {
        Bundle bundle = new Bundle();
        bundle.putParcelable("filter", filter);

        return bundle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FilterListAdapter.FilterCursor filter = null;

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                filter = bundle.getParcelable("filter");
            }
        } else {
            filter = savedInstanceState.getParcelable("filter");
        }

        adapter = new FilterListAdapter(getActivity().getApplicationContext(),
                                        filter);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.filter_list_fragment, container, false);

        final ListView listView = (ListView) layout.findViewById(R.id.filter_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                CmdrSystemInfo info = adapter.getItem(position);

                if ((info != null) && (listener != null)) {
                    listener.onSystemClick(info);
                }
            }
        });

        return layout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (context instanceof OnSystemClickListener) {
                listener = (OnSystemClickListener) context;
            } else {
                throw new ClassCastException("Activity does not implement OnSystemClickListener.");
            }
        } else {
            if (fragment instanceof OnSystemClickListener) {
                listener = (OnSystemClickListener) fragment;
            } else {
                throw new ClassCastException("Activity does not implement OnSystemClickListener.");
            }
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("filter", adapter.getFilter());
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        adapter.close();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(R.string.filter_list_activity_title);
        }
    }

    public void setFilter(FilterListAdapter.FilterCursor filter)
    {
        adapter.setFilter(filter);
    }
}
