package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 12/19/15.
 */
public class EDDBPortalFragment extends PortalBaseFragment
{
    private static final String LOG_NAME = EDDBPortalFragment.class.getSimpleName();

    @Override
    protected String getBaseUrl()
    {
        return "https://eddb.io";
    }

    @Override
    protected String getAllowedHost()
    {
        return "eddb.io";
    }

    @Override
    protected String getAllowedPath()
    {
        return null;
    }
}
