package com.fussyware.AndDiscovered.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[] { android.R.attr.listDivider };

    private Drawable mDivider;
    private int orientation;

    /**
     * Default divider will be used
     */
    public DividerItemDecoration(Context context, int orientation)
    {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();

        this.orientation = orientation;
    }

    /**
     * Custom divider will be used
     */
    public DividerItemDecoration(Context context, int orientation, int resId)
    {
        mDivider = ContextCompat.getDrawable(context, resId);
        this.orientation = orientation;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state)
    {
        int childCount = parent.getChildCount();
        int startPos = (orientation == LinearLayoutManager.VERTICAL) ? 0 : 1;

        for (int i = startPos; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top, bottom, left, right;

            if (orientation == LinearLayoutManager.VERTICAL) {
                left = parent.getPaddingLeft();
                right = parent.getWidth() - parent.getPaddingRight();
                top = child.getBottom() + params.bottomMargin;
                bottom = top + mDivider.getIntrinsicHeight();
            } else {
                top = parent.getPaddingTop();
                bottom = parent.getHeight() - parent.getPaddingBottom();
                left = child.getLeft() + params.leftMargin;
                right = left + mDivider.getIntrinsicWidth();
            }

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}
