package com.fussyware.AndDiscovered.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.Atmosphere;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.VolcanoType;
import com.fussyware.AndDiscovered.dialog.CelestialPlanetEditDialogFragment;

/**
 * Created by wes on 11/17/15.
 */
public class CelestialPlanetDetailsFragment
        extends Fragment
        implements CelestialPlanetEditDialogFragment.OnDoneListener
{
    private static final String LOG_NAME = CelestialPlanetDetailsFragment.class.getSimpleName();

    private final Object mutex = new Object();

    private PlanetBody celestialBody;

    private View mainView;
    private TextView massText;
    private TextView radiusText;
    private TextView surfaceTempText;
    private TextView surfacePressureText;
    private TextView atmosphereTypeText;
    private TextView rotationPeriodText;
    private TextView axisTiltText;
    private TextView volcanismText;

    private CheckBox terraformCheck;
    private CheckBox tidalLockedCheck;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                celestialBody = bundle.getParcelable("celestial_body");
            }
        } else {
            celestialBody = savedInstanceState.getParcelable("celestial_body");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.cb_planetary_details_layout,
                                    container,
                                    false);

        createViews(mainView);

        if (celestialBody == null) {
            mainView.setVisibility(View.GONE);
        } else {
            setCelestialBody(celestialBody);
        }

        return mainView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial_body", celestialBody);
    }

    public void setCelestialBody(PlanetBody body)
    {
        synchronized (mutex) {
            celestialBody = body;

            massText.setText((body.getMass() == null) ?
                             "" :
                             String.format("%1.4f", body.getMass()));
            radiusText.setText((body.getRadius() == null) ?
                               "" :
                               String.format("%1.2f", body.getRadius()));
            surfaceTempText.setText((body.getSurfaceTemp() == null) ?
                                    "" :
                                    String.format("%1.2f", body.getSurfaceTemp()));
            surfacePressureText.setText((body.getSurfacePressure() == null) ?
                                        "" :
                                        String.format("%1.2f", body.getSurfacePressure()));
            rotationPeriodText.setText((body.getRotationPeriod() == null) ?
                                        "" :
                                        String.format("%1.2f", body.getRotationPeriod()));
            axisTiltText.setText((body.getAxisTilt() == null) ?
                                 "" :
                                 String.format("%1.2f", body.getAxisTilt()));

            terraformCheck.setChecked(body.getTerraformable());
            tidalLockedCheck.setChecked((body.getTidalLocked() != null) && body.getTidalLocked());

            String [] values = getResources().getStringArray(R.array.atmosphere_types);
            String value = "";

            if (body.getAtmosphereType() != Atmosphere.Unknown) {
                value = values[body.getAtmosphereType().value];
            }

            atmosphereTypeText.setText(value);

            values = getResources().getStringArray(R.array.volcano_types);
            value = "";

            if (body.getVolcanism() != VolcanoType.Unknown) {
                value = values[body.getVolcanism().value];
            }

            volcanismText.setText(value);

            mainView.setVisibility(View.VISIBLE);
        }
    }

    public void onDone(Bundle bundle)
    {
        for (String key : bundle.keySet()) {
            double dbl = bundle.getDouble(key, 0.0);

            switch (key) {
                case CelestialPlanetEditDialogFragment.TAG_MASS:
                    celestialBody.setMass(dbl);
                    massText.setText(String.format("%1.4f", dbl));
                    break;
                case CelestialPlanetEditDialogFragment.TAG_RADIUS:
                    celestialBody.setRadius(dbl);
                    radiusText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialPlanetEditDialogFragment.TAG_SURFACETEMP:
                    celestialBody.setSurfaceTemp(dbl);
                    surfaceTempText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialPlanetEditDialogFragment.TAG_SURFACEPRESSURE:
                    celestialBody.setSurfacePressure(dbl);
                    surfacePressureText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialPlanetEditDialogFragment.TAG_ROTATIONPERIOD:
                    celestialBody.setRotationPeriod(dbl);
                    rotationPeriodText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialPlanetEditDialogFragment.TAG_AXISTILT:
                    celestialBody.setAxisTilt(dbl);
                    axisTiltText.setText(String.format("%1.2f", dbl));
                    break;
                case CelestialPlanetEditDialogFragment.TAG_ATMOSPHERETYPE: {
                    celestialBody.setAtmosphereType(Atmosphere.getAtmosphere(bundle.getInt(key)));

                    String[] scans = getResources().getStringArray(R.array.atmosphere_types);
                    atmosphereTypeText.setText(scans[bundle.getInt(key)]);
                    break;
                }
                case CelestialPlanetEditDialogFragment.TAG_TERRAFORM:
                    celestialBody.setTerraformable(bundle.getBoolean(key));
                    terraformCheck.setChecked(bundle.getBoolean(key));
                    break;
                case CelestialPlanetEditDialogFragment.TAG_VOLCANISM: {
                    celestialBody.setVolcanism(VolcanoType.getVolcanoType(bundle.getInt(key)));

                    String[] scans = getResources().getStringArray(R.array.volcano_types);
                    volcanismText.setText(scans[bundle.getInt(key)]);
                    break;
                }
                case CelestialPlanetEditDialogFragment.TAG_TIDALOCKED:
                    celestialBody.setTidalLocked(bundle.getBoolean(key));
                    tidalLockedCheck.setChecked(bundle.getBoolean(key));
                    break;
                default:
                    break;
            }
        }
    }

    private void createViews(View layout)
    {
        mainView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CelestialPlanetEditDialogFragment df = CelestialPlanetEditDialogFragment.newInstance(celestialBody);
                df.show(getChildFragmentManager(), FragmentTag.celestial_info_fragment.celestial_planet_dialog);
            }
        });

        massText = (TextView) layout.findViewById(R.id.cb_planet_mass_value);
        radiusText = (TextView) layout.findViewById(R.id.cb_planet_radius_value);
        surfaceTempText = (TextView) layout.findViewById(R.id.cb_planet_surfacetemp_value);
        surfacePressureText = (TextView) layout.findViewById(R.id.cb_planet_surfacepressure_value);
        atmosphereTypeText = (TextView) layout.findViewById(R.id.cb_planet_atmospheretype_value);
        rotationPeriodText = (TextView) layout.findViewById(R.id.cb_planet_rotationperiod_value);
        axisTiltText = (TextView) layout.findViewById(R.id.cb_planet_axistilt_value);
        volcanismText = (TextView) layout.findViewById(R.id.cb_planet_volcanism_value);

        terraformCheck = (CheckBox) layout.findViewById(R.id.cb_planet_terraformable_value);
        tidalLockedCheck = (CheckBox) layout.findViewById(R.id.cb_planet_tidallocked_value);
    }

    @Override
    public void onDone(String tag, Bundle changedItems)
    {
        onDone(changedItems);
    }
}
