package com.fussyware.AndDiscovered.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 12/11/15.
 */
public class SystemInfoSubMenuFragment extends Fragment
{
    public interface OnSubMenuListener
    {
        void onSubMenuCelestialClick();
        void onSubMenuJournalClick();
        void onSubMenuPhotoGalleryClick();
    }

    private OnSubMenuListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.system_info_submenu_fragment_layout,
                                       container,
                                       false);

        createCelestialButton(layout);
        createNotesButton(layout);
        createPhotoGalleryButton(layout);

        return layout;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof OnSubMenuListener) {
            listener = (OnSubMenuListener) activity;
        } else {
            throw new ClassCastException("Activity must implement OnSubMenuListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    private void createCelestialButton(View layout)
    {
        View view = layout.findViewById(R.id.celestial_button);

        if (view != null) {
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (listener != null) {
                        listener.onSubMenuCelestialClick();
                    }
                }
            });
        }
    }

    private void createNotesButton(View layout)
    {
        View view = layout.findViewById(R.id.notes_button);

        if (view != null) {
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (listener != null) {
                        listener.onSubMenuJournalClick();
                    }
                }
            });
        }
    }

    private void createPhotoGalleryButton(View layout)
    {
        View view = layout.findViewById(R.id.photo_gallery_button);

        if (view != null) {
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (listener != null) {
                        listener.onSubMenuPhotoGalleryClick();
                    }
                }
            });
        }
    }

}
