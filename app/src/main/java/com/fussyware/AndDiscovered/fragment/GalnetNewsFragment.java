package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 12/19/15.
 */
public class GalnetNewsFragment extends PortalBaseFragment
{
    private static final String LOG_NAME = GalnetNewsFragment.class.getSimpleName();

    @Override
    protected String getBaseUrl()
    {
        return "https://community.elitedangerous.com/galnet";
    }

    @Override
    protected String getAllowedHost()
    {
        return "community.elitedangerous.com";
    }

    @Override
    protected String getAllowedPath()
    {
        return "/galnet";
    }
}
