package com.fussyware.AndDiscovered.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.adapter.SystemListCursorAdapter;
import com.fussyware.AndDiscovered.dialog.OkAlertDialogFragment;
import com.fussyware.AndDiscovered.preference.PreferenceTag;

import java.util.Date;

/**
 * Created by wes on 8/29/15.
 */
public class SystemListFragment extends Fragment
{
    public interface SystemSelectedListener
    {
        void onSystemSelected(String system, View view, int position, long id);
    }

    private static final String LOG_NAME = SystemListFragment.class.getSimpleName();

    private SystemListCursorAdapter adapter;
    private SystemSelectedListener listener;

    private boolean reloadOnResume;

    private final SharedPreferences.OnSharedPreferenceChangeListener prefListener = new SharedPreferences.OnSharedPreferenceChangeListener()
    {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
        {
            if (PreferenceTag.DISPLAY_DAYS.equals(key)) {
                adapter.setDateRange(getDateRange(sharedPreferences.getInt(key, 365)));
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        int display_days = PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplicationContext())
                .getInt(PreferenceTag.DISPLAY_DAYS, 365);

        adapter = new SystemListCursorAdapter(getActivity());
        adapter.setDateRange(getDateRange(display_days));

        PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplicationContext())
                .registerOnSharedPreferenceChangeListener(prefListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.system_list_layout, container, false);

        ListView systemListView = (ListView) layout.findViewById(R.id.LargeSystemList);
        systemListView.setAdapter(adapter);
        systemListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                SystemListCursorAdapter.Item item = adapter.getItem(position);

                if (item instanceof SystemListCursorAdapter.SystemEntryView) {
                    SystemListCursorAdapter.SystemEntryView entry = (SystemListCursorAdapter.SystemEntryView) item;

                    ((ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE)).setPrimaryClip(
                            ClipData.newPlainText("SystemName", entry.map.get("system")));

                    Toast.makeText(getActivity(),
                                   "Copied system [" +
                                   entry.map.get("system") +
                                   "] to the clipboard",
                                   Toast.LENGTH_SHORT).show();
                }

                return true;
            }
        });

        systemListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (listener != null) {
                    SystemListCursorAdapter.Item item = adapter.getItem(position);

                    if (item instanceof SystemListCursorAdapter.SystemEntryView) {
                        SystemListCursorAdapter.SystemEntryView entry = (SystemListCursorAdapter.SystemEntryView) item;
                        listener.onSystemSelected(entry.map.get("system"),
                                                  view,
                                                  position,
                                                  id);
                    }
                }
            }
        });

        return layout;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplicationContext())
                .unregisterOnSharedPreferenceChangeListener(prefListener);

        adapter.destroy();
        adapter = null;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        Fragment fragment = getParentFragment();

        if (fragment == null) {
            if (!(context instanceof SystemSelectedListener)) {
                throw new ClassCastException("Listener not implemented.");
            }

            listener = (SystemSelectedListener) context;
        } else {
            if (!(fragment instanceof SystemSelectedListener)) {
                throw new ClassCastException("Listener not implemented.");
            }

            listener = (SystemSelectedListener) fragment;
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        listener = null;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPostExecute(Void aVoid)
            {
                if ((adapter != null) && (adapter.getCount() == 0)) {
                    DialogFragment fragment = OkAlertDialogFragment.newInstance(R.string.system_list_no_items_title,
                                                                                R.string.system_list_no_items_message);
                    fragment.show(getFragmentManager(), FragmentTag.system_list_fragment.no_items_dialog);
                }
            }

            @Override
            protected Void doInBackground(Void... params)
            {
                while ((adapter != null) && !adapter.isReady()) {
                    SystemClock.sleep(120);
                }

                return null;
            }
        }.execute();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(R.string.system_list_activity_title);
        }

        if (reloadOnResume) {
            reloadOnResume = false;
            adapter.notifyDatabaseChanged();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        reloadOnResume = true;
    }

    public void invalidate()
    {
        if (adapter != null) {
            adapter.notifyDatabaseChanged();
        }
    }

    private Date getDateRange(int value)
    {
        return new Date(new Date().getTime() - ((long) value * 24 * 60 * 60 * 1000));
    }
}
