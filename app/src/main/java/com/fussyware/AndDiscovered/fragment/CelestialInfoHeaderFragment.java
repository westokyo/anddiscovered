package com.fussyware.AndDiscovered.fragment;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.AsteroidBody;
import com.fussyware.AndDiscovered.celestial.CelestialBody;
import com.fussyware.AndDiscovered.celestial.PlanetBody;
import com.fussyware.AndDiscovered.celestial.ScanLevel;
import com.fussyware.AndDiscovered.celestial.StarBody;
import com.fussyware.AndDiscovered.dialog.TextDialogFragment;

import java.security.InvalidParameterException;

/**
 * Created by wes on 11/17/15.
 */
public class CelestialInfoHeaderFragment
        extends Fragment
        implements TextDialogFragment.OnDoneListener
{
    private static final String LOG_NAME = CelestialInfoHeaderFragment.class.getSimpleName();

    private final Object mutex = new Object();

    private CelestialBody celestialBody;

    private TextView nameText;
    private TextView descriptionText;

    private ImageView bodyIcon;
    private ImageView levelIcon;
    private ImageView firstDiscoveredIcon;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                celestialBody = bundle.getParcelable("celestial_body");
            }
        } else {
            celestialBody = savedInstanceState.getParcelable("celestial_body");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.cb_info_header_layout, container, false);

        createViews(layout);

        if (celestialBody != null) {
            setCelestialBody(celestialBody);
        }

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable("celestial_body", celestialBody);
    }

    public void setCelestialBody(@NonNull CelestialBody body)
    {
        synchronized (mutex) {
            celestialBody = body;

            populateName(body.getName());
            populateHeader();

            levelIcon.setSelected(body.getScanLevel() == ScanLevel.Level_3);
            firstDiscoveredIcon.setSelected(body.isFirstDiscovered());
        }
    }

    public void onDialogDone(@NonNull String text)
    {
        celestialBody.setName(text);
        populateName(text);
    }

    private void createViews(View layout)
    {
        nameText = (TextView) layout.findViewById(R.id.cb_body_name_title);
        descriptionText = (TextView) layout.findViewById(R.id.cb_description);

        bodyIcon = (ImageView) layout.findViewById(R.id.cb_icon);
        levelIcon = (ImageView) layout.findViewById(R.id.cb_level_selection_icon);
        firstDiscoveredIcon = (ImageView) layout.findViewById(R.id.cb_first_discover_icon);

        levelIcon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (v.isSelected()) {
                    celestialBody.setScanLevel(ScanLevel.Level_2);
                    v.setSelected(false);
                } else {
                    celestialBody.setScanLevel(ScanLevel.Level_3);
                    v.setSelected(true);
                }
            }
        });

        firstDiscoveredIcon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (v.isSelected()) {
                    celestialBody.setFirstDiscovered(false);
                    v.setSelected(false);
                } else {
                    celestialBody.setFirstDiscovered(true);
                    v.setSelected(true);
                }
            }
        });
    }

    private void populateName(String name)
    {
        if (name.isEmpty()) {
            nameText.setText(R.string.cb_name_set_text);
            nameText.setTextColor(getActivity().getResources().getColor(R.color.ed_cyan));
        } else {
            nameText.setText(name);
            nameText.setTextColor(getActivity().getResources().getColor(R.color.ed_primary_orange));
        }

        nameText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String content = celestialBody.getName();

                if (content.isEmpty()) {
                    content = celestialBody.getSystem().getSystem();
                }

                String title = getResources().getString(R.string.cb_name_dialog_title);
                String summary = getResources().getString(R.string.cb_name_dialog_summary);

                TextDialogFragment df = TextDialogFragment.newInstance(title,
                                                                       summary,
                                                                       content,
                                                                       InputType.TYPE_CLASS_TEXT |
                                                                       InputType.TYPE_TEXT_FLAG_CAP_WORDS);

                df.show(getChildFragmentManager(), FragmentTag.celestial_info_fragment.celestial_name_dialog);
            }
        });
    }

    private void populateHeader()
    {
        String description = "";

        TypedArray resourceArray = null;
        int position = 0;

        switch (celestialBody.getSatelliteCategory()) {
            case Star: {
                int value = ((StarBody) celestialBody).getType().value;
                String[] types;

                types = getResources().getStringArray(R.array.star_descriptions);
                description = types[value];

                resourceArray = getResources().obtainTypedArray(R.array.star_icons);
                position = ((StarBody) celestialBody).getType().value;

                break;
            }
            case Planet: {
                int value = ((PlanetBody) celestialBody).getType().value;
                String[] types;

                types = getResources().getStringArray(R.array.planet_descriptions);
                description = types[value];

                resourceArray = getResources().obtainTypedArray(R.array.planet_icons);
                position = ((PlanetBody) celestialBody).getType().value;

                break;
            }
            case Asteroid: {
                int value = ((AsteroidBody) celestialBody).getType().value;
                String[] types;

                types = getResources().getStringArray(R.array.mineral_descriptions);
                description = types[value];

//                resourceArray = getResources().obtainTypedArray(R.array.asteroid_icons);
//                position = ((AsteroidBody) celestialBody).getType().value;

                break;
            }
            case Unknown:
                throw new InvalidParameterException("A valid Satellite category must be provided.");
        }

        descriptionText.setText(description);
        bodyIcon.setImageDrawable(resourceArray.getDrawable(position));

        resourceArray.recycle();
    }

    @Override
    public void onDone(String tag, String text)
    {
        switch (tag) {
            case FragmentTag.celestial_info_fragment.celestial_name_dialog:
                onDialogDone(text);
                break;
            default:
                break;
        }
    }
}
