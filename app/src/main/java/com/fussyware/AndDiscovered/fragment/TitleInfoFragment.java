package com.fussyware.AndDiscovered.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.SystemInfo;
import com.fussyware.AndDiscovered.edutils.Position;

/**
 * Created by wes on 10/4/15.
 */
public class TitleInfoFragment extends Fragment
{
    private TextView titleText;
    private TextView positionText;

    private final Object mutex = new Object();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.system_title_layout, container, false);

        titleText = (TextView) layout.findViewById(R.id.SystemTitleLabel);
        positionText = (TextView) layout.findViewById(R.id.PositionText);

        if (savedInstanceState != null) {
            titleText.setText(savedInstanceState.getString("TitleInfoFragment.system"));
            positionText.setText(savedInstanceState.getString("TitleInfoFragment.position_text"));
        }

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString("TitleInfoFragment.system", titleText.getText().toString());
        outState.putString("TitleInfoFragment.position_text", positionText.getText().toString());
    }

    public void setSystem(@NonNull SystemInfo systemInfo)
    {
        synchronized (mutex) {
            titleText.setText(systemInfo.getSystem());

            if (systemInfo.getPosition() == null) {
                positionText.setText(getResources().getString(R.string.UnknownPosition));
            } else {
                positionText.setText(systemInfo.getPosition().toString());
            }
        }
    }

    public void onPositionCalculation(String text)
    {
        positionText.setText(text);
    }

    public void onPositionFound(Position position)
    {
        if (position == null) {
            positionText.setText(getResources().getString(R.string.UnknownPosition));
        } else {
            positionText.setText(position.toString());
        }
    }
}
