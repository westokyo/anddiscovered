package com.fussyware.AndDiscovered.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fussyware.AndDiscovered.R;

/**
 * Created by wes on 12/19/15.
 */
public abstract class PortalBaseFragment extends Fragment
{
    private static final String LOG_NAME = PortalBaseFragment.class.getSimpleName();

    private WebView webView;
    private String url;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            url = getBaseUrl();
        } else {
            url = savedInstanceState.getString("url");
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.portal_fragment_layout, container, false);

        webView = (WebView) layout.findViewById(R.id.portal_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new InternalWebClient());
        webView.loadUrl(url);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        if (webView != null) {
            url = webView.getUrl();
        }

        outState.putString("url", url);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        url = webView.getUrl();
    }

    protected abstract String getBaseUrl();
    protected abstract String getAllowedHost();
    protected abstract String getAllowedPath();

    private class InternalWebClient extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Uri uri = Uri.parse(url);
            String host = uri.getHost();
            String path = uri.getPath();

            if (getAllowedHost().equals(host)) {
                String allowedPath = getAllowedPath();

                if ((allowedPath != null) && !allowedPath.isEmpty()) {
                    if (allowedPath.startsWith(path)) {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            startActivity(new Intent(Intent.ACTION_VIEW, uri));

            return true;
        }
    }
}
