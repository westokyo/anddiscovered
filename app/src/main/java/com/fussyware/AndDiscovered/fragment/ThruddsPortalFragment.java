package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 12/19/15.
 */
public class ThruddsPortalFragment extends PortalBaseFragment
{
    private static final String LOG_NAME = ThruddsPortalFragment.class.getSimpleName();

    @Override
    protected String getBaseUrl()
    {
        return "http://www.elitetradingtool.co.uk/";
    }

    @Override
    protected String getAllowedHost()
    {
        return "www.elitetradingtool.co.uk";
    }

    @Override
    protected String getAllowedPath()
    {
        return null;
    }
}
