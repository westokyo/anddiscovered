package com.fussyware.AndDiscovered.fragment;

import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.celestial.StarType;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.statistics.StarInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wes on 1/12/16.
 */
public class StatsStarFragment extends BaseStatsFragment
{
    private static final String TAG = StatsStarFragment.class.getSimpleName();

    private final CmdrDbHelper dbHelper = CmdrDbHelper.getInstance();

    private int rowPadding;
    private final ArrayList<TextView> valueList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.stat_star_fragment_layout,
                                       container,
                                       false);

        float density = getResources().getDisplayMetrics().density;
        rowPadding = (int) (4.0f * density + 0.5f);

        TableLayout tableLayout = (TableLayout) layout.findViewById(R.id.stat_table_layout);
        TypedArray starNames = getResources().obtainTypedArray(R.array.star_types);

        for (int i = 0; i < starNames.length(); i++) {
            tableLayout.addView(createRow(starNames.getString(i)));
        }

        starNames.recycle();

        invalidated = true;

        return layout;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        valueList.clear();
    }


    @Override
    public void onResume()
    {
        super.onResume();
        onRefresh();
    }

    private TableRow createRow(String name)
    {
        TextView col1 = new TextView(getContext());
        col1.setText(String.format("%s:", name));
        col1.setGravity(Gravity.END);
        col1.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);

        TextView col2 = new TextView(getContext());
        col2.setGravity(Gravity.END);
        col2.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);

        valueList.add(col2);

        TableRow row = new TableRow(getContext());
        row.setPadding(0, rowPadding, 0, rowPadding);
        row.addView(col1);
        row.addView(col2);

        return row;
    }

    @Override
    public void onRefresh()
    {
        if (invalidated) {
            new RefreshTask().execute();
            invalidated = false;
        }
    }

    private class RefreshTask extends AsyncTask<Void, Void, int[]>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            for (int i = 0; i < valueList.size(); i++) {
                TextView view = valueList.get(i);
                view.setText(R.string.loading);
            }
        }

        @Override
        protected void onPostExecute(int[] ints)
        {
            super.onPostExecute(ints);

            for (int i = 0; i < valueList.size(); i++) {
                TextView view = valueList.get(i);
                view.setText(String.format("%,d", ints[i]));
            }
        }

        @Override
        protected int[] doInBackground(Void... params)
        {
            List<StarInfo> list = dbHelper.getStatStars();
            int[] starCounts = new int[StarType.values().length - 1];

            for (int i = 0; i < starCounts.length; i++) {
                starCounts[i] = 0;
            }

            for (StarInfo info : list) {
                if (info.getType() != StarType.Unknown) {
                    starCounts[info.getType().value]++;
                }
            }

            return starCounts;
        }
    }
}
