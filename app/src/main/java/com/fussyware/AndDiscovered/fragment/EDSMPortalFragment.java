package com.fussyware.AndDiscovered.fragment;

/**
 * Created by wes on 12/19/15.
 */
public class EDSMPortalFragment extends PortalBaseFragment
{
    private static final String LOG_NAME = EDSMPortalFragment.class.getSimpleName();

    @Override
    protected String getBaseUrl()
    {
        return "http://www.edsm.net/";
    }

    @Override
    protected String getAllowedHost()
    {
        return "www.edsm.net";
    }

    @Override
    protected String getAllowedPath()
    {
        return null;
    }
}
