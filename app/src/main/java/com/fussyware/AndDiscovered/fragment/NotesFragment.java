package com.fussyware.AndDiscovered.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fussyware.AndDiscovered.R;
import com.fussyware.AndDiscovered.eddatabase.CmdrDbHelper;
import com.fussyware.AndDiscovered.eddatabase.CmdrSystemInfo;
import com.fussyware.AndDiscovered.edutils.DebouncedTextWatcher;

/**
 * Created by wes on 9/21/15.
 */
public class NotesFragment extends Fragment
{
    private EditText notesText;
    private CmdrSystemInfo selectedSystem;
    private OnDoneListener listener;

    private String notes;

    private final Object mutex = new Object();

    public interface OnDoneListener
    {
        void onDone(String tag, String text);
    }

    public static NotesFragment newInstance(CmdrSystemInfo system)
    {
        Bundle bundle = new Bundle();
        bundle.putParcelable("system", system);

        NotesFragment fragment = new NotesFragment();
        fragment.setArguments(bundle);

        return fragment;

    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        if (activity instanceof OnDoneListener) {
            listener = (OnDoneListener) activity;
        } else {
            throw new ClassCastException("Activity does not implement OnDoneListener.");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            Bundle bundle = getArguments();

            if (bundle != null) {
                selectedSystem = bundle.getParcelable("system");
                notes = selectedSystem.getNote();
            } else {
                notes = "";
            }
        } else {
            selectedSystem = savedInstanceState.getParcelable("NotesFragment.system");
            notes = savedInstanceState.getString("NotesFragment.notes");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View layout = inflater.inflate(R.layout.notes_layout, container, false);

        createNotes(layout, savedInstanceState);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putParcelable("NotesFragment.system", selectedSystem);
        outState.putString("NotesFragment.notes", notes);
    }

    public void setSystem(CmdrSystemInfo system)
    {
        synchronized (mutex) {
            selectedSystem = system;
            notes = system.getNote();
            notesText.setText(notes);
        }
    }

    private void createNotes(View layout, Bundle savedInstanceState)
    {
        notesText = (EditText) layout.findViewById(R.id.NotesEditText);
        notesText.setText(notes);
        notesText.addTextChangedListener(new DebouncedTextWatcher(3000)
        {
            @Override
            public void debouncedTextChanged(Editable s)
            {
                if (selectedSystem != null) {
                    new AsyncTask<String, Void, String>()
                    {
                        @Override
                        protected void onPostExecute(String result)
                        {
                            if ((listener != null) && (!result.isEmpty())) {
                                notes = result;
                                listener.onDone(FragmentTag.notes_fragment.tag, result);
                            }
                        }

                        @Override
                        protected String doInBackground(String... strings)
                        {
                            CmdrSystemInfo info = CmdrDbHelper.getInstance().getSystem(strings[0]);
                            String ret = "";

                            if (info != null) {
                                info.setNote(strings[1]);
                                ret = strings[1];
                            }

                            return ret;
                        }
                    }.execute(selectedSystem.getSystem(), s.toString());
                }
            }
        });
    }
}
