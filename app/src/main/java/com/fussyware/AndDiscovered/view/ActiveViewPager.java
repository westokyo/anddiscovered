package com.fussyware.AndDiscovered.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by wes on 12/3/15.
 */
public class ActiveViewPager extends ViewPager
{
    private boolean pagingEnabled = true;

    public ActiveViewPager(Context context)
    {
        super(context);
    }

    public ActiveViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
        return pagingEnabled && super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        return pagingEnabled && super.onInterceptTouchEvent(ev);
    }

    public void setPagingEnabled(boolean enabled)
    {
        pagingEnabled = enabled;
    }

    public boolean isPagingEnabled()
    {
        return pagingEnabled;
    }
}
