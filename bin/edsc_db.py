#!/usr/bin/env python

import sqlite3
import sys
import urllib2
import json
import datetime

class DbInfo(object):
    TABLE_NAME = 'DbInfo'
    ID = "_id"
    COLUMN_NAME_CONFIG = "ConfigName";
    COLUMN_NAME_VALUE = "Value";

    CONFIG_NAME_DBVERSION = "DbVersion";
    CONFIG_NAME_LAST_EDSC_UPLOAD = "LastEDSCUpload";
    CONFIG_NAME_LAST_EDSC_DISTANCE_CHECK = "LastEDSCDistanceCheck";
    CONFIG_NAME_LAST_EDSC_SYSTEM_CHECK = "LastEDSCSystemCheck";

class EDSCSystems(object):
    TABLE_NAME = "EDSCSystems";
    ID = "_id"
    COLUMN_NAME_SYSTEM = "System";
    COLUMN_NAME_XCOORD = "xCoord";
    COLUMN_NAME_YCOORD = "yCoord";
    COLUMN_NAME_ZCOORD = "zCoord";

class EDSCDistances(object):
    TABLE_NAME = "EDSCDistances";
    ID = "_id"
    COLUMN_NAME_FROM = "FromSystem";
    COLUMN_NAME_TO = "ToSystem";
    COLUMN_NAME_DISTANCE = "Distance";
    COLUMN_NAME_EDSC_UPDATED = "EDSCUpdated";

def post(url, data):
    headers = { 'X-Requested-With' : 'urllib2','Content-Type': 'application/json', 'charset': 'utf-8' }

    print "POST: url [%s], data [%s]" % (url, data)
    req = urllib2.Request(url = url, data = data, headers = headers)
    response = urllib2.urlopen(req)

    if response.code == 200:
        return 200, json.load(response)
    else:
        return response.code, None

def pull_systems(conn, cursor):
    try:
        json_post = '{"data":{"ver":2,"test":false,"outputmode":2,"filter":{"cr":0,"date":"1969-12-31 18:00:00"}}}'
        code, json_response = post("http://edstarcoordinator.com/api.asmx/GetSystems", json_post)

        if code == 200:
            d = json_response['d']
            systems = d["systems"]

            sql = "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?);" % (EDSCSystems.TABLE_NAME, EDSCSystems.COLUMN_NAME_SYSTEM, EDSCSystems.COLUMN_NAME_XCOORD, EDSCSystems.COLUMN_NAME_YCOORD, EDSCSystems.COLUMN_NAME_ZCOORD)

            print "Systems: ", str(len(systems))
            for system in systems:
                position = system['coord']

                x = position[0]
                y = position[1]
                z = position[2]

                cursor.execute(sql, (system['name'], x, y, z))

            if (systems):
                conn.commit()
    except sqlite3.Error, e:
        print "SQLite error %s:" % e.args[0]

def pull_distances(conn, cursor):
    try:
        json_post = '{"data":{"ver":2,"test":false,"outputmode":1,"filter":{"knownstatus":0,"cr":1,"date":"1969-12-31 18:00:00"}}}'
        code, json_response = post("http://edstarcoordinator.com/api.asmx/GetDistances", json_post)

        if code == 200:
            d = json_response['d']
            distances = d['distances']

            sql = "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?);" % (EDSCDistances.TABLE_NAME, EDSCDistances.COLUMN_NAME_FROM, EDSCDistances.COLUMN_NAME_TO, EDSCDistances.COLUMN_NAME_DISTANCE, EDSCDistances.COLUMN_NAME_EDSC_UPDATED)

            print "Distances: ", str(len(distances))
            for dist in distances:
                _from = dist['name']
                _to_list = dist['refs']

                for _to in _to_list:
                    cursor.execute(sql, (_from, _to['name'], _to['dist'], 1))

            if (distances):
                conn.commit()
    except sqlite3.Error, e:
        print "SQLite error %s:" % e.args[0]

def setup_dbinfo(conn, cursor):
    try:
        sql = "INSERT INTO %s (%s, %s) VALUES (?, ?);" % (DbInfo.TABLE_NAME, DbInfo.COLUMN_NAME_CONFIG, DbInfo.COLUMN_NAME_VALUE)
        now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

        cursor.execute(sql, (DbInfo.CONFIG_NAME_DBVERSION, "1"))
        cursor.execute(sql, (DbInfo.CONFIG_NAME_LAST_EDSC_UPLOAD, "1969-12-31 18:00:00"))
        cursor.execute(sql, (DbInfo.CONFIG_NAME_LAST_EDSC_DISTANCE_CHECK, now))
        cursor.execute(sql, (DbInfo.CONFIG_NAME_LAST_EDSC_SYSTEM_CHECK, now))

        conn.commit()
    except sqlite3.Error, e:
        print "SQLite error %s:" % e.args[0]

conn = None

try:
    conn = sqlite3.connect('edsc.db')
    cursor = conn.cursor()

    cursor.execute("DROP INDEX IF EXISTS SystemIndex");
    cursor.execute("DROP INDEX IF EXISTS EDSCDistIndex");
    cursor.execute("DROP TABLE IF EXISTS %s" % EDSCSystems.TABLE_NAME);
    cursor.execute("DROP TABLE IF EXISTS %s" % EDSCDistances.TABLE_NAME);
    cursor.execute("DROP TABLE IF EXISTS %s" % DbInfo.TABLE_NAME);

    sql = "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT COLLATE NOCASE, %s DOUBLE, %s DOUBLE, %s DOUBLE)" % (EDSCSystems.TABLE_NAME, EDSCSystems.ID, EDSCSystems.COLUMN_NAME_SYSTEM, EDSCSystems.COLUMN_NAME_XCOORD, EDSCSystems.COLUMN_NAME_YCOORD, EDSCSystems.COLUMN_NAME_ZCOORD)
    cursor.execute(sql)

    sql = "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT COLLATE NOCASE, %s TEXT COLLATE NOCASE, %s DOUBLE, %s BOOLEAN)" % (EDSCDistances.TABLE_NAME, EDSCDistances.ID, EDSCDistances.COLUMN_NAME_FROM, EDSCDistances.COLUMN_NAME_TO, EDSCDistances.COLUMN_NAME_DISTANCE, EDSCDistances.COLUMN_NAME_EDSC_UPDATED)
    cursor.execute(sql)

    sql = "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT)" % (DbInfo.TABLE_NAME, DbInfo.ID, DbInfo.COLUMN_NAME_CONFIG, DbInfo.COLUMN_NAME_VALUE)
    cursor.execute(sql)

    sql = "CREATE INDEX SystemIndex ON %s(%s);" % (EDSCSystems.TABLE_NAME, EDSCSystems.COLUMN_NAME_SYSTEM)
    cursor.execute(sql)

    sql = "CREATE INDEX EDSCDistIndex ON %s(%s, %s);" % (EDSCDistances.TABLE_NAME, EDSCDistances.COLUMN_NAME_FROM, EDSCDistances.COLUMN_NAME_TO)
    cursor.execute(sql)

    pull_distances(conn, cursor)
    pull_systems(conn, cursor)

    setup_dbinfo(conn, cursor)
except sqlite3.Error, e:
    print "SQLite error %s:" % e.args[0]
    sys.exit(1)
finally:
    if conn:
        conn.close
