#!/usr/bin/env python

import sqlite3
import sys
import urllib2
import json
import datetime
import urllib
import gzip
import shutil
import time
import os

class DbInfo(object):
    TABLE_NAME = 'DbInfo'
    ID = "_id"
    COLUMN_NAME_CONFIG = "ConfigName";
    COLUMN_NAME_VALUE = "Value";

    CONFIG_NAME_LAST_EDSM_DISTANCE_CHECK = "LastEDSMDistanceCheck";
    CONFIG_NAME_LAST_EDSM_SYSTEM_CHECK = "LastEDSMSystemCheck";

class EDSMSystems(object):
    TABLE_NAME = "EDSMSystems";
    ID = "_id"
    COLUMN_NAME_SYSTEM = "System";
    COLUMN_NAME_XCOORD = "xCoord";
    COLUMN_NAME_YCOORD = "yCoord";
    COLUMN_NAME_ZCOORD = "zCoord";

class EDSMDistances(object):
    TABLE_NAME = "EDSMDistances";
    ID = "_id"
    COLUMN_NAME_FROM = "FromSystem";
    COLUMN_NAME_TO = "ToSystem";
    COLUMN_NAME_DISTANCE = "Distance";

def post(url, data):
    headers = { 'X-Requested-With' : 'urllib2','Content-Type': 'application/json', 'charset': 'utf-8' }

    print "POST: url [%s], data [%s]" % (url, data)
    req = urllib2.Request(url = url, data = data, headers = headers)
    response = urllib2.urlopen(req)

    if response.code == 200:
        return 200, json.load(response)
    else:
        return response.code, None

def get_utc():
    timestamp = datetime.datetime.utcnow() - datetime.timedelta(days = 1)                
    return int(time.mktime(timestamp.timetuple()) * 1000 + timestamp.microsecond / 1000)

def pull_systems(conn, cursor):
    try:
        filename, _ = urllib.urlretrieve("http://www.edsm.net/dump/systemsWithCoordinates.json")

        if filename:
            with open(filename, 'r') as json_file:
                json_map = json.load(json_file)
                
                system_list = json_map #["systems"]                
                tuple_list = list()
                
                for system in system_list:
                    name = system['name']
                    
                    if 'coords' in system:
                        coords = system['coords']
                        
                        if 'x' in coords:
                            x = coords['x']
                            y = coords['y']
                            z = coords['z']
                        else:
                            x = y = z = None
                    else:
                        x = y = z = None
                        
                    tuple_list.append((name, x, y, z))
                    
                sql = "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)" % (EDSMSystems.TABLE_NAME, EDSMSystems.COLUMN_NAME_SYSTEM, EDSMSystems.COLUMN_NAME_XCOORD, EDSMSystems.COLUMN_NAME_YCOORD, EDSMSystems.COLUMN_NAME_ZCOORD)
                cursor.executemany(sql, tuple_list)
                conn.commit()

                return get_utc()
#                 return datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    except sqlite3.Error, e:
        print "Systems: SQLite error %s:" % e.args[0]
        
    return '0'

def pull_distances(conn, cursor):
    try:
        filename, _ = urllib.urlretrieve("http://www.edsm.net/dump/distances.json")

        if filename:
            with open(filename, 'r') as json_file:
                json_map = json.load(json_file)

                distance_list = json_map #["distances"]                
                tuple_list = list()
                
                for distance in distance_list:
                    _from = distance['sys1']
                    _to = distance['sys2']
                    
                    if 'coords' in _to and 'coords' in _from:
                        _dist = distance['distance']
                        
                        tuple_list.append((_from['name'], _to['name'], _dist))
                
                sql = "INSERT INTO %s (%s, %s, %s) VALUES (?, ?, ?)" % (EDSMDistances.TABLE_NAME, EDSMDistances.COLUMN_NAME_FROM, EDSMDistances.COLUMN_NAME_TO, EDSMDistances.COLUMN_NAME_DISTANCE)
                cursor.executemany(sql, tuple_list)
                conn.commit()
                
                return get_utc()
#                 return datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    except sqlite3.Error, e:
        print "Distances: SQLite error %s:" % e.args[0]
        
    return '0'

def setup_dbinfo(conn, cursor, system_date, distance_date):
    print system_date, distance_date
    try:
        sql = "INSERT INTO %s (%s, %s) VALUES (?, ?);" % (DbInfo.TABLE_NAME, DbInfo.COLUMN_NAME_CONFIG, DbInfo.COLUMN_NAME_VALUE)

        cursor.execute(sql, (DbInfo.CONFIG_NAME_LAST_EDSM_SYSTEM_CHECK, str(distance_date)))
        cursor.execute(sql, (DbInfo.CONFIG_NAME_LAST_EDSM_DISTANCE_CHECK, str(system_date)))

        conn.commit()
    except sqlite3.Error, e:
        print "Setup: SQLite error %s:" % e.args[0]

conn = None

try:
    if (os.path.exists("edsm.db")):
        os.remove('edsm.db')
        print 'edsm db removed'

    if (os.path.exists("edsm.db.gz")):
        os.remove('edsm.db.gz')
        print 'edsm db gz removed'
        
    conn = sqlite3.connect('edsm.db')
    cursor = conn.cursor()

    cursor.execute("DROP INDEX IF EXISTS EDSMSystemIndex");
    cursor.execute("DROP INDEX IF EXISTS EDSMDistIndex");
    cursor.execute("DROP TABLE IF EXISTS %s" % EDSMSystems.TABLE_NAME);
    cursor.execute("DROP TABLE IF EXISTS %s" % EDSMDistances.TABLE_NAME);
    cursor.execute("DROP TABLE IF EXISTS %s" % DbInfo.TABLE_NAME);

    sql = "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT COLLATE NOCASE, %s DOUBLE, %s DOUBLE, %s DOUBLE)" % (EDSMSystems.TABLE_NAME, EDSMSystems.ID, EDSMSystems.COLUMN_NAME_SYSTEM, EDSMSystems.COLUMN_NAME_XCOORD, EDSMSystems.COLUMN_NAME_YCOORD, EDSMSystems.COLUMN_NAME_ZCOORD)
    cursor.execute(sql)

    sql = "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT COLLATE NOCASE, %s TEXT COLLATE NOCASE, %s DOUBLE)" % (EDSMDistances.TABLE_NAME, EDSMDistances.ID, EDSMDistances.COLUMN_NAME_FROM, EDSMDistances.COLUMN_NAME_TO, EDSMDistances.COLUMN_NAME_DISTANCE)
    cursor.execute(sql)

    sql = "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT)" % (DbInfo.TABLE_NAME, DbInfo.ID, DbInfo.COLUMN_NAME_CONFIG, DbInfo.COLUMN_NAME_VALUE)
    cursor.execute(sql)

    print "Pull systems from EDSM"
    system_date = pull_systems(conn, cursor)
#     print "Pull distances from EDSM"
#     distance_date = pull_distances(conn, cursor)
    print "Setup db info table"
    setup_dbinfo(conn, cursor, system_date, get_utc())    
#     setup_dbinfo(conn, cursor, system_date, distance_date)    

    sql = "CREATE INDEX EDSMSystemIndex ON %s(%s);" % (EDSMSystems.TABLE_NAME, EDSMSystems.COLUMN_NAME_SYSTEM)
    cursor.execute(sql)
    sql = "CREATE INDEX EDSMSystemCoordIndex ON %s(%s,%s,%s);" % (EDSMSystems.TABLE_NAME, EDSMSystems.COLUMN_NAME_XCOORD, EDSMSystems.COLUMN_NAME_YCOORD, EDSMSystems.COLUMN_NAME_ZCOORD)
    cursor.execute(sql)

    sql = "CREATE INDEX EDSMDistIndex ON %s(%s, %s);" % (EDSMDistances.TABLE_NAME, EDSMDistances.COLUMN_NAME_FROM, EDSMDistances.COLUMN_NAME_TO)
    cursor.execute(sql)
    sql = "CREATE INDEX EDSMFromDistIndex ON %s(%s);" % (EDSMDistances.TABLE_NAME, EDSMDistances.COLUMN_NAME_FROM)
    cursor.execute(sql)
    sql = "CREATE INDEX EDSMToDistIndex ON %s(%s);" % (EDSMDistances.TABLE_NAME, EDSMDistances.COLUMN_NAME_TO)
    cursor.execute(sql)
    sql = "CREATE INDEX EDSMDistanceIndex ON %s(%s);" % (EDSMDistances.TABLE_NAME, EDSMDistances.COLUMN_NAME_DISTANCE)
    cursor.execute(sql)
except sqlite3.Error, e:
    print "Main: SQLite error %s:" % e.args[0]
    sys.exit(1)
finally:
    if conn:
        cursor.close()
        conn.close()

with open('edsm.db', 'rb') as db, gzip.open('edsm.db.gz', 'wb') as gzdb:
     shutil.copyfileobj(db, gzdb)
     
print "All finished!"
